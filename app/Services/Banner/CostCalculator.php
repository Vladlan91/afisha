<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 27/11/2018
 * Time: 17:57
 */

namespace App\Services\Banner;


class CostCalculator
{
    private $price;

    public function __construct(int $price)
    {
        $this->price = $price;
    }

    public function calc(int $view)
    {
        return floor($this->price * ($view / 10));
    }
}