<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 03/11/2018
 * Time: 20:21
 */

namespace App\Services\Sms;


interface SmsSender
{
    public function send($namber, $text): void;
}