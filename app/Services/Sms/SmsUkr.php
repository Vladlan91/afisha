<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 03/11/2018
 * Time: 20:25
 */

namespace App\Services\Sms;


use GuzzleHttp\Client;

class SmsUkr implements SmsSender
{

    private $appId;
    private $url;
    private $client;

    public function __construct($appId, $url = 'https://smsc.ua/sys/send.php')
    {
        if (empty($appId)){
            throw new \InvalidArgumentException('Sms appId mast be send.');
        }
        $this->appId = $appId;
        $this->url = $url;
        $this->client = new Client();
    }

    public function send($number, $text): void
    {
        $this->client->post($this->url,[
            'form_params' => [
                'api_id' => $this->appId,
                'to' => '+' . trim($number, '+'),
                'text' => $text
            ],
        ]);
    }
}
