<?php

use \App\Entity\Region;
use \App\Entity\Page;
use \App\Entity\News;
use \App\Entity\Business\Company;
use \App\Entity\Adverts\Category;
use \App\Router\AdvertsPath;
use \App\Router\PagePath;
use \App\Router\NewPath;
use \App\Router\CompanyPath;

if(! function_exists('adverts_path')){
    function adverts_path(Region $region = null, Category $category = null)
    {
        return app()->make(AdvertsPath::class)->withRegion($region)->withCategory($category);
    }
}

if(! function_exists('page_path')){
    function page_path(Page $page)
    {
        return app()->make(PagePath::class)->withPage($page);
    }
}

if(! function_exists('news_path')){
    function news_path(News $news)
    {
        return app()->make(NewPath::class)->withPage($news);
    }
}

if(! function_exists('company_path')){
    function company_path(Company $company)
    {
        return app()->make(CompanyPath::class)->withPage($company);
    }
}