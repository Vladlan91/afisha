<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/11/2018
 * Time: 13:12
 */

namespace App\UseCase\Adverts;

use App\Entity\Adverts\Advert\Advert;
use App\Entity\Adverts\Category;
use App\Entity\Region;
use App\Http\Requests\Adverts\AttributesRequest;
use App\Http\Requests\Adverts\CreateRequest;
use App\Http\Requests\Adverts\EditRequest;
use App\Http\Requests\Adverts\PhotoRequest;
use App\Http\Requests\Adverts\RejectRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AdvertService
{
    public $phone = null;

    public function create($userId, $categoryId, $regionId, CreateRequest $request) : Advert
    {
        /** @var User $user */
        $user = User::findOrFail($userId);
//        dd($user);

        /** @var Category $category */
        $category = Category::findOrFail($categoryId);
//        dd($category);

        /** @var Region $region */
        $region = $regionId ? Region::findOrFail($regionId) : null;


        if(!empty($request['phone'])){
              $this->phone = $this->format_phone($request['phone']);
        }

        return DB::transaction(function () use ($request, $user, $category, $region) {
            if (!empty($request['avatar'])){
                /** @var Advert $advert */
                $advert = Advert::make([
                    'title' => $request['title'],
                    'slug' => str_slug($request['title']),
                    'content' => $request['content'],
                    'price' => $request['price'],
                    'phone' => $this->phone ?:null,
                    'type' => $request['type'],
                    'count_print' => $request['count_print'],
                    'address' => $request['address'],
                    'avatar' => $request['avatar']->store('adverts', 'local'),
                    'status' => Advert::STATUS_DRAFT,
                ]);
            }else{
                $advert = Advert::make([
                    'title' => $request['title'],
                    'slug' => str_slug($request['title']),
                    'content' => $request['content'],
                    'price' => $request['price'],
                    'phone' => $this->phone ?:null,
                    'type' => $request['type'],
                    'count_print' => $request['count_print'],
                    'address' => $request['address'],
                    'status' => Advert::STATUS_DRAFT,
                ]);
            }

            $advert->user()->associate($user);
            $advert->category()->associate($category);
            $advert->region()->associate($region);

            $advert->saveOrFail();

            $advert->calcPrice($category);

            foreach ($category->allAttributes() as $attribute){
                $value = $request['attributes'][$attribute->id] ?? null;
                if (!empty($value)){
                    $advert->values()->create([
                        'attribute_id' => $attribute->id,
                        'value' => $value,
                    ]);
                }
            }
            return $advert;

        });
    }

    public function getAdvert($id): Advert
    {
        return Advert::findOrFail($id);
    }

    public function addPhotos($id, PhotoRequest $request) : void
    {
        $advert = $this->getAdvert($id);
        dd($request);

        DB::transaction( function () use ($request, $advert){
                $advert->photos()->create([
                    'file' => $request['file']->store('adverts', 'local')
                ]);
            $advert->update();
        });
//
//        DB::transaction( function () use ($request, $advert){
//            foreach ($request['files'] as $file){
//                $advert->photos()->create([
//                    'file' => $file->store('app','local')
//                ]);
//            }
//            $advert->update();
//        });
    }

    public function edit($id, EditRequest $request): void
    {
        $advert = $this->getAdvert($id);
        $phone = null;
        if (!empty($request['phone'])){
            $phone = $this->format_phone($request['phone']);
        }
            $advert->update([
                'title' => $request['title'],
                'content' => $request['content'],
                'price' => $request['price'],
                'phone' => $phone ?:null,
                'address' => $request['address'],
                'errors' => $request['error']?:null,
            ]);
    }

    public function sendToModeration($id): void
    {
        $advert = $this->getAdvert($id);
        $advert->sendToModeration();
        flash('Оголошення подано на модерацію! ')->success();
    }

    public function sendToDraft($id): void
    {
        $advert = $this->getAdvert($id);
        $advert->sendToDraft();
    }

    public function moderate($id): void
    {
        $advert = $this->getAdvert($id);
        $advert->moderate(Carbon::now());
    }


    public function reject($id, RejectRequest $request) : void
    {
        $advert = $this->getAdvert($id);
        $advert->reject($request['reason']);

    }

    public function editAttributes($id, AttributesRequest $request): void
    {
        $advert = $this->getAdvert($id);

        DB::transaction(function () use ($request, $advert){
          $advert->values()->delete();
            foreach ($advert->category->allAttributes() as $attribute){
                $value = $request['attributes'][$attribute->id] ?? null;
                if (!empty($value)){
                    $advert->values()->create([
                        'attribute_id' => $attribute->id,
                        'value' => $value,
                    ]);
                }
            }
            $advert->update();
        });
    }

    public function remove($advert) : void
    {
        $advert = $this->getAdvert($advert);
        $advert->delete();
    }

    public function expire(Advert $advert): void
    {
        $advert->expire();
    }

    public function close($id): void
    {
        $advert = $this->getAdvert($id);
        $advert->close();
    }

    function format_phone($phone = '')
    {
        $rest = substr("$phone", 1);
        if($rest == 0) {
            $phone = preg_replace('/[^0-9]/', '', $phone); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 0);
            $phone_number['code'] = substr($phone, 0, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+38' . $phone_number['dialcode'] . ' (' . $phone_number['code'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone;
        }
        if($rest == '+') {
            $phone = preg_replace('/[^0-9]/', '', $phone); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 0);
            $phone_number['code'] = substr($phone, 0, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+38' . $phone_number['dialcode'] . ' (' . $phone_number['code'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone;
        }
    }





}