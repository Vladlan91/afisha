<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 19/11/2018
 * Time: 20:42
 */

namespace App\UseCase\Adverts;


use App\Entity\Adverts\Advert\Advert;
use App\User;

class FavoriteService
{

    public function add($userId, $advertId):void
    {
        $user = $this->getUser($userId);

        $advert = $this->getAdvert($advertId);

        $user->addToFavorites($advert->id);
    }

    public function remove($userId, $advertId):void
    {
        $user = $this->getUser($userId);

        $advert = $this->getUser($advertId);

        $user->removeFromFavorites($advert->id);

    }

    public function getUser($userId): User
    {
        return User::findOrFail($userId);
    }

    public function getAdvert($advertId): Advert
    {
        return Advert::findOrFail($advertId);
    }

}