<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 23/10/2018
 * Time: 15:40
 */

namespace App\UseCase\Banners;


use App\Entity\Adverts\Category;
use App\Entity\Banner\Banner;
use App\Entity\Region;
use App\Http\Requests\Banner\CreateRequest;
use App\Http\Requests\Banner\EditRequest;
use App\Http\Requests\Banner\FileRequest;
use App\Http\Requests\Banner\RejectReqest;
use App\Services\Banner\CostCalculator;
use App\User;
use Carbon\Carbon;


class BannerService
{

    /**
     * @var CostCalculator
     */
    private $calculator;

    public function __construct(CostCalculator $calculator)
    {

        $this->calculator = $calculator;
    }

    public function create(User $user, Category $category = null, Region $region = null, CreateRequest $request) : Banner
     {

        /** @var Banner $banner */
         $banner = Banner::make([
             'user_id'=> $user->id,
//             'category_id'=> $category->id ? $category->id : null,
//             'region_id'=> null,
             'name' => $request['name'],
             'limit' => $request['limit'],
             'url' => $request['url'],
             'format' => $request['format'],
             'file' => $request['file']->store('banners', 'local'),
             'status' => Banner::STATUS_DRAFT,

         ]);
         $banner->region()->associate($region);
         $banner->category()->associate($category);
         $banner->saveOrFail();
            $cost = $this->calculator->calc($banner->limit);
            $banner->update([
                'cost'=> $cost,
            ]);
         return $banner;

     }

    public function editByOwner($id, EditRequest $request) : void
    {
        $banner = $this->getBanner($id);
        if (!$banner->canBeChanged()){
            throw new \DomainException('Unable to edit the banner.');
        }
        $banner->update([
            'name'=> $request['name'],
            'limit'=> $request['limit'],
            'url'=> $request['url'],
        ]);
    }

    public function changeFileOwner($id, FileRequest $request) : void
    {
        $banner = $this->getBanner($id);
        if (!$banner->canBeChanged()){
            throw new \DomainException('Unable to edit the banner.');
        }
        $banner->update([
            'format' => $request['format'],
            'file'=> $request->file('file')->store('banners', 'local'),
        ]);
    }

    public function changeFileAdmin($id, FileRequest $request) : void
    {
        $banner = $this->getBanner($id);
        if (!$banner->canBeChanged()){
            throw new \DomainException('Unable to edit the banner.');
        }
        $banner->update([
            'format' => $request['format'],
            'file'=> $request->file('file')->store('banners', 'local'),
        ]);
    }


    public function editByAdmin($id, EditRequest $request) : void
    {
        $banner = $this->getBanner($id);
        $banner->update([
            'name'=> $request['name'],
            'limit'=> $request['limit'],
            'url'=> $request['url'],
        ]);
    }

    public function sendToModeration($id): void
    {
        $banner = $this->getBanner($id);
        $banner->sendToModeration();
    }

    public function cancelModeration($id): void
    {
        $banner = $this->getBanner($id);
        $banner->cancelModeration();
    }

    public function moderate($id): void
    {
        $banner = $this->getBanner($id);
        $banner->moderate();
    }

    public function reject($id, RejectReqest $request): void
    {
        $banner = $this->getBanner($id);
        $banner->reject($request['reason']);
    }

    public function order($id) : Banner
    {
        $banner = $this->getBanner($id);
        $banner->order();
        return $banner;
    }

    public function orderAdmin($id) : Banner
    {
        $banner = $this->getBanner($id);
        $banner->orderAdmin();
        return $banner;
    }

    public function pay($id): void
    {
        $banner = $this->getBanner($id);
        $banner->pay(Carbon::now());
    }

    public function removeByOwner($id): void
    {
        $banner = $this->getBanner($id);
       if (!$banner->canBeRemove()){
           throw new \DomainException('Unable to remove the banner.');
       }
       $banner->delete();
//       File::delete($banner->file);

    }

    public function removeByAdmin($id): void
    {
        $banner = $this->getBanner($id);
        $banner->delete();
        //File::delete($banner->file);

    }

    public function getBanner($id): Banner
    {
        return Banner::findOrFail($id);
    }


}