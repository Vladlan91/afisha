<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 23/10/2018
 * Time: 15:40
 */

namespace App\UseCase\Auth;


use App\Http\Requests\Auth\RegisterRequest;
use App\Mail\VerifyMail;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Facades\Mail;

class RegisterService
{
    private $mailer;
    private $dispatcher;

    public function __construct(Mailer $mailer, Dispatcher $dispatcher)
    {
        $this->mailer = $mailer;
        $this->dispatcher = $dispatcher;
    }

    public function register(RegisterRequest $request){

        $user = User::register(
            $request['name'],
            $request['email'],
            $request['password']
        );

        $this->dispatcher->dispatch(new Registered($user));
//        flash('Перейдіть у пошту, пройдіть по силці для верефікації ')->important()->error();
    }
        /** @var User $user */
    public function verify($id): void {
        $user = User::findOrFail($id);
        $user->verify();
    }

}