<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 28/12/2018
 * Time: 00:04
 */

namespace App\Http\Controllers;

use App\Entity\Adverts\Advert\Advert;
use App\Entity\Business\Business;
use App\Entity\Business\Company;
use App\Entity\Region;
use App\Entity\Suggestions\Suggestions;
use App\Router\CompanyPath;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index(Business $business = null, Region $region = null)
    {
        $query = Company::active()->with(['business', 'region'])->orderBy('type');
        if ($business){
            $query->forCategory($business);
            $category = Business::findOrFail($business->id);
        }

        if ($region){
            $query->forRegion($region);
            $region = Region::findOrFail($region->id);
        }
        if (!$region){
            $region = null;
        }
        $regions = $region ? $region->children()->orderBy('name')->getModels()
            : Region::roots()->orderBy('name')->getModels();

        $categories = $business
            ? $business->children()->defaultOrder()->getModels()
            : Business::whereIsRoot()->defaultOrder()->getModels();

        $companies = $query->paginate(40);

        $suggestions = Suggestions::inRandomOrder()->whereHas('company', function($q){
            $q->where('package', '!=', null);
        })->take(3)->getModels();

        $listCategories = Business::whereIsRoot()->defaultOrder()->getModels();
        $listRegion = Region::roots()->orderBy('name')->getModels();
        return view('company', compact('listCategories','listRegion','category', 'region', 'companies', 'regions', 'categories','suggestions'));

    }

    public function search(Request $request)
    {
        $region = null;
        $business = null;

        $query = Company::active()->with(['business', 'region'])->orderBy('type');
        if ($request['category'] != null){
            $category = Business::findOrFail($request['category']);
            if ($business){
                $query->forCategory($business);
            }
        }
        if ($request['region'] != null){
            $region = Region::findOrFail($request['region']);
            if ($region){
                $query->forRegion($region);
            }
        }
        if ($request['title'] != null){
            $query->forTitle($request['title']);
        }
        $regions = $region ? $region->children()->orderBy('name')->getModels()
            : Region::roots()->orderBy('name')->getModels();

        $categories = $business
            ? $business->children()->defaultOrder()->getModels()
            : Business::whereIsRoot()->defaultOrder()->getModels();

        $companies = $query->paginate(40);
        $suggestions = Suggestions::inRandomOrder()->whereHas('company', function($q){
            $q->where('package', '!=', null);
        })->take(3)->getModels();

        $listCategories = Business::whereIsRoot()->defaultOrder()->getModels();
        $listRegion = Region::roots()->orderBy('name')->getModels();
        return view('search', compact('listCategories','listRegion','category', 'region', 'companies', 'categories','suggestions'));

    }

    public function show(CompanyPath $path)
    {
        $company = $path->company;
        $adverts = Advert::active()->forCompany($company)->with(['category', 'region'])->orderByDesc('published_at')->get();
        $suggestions = Suggestions::where('company_id', $company->id )->getModels();
        return view('company.show', compact('company', 'suggestions','adverts'));
    }


}