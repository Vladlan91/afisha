<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 07/02/2019
 * Time: 17:13
 */

namespace App\Http\Controllers;
use App\Entity\Adverts\Advert\Advert;
use App\Entity\Banner\Banner;
use App\Entity\Ticket\Status;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;

class CronController extends Controller
{

    public function pages()
    {
        Schema::drop('pages');
    }

    public function banner()
    {
        $banners = Banner::orderByDesc('id')->where('status','=','active')->whereColumn('views','>=','limit')->getModels();
        foreach ($banners as $banner){
            $banner->status = Banner::STATUS_CLOSE;
            $banner->limit = 0;
            $banner->views = 0;
            $banner->published_at = null;
            $banner->save();
        }
    }

    public function advert()
    {
        $data = Carbon::today();
        $adverts = Advert::orderByDesc('id')->where('expires_at','<=', $data)->getModels();
        foreach ($adverts as $advert){
            $advert->status = Advert::STATUS_CLOSED;
            $advert->expires_at = null;
            $advert->published_at = null;
            $advert->save();
        }
    }
}