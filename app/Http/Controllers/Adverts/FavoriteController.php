<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 19/11/2018
 * Time: 21:23
 */

namespace App\Http\Controllers\Adverts;


use App\Entity\Adverts\Advert\Advert;
use App\UseCase\Adverts\FavoriteService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    private $services;

    public function __construct(FavoriteService $service)
    {
        $this->services = $service;
        $this->middleware('auth');
    }

    public function add(Advert $advert)
    {

        try {
            $this->services->add(Auth::id(), $advert->id);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert)->with('success', 'Оголошення додано до вподобаних.');

    }

    public function remove(Advert $advert)
    {
        try {
            $this->services->remove(Auth::id(), $advert->id);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);


    }

}