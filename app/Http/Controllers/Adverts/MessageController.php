<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 07/12/2018
 * Time: 17:33
 */

namespace App\Http\Controllers\Adverts;


use App\Http\Controllers\Controller;
use App\Entity\Adverts\Advert\Advert;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public function sendmessage( Advert $advert)
    {
        if (empty(Auth::user())){
            flash('Авторизуйтесь для того щоб написати атору!')->error();
            return redirect()->route('adverts.show', compact('advert'));
        }else{
            return view('adverts.message', compact('advert'));
        }

    }

    public function send( Advert $advert, Request $request)
    {
        $advert = $this->getAdvert($advert->id);
        $advert->writeClientMessage(Auth::id(),  $request['message']);
        $user = Auth::user();
        return redirect()->route('adverts.show', compact('advert', 'user'));

    }

    private function getAdvert(int $id)
    {
        return Advert::findOrFail($id);
    }

}