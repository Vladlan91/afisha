<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 14/11/2018
 * Time: 15:06
 */

namespace App\Http\Controllers\Adverts;


use App\Entity\Adverts\Advert\Advert;
use App\Entity\Adverts\Advert\Photo;
use App\Entity\Adverts\Category;
use App\Entity\Business\Company;
use App\Entity\LiqPay;
use App\Entity\Suggestions\Suggestions;
use App\Entity\Banner\Banner;
use App\Entity\Region;
use App\Http\Controllers\Controller;
use App\Http\Requests\Adverts\PhotoRequest;
use App\Router\AdvertsPath;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class AdvertController extends Controller
{
//    public function index(AdvertsPath $path)
//    {
//        $query = Advert::active()->with(['category', 'region'])->orderByDesc('published_at');
//
//        if ($category = $path->category){
//            $query->forCategory($category);
//        }
//
//        if ($region = $path->region){
//            $query->forRegion($region);
//        }
//        if (!$region){
//            $region = null;
//        }
//        $regions = $region ? $region->children()->orderBy('name')->getModels()
//            : Region::roots()->orderBy('name')->getModels();
//
//        $categories = $category
//            ? $category->children()->defaultOrder()->getModels()
//            : Category::whereIsRoot()->defaultOrder()->getModels();
//
//        $adverts = $query->paginate(20);
//
//        return view('adverts.index', compact('category', 'region', 'adverts', 'regions', 'categories'));
//
//    }

//->where('id', 'LIKE', '%'.$ids.'%') //adverts

    public function search(Request $request)
    {
        $region = null;
        $category = null;

        $query = Advert::active()->with(['category', 'region'])->orderByDesc('type');
        if ($request['category'] != null){
            $category = Category::findOrFail($request['category']);
            if ($category){
                $query->forCategory($category);
            }
        }
        if ($request['region'] != null){
            $region = Region::findOrFail($request['region']);
            if ($region){
                $query->forRegion($region);
            }
        }
        if ($request['title'] != null){
                $query->forTitle($request['title']);
        }
        $regions = $region ? $region->children()->orderBy('name')->getModels()
            : Region::roots()->orderBy('name')->getModels();

        $categories = $category
            ? $category->children()->defaultOrder()->getModels()
            : Category::whereIsRoot()->defaultOrder()->getModels();

        $adverts = $query->paginate(40);

        $banner1 = Banner::inRandomOrder()->where('format','!=', '240x440')->whereHas('user', function($q){
            $q->where('package', '!=', null);
        })->take(1)->getModels();
        foreach ($banner1 as $ban){
            $id = $ban->id;
        }
        if (!empty($banner1)){
            $banner2 = Banner::inRandomOrder()->where('id','!=', $id)->where('format','!=', '240x440')->whereHas('user', function($q){
                $q->where('package', '!=', null);
            })->take(1)->getModels();
        }else{
            $banner2 = Banner::inRandomOrder()->where('format','!=', '240x440')->whereHas('user', function($q){
                $q->where('package', '!=', null);
            })->take(1)->getModels();
        }

        return view('adverts.index', compact('category', 'region', 'adverts', 'regions', 'categories', 'banner1', 'banner2'));

    }
    public function searchAtr(Request $request, Category $category)
    {
        $sql_part = null;
        $sql_part2 = null;
        foreach ($category->allAttributes() as $attribute){
            $value = $request['attributes'][$attribute->id] ?? null;
            if ($value){
                $advert_value = DB::table('advert_advert_values')
                    ->whereIn('value', [$value])
                    ->where('attribute_id', [$attribute->id])->get();
                $array1[] = $advert_value;
            }
        }
        $sql_part2 = rtrim($sql_part2,',');
        $sql_part = rtrim($sql_part,',');


        $advert_value = DB::table('advert_advert_values')
            ->whereIn('value', [$sql_part])
            ->where('attribute_id', [$sql_part2])->get();

        dd($advert_value);


        $advert = DB::table('advert_adverts')
            ->whereIn('id', [$sql_part])
            ->get();

        Builder::macro('advert_advert_values', function ($attributes, string $searchTerm) {
            foreach (array_wrap($attributes) as $attribute) {
                $this->orWhere($attribute, 'LIKE', "%{$searchTerm}%");
            }
            return $this;
        });

        $region = null;
        $category = null;

        $query = Advert::active()->with(['category', 'region'])->orderByDesc('type');
        if ($request['category'] != null){
            $category = Category::findOrFail($request['category']);
            if ($category){
                $query->forCategory($category);
            }
        }
        if ($request['region'] != null){
            $region = Region::findOrFail($request['region']);
            if ($region){
                $query->forRegion($region);
            }
        }
        if ($request['title'] != null){
            $query->forTitle($request['title']);
        }
        $regions = $region ? $region->children()->orderBy('name')->getModels()
            : Region::roots()->orderBy('name')->getModels();

        $categories = $category
            ? $category->children()->defaultOrder()->getModels()
            : Category::whereIsRoot()->defaultOrder()->getModels();

        $adverts = $query->paginate(40);

        return view('adverts.index', compact('category', 'region', 'adverts', 'regions', 'categories'));

    }
    public function index(Region $region = null, Category $category = null)
    {
        $query = Advert::active()->with(['category', 'region'])->orderByDesc('type');

        if ($category){
            $query->forCategory($category);
        }

        if ($region){
            $query->forRegion($region);
        }
        if (!$region){
            $region = null;
        }
        $regions = $region ? $region->children()->orderBy('name')->getModels()
            : Region::roots()->orderBy('name')->getModels();

        $categories = $category
            ? $category->children()->defaultOrder()->getModels()
            : Category::whereIsRoot()->defaultOrder()->getModels();

        $adverts = $query->paginate(40);

        return view('adverts.index', compact('category', 'region', 'adverts', 'regions', 'categories'));

    }

    public function all(Category $category = null)
    {
        $query = Advert::active()->with(['category', 'region'])->orderByDesc('type');

        if ($category){
            $query->forCategory($category);
        }


        $region = null;
        $regions = $region ? $region->children()->orderBy('name')->getModels()
            : Region::roots()->orderBy('name')->getModels();

        $categories = $category
            ? $category->children()->defaultOrder()->getModels()
            : Category::whereIsRoot()->defaultOrder()->getModels();

        $adverts = $query->paginate(40);

        $banner1 = Banner::inRandomOrder()->where('format','!=', '240x440')->whereHas('user', function($q){
            $q->where('package', '!=', null);
        })->take(1)->getModels();
        foreach ($banner1 as $ban){
            $id = $ban->id;
        }
        if (!empty($banner1)){
            $banner2 = Banner::inRandomOrder()->where('id','!=', $id)->where('format','!=', '240x440')->whereHas('user', function($q){
                $q->where('package', '!=', null);
            })->take(1)->getModels();
        }else{
            $banner2 = Banner::inRandomOrder()->where('format','!=', '240x440')->whereHas('user', function($q){
                $q->where('package', '!=', null);
            })->take(1)->getModels();
        }
        return view('adverts.index', compact('category', 'region', 'adverts', 'regions', 'categories', 'banner1','banner2'));

    }

    public function showTown(Region $region = null)
    {
        $query = Advert::active()->with(['category', 'region'])->orderByDesc('type');

        if ($region){
            $query->forRegion($region);
        }
        $adverts = $query->paginate(40);
        $category = null;
        $categories = $category
            ? $category->children()->defaultOrder()->getModels()
            : Category::whereIsRoot()->defaultOrder()->getModels();

        $regions = $region ? $region->children()->orderBy('name')->getModels()
            : Region::roots()->orderBy('name')->getModels();

        $banner1 = Banner::inRandomOrder()->where('format','!=', '240x440')->whereHas('user', function($q){
            $q->where('package', '!=', null);
        })->take(1)->getModels();
        foreach ($banner1 as $ban){
            $id = $ban->id;
        }
        $banner2 = Banner::inRandomOrder()->where('id','!=', $id)->where('format','!=', '240x440')->whereHas('user', function($q){
            $q->where('package', '!=', null);
        })->take(1)->getModels();

        return view('adverts.index', compact('category', 'region', 'adverts', 'regions', 'categories', 'banner1','banner2'));
    }


    public function show(Advert $advert)
    {
        if (!($advert->isActive() || Gate::allows('show-advert', $advert))) {
            abort(404);
        }
        $user = Auth::user();
        $query = Advert::orderByDesc('updated_at');
        $query->where('category_id', $advert->category_id);
        $query->where('id', '!=', $advert->id);
        $adverts = $query->paginate(3);

        $suggestions = Suggestions::inRandomOrder()->whereHas('company', function($q){
            $q->where('package', '!=', null);
        })->take(2)->getModels();

        $public_key = 'i99522379853';
        $private_key = '0y9X3EUCUbf5fvLDnmTmOv3feVOukt8F0noWUwSR';
        $liqpay = new LiqPay($public_key, $private_key);
        $html = $liqpay->cnb_form(array(
            'action'         => 'pay',
            'amount'         => $advert->print_price - $user->balance,
            'currency'       => 'UAH',
            'server_url'     => 'UAH',
            'result_url'     => 'http://localhost:8888/adverts/show/'. $advert->id,
            'description'    => 'Поповнення особистого балансу на сайті АФІША ПРИКАРПАТТЯ',
            'order_id'       => $advert->id,
            'sandbox'        => '1',
            'language'       => 'uk',
            'logo'           => 'http://ap.if.ua/images/logo.png',
            'version'        => '3'
        ));

        return view('adverts.show', compact('html','advert','user', 'adverts','suggestions'));
    }

    public function showuserall(User $user)
    {
        $avtor = $user;
        $user = Auth::user();
        $adverts = Advert::active()->forUser($avtor)->with(['category', 'region'])->orderByDesc('published_at')->get();
        $suggestions = Suggestions::inRandomOrder()->whereHas('company', function($q){
            $q->where('package', '!=', null);
        })->take(2)->getModels();

        return view('adverts.showuserall', compact('adverts','user','suggestions','avtor'));
    }

    public function showcompanyall(Company $company)
    {
        $user = Auth::user();

        $adverts = Advert::active()->forCompany($company)->with(['category', 'region'])->orderByDesc('published_at')->get();
        $suggestions = Suggestions::inRandomOrder()->whereHas('company', function($q){
            $q->where('package', '!=', null);
        })->take(2)->getModels();

        return view('adverts.showcompanyall', compact('adverts','user','suggestions','company'));
    }

    public function phone(Advert $advert): string
    {
        if (!($advert->isActive() || Gate::allows('show-advert', $advert))) {
            abort(403);
        }
        if ($advert->hasPhone()){

            return $advert->phone;
        }else{
            return $advert->user->phone;
        }

    }

    public function photos(Advert $advert)
    {
        if (!($advert->isActive() || Gate::allows('show-advert', $advert))) {
            abort(404);
        }

        return view('adverts.edit.photos', compact('advert'));
    }

    public function save(Advert $advert, PhotoRequest $request)
    {
        if (!($advert->isActive() || Gate::allows('show-advert', $advert))) {
            abort(404);
        }
        $photo = Photo::create([
        'advert_id' => $advert->id,
        'file' => $request['file']->store('adverts', 'local'),
                    ]);
        $photo->saveOrFail();
        $user = Auth::user();
        return redirect()->route('adverts.show', compact('advert','user'));
    }

}