<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 13/01/2019
 * Time: 13:02
 */

namespace App\Http\Controllers;


use App\Entity\Business\Company;
use App\Entity\Suggestions\Suggestions;

class SuggestionsController extends Controller
{
    public function show(Company $company, Suggestions $suggestions){

        return view('suggestions.show', compact('company', 'suggestions'));
    }

}