<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 03/12/2018
 * Time: 13:37
 */

namespace App\Http\Controllers\Cabinet;


use App\Entity\Ticket\Ticket;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\UseCase\Tickets\TicketService;
use App\Http\Requests\Ticket\CreateRequest;
use App\Http\Requests\Ticket\MessageRequest;

class TicketController extends Controller
{
    private $service;

    public function __construct(TicketService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $tickets = Ticket::forUser(Auth::user())->orderByDesc('updated_at')->paginate(20);

        return view('cabinet.tickets.index', compact('tickets'));
    }

    public function show(Ticket $ticket)
    {
        return view('cabinet.tickets.show', compact('ticket'));
    }

    public function create()
    {
        return view('cabinet.tickets.create');
    }

    public function store(CreateRequest $request)
    {
        try{
            $ticket = $this->service->create(Auth::id(), $request);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.tickets.show', $ticket);

    }

    public function message(MessageRequest $request, Ticket $ticket)
    {
        try{
            $this->service->message(Auth::id(), $ticket->id, $request);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.tickets.show', $ticket);

    }

    public function destroy(Ticket $ticket)
    {
        $this->checkAccess($ticket);
        try{
            $this->service->removeByOwner($ticket->id);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.tickets.index', $ticket);

    }


    public function checkAccess(Ticket $ticket): void
    {
        if (!Gate::allows('manage-own-ticket', $ticket)){
            abort(403);
        }
    }

}