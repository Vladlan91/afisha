<?php

namespace App\Http\Controllers\Cabinet;


use App\Http\Controllers\Controller;
use App\Http\Requests\PhoneRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function index(){
        $user = Auth::user();

        return view('cabinet.profile.home', compact('user'));
    }

    public function edit(){

        $user = Auth::user();

        return view('cabinet.profile.edit', compact('user'));

    }

    public function update(PhoneRequests $request){
        $user = Auth::user();
        $phone = $this->format_phone($request['phone']);
        $oldAvatar = $user->avatar;
        $user->update($request->only('name', 'last_name', 'email'));
        $user->phone_verified = true;
        $user->phone = $phone;
        if ($request['avatar']){
            $user->avatar = $request['avatar']->store('users', 'local');
            if ($oldAvatar != null){
                unlink(public_path(). '/app/'. $oldAvatar);
            }
        }
        $user->saveOrFail();
        return redirect()->route('cabinet.profile.home');
    }


    function format_phone($phone = '')
    {
        $phone = preg_replace('/[^0-9]/', '', $phone); // вернет 79851111111
        $phone_number['dialcode'] = substr($phone, 0, 0);
        $phone_number['code']  = substr($phone, 0, 3);
        $phone_number['phone'] = substr($phone, -7);
        $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
        $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
        $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

        $format_phone = '+38' . $phone_number['dialcode'] . ' ('. $phone_number['code'] .') ' . implode('-', $phone_number['phone_arr']);

        return $format_phone;
    }

}
