<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 28/12/2018
 * Time: 20:23
 */

namespace App\Http\Controllers\Cabinet\Company;

use App\Entity\Business\Business;
use App\Entity\Business\Company;
use App\Entity\Region;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequests;
use App\Router\CompanyPath;
use Auth;
use Illuminate\Http\Request;


class CompanyController extends Controller
{
    public function index()
    {
        $company = Company::forUser(Auth::user())->orderByDesc('id')->paginate(20);

        return view('cabinet.company.index', compact('company'));
    }

    public function business(Business $business = null, Region $region = null)
    {

        if (!empty($business)){
            $morebusiness = Business::where('parent_id', $business->id)->orderBy('name')->get();
            if ($morebusiness->count() < 1){
                $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
                return view('cabinet.company.create.region', compact('business','regions', 'region'));
            }
            if (!empty($morebusiness)){
                return view('cabinet.company.create.business', compact('morebusiness'));
            } else{
                $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
                return view('cabinet.company.create.region', compact('business','regions', 'region'));
            }
        }
        $morebusiness = Business::defaultOrder()->withDepth()->get()->toTree();
        return view('cabinet.company.create.business', compact('morebusiness'));
    }

    public function region(Business $business, Region $region = null)
    {
        if (!empty($region)){
            $regions = Region::where('parent_id', $region->id)->orderBy('name')->get();
            if ($regions->count() < 1){
                return view('cabinet.company.create.company', compact('business', 'region'));
            }
        }

        $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
        return view('cabinet.company.create.region', compact('business','regions', 'region'));
    }

    public function company(Business $business,  Region $region = null)
    {
        return view('cabinet.company.create.company', compact('business', 'region'));
    }

    public function store(CompanyRequests $request, Business $business,  Region $region = null)
    {
        $phone = $this->format_phone($request['phone']);
        if (!empty($request['logo'])){
            $company = Company::create([
                'title' => $request['title'],
                'user_id' => Auth::id(),
                'category_id' => $business->id,
                'region_id' => $region->id,
                'logo' => $request['logo']->store('companies', 'local'),
                'location' => $request['location'],
                'phone' => $phone,
                'site' => $request['site'],
                'email' => $request['email'],
                'status' => Company::STATUS_DRAFT,
                'slug' => str_slug($request['title']),
                'address' => $request['address'],
                'content' => $request['content'],
            ]);
        }else{
            $company = Company::create([
                'title' => $request['title'],
                'user_id' => Auth::id(),
                'category_id' => $business->id,
                'region_id' => $region->id,
                'location' => $request['location'],
                'phone' => $phone,
                'site' => $request['site'],
                'email' => $request['email'],
                'status' => Company::STATUS_DRAFT,
                'slug' => str_slug($request['title']),
                'address' => $request['address'],
                'content' => $request['content'],
            ]);
        }

        return redirect()->route('cabinet.company.index');
    }
    public function send(Company $company)
    {
        $company->sendToModeration();
        return redirect()->route('cabinet.company.edit');
    }

    public function editForm(Company $company)
    {
        return view('cabinet.company.edit', compact('company'));
    }

    public function edit(Request $request, Company $company)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',
        ]);
        $oldAvatar = $company->logo;
        $oldUser = $company->user_id;
        $company->update([
            'title' => $request['title'],
            'location' => $request['location'],
            'phone' => $request['phone'],
            'site' => $request['site'],
            'email' => $request['email'],
            'status' => Company::STATUS_DRAFT,
            'slug' => str_slug($request['title']),
            'user_id' => $oldUser,
            'address' => $request['address'],
            'content' => $request['content'],
        ]);
        if ($request['logo']){
            $company->logo = $request['logo']->store('companies', 'local');
            if ($oldAvatar != null){
                unlink(public_path(). '/app/'. $oldAvatar);
            }
            $company->saveOrFail();
        }
        return redirect()->route('cabinet.company.index');
    }

    public function destroy(Company $company){

        if (isset($company->logo)){
            unlink(public_path(). '/app/'. $company->logo);
        }
        $company->delete();

        return redirect()->route('cabinet.company.index');
    }

    function format_phone($phone = '')
    {
        $phone = preg_replace('/[^0-9]/', '', $phone); // вернет 79851111111
        $phone_number['dialcode'] = substr($phone, 0, 0);
        $phone_number['code']  = substr($phone, 0, 3);
        $phone_number['phone'] = substr($phone, -7);
        $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
        $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
        $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

        $format_phone = '+38' . $phone_number['dialcode'] . ' ('. $phone_number['code'] .') ' . implode('-', $phone_number['phone_arr']);

        return $format_phone;
    }

}