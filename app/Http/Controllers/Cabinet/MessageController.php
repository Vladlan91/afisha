<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 07/12/2018
 * Time: 21:10
 */

namespace App\Http\Controllers\Cabinet;

use App\Entity\Adverts\Advert\Dialog\Message;
use Illuminate\Support\Facades\Auth;
use App\Entity\Adverts\Advert\Advert;
use App\Entity\Adverts\Advert\Dialog\Dialog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        $dialogs = Dialog::forUser(Auth::user())->orderBy('id')->paginate(20);

        return view('cabinet.messages.index', compact('dialogs'));
    }

    public function show( Dialog $dialog)
    {

        if (Auth::id() !== $dialog->client_id){
            $dialog->readByClient();
        }else{
            $dialog->readByOwner();
        }
         $id = $dialog->id;
         $dialogs = Dialog::forUser(Auth::user())->orderBy('id')->paginate(20);

        return view('cabinet.messages.show', compact('dialogs', 'dialog', 'id'));
    }

    public function send( Dialog $dialog, Request $request)
    {
        $id = $dialog->id;
        $dialogs = Dialog::forUser(Auth::user())->orderBy('id')->paginate(20);

        $advert = $this->getAdvert($dialog->advert_id);
        $advert->writeOwnerMessage($dialog->client_id,  $request['message']);
        return redirect()->route('cabinet.messages.show', $dialog);

    }

    private function getAdvert(int $id)
    {
        return Advert::findOrFail($id);
    }



}