<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 27/11/2018
 * Time: 11:04
 */

namespace App\Http\Controllers\Cabinet\Banners;


use App\Http\Controllers\Controller;
use App\Http\Requests\Banner\EditRequest;
use App\Http\Requests\Banner\FileRequest;
use Auth;
use App\UseCase\Banners\BannerService;
use App\Entity\Banner\Banner;
use Illuminate\Support\Facades\Gate;

class BannerController extends Controller
{
    private $service;

    public function __construct(BannerService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $banners = Banner::forUser(Auth::user())->orderByDesc('id')->paginate(20);

        return view('cabinet.banners.index', compact('banners'));
    }

    public function show(Banner $banner)
    {
        $this->checkAccess($banner);

        return view('cabinet.banners.show', compact('banner'));
    }

    public function editForm(Banner $banner)
    {
        $this->checkAccess($banner);
        if (!$banner->canBeChanged()){
            return redirect()->route('cabinet.banners.show', $banner)->with('error', 'Unable to edit.');
        }
        return view('cabinet.banners.edit', compact('banner'));
    }

    public function edit(EditRequest $request, Banner $banner)
    {
        $this->checkAccess($banner);
        try{
            $this->service->editByOwner($banner->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.banners.show', $banner);
    }

    public function fileForm(Banner $banner)
    {
        $this->checkAccess($banner);
        if (!$banner->canBeChanged()){
            return redirect()->route('cabinet.banners.show', $banner)->with('error', 'Unable to edit.');
        }
        $formats = $banner->formatsList();

        return view('cabinet.banners.file', compact('banner','formats'));
    }

    public function file(FileRequest $request, Banner $banner)
    {
        $this->checkAccess($banner);
        try{
            $this->service->changeFileOwner($banner->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.banners.show', $banner);
    }

    public function send(Banner $banner)
    {
        $this->checkAccess($banner);
        try{
            $this->service->sendToModeration($banner->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.banners.show', $banner);
    }

    public function cancel(Banner $banner)
    {
        $this->checkAccess($banner);
        try{
            $this->service->cancelModeration($banner->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.banners.show', $banner);
    }

    public function order(Banner $banner)
    {
        $this->checkAccess($banner);
        try{
            $banner = $this->service->order($banner->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.banners.show', $banner);
    }

    public function destroy(Banner $banner)
    {
        try{
            $this->service->removeByOwner($banner->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.banners.index', $banner);
    }

    public function checkAccess(Banner $banner): void
    {
        if (!Gate::allows('manage-own-banner', $banner)){
            abort(403);
        }
    }

}