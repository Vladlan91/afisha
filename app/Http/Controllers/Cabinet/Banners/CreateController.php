<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 27/11/2018
 * Time: 11:04
 */

namespace App\Http\Controllers\Cabinet\Banners;


use App\Http\Controllers\Controller;
use App\Http\Requests\Banner\CreateRequest;
use App\UseCase\Banners\BannerService;
use App\Entity\Adverts\Category;
use App\Entity\Region;
use Auth;
use App\Entity\Banner\Banner;

class CreateController extends Controller
{
    private $service;

    public function __construct(BannerService $service)
    {
        $this->service = $service;

    }

    public function category(Category $category = null, Region $region = null)
    {

        if (!empty($category)){
            $categories = Category::where('parent_id', $category->id)->orderBy('name')->get();
            if ($categories->count() < 1){
                $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
                return view('cabinet.banners.create.region', compact('category','regions', 'region'));
            }
            if (!empty($categories)){
                return view('cabinet.banners.create.category', compact('categories'));
            } else{
                $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
                return view('cabinet.banners.create.region', compact('category','regions', 'region'));
            }
        }

        $categories = Category::defaultOrder()->withDepth()->get()->toTree();
        return view('cabinet.banners.create.category', compact('categories'));
    }

    public function all(Region $region = null)
    {
        $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
        return view('cabinet.banners.create.reg', compact('regions'));

    }

    public function region(Category $category= null, Region $region = null)
    {
        if (!empty($region)){
            $regions = Region::where('parent_id', $region->id)->orderBy('name')->get();
            if ($regions->count() < 1){
                return view('cabinet.banners.create.banner', compact('category', 'region'));
            }
        }

        $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
        return view('cabinet.banners.create.region', compact('category','regions', 'region'));
    }

    public function bannerall(Region $region = null){
        $formats = Banner::formatsList();
        return view('cabinet.banners.create.bannerall', compact('formats','region'));
    }

    public function banner(Category $category = null, Region $region = null){
        $formats = Banner::formatsList();

        return view('cabinet.banners.create.banner', compact('category', 'region', 'formats'));

    }

    public function storeall(CreateRequest $request, Region $region = null)
    {
        try{
            $banner = $this->service->create(
                Auth::user(),
                null,
                $region ? $region: null,
                $request

            );
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('cabinet.banners.show', $banner);
    }

    public function store(CreateRequest $request, Category $category = null, Region $region = null)
    {
        try{
            $banner = $this->service->create(
                Auth::user(),
                $category ? $category: null,
                $region ? $region: null,
                $request

            );
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('cabinet.banners.show', $banner);
    }

}