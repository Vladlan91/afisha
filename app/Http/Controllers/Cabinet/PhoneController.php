<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 03/11/2018
 * Time: 12:51
 */

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Services\Sms\SmsSender;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    private $sms;

    public function __construct(SmsSender $sms)
    {
        $this->sms = $sms;
    }

    public function request(Request $request){
        $user = Auth::user();
        try{
            $token = $user->requestPhoneVerification(Carbon::now());
            //$this->sms->send($user->phone, 'Phone verify token: ' . $token);
        }catch (\DomainException $e){
            flash('Код для підтвердження телефону вже надіслано.')->info();
        }
        return redirect()->route('cabinet.profile.phone');
    }

    public function form(){
        $user = Auth::user();
        return view('cabinet.profile.phone', compact('user'));
    }

    public function verify(Request $request){
        $this->validate($request,[
            'token' =>'required|string|max:255'
        ]);
        $user = Auth::user();
        try{
            $user->verifyPhone($request['token'], Carbon::now());
        }catch (\DomainException $e){
            flash('Не вірний код, телефон не підтверджено.')->error();
            return redirect()->route('cabinet.profile.phone');
        }
        return redirect()->route('cabinet.profile.home');
    }

    public function auth(){
        $user = Auth::user();
        if ($user->isPhoneAuthEnabled()){
            $user->disablePhoneAuth();
        }else{
            $user->enablePhoneAuth();
        }
        return redirect()->route('cabinet.profile.home');
    }

}