<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 19/11/2018
 * Time: 21:23
 */

namespace App\Http\Controllers\Cabinet;


use App\Entity\Adverts\Advert\Advert;
use App\UseCase\Adverts\FavoriteService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class FavoriteController extends Controller
{
    private $services;

    public function __construct(FavoriteService $service)
    {
        $this->services = $service;
        $this->middleware('auth');
    }

    public function index()
    {
        $adverts = Advert::favoredByUser(Auth::user())->orderByDesc('id')->paginate(20);

        return view('cabinet.favorites.index', compact('adverts'));
    }


    public function remove(Advert $advert)
    {
        try {
            $this->services->remove(Auth::id(), $advert->id);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.favorites.index', $advert);


    }

}