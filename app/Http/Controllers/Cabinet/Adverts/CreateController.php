<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 04/11/2018
 * Time: 20:51
 */

namespace App\Http\Controllers\Cabinet\Adverts;


use App\Entity\Adverts\Advert\Advert;
use App\Entity\Adverts\Category;
use App\Entity\Region;
use App\Http\Controllers\Controller;
use App\Http\Middleware\FilledProfile;
use App\Http\Requests\Adverts\CreateRequest;
use App\UseCase\Adverts\AdvertService;
use Auth;

class CreateController extends Controller
{
    private $service;

    public function __construct(AdvertService $service)
    {
        $this->service = $service;
        $this->middleware( FilledProfile::class);
    }

    public function category(Category $category = null, Region $region = null)
    {

        if (!empty($category)){
            $categories = Category::where('parent_id', $category->id)->orderBy('name')->get();
            if ($categories->count() < 1){
                $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
                return view('cabinet.adverts.create.region', compact('category','regions', 'region'));
            }
            if (!empty($categories)){
                return view('cabinet.adverts.create.category', compact('categories'));
            } else{
                $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
                return view('cabinet.adverts.create.region', compact('category','regions', 'region'));
            }
        }
        $categories = Category::defaultOrder()->withDepth()->get()->toTree();
        return view('cabinet.adverts.create.category', compact('categories'));
    }

    public function region(Category $category, Region $region = null)
    {

        if (!empty($region)){
            $regions = Region::where('parent_id', $region->id)->orderBy('name')->get();
            if ($regions->count() < 1){
                $types = Advert::typeListUser();
                $times = Advert::timeListUser();
                return view('cabinet.adverts.create.advert', compact('category', 'times','region','types'));
            }
        }

        $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
        return view('cabinet.adverts.create.region', compact('category','regions', 'region'));
    }

    public function advert(Category $category,  Region $region = null)
    {
        $types = Advert::typeListUser();
        if (Auth::user()->isAdmin()){
            $times = Advert::timeListAdmin();
        }else{
            $times = Advert::timeListUser();
        }
        return view('cabinet.adverts.create.advert', compact('category', 'times','region','types'));
    }

    public function store(CreateRequest $request, Category $category, Region $region = null)
    {

        try{
            $advert = $this->service->create(
                Auth::id(),
                $category->id,
                $region ? $region->id : null,
                $request
            );
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('adverts.show', $advert);
    }




}