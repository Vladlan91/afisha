<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 03/11/2018
 * Time: 17:39
 */

namespace App\Http\Controllers\Cabinet\Adverts;

use App\Entity\Adverts\Advert\Advert;
use App\Http\Middleware\FilledProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdvertController extends Controller
{
    public function __construct()
    {
        $this->middleware(FilledProfile::class);
    }

    public function index()
    {
        $adverts = Advert::forUser(Auth::user())->orderByDesc('id')->paginate(20);
        return view('cabinet.adverts.index', compact('adverts'));
    }

}