<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 14/11/2018
 * Time: 13:58
 */

namespace App\Http\Controllers\Cabinet\Adverts;


use App\Entity\Adverts\Advert\Advert;
use App\Entity\Business\Company;
use App\Http\Controllers\Controller;
use App\Http\Middleware\FilledProfile;
use App\Http\Requests\Adverts\AttributesRequest;
use App\Http\Requests\Adverts\EditRequest;
use App\Http\Requests\Adverts\PhotoRequest;
use App\UseCase\Adverts\AdvertService;
use Illuminate\Support\Facades\Gate;
use Auth;
use Illuminate\Http\Request;

class ManageController extends Controller
{
    private $service;

    public function __construct(AdvertService $service)
    {
        $this->service = $service;
        $this->middleware(FilledProfile::class);
    }

    public function editForm(Advert $advert)
    {
        $this->checkAccess($advert);
        return view('adverts.edit.advert', compact('advert'));
    }

    public function edit(EditRequest $request, Advert $advert)
    {
        $this->checkAccess($advert);
        try {
            $this->service->edit($advert->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        flash('Зміни збережено в оголошені - '. $advert->title)->success();
        return redirect()->route('adverts.show', $advert);
    }

    public function attributesForm(Advert $advert)
    {
        $this->checkAccess($advert);
        return view('adverts.edit.attributes', compact('advert'));
    }

    public function attributes(AttributesRequest $request, Advert $advert)
    {
        $this->checkAccess($advert);
        try{
            $this->service->editAttributes($advert->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('adverts.show', $advert);   //????
    }

    public function companyForm(Advert $advert)
    {
        if (Auth::user()->isAdmin()){
            $company = Company::orderByDesc('id')->get();
        }else{
            $company = Company::forUser(Auth::user())->orderByDesc('id')->get();
        }
        return view('adverts.company.add', compact('advert','company'));
    }

    public function company(Request $request, Advert $advert)
    {
        $this->validate($request, [
            'company_id' => 'nullable|integer|exists:companies,id',
        ]);

        $advert->update([
            'company_id' => $request['company_id'],
        ]);

        return redirect()->route('adverts.show', $advert);
    }

    public function photosForm(Advert $advert)
    {
        $this->checkAccess($advert);
        return view('adverts.edit.photos', compact('advert'));
    }

    public function photos(PhotoRequest $request, Advert $advert)
    {
        $this->checkAccess($advert);
        try{
            $this->service->addPhotos($advert->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);   //????
    }

    public function send(Advert $advert)
    {
        $this->checkAccess($advert);
        try{
            $this->service->sendToModeration($advert->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);
    }

    public function close(Advert $advert)
    {
        $this->checkAccess($advert);
        try{
            $this->service->close($advert->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);
    }

    public function destroy( Advert $advert)
    {
        $this->checkAccess($advert);
        try{
            $this->service->remove($advert->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        flash('Оголошення видалено')->success();
        return redirect()->route('cabinet.adverts.index', $advert);
    }

    public function checkAccess(Advert $advert): void
    {
        if (!Gate::allows('manage-own-advert', $advert)){
            abort(403);
        }
    }



}