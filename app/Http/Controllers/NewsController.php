<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30/12/2018
 * Time: 16:27
 */

namespace App\Http\Controllers;


use App\Entity\News;
use App\Entity\NewsCat;
use App\Router\NewPath;

class NewsController extends Controller
{
    public function index(NewsCat $category = null)
    {
        $query = News::orderByDesc('id');
        if ($category){
            $query->forCategory($category);
        }
        $newsCat = NewsCat::orderByDesc('id')->getModels();

        $news = $query->paginate(10);
        return view('news.index', compact('news','newsCat'));
    }

    public function show(NewPath $path)
    {
        $news = $path->news;
        $newsCat = NewsCat::orderByDesc('id')->getModels();
        return view('news.show', compact('news','newsCat'));
    }


}
