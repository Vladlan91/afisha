<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Mail\DemoMail;
use App\Services\Sms\SmsSender;
use App\User;
use Dotenv\Exception\ValidationException;
use Illuminate\Support\Facades\File;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Socialite;


class LoginController extends Controller
{

    use ThrottlesLogins;

    private $sms;

    public function __construct(SmsSender $sms)
    {
        $this->middleware('guest')->except('logout');
        $this->sms = $sms;
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function redirectToFacebookProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderFacebookCallback()
    {

        $auth_user = Socialite::driver('facebook')->user();
        $user = User::updateOrCreate(
            [
                'provider_id' => $auth_user->id
            ],
            [
                'token' => $auth_user->token,
                'name'  =>  $auth_user->original['first_name'],
                'provider' => 'facebook',
                'last_name'  =>  $auth_user->original['last_name'],
                'role'  =>  'user'

            ]
        );

        Auth::login($user, true);
        return redirect()->to('/');
    }

    public function redirectToGoogleProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderGoogleCallback()
    {
        $user = Socialite::driver('google')->user();

        if ($user) {
            $local_user = User::whereEmail($user->getEmail())->first();
            if (!$local_user) {
                $fragment = explode(' ', $user->getName());
                $local_user = User::create([
                    'name' => isset($fragment[0]) ? $fragment[0] : '',
                    'last_name' => isset($fragment[1]) ? $fragment[1] : '',
                    'email' => $user->getEmail(),
                    'provider_id' => $user->avatar,
                    'provider' => 'google',
                    'role' => 'user'
                ]);
            }
            auth()->login($local_user, true);
            return redirect()->to('/');
        }

    }


        public function login(LoginRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $this->sendLockoutResponse($request);
        }
        $authenticate = Auth::attempt(['email' => $request['email'], 'password' => bcrypt($request['password'])]);


        if ($authenticate){
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);
            $user = Auth::user();
            if ($user->status !== User::STATUS_ACTIV){

                $_SESSION['verify_token'] = $user->verify_token;
                \Mail::send('emails.demo', array('test' => 'Test User'), function ($massage){
                    $massage->to('ivanitskiyvv32@gmail.com', 'Test')->subject('Wellcom');
                });

                Auth::logout();
                flash('Перейдіть у пошту, пройдіть по силці для верефікації ')->important()->info();
                return back();
            }

            $user = Auth::user();
            $_SESSION['user'] = $user;
            $_SESSION['user']['role'] = $user->role;

            return redirect()->intended(route('cabinet.home'));
        }

        $this->incrementLoginAttempts($request);

        throw \Illuminate\Validation\ValidationException::withMessages([ 'email' => 'Невірний емейл чи пароль', ]);
    }

    public function logout(Request $request){
        Auth::guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('home');
    }

    public function username(){
        return 'email';
    }




}
