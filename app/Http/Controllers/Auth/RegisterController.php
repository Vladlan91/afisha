<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\RegisterRequest;
use App\Mail\VerifyMail;
use App\UseCase\Auth\RegisterService;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    private $service;

    public function __construct(RegisterService $service)
    {
        $this->middleware('guest');
        $this->service = $service;
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    protected function register(RegisterRequest $request)
    {

        $this->service->register($request);

        return redirect()->route('login');
    }

    public function verify($token){
        if (!$user = User::where('verify_token', $token)->first()){
            flash('Перейдіть у пошту, пройдіть по силці для верефікації ')->important()->error();
            return redirect()->route('login');
        }

        try{
            $this->service->verify($user->id);
            flash('Ваш емейл пройщов верефікацію ')->important()->success();
            return redirect()->route('login');
        }catch (\DomainException $e){
            flash('Перейдіть у пошту, пройдіть по силці для верефікації ')->important()->error();
            return redirect()->route('login');
        }

    }

}
