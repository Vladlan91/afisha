<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12/02/2019
 * Time: 14:53
 */

namespace App\Http\Controllers\Admin;


use App\Entity\Order;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $query = Order::orderByDesc('id');

        if (!empty($value = $request->get('id'))){
            $query->where('id', 'like', '%'. $value .'%');
        }

        if (!empty($value = $request->get('type'))){
            $query->where('system', '=',$value );
        }

        if (!empty($value = $request->get('action'))){
            $query->where('action', $value );
        }
        if (!empty($request->get('trip-start'))){
            $value = Carbon::make($request->get('trip-start'));
            $query->where('created_at','>=', $value->toDateString().' 00:00:00');
        }
        if (!empty($request->get('trip-finish'))){
            $value = Carbon::make($request->get('trip-finish'));
            $query->where('created_at','<=', $value->toDateString().' 00:00:00');
        }


        $order = $query->paginate(40);
        $types = Order::statusList();
        $actionList = Order::actionList();
        return view('admin.order.index', compact('order', 'types', 'actionList'));
    }

}