<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 14/11/2018
 * Time: 14:28
 */

namespace App\Http\Controllers\Admin\Adverts;


use App\Entity\Adverts\Advert\Advert;
use App\Http\Controllers\Controller;
use App\Http\Middleware\FilledProfile;
use App\Http\Requests\Adverts\AttributesRequest;
use App\Http\Requests\Adverts\PhotoRequest;
use App\UseCase\Adverts\AdvertService;

class ManageController extends Controller
{
    private $service;

    public function __construct(AdvertService $service)
    {
        $this->service = $service;
        $this->middleware(FilledProfile::class);
    }

    public function attributes(Advert $advert)
    {
        return view('adverts.edit.attributes', compact('advert'));
    }

    public function updateAttributes(AttributesRequest $request, Advert $advert)
    {
        try{
            $this->service->editAttributes($advert->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('adverts.show', $advert);   //????
    }

    public function photos(Advert $advert)
    {
        return view('adverts.edit.photos', compact('advert'));
    }

    public function updatePhotos(PhotoRequest $request, Advert $advert)
    {
        try{
            $this->service->addPhotos($advert->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);   //????
    }

    public function destroy( Advert $advert)
    {
        try{
            $this->service->remove($advert->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return back();
    }

    public function moderate( Advert $advert)
    {
        try{
            $this->service->moderate($advert->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return back();
    }
}