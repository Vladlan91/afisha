<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 17/11/2018
 * Time: 01:11
 */

namespace App\Http\Controllers\Admin\Adverts;


use App\Entity\Adverts\Advert\Advert;
use App\Entity\Adverts\Category;
use App\Entity\Banner\Banner;
use App\Entity\LiqPay;
use App\Entity\Order;
use App\Entity\Region;
use App\Entity\SystemBalance;
use App\Http\Controllers\Controller;
use App\Http\Requests\Adverts\RejectRequest;
use App\UseCase\Adverts\AdvertService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\Adverts\AttributesRequest;
use App\Http\Requests\Adverts\EditRequest;
use App\Http\Requests\Adverts\PhotoRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Adverts\CreateRequest;
use Illuminate\Support\Facades\Gate;

class AdvertController extends Controller
{

    private $service;
    public $request;
    public $public_key = 'i99522379853';
    public $private_key = '0y9X3EUCUbf5fvLDnmTmOv3feVOukt8F0noWUwSR';

    public function __construct(AdvertService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $query = Advert::orderByDesc('updated_at');

        if (!empty($value = $request->get('id'))){
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('type'))){
            if ($value == 'А+Б+В'){
                $query->where('type', '=', 'A')->orWhere('type', '=', 'Б')->orWhere('type', '=', 'В');
            }elseif ($value == 'Г+Д'){
                $query->where('type', '=', 'Г')->orWhere('type', '=', 'Д');
            }else{
                $query->where('type', $value);
            }
        }
        if (!empty($request->get('trip-start'))){
            $value = Carbon::make($request->get('trip-start'));
            $query->where('created_at','>=', $value->toDateString().' 00:00:00');
        }
        if (!empty($request->get('trip-finish'))){
            $value = Carbon::make($request->get('trip-finish'));
            $query->where('created_at','<=', $value->toDateString().' 00:00:00');
        }
        if (!empty($value = $request->get('title'))){
            $query->where('title', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('address'))){
            $query->where('address', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('user'))){
            $query->where('user_id', $value);
        }
        if (!empty($value = $request->get('error'))){
            $query->where('errors', $value);
        }
        if (!empty($value = $request->get('region'))){
            $query->where('region_id', $value);
        }
        if (!empty($value = $request->get('category'))){
            $query->where('category_id', $value);
        }
        if (!empty($value = $request->get('status'))){
            $query->where('status', $value);
        }

        $adverts = $query->paginate(20);
        $this->request = $request;
        $data = $query->get();

        $this->createXML($data);
        $this->createTxt($data);
        $regions = Region::roots()->orderBy('name')->getModels();

        $categories = Category::defaultOrder()->withDepth()->get();

        $statuses =  Advert::statusesList();

        $types =  Advert::typeListUser();

        return view('admin.adverts.adverts.index', compact('adverts','types','regions', 'categories', 'statuses'));

    }
    public function newForm(){
        $types = Advert::typeListAdmin();
        $times = Advert::timeListAdmin();
        $regions = Region::roots()->orderBy('name')->getModels();
        $categories = Category::defaultOrder()->withDepth()->get();
        return view('admin.adverts.adverts.newadverts', compact('times','regions','types','categories'));
    }
    public function storeNew(CreateRequest $request)
    {
        try{
            $advert = $this->service->create(
                $request->get('user_id') ?: \Auth::id(),
                $request->get('category'),
                $request->get('region') ?: null,
                $request
            );
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('adverts.show', $advert);
    }

    public function getCategory(Request $request) {
        $value = $request->get('value');
        $category = Category::where('id', $value)->first();
        $output = '<div class="row">';
        foreach($category->allAttributes() as $attribute){
            $output .= '<div class="col-md-3"><div class="form-group"><label for="attribute_'.$attribute->id.'">'.$attribute->name.'</label>';
            if($attribute->isSelect()){
                $output .= '<select class="form-control" id="attribute_'.$attribute->id.'" name="attributes['. $attribute->id .']"><option value=""></option>';
                foreach($attribute->variants as $variant){
                    $output .= '<option value="'.$variant.'">'.$variant.'</option>';
                }
                $output .= '</select></div></div>';
            }elseif ($attribute->isCheckbox()){
                $output .= '<input type="checkbox"  id="attribute_'. $attribute->id .'" name="attributes['. $attribute->id.']">';
                $output .= '</div></div>';
            }
        }
        $output .='</div>';
        echo $output;
    }

    public function editForm(Advert $advert)
    {
        $this->checkAccess($advert);
        return view('adverts.edit.advert', compact('advert'));
    }

    public function rejectForm(Advert $advert)
    {
        $this->checkAccess($advert);
        return view('admin.adverts.adverts.reject', compact('advert'));
    }

    public function reject(RejectRequest $request, Advert $advert)
    {
        try {
            $this->service->reject($advert->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);
    }

    public function edit(EditRequest $request, Advert $advert)
    {
        $this->checkAccess($advert);
        try {
            $this->service->edit($advert->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);
    }

    public function attributesForm(Advert $advert)
    {
        $this->checkAccess($advert);
        return view('adverts.edit.attributes', compact('advert'));
    }

    public function attributes(AttributesRequest $request, Advert $advert)
    {
        $this->checkAccess($advert);

        try{
            $this->service->editAttributes($advert->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('adverts.show', $advert);   //????
    }

    public function photosForm(Advert $advert)
    {
        $this->checkAccess($advert);
        return view('adverts.edit.photos', compact('advert'));
    }

    public function photos(PhotoRequest $request, Advert $advert)
    {
        $this->checkAccess($advert);
        try{
            $this->service->addPhotos($advert->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);   //????
    }

    public function moderate(Advert $advert)
    {

        $this->checkAccess($advert);

        try{
            $this->service->moderate($advert->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        flash('Оголошення - '. $advert->title .' - активовано')->success();
        return redirect()->route('admin.adverts.adverts.index', $advert);
    }

    public function send(Advert $advert)
    {

        $this->checkAccess($advert);

        try{
            $this->service->sendToModeration($advert->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        flash('Оголошення - '. $advert->title .' - на модерації')->success();
        return redirect()->route('adverts.show', $advert);
    }

    public function sendToDraft(Advert $advert)
    {

        $this->checkAccess($advert);

        try{
            $this->service->sendToDraft($advert->id);
        }catch (\DomainException $e){

            return back()->with('error', $e->getMessage());
        }
        flash('Оголошення - '. $advert->title .' - присвоєно статус чорновик')->success();
        return redirect()->route('admin.adverts.adverts.index', $advert);
    }

    public function close(Advert $advert)
    {
        $this->checkAccess($advert);
        try{
            $this->service->close($advert->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);
    }

    public function destroy(Advert $advert)
    {

        $this->checkAccess($advert);
        try{
            $this->service->remove($advert->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.adverts.adverts.index', $advert);
    }


    public function checkAccess(Advert $advert): void
    {
        if (!Gate::allows('admin-panel', $advert)){
            abort(403);
        }
    }
    public function createTxt($data){
        $file_name = 'afisha.txt';
        $myfile = fopen(public_path()."/".$file_name, "w");
        foreach ($data as $item){
            $txt = $item->category->getPa()."\n";
            $txt .= "$item->id / Тип $item->type / $item->content.";
            if($item->price){
                $txt .= " Ц. $item->price";
            }if ($item->phone){
                $txt .= " T. $item->phone /\n";
            }else{
                $txt .= "\n";
            }
            fwrite($myfile, $txt);
        }

        fclose($myfile);
    }

    public function createXML($data)
    {
        $title = 'Оголошення';
        $rowCount = $data->count();

        $xmlDOC = new \DOMDocument('0.1', 'UTF-8');
        $root =  $xmlDOC->appendChild($xmlDOC->createElement("user_info"));
        $root->appendChild($xmlDOC->createElement("title", $title));
        $root->appendChild($xmlDOC->createElement("totals_rows", $rowCount));
        $tabUsers = $root->appendChild($xmlDOC->createElement("rows"));

        foreach ($data as $item){
            if (isset($item)){
                $tabUsers->appendChild($xmlDOC->createElement('title', $item['title']));
                $tabUsers->appendChild($xmlDOC->createElement('price', $item['price']));
                $tabUsers->appendChild($xmlDOC->createElement('type', $item['type']));
                $tabUsers->appendChild($xmlDOC->createElement('status', $item['status']));
                $tabUsers->appendChild($xmlDOC->createElement('published_at', $item['published_at']));
                $tabUsers->appendChild($xmlDOC->createElement('expires_at', $item['expires_at']));
            }
        }
        header("Content-Type: text/plain; charset=utf-8");


        $xmlDOC->formatOutput = true;
        $file_name = 'afisha.xml';
        $xmlDOC->save(public_path()."/".$file_name);
    }

    public function update(Request $request)
    {
        $ids = $request->input('update');
        $adverts = Advert::whereIn('id', $ids)->getModels();
        foreach ($adverts as $item){
            $item->moderateAdmin(Carbon::now());
        }
        return redirect()->route('admin.adverts.adverts.index');


    }

    public function fetch(Request $request)
    {
        if($request->get('query')){
            $query = $request->get('query');
            $data = Category::where('name','LIKE','%'. $query . '%')->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach ($data as $row){
                $output .= '<li><a href="#">'.$row->name.'</a> </li>';
            }
            $output .='</ul>';
            echo $output;
        }
    }

    public function moderationAdvert(Request $request)
    {
        $result = \Auth::user()->haveMoney($request->get('price'));
        if ($result){
            \Auth::user()->withdraw($request->get('price'));
            $advert = Advert::where('id', $request->get('advertid'))->first();
            $advert->sendToModeration();

//            SystemBalance::create([
//                'type' => SystemBalance::STATUS_ADVERT,
//                'sum' => $request->get('price'),
//                'user_id' => \Auth::id(),
//            ]);
            Order::create([
                'user_id' => \Auth::id(),
                'sum' =>  $num = -1 * abs($request->get('price')),
                'system'=> Order::STATUS_USERPANEL,
                'action' => Order::STATUS_ADVERT,
                'admin_check'=> 0,
            ]);

        }else{

            return false;
        }
    }

    public function orderBanner(Request $request)
    {
        $result = \Auth::user()->haveMoney($request->get('price'));
        if ($result){
            \Auth::user()->withdraw($request->get('price'));
            $banner = Banner::where('id', $request->get('bannerid'))->first();
            $banner->order();

            SystemBalance::create([
                'type' => SystemBalance::STATUS_BANNER,
                'sum' => $banner->cost,
                'user_id' => \Auth::id(),
            ]);

            Order::create([
                'user_id' => \Auth::id(),
                'sum' => $num = -1 * abs($banner->cost),
                'action' => Order::STATUS_BANNER,
                'system' => Order::STATUS_USERPANEL,
                'admin_сheck' => 0,
            ]);

        }else{
            return false;
        }
    }



}