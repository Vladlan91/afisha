<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 04/11/2018
 * Time: 20:51
 */

namespace App\Http\Controllers\Admin\Adverts;


use App\Entity\Adverts\Advert\Advert;
use App\Entity\Adverts\Category;
use App\Entity\Region;
use App\Http\Controllers\Controller;
use App\Http\Middleware\FilledProfile;
use App\Http\Requests\Adverts\CreateRequest;
use App\UseCase\Adverts\AdvertService;
use App\User;
use Auth;

class CreateController extends Controller
{
    private $service;

    public function __construct(AdvertService $service)
    {
        $this->service = $service;
        $this->middleware( FilledProfile::class);
    }

    public function category(User $user, Category $category = null, Region $region = null)
    {

        if (!empty($category)){
            $categories = Category::where('parent_id', $category->id)->orderBy('name')->get();
            if ($categories->count() < 1){
                $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
                return view('admin.adverts.adverts.create.region', compact('user','category','regions', 'region'));
            }
            if (!empty($categories)){
                return view('admin.adverts.adverts.create.category', compact('user','categories'));
            } else{
                $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
                return view('admin.adverts.adverts.create.region', compact('user','category','regions', 'region'));
            }
        }
        $categories = Category::defaultOrder()->withDepth()->get()->toTree();
        return view('admin.adverts.adverts.create.category', compact('user','categories'));
    }

    public function region(User $user, Category $category, Region $region = null)
    {

        if (!empty($region)){
            $regions = Region::where('parent_id', $region->id)->orderBy('name')->get();
            if ($regions->count() < 1){
                $types = Advert::typeListUser();
                $times = Advert::timeListUser();
                return view('admin.adverts.adverts.create.advert', compact('user','category', 'times','region','types'));
            }
        }

        $regions = Region::where('parent_id', $region ? $region->id : null)->orderBy('name')->get();
        return view('admin.adverts.adverts.create.region', compact('user','category','regions', 'region'));
    }

    public function advert(User $user, Category $category,  Region $region = null)
    {
        $types = Advert::typeListUser();
        if (Auth::user()->isAdmin()){
            $times = Advert::timeListAdmin();
        }else{
            $times = Advert::timeListUser();
        }
        return view('admin.adverts.adverts.create.advert', compact('user','category', 'times','region','types'));
    }

    public function store(CreateRequest $request, User $user, Category $category, Region $region = null)
    {

        try{
            $advert = $this->service->create(
                $user->id,
                $category->id,
                $region ? $region->id : null,
                $request
            );
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('adverts.show', $advert);
    }




}