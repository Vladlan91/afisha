<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 29/10/2018
 * Time: 17:08
 */

namespace App\Http\Controllers\Admin\Adverts;


use App\Entity\Adverts\Attribute;
use App\Entity\Adverts\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AttributeController extends Controller
{

    public function create(Category $category){

        $types = Attribute::typesList();

        return view('admin.adverts.categories.attributes.create', compact('types','category'));
    }

    public function store(Request $request, Category $category)
    {

        $this->validate($request,[
            'name' => 'required|string|max:255',
            'type' => ['required','string','max:255', Rule::in(array_keys(Attribute::typesList()))],
        ]);

        if ((bool)$request['checkbox']){
            $attributes = $category->attributes()->create([
                'name' => $request['name'],
                'type' => $request['type'],
                'required' => (bool)$request['required'],
                'sort' => $request['sort'],
                'checkbox' =>(bool)$request['checkbox'],
            ]);
        }else{
            $attributes = $category->attributes()->create([
                'name' => $request['name'],
                'type' => $request['type'],
                'required' => (bool)$request['required'],
                'variants' => array_map('trim', preg_split('#[\r\n]+#', $request['variants'])),
                'sort' => $request['sort'],
                'checkbox' =>(bool)$request['checkbox'],
            ]);
        }

        return redirect()->route('admin.adverts.categories.attributes.show', [$category, $attributes]);
    }

    public function show(Category $category, Attribute $attribute)
    {
        return view('admin.adverts.categories.attributes.show', compact('category', 'attribute'));
    }

    public function edit(Category $category, Attribute $attribute)
    {
        $types = Attribute::typesList();

        $result = json_encode($attribute->variants, JSON_UNESCAPED_UNICODE);
        $result = str_replace('"', '', $result);
        $result = str_replace(',', "\n", $result);
        $result = str_replace('[', '', $result);
        $result = str_replace(']', '', $result);

        return view('admin.adverts.categories.attributes.edit', compact('attribute','category','types','result'));
    }

    public function update(Request $request, Category $category, Attribute $attribute)
    {

        $this->validate($request,[
            'name' => 'required|string|max:255',
            'type' => ['required','string','max:255', Rule::in(array_keys(Attribute::typesList()))],
            'required' => 'nullable|string|max:255',
            'variants' => 'nullable|string',
            'sort' => 'required|integer',
        ]);

        $category->attributes()->findOrFail($attribute->id)->update([
            'name' => $request['name'],
            'type' => $request['type'],
            'required' => (bool)$request['required'],
            'variants' => array_map('trim', preg_split('#[\r\n]+#', $request['variants'])),
            'sort' => $request['sort'],
        ]);

        return redirect()->route('admin.adverts.categories.attributes.show', [$category, $attribute]);
    }

    public function destroy(Category $category, Attribute $attribute){

        $category->attributes()->findOrFail($attribute->id)->delete();

        return redirect()->route('admin.adverts.categories.index');
    }

}