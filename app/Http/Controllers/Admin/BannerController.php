<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 27/11/2018
 * Time: 11:39
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Banner\FileRequest;
use Illuminate\Http\Request;
use App\Entity\Banner\Banner;
use App\UseCase\Banners\BannerService;

class BannerController extends Controller
{

    private $service;

    public function __construct(BannerService $service)
    {
        $this->service = $service;
        $this->middleware('can:manage-banners');
    }

    public function index(Request $request)
    {
        $query = Banner::orderByDesc('updated_at');

        if (!empty($value = $request->get('id'))){
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('user'))){
            $query->where('user_id', $value);
        }
        if (!empty($value = $request->get('region'))){
            $query->where('region_id', $value);
        }
        if (!empty($value = $request->get('category'))){
            $query->where('category_id', $value);
        }
        if (!empty($value = $request->get('status'))){
            $query->where('status', $value);
        }

        $banners = $query->paginate(20);

        $statuses = Banner::statusesList();

        return view('admin.banners.index', compact('banners', 'statuses'));

    }

    public function show(Banner $banner)
    {
        return view('admin.banners.show', compact('banner'));
    }

    public function editForm(Banner $banner)
    {
        return view('admin.banners.edit', compact('banner'));
    }

    public function edit(EditRequest $request, Banner $banner)
    {
        try{
            $this->service->editByAdmin($banner->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.banners.show', $banner);
    }

    public function fileForm(Banner $banner)
    {
        return view('admin.banners.edit', compact('banner'));
    }

    public function file(FileRequest $request, Banner $banner)
    {
        try{
            $this->service->changeFileAdmin($banner->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.banners.show', $banner);
    }

    public function send(Banner $banner)
    {
        try{
            $this->service->sendToModeration($banner->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.banners.show', $banner);
    }

//    public function moderate(Banner $banner)
//    {
//        try{
//            $this->service->moderate($banner->id);
//        }catch (\DomainException $e){
//            return back()->with('error', $e->getMessage());
//        }
//        return redirect()->route('admin.banners.show', $banner);
//    }

    public function moderateAdmin(Banner $banner)
    {
        try{
            $this->service->moderate($banner->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.banners.show', $banner);
    }

    public function order(Banner $banner)
    {
        try{
            $banner = $this->service->order($banner->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.banners.show', $banner);
    }

    public function orderAdmin(Banner $banner)
    {
        try{
            $banner = $this->service->orderAdmin($banner->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.banners.show', $banner);
    }

    public function rejectForm(Banner $banner)
    {
        return view('admin.banners.reject', compact('banner'));
    }

    public function reject(RejectRequest $request, Banner $banner)
    {
        try{
            $this->service->reject($banner->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.banners.show', $banner);
    }

    public function pay(Banner $banner)
    {
        try{
            $this->service->pay($banner->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.banners.show', $banner);
    }

    public function destroy(Banner $banner)
    {
        try{
            $this->service->removeByAdmin($banner->id);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.banners.index', $banner);
    }

}