<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30/12/2018
 * Time: 18:51
 */

namespace App\Http\Controllers\Admin;


use App\Entity\NewsCat;
use App\Entity\Page;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NewscatController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:manage-pages');
    }

    public function index(){
        $newscat = NewsCat::all();
        return view('admin.newscat.index', compact('newscat'));
    }

    public function create(){

        return view('admin.newscat.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
        ]);

        $newscat = NewsCat::create([
            'title' => $request['title'],
        ]);

        return redirect()->route('admin.newscat.show', $newscat);
    }

    public function show(Newscat $newscat)
    {

        return view('admin.newscat.show', compact('newscat'));
    }

    public function edit(Newscat $newscat)
    {
        return view('admin.newscat.edit', compact('parents','newscat'));
    }

    public function update(Request $request, Newscat $newscat)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',

        ]);


        $newscat->update([
            'title' => $request['title'],

        ]);

        return redirect()->route('admin.newscat.show', $newscat);
    }

    public function destroy(Newscat $newscat){

        $newscat->delete();

        return redirect()->route('admin.newscat.index');
    }

}