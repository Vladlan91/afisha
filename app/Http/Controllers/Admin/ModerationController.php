<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 02/12/2018
 * Time: 19:52
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

class ModerationController extends Controller
{
    public function index()
    {
        return view('admin.moderate.index');
    }

}