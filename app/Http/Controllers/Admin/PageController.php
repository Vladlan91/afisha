<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 01/12/2018
 * Time: 15:03
 */

namespace App\Http\Controllers\Admin;


use App\Entity\Page;
use App\Http\Requests\PageRequests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:manage-pages');
    }

    public function index(){
        $pages = Page::defaultOrder()->withDepth()->get();

        return view('admin.pages.index', compact('pages'));
    }

    public function create(){

        $parents = Page::defaultOrder()->withDepth()->get();

        return view('admin.pages.create', compact('parents'));
    }

    public function store(PageRequests $request)
    {

        $page = Page::create([
            'title' => $request['title'],
            'slug' => str_slug($request['title']),
            'parent_id' => $request['parent'],
            'menu_title' => $request['menu_title'],
            'description' => $request['description'],
            'content' => $request['content'],
        ]);

        return redirect()->route('admin.pages.show', $page);
    }

    public function show(Page $page)
    {

        return view('admin.pages.show', compact('page'));
    }

    public function edit(Page $page)
    {
        $parents = Page::defaultOrder()->withDepth()->get();

        return view('admin.pages.edit', compact('parents','page'));
    }

    public function update(PageRequests $request, Page $page)
    {

        $page->update([
            'title' => $request['title'],
            'slug' =>  $request['slug'],
            'parent_id' => $request['parent'],
            'menu_title' => $request['menu_title'],
            'description' => $request['description'],
            'content' => $request['content'],

        ]);

        return redirect()->route('admin.pages.show', $page);
    }

    public function destroy(Page $page){

        $page->delete();

        return redirect()->route('admin.pages.index');
    }

    public function first(Page $page){

        if($first = $page->siblings()->defaultOrder()->first()){
            $page->insertBeforeNode($first);
        }
        return redirect()->route('admin.pages.index');

    }

    public function up(Page $page){

        $page->up();

        return redirect()->route('admin.pages.index');
    }

    public function down(Page $page){

        $page->down();

        return redirect()->route('admin.pages.index');
    }

    public function last(Page $page){

        if($first = $page->siblings()->defaultOrder('desc')->first()){
            $page->insertAfterNode($first);
        }
        return redirect()->route('admin.pages.index');

    }
}
