<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Banner\Afisha;

class AfishaController extends Controller
{

    public function index(Request $request)
    {
        $query = Afisha::orderByDesc('id');

        if (!empty($value = $request->get('id'))){
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('user'))){
            $query->where('user_id', $value);
        }
        if (!empty($value = $request->get('region'))){
            $query->where('region_id', $value);
        }
        if (!empty($value = $request->get('category'))){
            $query->where('category_id', $value);
        }
        if (!empty($value = $request->get('status'))){
            $query->where('status', $value);
        }

        $banners = $query->paginate(20);
        $statuses = Afisha::statusesList();

        return view('admin.afisha.index', compact('banners', 'statuses'));
    }

    public function create()
    {
        $statuses = Afisha::statusesList();
        return view('admin.afisha.create', compact('statuses'));
    }

    public function store(Request $request)
    {

        $afisha = Afisha::create([
            'name' => $request['title'],
            'file' =>  $request['file']->store('banners', 'local'),
            'url' => str_slug($request['url']),
            'status' => $request['status'],

        ]);

        return redirect()->route('admin.afisha.index');
    }

    public function edit(Afisha $afisha)
    {
        $afisha->changesStatus();
        return redirect()->route('admin.afisha.index');
    }

    public function update(Afisha $afisha)
    {
        $afisha->changesStatus();
        return redirect()->route('admin.afisha.index');
    }

    public function destroy()
    {
        return redirect()->route('admin.users.index');
    }
}
