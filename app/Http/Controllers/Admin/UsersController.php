<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Adverts\Advert\Advert;
use App\Entity\Order;
use App\Http\Requests\Admin\Users\CreateRequst;
use App\Http\Requests\Admin\Users\UpdateRequst;
use App\Http\Requests\MoneyRequest;
use App\UseCase\Auth\RegisterService;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    public $service;
    public function __construct( RegisterService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $query = User::orderByDesc('id');

        if (!empty($value = $request->get('id'))){
            $query->where('id', 'like', '%'. $value .'%');
        }

        if (!empty($value = $request->get('email'))){
            $query->where('email', 'like', '%'. $value .'%');
        }
        if (!empty($value = $request->get('phone'))){
            $query->where('phone', 'like', '%'. $value .'%');
        }

        if (!empty($value = $request->get('name'))){
            $query->where('name', 'like', '%'. $value .'%');
        }

        $users = $query->paginate(20);

        $statuses = [
            User::STATUS_ACTIV => 'activ',
            User::STATUS_WAITE => 'waite',
        ];


        return view('admin.users.index', compact('users','statuses'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(CreateRequst $request)
    {

        $user = User::create($request->only(['name', 'email']) + [
            'status' => User::STATUS_ACTIV,
            'role' => User::ROLE_USER,
            'password' => bcrypt(Str::random()),
        ]);

        return redirect()->route('admin.users.show', $user);
    }

    public function show(User $user)
    {
        $query = Advert::forUser($user);

        $adverts = $query->paginate(20);

        return view('admin.users.show', compact('user','adverts'));
    }

    public function addMoneyForm(User $user){
        $type = Order::actionList();
        return view('admin.users.addmoney',compact('user','type'));
    }

    public function getMoneyForm(User $user){
        $type = Order::actionList();
        return view('admin.users.getmoney', compact('user','type'));
    }

    public function getMoney(MoneyRequest $request, User $user){
        try{
            if ($user->haveMoney($request['balance'])){
                $user->balance -= $request['balance'];
                $user->saveOrFail();
                Order::create([
                    'user_id' => $user->id,
                    'sum' => -1 * $request['balance'],
                    'action' => $request['action'],
                    'system' => 'adminpanel',
                    'admin_сheck' => User::ROLE_ADMIN,
                ]);
            }else{
                throw new \DomainException('У користувача відсутні кошти на балансі');
            }

        }catch (\DomainException $e){
            flash('У користувача не достатньо коштів для списання! ')->success();
            return back();
        }

        return redirect()->route('admin.user.balance.get', $user);
    }

    public function addMoney(MoneyRequest $request, User $user){

        $user->balance += $request['balance'];
        $user->saveOrFail();
        Order::create([
                'user_id' => $user->id,
                'sum' => $request['balance'],
                'action' => $request['action'],
                'system' => 'adminpanel',
                'admin_сheck' => User::ROLE_ADMIN,
            ]);

        return redirect()->route('admin.user.balance.add', $user);
    }

    public function order(User $user, Request $request)
    {
        $query = Order::forUser($user)->orderByDesc('id');

        if (!empty($value = $request->get('id'))){
            $query->where('id', 'like', '%'. $value .'%');
        }

        if (!empty($value = $request->get('type'))){
            $query->where('system', '=',$value );
        }

        if (!empty($value = $request->get('action'))){
            $query->where('action', $value );
        }
        if (!empty($request->get('trip-start'))){
            $value = Carbon::make($request->get('trip-start'));
            $query->where('created_at','>=', $value->toDateString().' 00:00:00');
        }
        if (!empty($request->get('trip-finish'))){
            $value = Carbon::make($request->get('trip-finish'));
            $query->where('created_at','<=', $value->toDateString().' 00:00:00');
        }


        $order = $query->paginate(20);
        $types = Order::statusList();
        $actionList = Order::actionList();
        return view('admin.users.order', compact('user','order', 'types', 'actionList'));
    }

    public function edit(User $user)
    {
        $statuses = [
            User::STATUS_ACTIV => 'activ',
            User::STATUS_WAITE => 'waite',
        ];

        $roles = [
            User::ROLE_USER => 'user',
            User::ROLE_ADMIN => 'admin',
        ];
        return view('admin.users.edit', compact('user','statuses','roles'));
    }

    public function update(UpdateRequst $request, User $user)
    {
        $user->update($request->only(['name', 'email', 'status' , 'role']));

        return redirect()->route('admin.users.show', $user);
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('admin.users.index');
    }

    public function verify(User $user){
        $this->service->verify($user->id);
        return redirect()->route('admin.users.show', $user);
    }
}
