<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 15/12/2018
 * Time: 10:36
 */

namespace App\Http\Controllers\Admin;

use App\Entity\Adverts\Advert\Advert;
use App\Entity\Adverts\Category;
use App\Entity\Business\Business;
use App\Entity\Business\Company;
use App\Entity\Region;
use App\Entity\Suggestions\Suggestions;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

class CompanyController extends Controller
{
    public function index(Request $request)
    {
        $query = Company::orderByDesc('created_at');

        if (!empty($value = $request->get('id'))){
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('title'))){
            $query->where('title', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('user'))){
            $query->where('user_id', $value);
        }
        if (!empty($value = $request->get('region'))){
            $query->where('region_id', $value);
        }
        if (!empty($value = $request->get('category'))){
            $query->where('category_id', $value);
        }
        $companies = $query->paginate(10);

        return view('admin.company.index', compact('companies'));
    }
    public function create(Business $business){

        $regions = Region::roots()->orderBy('name')->getModels();
        $category = Business::defaultOrder()->withDepth()->get();

        return view('admin.business.company.create', compact('category','business', 'regions'));
    }

    public function store(Request $request, Business $business)
    {

        $this->validate($request,[
            'title' => 'required|string|max:255',
            'category_id' => 'nullable|integer|exists:business,id',
            'region_id' => 'nullable|integer|exists:regions,id',
        ]);

        $company = Company::create([
            'title' => $request['title'],
            'category_id' => $request['category_id'],
            'region_id' => $request['region_id'],
            'logo' => $request['logo']->store('companies', 'local'),
            'location' => $request['location'],
            'phone' => $request['phone'],
            'site' => $request['site'],
            'email' => $request['email'],
            'status' => Company::STATUS_ACTIVE,
            'slug' => str_slug($request['title']),
            'user_id' => Auth::id(),
            'address' => $request['address'],
            'content' => $request['content'],
        ]);

        return redirect()->route('admin.business.company.show', [$business, $company]);
    }

    public function show(Business $business, Company $company)
    {
        $adverts = Advert::active()->forCompany($company)->with(['category', 'region'])->orderByDesc('published_at')->get();
        $suggestions = Suggestions::where('company_id', $company->id )->getModels();
        return view('admin.business.company.show', compact('business', 'company','suggestions','adverts'));
    }

    public function edit(Business $business, Company $company)
    {

        $regions = Region::roots()->orderBy('name')->getModels();
        $category = Business::defaultOrder()->withDepth()->get();

        return view('admin.business.company.edit', compact('company', 'business', 'regions', 'category'));
    }

    public function update(Request $request, Business $business, Company $company)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',
            'category_id' => 'nullable|integer|exists:business,id',
            'region_id' => 'nullable|integer|exists:regions,id',
        ]);
        $oldAvatar = $company->logo;
        $oldUser = $company->user_id;
        $company->update([
            'title' => $request['title'],
            'category_id' => $request['category_id'],
            'region_id' => $request['region_id'],
            'location' => $request['location'],
            'phone' => $request['phone'],
            'site' => $request['site'],
            'email' => $request['email'],
            'status' => Company::STATUS_ACTIVE,
            'slug' => str_slug($request['title']),
            'user_id' => $oldUser,
            'address' => $request['address'],
            'content' => $request['content'],
        ]);
        if ($request['logo']){
            $company->logo = $request['logo']->store('companies', 'local');
            if ($oldAvatar != null){
                unlink(storage_path(). '/app/public/'. $oldAvatar);
            }
            $company->saveOrFail();
        }

        return redirect()->route('admin.business.company.show', [$business, $company]);
    }

    public function send(Business $business, Company $company)
    {
        $company->moderate(Carbon::now());

        return redirect()->route('admin.business.company.show', [$business, $company]);
    }

    public function destroy(Business $business, Company $company){

        if ($company->logo != null){
            unlink(public_path(). '/app/'. $company->logo);
        }
        $company->findOrFail($company->id)->delete();
        $company->suggestion()->delete();

        return redirect()->route('admin.business.show', $business);

    }

}