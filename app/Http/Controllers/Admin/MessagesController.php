<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 15/01/2019
 * Time: 14:59
 */

namespace App\Http\Controllers\Admin;

use App\Entity\Message;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    public function index( Request $request)
    {
        $query = Message::orderByDesc('created_at');

        if (!empty($value = $request->get('name'))){
            $query->where('name', 'like', '%' . $value . '%');
        }

        $messages = $query->paginate(20);

        return view('admin.messages.index', compact('messages'));
    }

    public function show(Message $message)
    {
        $message->view();

        return view('admin.messages.show', compact('message'));
    }

    public function destroy(Message $message){

        $message->delete();

        return redirect()->route('admin.messages.index');
    }

}