<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Adverts\Category;
use App\Entity\NewCategory;
use App\Entity\NewOrder;
use App\Entity\NewsCat;
use App\Entity\NewNews;
use App\Entity\News;
use App\Entity\NewUser;
use App\Entity\Order;
use App\Entity\SystemBalance;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    public  $day = null;

    public function index()
    {
        $userAdmin = User::orderBy('id')->where('role','=','admin')->sum('balance');
        $userUser = User::orderBy('id')->where('role','=','user')->sum('balance');
        $order = Order::orderBy('id')->where('created_at', 'like', '%'.Carbon::today()->toDateString().'%')->getModels();
        $systemBalanceAdverts = SystemBalance::orderBy('id')->where('type','adverts')->where('created_at', 'like', '%'.Carbon::today()->toDateString().'%')->sum('sum');
        $systemBalanceBanner = SystemBalance::orderBy('id')->where('type','banner')->where('created_at', 'like', '%'.Carbon::today()->toDateString().'%')->sum('sum');
        $systemBalancePackage = SystemBalance::orderBy('id')->where('type','package')->where('created_at', 'like', '%'.Carbon::today()->toDateString().'%')->sum('sum');
        $allSum = null;
        $sum = null;
        $sumAdvert = null;
        $sumPackage = null;
        $sumBanner = null;
        $writtenOff = null;
        $writtenOffUser = null;
        $writtenOffAdmin = null;
        $allSumUser = null;
        $allSumAdmin = null;
        foreach ($order as $item){
            if ($item->sum < 1 ){
                if ($item->action == 'adverts')
                {
                    $sumAdvert += +1 * abs($item->sum);
                }elseif($item->action == 'banner')
                {
                    $sumBanner += +1 * abs($item->sum);
                }else
                {
                    $sumPackage += +1 * abs($item->sum);
                }
                $allSum += $item->sum;
                if ($item->system == 'EasyPay'){
                    $allSumUser += $item->sum;
                }elseif($item->system =='adminpanel'){
                    $allSumAdmin += $item->sum;
                }
            }
            if ($item->sum < 0 ){
                $writtenOff += $item->sum;
                if ($item->system == 'userpanel'){
                    $writtenOffUser += $item->sum;
                }elseif($item->system =='adminpanel'){
                    $writtenOffAdmin += $item->sum;
                }
            }
            $sum +=$item->sum;
        }

        return view('admin.home', compact('systemBalanceAdverts','systemBalanceBanner','allSum','allSumAdmin','allSumUser','writtenOff','writtenOffUser','writtenOffAdmin','sumAdvert','sumBanner','sumPackage','userAdmin','userUser'));
    }
    public function indexWeek()
    {
        $userAdmin = User::orderBy('id')->where('role','=','admin')->sum('balance');
        $userUser = User::orderBy('id')->where('role','=','user')->sum('balance');
        $order = Order::orderBy('id')->where('created_at', '>', Carbon::today()->subWeek()->toDateString())->getModels();
        $systemBalanceAdverts = SystemBalance::orderBy('id')->where('type','adverts')->where('created_at', '>', Carbon::today()->subWeek()->toDateString())->sum('sum');
        $systemBalanceBanner = SystemBalance::orderBy('id')->where('type','banner')->where('created_at', '>', Carbon::today()->subWeek()->toDateString())->sum('sum');
        $systemBalancePackage = SystemBalance::orderBy('id')->where('type','package')->where('created_at', '>', Carbon::today()->subWeek()->toDateString())->sum('sum');
        $allSum = null;
        $sum = null;
        $sumAdvert = null;
        $sumPackage = null;
        $sumBanner = null;
        $writtenOff = null;
        $writtenOffUser = null;
        $writtenOffAdmin = null;
        $allSumUser = null;
        $allSumAdmin = null;
        foreach ($order as $item){
            if ($item->sum > 1 ){
                if ($item->action == 'adverts')
                {
                    $sumAdvert += $item->sum;
                }elseif($item->action == 'banner')
                {
                    $sumBanner += $item->sum;
                }else
                {
                    $sumPackage += $item->sum;
                }
                $allSum += $item->sum;
                if ($item->system == 'EasyPay'){
                    $allSumUser += $item->sum;
                }elseif($item->system =='adminpanel'){
                    $allSumAdmin += $item->sum;
                }
            }
            if ($item->sum < 0 ){
                $writtenOff += $item->sum;
                if ($item->system == 'userpanel'){
                    $writtenOffUser += $item->sum;
                }elseif($item->system =='adminpanel'){
                    $writtenOffAdmin += $item->sum;
                }
            }
            $sum +=$item->sum;
        }
        return view('admin.indexWeek', compact('systemBalanceAdverts','systemBalanceBanner','allSum','allSumAdmin','allSumUser','writtenOff','writtenOffUser','writtenOffAdmin','sumAdvert','sumBanner','sumPackage','userAdmin','userUser'));
    }

    public function indexMonth()
    {
        $userAdmin = User::orderBy('id')->where('role','=','admin')->sum('balance');
        $userUser = User::orderBy('id')->where('role','=','user')->sum('balance');
        $order = Order::orderBy('id')->where('created_at', '>', Carbon::today()->subMonth()->toDateString())->getModels();
        $systemBalanceAdverts = SystemBalance::orderBy('id')->where('type','adverts')->where('created_at', '>', Carbon::today()->subMonth()->toDateString())->sum('sum');
        $systemBalanceBanner = SystemBalance::orderBy('id')->where('type','banner')->where('created_at', '>', Carbon::today()->subMonth()->toDateString())->sum('sum');
        $systemBalancePackage = SystemBalance::orderBy('id')->where('type','package')->where('created_at', '>', Carbon::today()->subMonth()->toDateString())->sum('sum');
        $allSum = null;
        $sum = null;
        $sumAdvert = null;
        $sumPackage = null;
        $sumBanner = null;
        $writtenOff = null;
        $writtenOffUser = null;
        $writtenOffAdmin = null;
        $allSumUser = null;
        $allSumAdmin = null;
        foreach ($order as $item){
            if ($item->sum > 1 ){
                if ($item->action == 'adverts')
                {
                    $sumAdvert += $item->sum;
                }elseif($item->action == 'banner')
                {
                    $sumBanner += $item->sum;
                }else
                {
                    $sumPackage += $item->sum;
                }
                $allSum += $item->sum;
                if ($item->system == 'EasyPay'){
                    $allSumUser += $item->sum;
                }elseif($item->system =='adminpanel'){
                    $allSumAdmin += $item->sum;
                }
            }
            if ($item->sum < 0 ){
                $writtenOff += $item->sum;
                if ($item->system == 'userpanel'){
                    $writtenOffUser += $item->sum;
                }elseif($item->system =='adminpanel'){
                    $writtenOffAdmin += $item->sum;
                }
            }
            $sum +=$item->sum;
        }
        return view('admin.indexMonth', compact('systemBalanceAdverts','systemBalanceBanner','allSum','allSumAdmin','allSumUser','writtenOff','writtenOffUser','writtenOffAdmin','sumAdvert','sumBanner','sumPackage','userAdmin','userUser'));
    }

    public function deleteCategory(){
        DB::table('advert_categories')->delete();
        DB::table('news_category')->delete();
        return redirect()->route('admin.home');
    }

    public function removeCategory(){
        $сategoryNews = NewCategory::orderBy('id')->where('parentid','=','469')->getModels();
        $newCategory = NewCategory::orderBy('id')->where('id','<','468')->getModels();
        foreach ($newCategory as $item) {
            $category = new Category();
            $category->create([
                'id' => $item->id,
                'name' => $item->name,
                'description' => $item->descr,
                'slug' => $item->alt_name,
            ]);
        }

        foreach ($сategoryNews as $item) {
            $news_category = new NewsCat();
            $news_category->create([
                'id' => $item->id,
                'title' => $item->name,
            ]);
        }

        foreach ($newCategory as $item) {
            $cate = Category::where('id', $item->id)->getModels();
            foreach ($cate as $cat)
                if ($cat->id == $item->id) {
                    if($item->parentid == 6){
                        $cat->update([
                            'parent_id' => null
                        ]);
                    }else{
                        $cat->update([
                            'parent_id' => $item->parentid
                        ]);
                    }
                }
        }
        DB::table('advert_categories')->where('id','<','7')->delete();

        return redirect()->route('admin.home');
    }

    public function removeOrder()
    {
        $transaction = NewOrder::orderByDesc('guest')->getModels();

        foreach ($transaction as $item){
            $order = new Order();
            $order->create([
                'id' => $item->id,
                'user_id' => $item->guest,
                'sum'=> $item->sum,
                'system'=> $item->system?:'error',
                'payment_id'=> $item->payment_id?:null,
                'action'=> 'adverts',
                'created_at'=> $item->created,
                'admin_check'=> 1
            ]);
        }
        return redirect()->route('admin.home');
    }

    public function deleteOrder()
    {
        DB::table('orders')->delete();

        return redirect()->route('admin.home');
    }

    public function deleteNews()
    {
        DB::table('news')->delete();

        return redirect()->route('admin.home');
    }

    public function removeNews(){
//        $сategoryNews = NewCategory::orderBy('id')->where('parentid','=','469')->getModels();
//        foreach ($сategoryNews as $item) {
//            $news_category = new NewsCat();
//            $news_category->create([
//                'id' => $item->id,
//                'title' => $item->name,
//            ]);
//        }
        $newNews = NewNews::orderBy('id')->whereIn('category', [844, 845, 846, 847, 848, 849, 850,])->getModels();
        foreach ($newNews as $item) {
            $news = new News();
            $news->create([
                'id' => $item->id,
                'title' => $item->title,
                'category_id' => $item->category,
                'slug' => $item->alt_name,
                'avatar' => 'asdasd',
                'view' => 0,
                'content' => $item->full_story,
                'description' => $item->descr

            ]);
        }

//        $newNews = NewNews::orderBy('id')->where('category','=','844')->getModels();
//        foreach ($newNews as $item) {
//            $news = new News();
//            $news->create([
//                'id' => $item->id,
//                'title' => $item->title,
//                'category_id' => $item->category,
//                'slug' => $item->alt_name,
//                'avatar' => 'asdasd',
//                'view' => 0,
//                'content' => $item->full_story,
//                'description' => $item->descr
//
//            ]);
//        }
//        $newNews = NewNews::orderBy('id')->where('category','=','845')->getModels();
//        foreach ($newNews as $item) {
//            $news = new News();
//            $news->create([
//                'id' => $item->id,
//                'title' => $item->title,
//                'category_id' => $item->category,
//                'slug' => $item->alt_name,
//                'avatar' => 'asdasd',
//                'view' => 0,
//                'content' => $item->full_story,
//                'description' => $item->descr
//
//            ]);
//        }
//        $newNews = NewNews::orderBy('id')->where('category','=','846')->getModels();
//        foreach ($newNews as $item) {
//            $news = new News();
//            $news->create([
//                'id' => $item->id,
//                'title' => $item->title,
//                'category_id' => $item->category,
//                'slug' => $item->alt_name,
//                'avatar' => 'asdasd',
//                'view' => 0,
//                'content' => $item->full_story,
//                'description' => $item->descr
//
//            ]);
//        }
//        $newNews = NewNews::orderBy('id')->where('category','=','847')->getModels();
//        foreach ($newNews as $item) {
//            $news = new News();
//            $news->create([
//                'id' => $item->id,
//                'title' => $item->title,
//                'category_id' => $item->category,
//                'slug' => $item->alt_name,
//                'avatar' => 'asdasd',
//                'view' => 0,
//                'content' => $item->full_story,
//                'description' => $item->descr
//
//            ]);
//        }
//        $newNews = NewNews::orderBy('id')->where('category','=','848')->getModels();
//        foreach ($newNews as $item) {
//            $news = new News();
//            $news->create([
//                'id' => $item->id,
//                'title' => $item->title,
//                'category_id' => $item->category,
//                'slug' => $item->alt_name,
//                'avatar' => 'asdasd',
//                'view' => 0,
//                'content' => $item->full_story,
//                'description' => $item->descr
//
//            ]);
//        }
//        $newNews = NewNews::orderBy('id')->where('category','=','849')->getModels();
//        foreach ($newNews as $item) {
//            $news = new News();
//            $news->create([
//                'id' => $item->id,
//                'title' => $item->title,
//                'category_id' => $item->category,
//                'slug' => $item->alt_name,
//                'avatar' => 'asdasd',
//                'view' => 0,
//                'content' => $item->full_story,
//                'description' => $item->descr
//
//            ]);
//        }
//        $newNews = NewNews::orderBy('id')->where('category','=','850')->getModels();
//        foreach ($newNews as $item) {
//            $news = new News();
//            $news->create([
//                'id' => $item->id,
//                'title' => $item->title,
//                'category_id' => $item->category,
//                'slug' => $item->alt_name,
//                'avatar' => 'asdasd',
//                'view' => 0,
//                'content' => $item->full_story,
//                'description' => $item->descr
//
//            ]);
//        }

        return redirect()->route('admin.home');
    }

    public function deleteUser()
    {
        User::where('id','!=', 20000)->delete();

        return redirect()->route('admin.home');
    }

    public function removeUser()
    {
//        $newUser = NewUser::orderBy('user_id')->where('user_id','<','2000')->getModels();
//        foreach ($newUser as $users){
//            $user = new User();
//            $user->create([
//                'id' => $users->user_id,
//                'name' => $users->fullname?:$users->name,
//                'email' => $users->email,
//                'password' => $users->password,
//                'updated_at' => '2019-02-10 15:36:35',
//                'created_at' => '2019-02-10 15:36:35',
//                'role' => $users->user_group == 4 ? 'user' : 'admin',
//                'status' => 'activ',
//                'balance'=> $users->balance,
//                'phone'=> $this->format_phone($users->tel),
//                'address'=> $users->address?:null
//            ]);
//        }
//
//        $newUser = NewUser::orderBy('user_id')->where('user_id','>','1999')->where('user_id','<','4000')->getModels();
//        foreach ($newUser as $users){
//            $user = new User();
//            $user->create([
//                'id' => $users->user_id,
//                'name' => $users->fullname?:$users->name,
//                'email' => $users->email,
//                'password' => $users->password,
//                'updated_at' => '2019-02-10 15:36:35',
//                'created_at' => '2019-02-10 15:36:35',
//                'role' => $users->user_group == 4 ? 'user' : 'admin',
//                'status' => 'activ',
//                'balance'=> $users->balance,
//                'phone'=> $this->format_phone($users->tel),
//                'address'=> $users->address?:null
//            ]);
//        }
//
//        $newUser = NewUser::orderBy('user_id')->where('user_id','>','3999')->where('user_id','<','8000')->getModels();
//        foreach ($newUser as $users){
//            $user = new User();
//            $user->create([
//                'id' => $users->user_id,
//                'name' => $users->fullname?:$users->name,
//                'email' => $users->email,
//                'password' => $users->password,
//                'updated_at' => '2019-02-10 15:36:35',
//                'created_at' => '2019-02-10 15:36:35',
//                'role' => $users->user_group == 4 ? 'user' : 'admin',
//                'status' => 'activ',
//                'balance'=> $users->balance,
//                'phone'=> $this->format_phone($users->tel),
//                'address'=> $users->address?:null
//            ]);
//        }
//
//        $newUser = NewUser::orderBy('user_id')->where('user_id','>','7999')->where('user_id','<','12000')->getModels();
//        foreach ($newUser as $users){
//            $user = new User();
//            $user->create([
//                'id' => $users->user_id,
//                'name' => $users->fullname?:$users->name,
//                'email' => $users->email,
//                'password' => $users->password,
//                'updated_at' => '2019-02-10 15:36:35',
//                'created_at' => '2019-02-10 15:36:35',
//                'role' => $users->user_group == 4 ? 'user' : 'admin',
//                'status' => 'activ',
//                'balance'=> $users->balance,
//                'phone'=> $this->format_phone($users->tel),
//                'address'=> $users->address?:null
//            ]);
//        }

        $newUser = NewUser::orderBy('user_id')->where('user_id','>','11859')->getModels();
        foreach ($newUser as $users){
            $user = new User();
            $user->create([
                'id' => $users->user_id,
                'name' => $users->fullname?:$users->name,
                'email' => $users->email,
                'password' => $users->password,
                'updated_at' => '2019-02-10 15:36:35',
                'created_at' => '2019-02-10 15:36:35',
                'role' => $users->user_group == 4 ? 'user' : 'admin',
                'status' => 'activ',
                'balance'=> $users->balance,
                'phone'=> $this->format_phone($users->tel),
                'address'=> $users->address?:null
            ]);
        }
        return redirect()->route('admin.home');
    }

    function format_phone($phones = '')
    {
        $count= str_replace([' ', '-'], '', $phones);
        $rest = substr($phones, 0,1);
        $null = substr($phones, 0,2);
        if($rest === '0' && grapheme_strlen($count) == 10 && $null != '00') {
            $phone = preg_replace('/[^0-9]/', '', $phones); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 0);
            $phone_number['code'] = substr($phone, 0, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+38' . $phone_number['dialcode'] . ' (' . $phone_number['code'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone;
        }
        if($rest === '(' && grapheme_strlen($count) == 12 ) {
            $phone = preg_replace('/[^0-9]/', '', $phones); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 0);
            $phone_number['code'] = substr($phone, 0, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+38' . $phone_number['dialcode'] . ' (' . $phone_number['code'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone;
        }
        if($rest === '+' && grapheme_strlen($count) == 13) {
            $phone = preg_replace('/[^0-9]/', '', $phones); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 2);
            $phone_number['code'] = substr($phone, 2, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+' . $phone_number['dialcode'] . ' (' . $phone_number['code'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone ;
        }
        if($rest === '3' && grapheme_strlen($count) == 12) {
            $phone = preg_replace('/[^0-9]/', '', $phones); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 2);
            $phone_number['code'] = substr($phone, 2, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+' . $phone_number['dialcode'] . ' (' . $phone_number['code'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone ;
        }
        if($rest === '5' && grapheme_strlen($count) == 9) {
            $phone = preg_replace('/[^0-9]/', '', $phones); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 2);
            $phone_number['code'] = substr($phone, 2, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+38 (0' . $phone_number['dialcode'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone ;
        }
        if($rest === '4' && grapheme_strlen($count) == 9) {
            $phone = preg_replace('/[^0-9]/', '', $phones); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 2);
            $phone_number['code'] = substr($phone, 2, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+38 (0' . $phone_number['dialcode'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone ;
        }
        if($rest === '6' && grapheme_strlen($count) == 9) {
            $phone = preg_replace('/[^0-9]/', '', $phones); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 2);
            $phone_number['code'] = substr($phone, 2, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+38 (0' . $phone_number['dialcode'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone ;
        }
        if($rest === '7' && grapheme_strlen($count) == 9) {
            $phone = preg_replace('/[^0-9]/', '', $phones); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 2);
            $phone_number['code'] = substr($phone, 2, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+38 (0' . $phone_number['dialcode'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone ;
        }
        if($rest === '8' && grapheme_strlen($count) == 9) {
            $phone = preg_replace('/[^0-9]/', '', $phones); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 2);
            $phone_number['code'] = substr($phone, 2, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+38 (0' . $phone_number['dialcode'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone ;
        }
        if($rest === '9' && grapheme_strlen($count) == 9) {
            $phone = preg_replace('/[^0-9]/', '', $phones); // вернет 79851111111
            $phone_number['dialcode'] = substr($phone, 0, 2);
            $phone_number['code'] = substr($phone, 2, 3);
            $phone_number['phone'] = substr($phone, -7);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 0, 3);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 3, 2);
            $phone_number['phone_arr'][] = substr($phone_number['phone'], 5, 2);

            $format_phone = '+38 (0' . $phone_number['dialcode'] . ') ' . implode('-', $phone_number['phone_arr']);

            return $format_phone ;
        }
        return null ;

    }

}
