<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 03/12/2018
 * Time: 14:00
 */

namespace App\Http\Controllers\Admin;


use App\Entity\Ticket\Status;
use App\Entity\Ticket\Ticket;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UseCase\Tickets\TicketService;
use App\Http\Requests\Ticket\MessageRequest;

class TicketController extends Controller
{
    private $service;

    public function __construct(TicketService $service)
    {
        $this->service = $service;
        $this->middleware('can:manage-tickets');
    }

    public function index(Request $request)
    {

        $query = Ticket::orderByDesc('updated_at');

        if (!empty($value = $request->get('id'))){
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('user'))){
            $query->where('user_id', $value);
        }
        if (!empty($value = $request->get('status'))){
            $query->where('status', $value);
        }

        $tickets = $query->paginate(20);

        $statuses = Status::statusesList();

        return view('admin.tickets.index', compact('tickets', 'statuses'));

    }

    public function show(Ticket $ticket)
    {
        return view('admin.tickets.show', compact('ticket'));
    }

    public function editForm(Ticket $ticket)
    {
        return view('admin.tickets.edit', compact('ticket'));
    }

    public function edit(EditRequest $request, Ticket $ticket)
    {
        try{
            $this->service->edit($ticket->id, $request);
        }catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.show', $ticket);
    }

    public function close(Ticket $ticket)
    {
        try{
            $this->service->close(Auth::id(), $ticket->id);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.show', $ticket);

    }

    public function approve(Ticket $ticket)
    {
        try{
            $this->service->approve(Auth::id(), $ticket->id);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.show', $ticket);

    }

    public function reopen( Ticket $ticket)
    {
        try{
            $this->service->reopen(Auth::id(), $ticket->id);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.show', $ticket);

    }

    public function message(MessageRequest $request, Ticket $ticket)
    {
        try{
            $this->service->message(Auth::id(), $ticket->id, $request);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.show', $ticket);

    }

    public function destroy(Ticket $ticket)
    {
        try{
            $this->service->removeByAdmin($ticket->id);
        } catch (\DomainException $e){
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.index', $ticket);

    }
}