<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30/12/2018
 * Time: 16:29
 */

namespace App\Http\Controllers\Admin;


use App\Entity\News;
use App\Entity\NewsCat;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequests;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:manage-pages');
    }

    public function index(){
        $news = News::all();


        return view('admin.news.index', compact('news'));
    }

    public function create(){

        $newsCategory = NewsCat::all();

        return view('admin.news.create', compact('newsCategory'));
    }

    public function store(NewsRequests $request)
    {

        if (!empty($request['avatar'])){
            $news = News::create([
                'title' => $request['title'],
                'avatar' =>  $request['avatar']->store('news', 'local'),
                'slug' => str_slug($request['title']),
                'category_id' => $request['category_id'],
                'description' => $request['description'],
                'content' => $request['content'],
            ]);
        }else{
            $news = News::create([
                'title' => $request['title'],
                'slug' => str_slug($request['title']),
                'category_id' => $request['category_id'],
                'description' => $request['description'],
                'content' => $request['content'],
            ]);
        }


        return redirect()->route('admin.news.show', $news);
    }

    public function show(News $news)
    {
        return view('admin.news.show', compact('news'));
    }

    public function edit(News $news)
    {
        $newsCategory = NewsCat::all();

        return view('admin.news.edit', compact('news','newsCategory'));
    }

    public function update(Request $request, News $news)
    {

        $this->validate($request,[
            'title' => 'required|string|max:255',
            'category_id' => 'nullable|integer|exists:news_category,id',
            'content' => 'nullable|string',
            'description' => 'nullable|string|max:255',
        ]);

        $oldAvatar = $news->avatar;
        $news->update([
            'title' => $request['title'],
            'slug' => str_slug($request['title']),
            'category_id' => $request['category_id'],
            'description' => $request['description'],
            'content' => $request['content'],

        ]);
        if ($request['avatar']){
            $news->avatar =  $request['avatar']->store('news', 'local');
            if ($oldAvatar != null){
                unlink(public_path(). '/app/'. $oldAvatar);
            }
        }

        return redirect()->route('admin.news.show', $news);
    }

    public function destroy(News $news){

        $news->delete();

        return redirect()->route('admin.news.index');
    }

}