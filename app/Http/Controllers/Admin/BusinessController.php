<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 13/12/2018
 * Time: 12:29
 */

namespace App\Http\Controllers\Admin;

use App\Entity\Business\Business;
use App\Entity\Business\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessController extends Controller
{
    public function index(){
        $business = Business::defaultOrder()->withDepth()->get();

        return view('admin.business.index', compact('business'));
    }

    public function create(){

        $parents = Business::defaultOrder()->withDepth()->get();

        return view('admin.business.create', compact('parents'));
    }

    public function store(Request $request)
    {

        $this->validate($request,[
            'name' => 'required|string|max:255',
            //'slug' => 'required|string|max:255',
            'parent' => 'nullable|integer|exists:business,id',
        ]);

        $category = Business::create([
            'name' => $request['name'],
            'slug' => str_slug($request['name']),
            'parent_id' => $request['parent'],
        ]);

        return redirect()->route('admin.business.show', $category);
    }

    public function show(Business $business)
    {
        $query = Company::active()->orderByDesc('published_at');

        $query->forCategory($business);

        $companies = $query->paginate(10);

        return view('admin.business.show', compact('business', 'companies'));
    }

    public function edit(Business $business)
    {
        $parents = Business::defaultOrder()->withDepth()->get();

        return view('admin.business.edit', compact('parents','business'));
    }

    public function update(Request $request, Business $business)
    {

        $this->validate($request,[
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
            'parent' => 'nullable|integer|exists:advert_categories,id',
        ]);

        $business->update([
            'name' => $request['name'],
            'slug' => $request['slug'],
            'parent_id' => $request['parent'],
        ]);

        return redirect()->route('admin.business.show', $business);
    }

    public function destroy(Business $business){

        $business->delete();

        return redirect()->route('admin.business.index');
    }

    public function first(Business $business){

        if($first = $business->siblings()->defaultOrder()->first()){
            $business->insertBeforeNode($first);
        }
        return redirect()->route('admin.business.index');

    }

    public function up(Business $business){

        $business->up();

        return redirect()->route('admin.business.index');
    }

    public function down(Business $business){

        $business->down();

        return redirect()->route('admin.business.index');
    }

    public function last(Business $business){

        if($first = $business->siblings()->defaultOrder('desc')->first()){
            $business->insertAfterNode($first);
        }
        return redirect()->route('admin.business.index');

    }
}