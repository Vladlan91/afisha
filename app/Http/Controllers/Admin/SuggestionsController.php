<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 11/01/2019
 * Time: 12:22
 */

namespace App\Http\Controllers\Admin;


use App\Entity\Business\Business;
use App\Entity\Business\Company;
use App\Entity\Suggestions\Suggestions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Adverts\PhotoRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SuggestionsController extends Controller
{
    public function index()
    {
        return view('admin.suggestions.index');
    }

    public function create(Company $company)
    {
        return view('admin.suggestions.create', compact('company'));
    }

    public function store(Request $request, Company $company)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'company_id' => 'nullable|integer|exists:companies,id',
            'content' => 'nullable|string',
        ]);

        $suggestions = Suggestions::create([
            'title' => $request['title'],
            'avatar' =>  $request['avatar']->store('suggestions', 'local'),
            'slug' => str_slug($request['title']),
            'company_id' => $company->id,
            'content' => $request['content'],
            'address' => $request['address'],
        ]);

        return redirect()->route('admin.business.company.show', [$company->business->id, $company->id ]);
    }
    public function show(Suggestions $suggestions)
    {
        return view('admin.suggestions.show', compact('suggestions'));
    }

    public function editForm(Suggestions $suggestions)
    {
        return view('admin.suggestions.edit', compact('suggestions'));
    }
    public function edit(Request $request, Suggestions $suggestions)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'company_id' => 'nullable|integer|exists:companies,id',
            'content' => 'nullable|string',
        ]);
        $oldSuggestions = $suggestions->company_id;
        $oldAvatar = $suggestions->avatar;
        $suggestions->update([
            'title' => $request['title'],
            'price' => $request['price'],
            'slug' => str_slug($request['title']),
            'company_id' => $oldSuggestions,
            'content' => $request['content'],
            'address' => $request['address'],
            'movie' => $request['movie'],
        ]);
        if ($request['avatar']){
            $suggestions->avatar = $request['avatar']->store('suggestions', 'local');
            if ($oldAvatar != null){
                unlink(public_path(). '/app/'. $oldAvatar);
            }
            $suggestions->saveOrFail();
        }

        return redirect()->route('admin.suggestions.show', $suggestions);

    }

    public function photosForm(Suggestions $suggestions)
    {
        return view('admin.suggestions.photos', compact('suggestions'));
    }

    public function  photos(PhotoRequest $request, Suggestions $suggestions)
    {
        $this->validate($request, [
            'file' => 'required|image|mimes:jpg,jpeg,png',
        ]);

        $suggestions->photo()->create([
            'file' => $request['file']->store('suggestions', 'local'),
            'suggestion_id' => $suggestions->id,
        ]);

        return redirect()->route('admin.suggestions.show', $suggestions);
    }


    public function destroy(Suggestions $suggestions){

        $company = $suggestions->getCompany($suggestions->company_id);

        if ($suggestions->avatar != null){
            unlink(public_path(). '/app/'. $suggestions->avatar);
        }
        $suggestions->findOrFail($suggestions->id)->delete();

        return redirect()->route('admin.business.company.show', [$company->category_id, $suggestions->company_id]);

    }

    public function photoDelete(Suggestions $suggestions, $id)
    {
        $photo = $suggestions->getPhoto($id);
        if (isset($photo)){
            unlink(public_path(). '/app/'. $photo->file);
            $photo->findOrFail($photo->id)->delete();
        }
        return redirect()->route('admin.suggestions.show', $suggestions);

    }


}