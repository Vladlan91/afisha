<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 29/11/2018
 * Time: 20:16
 */

namespace App\Http\Controllers;


use App\Entity\Banner\Banner;
use App\UseCase\Banners\BannerService;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    private $service;

    public function __construct(BannerService $service)
    {
        $this->service = $service;
    }

    public function get(Request $request)

    {
        $format = $request['format'];
        $category = $request['category'];
        $region = $request['region'];

        if(empty($category) && empty($region)){

            $query = Banner::inRandomOrder()->where('category_id', null)->where('region_id', null)->where('format', $format)->get();
        }
        elseif(!empty($category) && !empty($region)){

            $query = Banner::inRandomOrder()->where('category_id', $category)->where('region_id', $region)->where('format', $format)->get();
        }
        elseif (!empty($region && !isset($category))){

            $query = Banner::inRandomOrder()->where('region_id', $region)->where('format', $format)->get();
        }
        elseif(!empty($category  && !isset($region))){
            $query = Banner::inRandomOrder()->where('category_id', $category)->where('region_id', null)->where('format', $format)->get();
        }
        else{
            $query = Banner::inRandomOrder()->where('format', $format);
        }

        $banner = $query->take(1);

        foreach ($banner as $ban){
            $ban->views();
        }
        return view('banner.get', compact('banner'));

    }

    public function click(Banner $banner)
    {
        $banner->click();

        return redirect($banner->url);
    }

}