<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 01/12/2018
 * Time: 16:36
 */

namespace App\Http\Controllers;


use App\Entity\Page;
use App\Router\PagePath;

class PageController extends Controller
{
    public function show(PagePath $path)
    {
        $page = $path->page;

        return view('page', compact('page'));
    }

//    public function show(Page $page)
//    {
//        return view('page', compact('page'));
//    }

}