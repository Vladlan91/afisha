<?php

namespace App\Http\Controllers;

use App\Entity\Adverts\Advert\Advert;
use App\Entity\Adverts\Category;
use App\Entity\Banner\Banner;
use App\Entity\Message;
use App\Entity\News;
use App\Entity\NewUser;
use App\Entity\Region;
use App\Entity\Suggestions\Suggestions;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Lang;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {

        $regions = Region::roots()->orderBy('name')->getModels();

        $categories = Category::whereIsRoot()->defaultOrder()->getModels();

        $query = Advert::active()->with(['category', 'region'])->orderByDesc('type');
        $adverts = $query->paginate(18);
        $suggestions = Suggestions::inRandomOrder()->whereHas('company', function($q){
            $q->where('package', '!=', null);
        })->take(3)->getModels();

        $banner1 = Banner::inRandomOrder()->where('format','!=', '240x440')->whereHas('user', function($q){
            $q->where('package', '!=', null);
        })->take(1)->getModels();
        foreach ($banner1 as $ban){
            $id = $ban->id;
        }
//        Cache::forget('category');
//        $va = Cache::rememberForever('category', function () {
//            return MenuClass::getV();
//        });
//        $value = Cache::get('category');
//
//        dd($value);
        if (!empty($banner1)){
            $banner2 = Banner::inRandomOrder()->where('id','!=', $id)->where('format','!=', '240x440')->whereHas('user', function($q){
                $q->where('package', '!=', null);
            })->take(1)->getModels();
        }else{
            $banner2 = Banner::inRandomOrder()->where('format','!=', '240x440')->whereHas('user', function($q){
                $q->where('package', '!=', null);
            })->take(1)->getModels();
        }


        return view('home', compact('regions', 'adverts2','categories','adverts','suggestions','banner1','banner2'));
    }

    public function about()
    {
        $news = News::orderByDesc('id')->take(6)->getModels();
        return view('about',compact('news'));
    }

    public function message(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:255',
        ]);

        $newscat = Message::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'text' => $request['text'],
        ]);

        return redirect()->route('about');
    }
}
