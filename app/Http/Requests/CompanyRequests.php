<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/02/2019
 * Time: 14:27
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CompanyRequests extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return
            [
                'title' => 'required|string|max:255',
                'category_id' => 'nullable|integer|exists:business,id',
                'region_id' => 'nullable|integer|exists:regions,id',
                'phone' => 'regex:/(0)[0-9]{9}/|max:10',
                'email' => 'required|string|email|max:255',
                'address'=>'required|string'
            ];

    }

    public function messages()
    {
        return [
            'phone.regex' => 'Формат телефону повинен відповідати - 0974573714',
            'phone.max' => 'Кількість чисел не повино перевищувати - 10',
            'name.max' => 'І\'мя перевищує довжину',
            'title.max' => 'Назва перевищує довжину',
            'title.required' => 'Назва, обов\'язкове для заповнення',
            'phone.required' => 'Телефон, обов\'язкове для заповнення',
            'email.email' => 'Емейл повинен відповідати валідації емейлу',
            'email.required' => 'Емейл, обов\'язкове для заповнення',
            'address.required' => 'Адреса, обов\'язкове для заповнення',
        ];
    }
}