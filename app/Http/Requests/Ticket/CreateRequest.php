<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 04/12/2018
 * Time: 09:21
 */

namespace App\Http\Requests\Ticket;


use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules() : array
    {
        return [
            'subject' => 'required|string|max:255',
            'content' => 'required|string',
        ];
    }

}