<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/02/2019
 * Time: 11:57
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PhoneRequests extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return
            [
                'name' =>'required|string|max:255',
                'last_name' =>'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,id,'. \Auth::id(),
                'phone' => 'regex:/(0)[0-9]{9}/|max:10',


            ];

    }

    public function messages()
    {
        return [
            'phone.regex' => 'Формат телефону повинен відповідати - 0974573714',
            'phone.max' => 'Кількість чисел не повино перевищувати - 10',
            'email.email' => 'Емейл повинен відповідати валідації емейлу',
            'name.max' => 'І\'мя перевищує довжину',
            'last_name.max' => 'Фамілія перевищує довжину',
        ];
    }
}