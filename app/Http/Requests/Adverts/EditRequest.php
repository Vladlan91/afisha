<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 17/11/2018
 * Time: 22:56
 */

namespace App\Http\Requests\Adverts;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string|max:255',
            'address' => 'required|string',
            'phone' => 'regex:/(0)[0-9]{9}/|max:10',
        ];

    }

    public function messages()
    {
        return [
            'content.required' => 'Поле, обов\'язкове для заповнення!',
            'title.required' => 'Поле, обов\'язкове для заповнення!',
            'address.required' => 'Поле, обов\'язкове для заповнення!',
            'content.max' => 'Ліміт на текст, не більше 250 символів!',
            'phone.regex' => 'Формат телефону повинен відповідати - 0974573714',
            'phone.max' => 'Кількість чисел не повино перевищувати - 10',
        ];
    }
}