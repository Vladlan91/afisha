<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 08/11/2018
 * Time: 18:09
 */

namespace App\Http\Requests\Adverts;

use Illuminate\Foundation\Http\FormRequest;

class RejectRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return[
            'reason' => 'required|string',
        ];
    }

}