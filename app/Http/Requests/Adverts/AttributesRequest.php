<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 14/11/2018
 * Time: 13:50
 */

namespace App\Http\Requests\Adverts;


use Illuminate\Foundation\Http\FormRequest;

class AttributesRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules() :array
    {
        $items =[];

        foreach ($this->advert->category->allAttributes() as $attribute) {
            $rules = [
                $attribute->required ? 'required' : 'nullable',
            ];
            if ($attribute->isInteger()){
                $rules[] = 'integer';
            } elseif ($attribute->isFloat()){
                $rules[] = 'numeric';
            }else{
                $rules[] = 'string';
                $rules[] = 'max:255';
            }
            if ($attribute->isSelect()){
                $rules[] = Rule::in($attribute->variants);
            }
            $items['attribute.' . $attribute->id] = $rules;
        }

        return  $items;

    }
}