<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/11/2018
 * Time: 15:41
 */

namespace App\Http\Requests\Adverts;


use App\Entity\Adverts\Category;
use App\Entity\Region;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


/**
 * @property Category $category
 * @property Region $region
 */

class CreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $items =[];

//        foreach ($this->category->allAttributes() as $attribute) {
//            $rules = [
//                $attribute->required ? 'required' : 'nullable',
//            ];
//            if ($attribute->isInteger()){
//                $rules[] = 'integer';
//            } elseif ($attribute->isFloat()){
//                $rules[] = 'numeric';
//            }else{
//                $rules[] = 'string';
//                $rules[] = 'max:255';
//            }
//            if ($attribute->isSelect()){
//                $rules[] = Rule::in($attribute->variants);
//            }
//            $items['attribute.' . $attribute->id] = $rules;
//        }

        return array_merge(
            [
            'title' => 'required|string',
            'content' => 'required|string|max:255',
            'address' => 'required|string',
            'phone' => 'regex:/(0)[0-9]{9}/|max:10',
        ], $items);

    }

    public function messages()
    {
        return [
            'content.required' => 'Поле, обов\'язкове для заповнення!',
            'title.required' => 'Поле, обов\'язкове для заповнення!',
            'address.required' => 'Поле, обов\'язкове для заповнення!',
            'content.max' => 'Ліміт на текст, не більше 250 символів!',
            'phone.regex' => 'Формат телефону повинен відповідати - 0974573714',
            'phone.max' => 'Кількість чисел не повино перевищувати - 10',
        ];
    }

}