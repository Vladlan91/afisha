<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 08/11/2018
 * Time: 18:08
 */

namespace App\Http\Requests\Adverts;

use Illuminate\Foundation\Http\FormRequest;

class PhotoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'file.*' => 'required|image|mimes:jpg,jpeg,png',
        ];

    }

}