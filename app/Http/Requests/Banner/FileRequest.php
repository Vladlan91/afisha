<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 28/11/2018
 * Time: 17:34
 */

namespace App\Http\Requests\Banner;


use App\Entity\Banner\Banner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FileRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {

        list ($width, $height) = [0,0];
        if ($format = $this->input('format')){
            list ($width, $height) = explode('x', $format);
        }

        return
            [
                'format' => ['required', 'string', Rule::in(Banner::formatsList())],
                'file' => 'required|image|mimes:jpeg,jpg,png|dimensions::width='. $width . ',height='. $height,
            ];

    }
}