<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 27/11/2018
 * Time: 17:08
 */

namespace App\Http\Requests\Banner;


use Illuminate\Foundation\Http\FormRequest;


class EditRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return
            [
                'name' => 'required|string',
                'limit' => 'required|integer',
                'url' => 'required|url',
            ];

    }

}