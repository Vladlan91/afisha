<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/11/2018
 * Time: 15:41
 */

namespace App\Http\Requests\Banner;


use App\Entity\Adverts\Category;
use App\Entity\Banner\Banner;
use App\Entity\Region;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


/**
 * @property Category $category
 * @property Region $region
 */

class CreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        list ($width, $height) = [0,0];
        if ($format = $this->input('format')){
            list ($width, $height) = explode('x', $format);
        }

        return
            [
            'name' => 'required|string',
            'limit' => 'required|integer',
            'url' => 'required|url',
            'format' => ['required', 'string', Rule::in(Banner::formatsList())],
            'file' => 'required|image|mimes:jpeg,jpg,png|dimensions::width='. $width . ',height='. $height,
        ];

    }

    public function messages()
    {
        return [
            'file.dimensions' => 'Розмір фото повино бути -'. $format = $this->input('format'),
            'file.required' => 'Фото, обов\'язкове для заповнення',
            'name.required' => 'Назва, обов\'язкове для заповнення',
            'url.required' => 'Посилання, обов\'язкове для заповнення',
            'limit.required' => 'Кількість перегладів, обов\'язкове для заповнення',
        ];
    }

}
