<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/02/2019
 * Time: 18:22
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class NewsRequests extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return
            [
                'title' => 'required|string|max:255',
                'avatar' => 'required|image|mimes:jpeg,jpg,png',
                'content'=>'required|string'
            ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Назва, обов\'язкове для заповнення',
            'avatar.required' => 'Фото, обов\'язкове для заповнення',
            'content.required' => 'Контент, обов\'язкове для заповнення',
        ];
    }
}