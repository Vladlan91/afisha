<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/02/2019
 * Time: 17:56
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PageRequests extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return
            [
                'title' => 'required|string|max:255',
                'menu_title' => 'required|string|max:255',
                'parent' => 'nullable|integer|exists:pages,id',
                'content' => 'nullable|string',
                'description' => 'nullable|string|max:255',
            ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Назва, обов\'язкове для заповнення',
            'menu_title.required' => 'Назва в меню, обов\'язкове для заповнення',
            'email.required' => 'Емейл, обов\'язкове для заповнення',
            'address.required' => 'Адреса, обов\'язкове для заповнення',
        ];
    }
}