<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 14/02/2019
 * Time: 12:23
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MoneyRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return
            [
                'balance' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'action' => 'required',

            ];
    }

    public function messages()
    {
        return [
            'balance.required' => 'Поле, обов\'язкове для заповнення',
            'action.required' => 'Поле, обов\'язкове для заповнення',
        ];
    }
}