<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 03/11/2018
 * Time: 17:53
 */

namespace App\Http\Middleware;


use Auth;
use Closure;

class FilledProfile
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if (empty($user->hasFilledProfile())){
            return redirect()->route('cabinet.profile.home');
        }

        return $next($request);
    }

}