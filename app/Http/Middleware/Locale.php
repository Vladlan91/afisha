<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 24/11/2018
 * Time: 03:19
 */

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use Config;

class Locale
{

    public function handle($request, Closure $next)
    {
        App::setLocale(env('APP_LOCALE'));
        return $next($request);
    }

}