<?php

namespace App\Providers;

use App\Entity\Adverts\Category;
use App\Entity\Banner\Afisha;
use App\Entity\Business\Business;
use App\Entity\News;
use App\Entity\Page;
use App\Entity\Region;
use App\Menu;
use App\Services\Banner\CostCalculator;
use App\Services\Sms\SmsSender;
use App\Services\Sms\SmsUkr;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::composer('layouts.dropdown.dropdown', function (\Illuminate\Contracts\View\View $view){
            $view->with('menuPage', Page::whereIsRoot()->defaultOrder()->getModels());
        });
        View::composer('layouts.dropdown.dropdown', function (\Illuminate\Contracts\View\View $view){
            $view->with('business', Business::defaultOrder()->getModels());
        });
        View::composer('layouts.dropdown.dropdown', function (\Illuminate\Contracts\View\View $view){
            $view->with('news', News::orderByDesc('id')->take(4)->getModels());
        });
        View::composer('layouts.dropdown.dropdown', function (\Illuminate\Contracts\View\View $view){
            $view->with('cats', Category::where('parent_id', '=', null)->defaultOrder()->withDepth()->get());
        });
        View::composer('layouts.dropdown.dropdown', function (\Illuminate\Contracts\View\View $view){
            $view->with('regs', Region::where('parent_id', '=', null)->orderBy('name')->getModels());
        });
        View::composer('layouts.dropdown.headerbanner', function (\Illuminate\Contracts\View\View $view){
            $view->with('banner', Afisha::inRandomOrder()->where('status', '=', 'active')->take(1)->orderBy('name')->getModels());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CostCalculator::class, function (Application $app){
            $config = $app->make('config')->get('banner');
            return new CostCalculator($config['price']);
        });

        $this->app->singleton(SmsSender::class, function (Application $app) {
            $config = $app->make('config')->get('sms');
            if (!empty($config['url'])) {
                return new SmsUkr($config['app_id'], $config['url']);
            }
            return new SmsUkr($config['app_id']);
        });
    }
}
