<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 25/01/2019
 * Time: 14:45
 */

namespace App\Router;


use App\Entity\News;
use Illuminate\Contracts\Routing\UrlRoutable;

class NewPath  implements UrlRoutable
{
    /** @var Page */
    public $news;

    public function withPage(News $news) : self
    {
        $clone = clone $this;
        $clone->news = $news;
        return $clone;
    }

    public function getRouteKey()
    {
        if (!$this->news){
            throw new \BadMethodCallException('Empty news');
        }
        return $this->news->slug;
    }

    public function getRouteKeyName(): string
    {
        return 'news_path';
    }

    public function resolveRouteBinding($value)
    {
        $news = News::where('slug', $value)->first();

        return $this
            ->withPage($news);
    }
}
