<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 25/01/2019
 * Time: 15:46
 */

namespace App\Router;
use Illuminate\Contracts\Routing\UrlRoutable;
use \App\Entity\Business\Company;


class CompanyPath implements UrlRoutable
{
    /** @var Company */
    public $company;

    public function withPage(Company $company) : self
    {
        $clone = clone $this;
        $clone->company = $company;
        return $clone;
    }

    public function getRouteKey()
    {
        if (!$this->company){
            throw new \BadMethodCallException('Empty news');
        }
        return $this->company->slug;
    }

    public function getRouteKeyName(): string
    {
        return 'company_path';
    }

    public function resolveRouteBinding($value)
    {
        $company = Company::where('slug', $value)->first();

        return $this
            ->withPage($company);
    }
}
