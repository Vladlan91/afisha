<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 01/12/2018
 * Time: 15:24
 */

namespace App\Router;


use App\Entity\Page;
use Illuminate\Contracts\Routing\UrlRoutable;

class PagePath implements UrlRoutable
{
    /** @var Page */
    public $page;

    public function withPage(Page $page) : self
    {
        $clone = clone $this;
        $clone->page = $page;
        return $clone;
    }

    public function getRouteKey()
    {
        if (!$this->page){
           throw new \BadMethodCallException('Empty page');
        }
        return $this->page->getPath();
    }

    public function getRouteKeyName(): string
    {
        return 'page_path';
    }

    public function resolveRouteBinding($value)
    {
        $chunks = explode('/', $value);

        $page = null;
        do{
            $slug = reset($chunks);
            if ($slug && $next = Page::where('slug', $slug)->where('parent_id', $page ? $page->id : null)->first()){
                $page = $next;
                array_shift($chunks);
            }
        } while (!empty($slug) && !empty($next));

        if (!empty($chunks)){
            abort(404);
        }

        return $this
            ->withPage($page);
    }
}