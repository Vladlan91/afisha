<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 19/02/2019
 * Time: 11:47
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class NewCategory extends Model
{
    protected $table = 'dle_category';

    protected $guarded = ['id'];


}