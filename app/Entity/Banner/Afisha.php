<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 27/01/2019
 * Time: 21:57
 */

namespace App\Entity\Banner;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Banner
 * @package App\Entity\Afisha
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $file
 * @property string $status
 */
class Afisha extends Model
{
    public const STATUS_DRAFT = 'draft';
    public const STATUS_ACTIVE = 'active';


    protected $table = 'banner_afisha';

    protected $guarded = ['id'];

    public static function statusesList() : array
    {
        return [
            self::STATUS_DRAFT => 'Draft',
            self::STATUS_ACTIVE => 'Active',
        ];
    }

    public function isDraft()
    {
        return $this->status == self::STATUS_DRAFT;
    }

    public function changesStatus(): void
    {
        if ($this->isDraft()){
            $this->update([
                'status' => self::STATUS_ACTIVE,
            ]);
        }else{
            $this->update([
                'status' => self::STATUS_DRAFT,
            ]);
        }
    }
}