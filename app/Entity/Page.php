<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 01/12/2018
 * Time: 14:54
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
/**
 * @property int $id
 * @property string $title
 * @property string $menu_title
 * @property string $content
 * @property string $description
 * @property string $slug
 *
 * @property int $depth
 * @property Page $parent
 * @property Page[] $children
 */
class Page extends Model
{
    use NodeTrait;

    protected $table = 'pages';


    protected $guarded = ['id'];

    public function getPath(): string
    {
        return implode('/', array_merge($this->ancestors()->defaultOrder()->pluck('slug')->toArray(),[$this->slug]));
    }

    public function getMenuTitle() : string
    {
        return $this->menu_title ?: $this->title;
    }

    public function getPa(): string
    {
        return implode('/', array_merge($this->ancestors()->defaultOrder()->pluck('name')->toArray(),[$this->name]));
    }

    public function parent(){
        return $this->belongsTo(static::class, 'parent_id', 'id');
    }


    public function children(){
        return $this->hasMany(Page::class, 'parent_id', 'id');
    }




}