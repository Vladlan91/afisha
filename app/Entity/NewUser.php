<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 10/02/2019
 * Time: 23:54
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $view
 * @property string $text
 */
class NewUser extends Model
{
    protected $table = 'dle_users';

    protected $guarded = ['id'];


}