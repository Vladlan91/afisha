<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 03/12/2018
 * Time: 11:59
 */

namespace App\Entity\Ticket;


use App\User;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class Message
 * @property int $id
 * @property int $ticket_id
 * @property int $user_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $status
 *
 */

class Status extends Model
{
    public const OPEN = 'open';
    public const APPROVED = 'approved';
    public const CLOSED = 'closed';

    protected $table = 'ticket_statuses';

    protected $guarded = ['id'];

    public static function statusesList(): array
    {
        return [
            self::OPEN => 'Open',
            self::APPROVED => 'Approved',
            self::CLOSED => 'Closed',
        ];
    }

    public function isApproved()
    {
        return $this->status === Status::APPROVED;
    }

    public function isOpen()
    {
        return $this->status === Status::OPEN;
    }

    public function isClosed()
    {
        return $this->status === Status::CLOSED;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}