<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 03/12/2018
 * Time: 11:51
 */

namespace App\Entity\Ticket;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Entity\Ticket\Status;

/**
 * Class Ticket
 * @property int $id
 * @property int $user_id
 * @property string $status
 * @property string $subject
 * @property string $content
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @method Builder forUser(User $user)
 */

class Ticket extends Model
{
    protected $table = 'ticket_tickets';

    protected $guarded = ['id'];

    public static function new(int $userId, string $subject, string $content) :self
    {
        $ticket = Ticket::create([
            'user_id' => $userId,
            'subject' => $subject,
            'content' => $content,
            'status' => Status::OPEN,
        ]);
        $ticket->setStatus(Status::OPEN, $userId);
        return $ticket;
    }

    public function edit(string $subject, string $content): void
    {
        $this->update([
            'subject' => $subject,
            'content' => $content,
        ]);
    }

    public function approve(int $userId): void
    {
        if ($this->isApproved()){
            throw new \DomainException('Звернення уже прийнято до виконання!');
        }
        $this->setStatus(Status::APPROVED, $userId);
    }

    public function addMessage(int $userId, $id, $message) : void
    {
        if (!$this->allowsMessage()){
            flash('Звернення закрите для повідомлень!')->info();
            throw new \DomainException();
        }
        $this->messages()->create([
            'user_id' => $userId,
            'ticket_id' => $id,
            'message' => $message,
        ]);
        $this->update();
    }
    public function allowsMessage(): bool
    {
        return !$this->isClosed();
    }

    public function close(int $userId): void
    {
        if ($this->isClosed()){
            throw new \DomainException('Звернення уже закрите!');
        }
        $this->setStatus(Status::CLOSED, $userId);
    }

    public function reopen(int $userId): void
    {
        if (!$this->isClosed()){
            throw new \DomainException('Звернення не закрито!');
        }
        $this->setStatus(Status::APPROVED, $userId);
    }

    public function canBeRemoved(): bool
    {
        return $this->isOpen();
    }

    public function isApproved(): bool
    {
        return $this->status === Status::APPROVED;
    }

    public function isOpen(): bool
    {
        return $this->status === Status::OPEN;
    }

    public function isClosed(): bool
    {
        return $this->status === Status::CLOSED;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'ticket_id', 'id');
    }

    public function setStatus($status, int $userId = null): void
    {
        $this->statuses()->create(['status' => $status, 'user_id' => $userId]);
        $this->update(['status' => $status]);
    }

    public function statuses()
    {
        return $this->hasMany(Status::class, 'ticket_id', 'id');
    }

    public function scopeForUser(Builder $query, User $user)
    {
        return $query->where('user_id', $user->id);
    }
}