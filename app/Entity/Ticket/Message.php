<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 03/12/2018
 * Time: 13:31
 */

namespace App\Entity\Ticket;


use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\User;

/**
 * Class Message
 * @property int $id
 * @property int $user_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $message
 *
 */

class Message extends Model
{
    protected $table = 'ticket_messages';

    protected $guarded = ['id'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}