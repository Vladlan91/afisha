<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 11/02/2019
 * Time: 23:51
 */

namespace App\Entity;


use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property int $user_id
 * @property decimal $sum
 * @property integer $system
 * @property integer $payment_id
 * @property string $action
 * @property int $admin_check
 *
 * @method Builder forUser(User $user)
 */

class Order extends Model
{

    public const STATUS_EASYPAY ='EasyPay';
    public const STATUS_ERROR = 'error';
    public const STATUS_USERPANEL = 'userpanel';
    public const STATUS_ADMINPANEL = 'adminpanel';
    public const STATUS_BANNER = 'banner';
    public const STATUS_ADVERT= 'adverts';
    public const STATUS_PACKAGE = 'package';

    protected $table = 'orders';

    protected $fillable = ['id', 'user_id', 'sum', 'system', 'payment_id', 'action', 'admin_check', 'created_at'];


    public static function statusList(): array
    {
        return [
            self::STATUS_EASYPAY => 'EasyPay',
            self::STATUS_ERROR => 'error',
            self::STATUS_USERPANEL => 'userpanel',
            self::STATUS_ADMINPANEL => 'adminpanel',

        ];
    }

    public static function actionList(): array
    {
        return [
            self::STATUS_BANNER => 'banners',
            self::STATUS_ADVERT=> 'adverts',
            self::STATUS_PACKAGE => 'packages',
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeForUser(Builder $query, User $user)
    {
        return $query->where('user_id', $user->id);
    }
}