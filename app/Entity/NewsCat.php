<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30/12/2018
 * Time: 17:12
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title

 */

class NewsCat extends Model
{
    protected $table = 'news_category';

    protected $fillable = ['id','title'];
}