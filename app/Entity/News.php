<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30/12/2018
 * Time: 16:23
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $title
 * @property string $avatar
 * @property int $news_category
 * @property int $view
 * @property string $content
 * @property string $description
 * @property string $slug
 *
 *
 * @method Builder forCategory(NewsCat $category)

 */
class News extends Model
{
    protected $table = 'news';

    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(NewsCat::class, 'category_id', 'id');
    }

    public function scopeForCategory(Builder $query, NewsCat $category)
    {
        return $query->where('category_id', $category->id)->getModels();
    }

}