<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 13/12/2018
 * Time: 12:22
 */

namespace App\Entity\Business;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Carbon\Carbon;

/**
 * @property int $id
 * * @property string $slug
 * @property string $name
 * @property int $parent_id
 * @property Business $parent
 */
class Business extends Model
{
    use NodeTrait;

    protected $table = 'business';

    public  $timestamps = false;

    protected $fillable = ['name', 'slug', 'parent_id'];

    public function getPath(): string
    {
        return implode('/', array_merge($this->ancestors()->defaultOrder()->pluck('slug')->toArray(),[$this->slug]));
    }

    public function getPa(): string
    {
        return implode('/', array_merge($this->ancestors()->defaultOrder()->pluck('name')->toArray(),[$this->name]));
    }

    public function parent(){
        return $this->belongsTo(static::class, 'parent_id', 'id');
    }

    public function hasChildren($categoryId)
    {
        $category = Business::findOrFail($categoryId);
        dd($category->children()-$this->name);
        if (!empty($category->children)){
            return true;
        }
        return false;
    }

    public function children(){
        return $this->hasMany(Business::class, 'parent_id', 'id');
    }
}