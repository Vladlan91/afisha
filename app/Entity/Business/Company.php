<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 13/12/2018
 * Time: 14:36
 */

namespace App\Entity\Business;

use App\Entity\Adverts\Advert\Advert;
use App\Entity\Region;
use App\Entity\Suggestions\Suggestions;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * @property int $id
 * @property int $user_id
 * @property int $category_id
 * @property int $region_id
 * @property string $title
 * @property text $content
 * @property string $slug
 * @property string $phone
 * @property string $site
 * @property string $email
 * @property string $logo
 * @property text $address
 * @property string $location
 * @property string $status
 * @property string $reject_reason
 * @property Carbon $update_at
 * @property Carbon $published_at
 * @property Carbon $expires_at
 *
 * @property Business $business
 * @property Region $region
 * @property User $user
 * @method Builder forUser(User $user)
 * @method Builder forTitle($value)
 *
 * @method Builder forCategory(Business $business)
 * @method Builder forRegion(Region $region)
 * @method Builder active()
 */
class Company extends Model
{
    public const STATUS_DRAFT = 'draft';
    public const STATUS_MODERATION = 'moderation';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_CLOSED = 'closed';

    protected $table = 'companies';

    protected $guarded = ['id'];

    protected $casts = [
        'published_at' => 'datetime',
        'expires_at' => 'datetime',
    ];

    public static function statusesList(): array
    {
        return [
            self::STATUS_CLOSED => 'closed',
            self::STATUS_DRAFT => 'draft',
            self::STATUS_MODERATION => 'moderation',
            self::STATUS_ACTIVE => 'active',
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function sendToModeration(): void
    {
        if (!$this->isDraft()){
            throw new \DomainException('Advert is not a draft');
        }

        $this->update([
            'status' => self::STATUS_MODERATION,
            'reject_reason' => null,
            'published_at' => null,
            'expires_at' => null,
        ]);
    }
    public function moderate(Carbon $date): void
    {
        if ($this->status !== self::STATUS_MODERATION){
            flash('Advert is not sent to moderation ')->important()->info();
            throw new \DomainException('Advert is not sent to moderation');
        }

        $this->update([
            'published_at' => $date,
            'expires_at' => $date->copy()->addDays(7),
            'status' => self::STATUS_ACTIVE,
        ]);

    }

    public function sendToDraft(): void
    {
        $this->update([
            'status' => self::STATUS_DRAFT,
        ]);
    }

    public function reject($reason): void
    {
        $this->update([
            'status' => self::STATUS_DRAFT,
            'reject_reason' => $reason,


        ]);
    }

    public function expire(): void
    {
        $this->update([
            'status' => self::STATUS_CLOSED,

        ]);
    }

    public function close(): void
    {
        $this->update([
            'status' => self::STATUS_CLOSED,

        ]);
    }

    public function isDraft(): bool
    {
        return $this->status === self::STATUS_DRAFT;
    }

    public function isClosed(): bool
    {
        return $this->status === self::STATUS_CLOSED;
    }

    public function isOnModeration()
    {
        return $this->status === self::STATUS_MODERATION;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function business()
    {
        return $this->belongsTo(Business::class, 'category_id', 'id');
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }

    public function suggestion()
    {
        return $this->hasMany(Suggestions::class, 'company_id', 'id');
    }

    public function adverts()
    {
        return $this->hasMany(Advert::class, 'company_id', 'id');
    }


    public function scopeForUser(Builder $query, User $user)
    {
        return $query->where('user_id', $user->id);
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }


    public function scopeForCategory(Builder $query, Business $business)
    {
        return $query->whereIn('category_id', array_merge(
            [$business->id],
            $business->descendants()->pluck('id')->toArray()
        ));
    }

    public function scopeForRegion(Builder $query, Region $region)
    {
        $ids = [$region->id];
        $childrenIds = $ids;
        while ($childrenIds = Region::where(['parent_id' => $childrenIds])->pluck('id')->toArray()){
            $ids = array_merge($ids, $childrenIds);
        }
        return $query->whereIn('region_id', $ids);
    }

    public function scopeForTitle(Builder $query, $value)
    {

        return $query->where('title', 'like', '%' . $value . '%');
    }






}