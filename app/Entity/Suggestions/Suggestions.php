<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 11/01/2019
 * Time: 14:16
 */

namespace App\Entity\Suggestions;



use App\Entity\Business\Company;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $company_id
 * @property string $title
 * @property string $slug
 * @property string $movie
 * @property string $avatar
 * @property int $price
 * @property string $address
 * @property string $content

 */

class Suggestions extends Model
{
    protected $table = 'suggestion';

    protected $guarded = ['id'];

    public function photo()
    {
        return $this->hasMany(Photo::class, 'suggestion_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function getCompany(int $id)
    {
        return Company::findOrFail($id);
    }

    public function getPhoto(int $id)
    {
        return Photo::findOrFail($id);
    }

    public function scopeForCompany(Builder $query, Company $company)
    {
        return $query->whereHas('favorites', function (Builder $query) use ($company){
            $query->where('user_id', $user->id);
        });
    }

}