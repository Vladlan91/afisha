<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/11/2018
 * Time: 15:33
 */

namespace App\Entity\Suggestions;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $file
 */

class Photo extends Model
{
    protected $table = 'suggestion_photos';

    public $timestamps = false;

    protected $fillable = ['suggestion_id','file'];
}
