<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 12/02/2019
 * Time: 10:04
 */

namespace App\Entity;


use Illuminate\Database\Eloquent\Model;

class NewOrder extends Model
{
    protected $table = 'transaction';

    protected $guarded = ['id'];


}