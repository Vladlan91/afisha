<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 15/01/2019
 * Time: 13:35
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;


/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $view
 * @property string $text
 */



class Message extends Model
{
    protected $table = 'messages';

    protected $guarded = ['id'];

    public function view()
    {
        $this->update([
            'view' => 'view',
        ]);
    }

    public function isView()
    {
        return $this->view === 'view';
    }


}
