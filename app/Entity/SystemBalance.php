<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 24/02/2019
 * Time: 15:15
 */

namespace App\Entity;


use Illuminate\Database\Eloquent\Model;

class SystemBalance extends Model
{
    protected $table = 'system_balance';


    protected $guarded = ['id'];

    public const STATUS_BANNER = 'banner';
    public const STATUS_ADVERT= 'adverts';
    public const STATUS_PACKAGE = 'package';

    public static function statusesList() : array
    {
        return [
            self::STATUS_BANNER => 'banner',
            self::STATUS_ADVERT => 'adverts',
            self::STATUS_PACKAGE => 'package',
        ];
    }

}