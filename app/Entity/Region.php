<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 25/10/2018
 * Time: 09:40
 */

namespace App\Entity;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
/**
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property int|null $parent_id
 *
 * @property Region $parent
 * @property Region[] $children
 *
 * @method Builder roots()
 */
class Region extends Model
{

    public  $timestamps = false;

    protected $fillable = ['name', 'slug', 'parent_id'];

    public function getPath(): string
    {
        return ($this->parent ? $this->parent->getPath(). '/' : '') . $this->slug;
    }

    public function getAddress(): string
    {
        return ($this->parent ? $this->parent->getAddress() . ', ' : '') . $this->name;
    }

    public function parent(){
        return $this->belongsTo(static::class, 'parent_id', 'id');
    }
    public function hasChildren($regionId)
    {
        $category = Region::where('parent_id', $regionId)->getModels();
        if ($category){
            return true;
        }
        return false;
    }

    public function children(){
        return $this->hasMany(static::class, 'parent_id', 'id');
    }

    public function scopeRoots(Builder $query)
    {
        return $query->where('parent_id', null);
    }



}