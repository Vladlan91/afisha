<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 29/10/2018
 * Time: 14:22
 */

namespace App\Entity\Adverts;

use Illuminate\Database\Eloquent\Model;
/**
 * @property int $id
 * @property int $category_id
 * @property string $type
 * @property string $name
 * @property string $default
 * @property boolean $required
 * @property array $variants
 * @property array $variantsTwo
 * @property int $sort
 */
class Attribute extends Model
{
    public const TYPE_STRING = 'string';
    public const TYPE_INTEGER = 'integer';
    public const TYPE_FLOAT = 'float';
    public const TYPE_CHECKBOX = 'checkbox';

    protected $table = 'advert_attributes';

    public $timestamps = false;

    protected $guarded = ['id'];

    protected $casts = [
        'variants' => 'array',
    ];

    public static function typesList() :array {
        return [
            self::TYPE_STRING => 'string',
            self::TYPE_INTEGER => 'integer',
            self::TYPE_FLOAT => 'float',
            self::TYPE_CHECKBOX => 'checkbox',
        ];
    }

    public function isString():bool {
        return $this->type === self::TYPE_STRING;
    }
    public function isInteger():bool {
        return $this->type === self::TYPE_INTEGER;
    }
    public function isFloat():bool {
        return $this->type === self::TYPE_FLOAT;
    }

    public function isCheckbox():bool {
        return $this->type === self::TYPE_CHECKBOX;
    }
    public function isSelect():bool {
        return count($this->variants) > 0;
    }

    public function isNumber():bool {
        return $this->isInteger() || $this->isFloat();
    }

}