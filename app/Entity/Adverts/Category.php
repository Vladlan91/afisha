<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 28/10/2018
 * Time: 17:40
 */

namespace App\Entity\Adverts;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
/**
 * @property int $id
 * * @property string $slug
 * @property string $name
 * @property int $parent_id
 * @property int $price
 * @property Category $parent
 */
class Category extends Model
{
    use NodeTrait;

    protected $table = 'advert_categories';

    public  $timestamps = false;

    protected $fillable = ['id', 'name', 'description','slug','price', 'parent_id'];

    public function getPath(): string
    {
        return implode('/', array_merge($this->ancestors()->defaultOrder()->pluck('slug')->toArray(),[$this->slug]));
    }

    public function getPa(): string
    {
        return implode('/', array_merge($this->ancestors()->defaultOrder()->pluck('name')->toArray(),[$this->name]));
    }

    public function parent(){
        return $this->belongsTo(static::class, 'parent_id', 'id');
    }

    public function isParent()
    {
        return $this->parent_id === null;
    }

    public function hasChildren($categoryId)
    {
        $category = Category::where('parent_id', $categoryId)->getModels();
        if ($category){
            return true;
        }
        return false;
    }

    public function children(){
        return $this->hasMany(static::class, 'parent_id', 'id');
    }


    public function parentAttributes()
    {
        return $this->parent ? $this->parent->allAttributes() : [];
    }
        /**
        * @return Attribute[]
        */
    public function allAttributes() : array
    {
        return array_merge($this->parentAttributes(), $this->attributes()->orderBy('sort')->getModels());
    }

    public function attributes()
    {
        return $this->hasMany(Attribute::class, 'category_id', 'id');
    }


    public function getPrice()
    {
        if ($this->price !== null){
            return $this->price;
        }else{
            if (!$this->isParent()){
                return $this->parent->getPrice();
            }else{
                return 0;
            }

        }

    }

    public function dataPrint($data)
    {
        $dataArray =[];
        $dayPublished = Carbon::createFromFormat('Y-m-d H:s:i', $data);
        $standart = Carbon::createFromFormat('Y-m-d H:s:i', '2015-10-10 15:00:00');
        if ($dayPublished->dayOfWeek === Carbon::MONDAY) {
            $firstDayPrint = $dayPublished->addDays(3);
        }elseif ($dayPublished->dayOfWeek === Carbon::TUESDAY)
        {
            if ($standart->format('H:i') > $dayPublished->format('H:i')) {
                $firstDayPrint = $dayPublished->addDays(2);
            }else{
                $firstDayPrint = $dayPublished->addDays(9);
            }
        }elseif ($dayPublished->dayOfWeek === Carbon::WEDNESDAY)
        {
            $firstDayPrint = $dayPublished->addDays(8);
        }elseif ($dayPublished->dayOfWeek === Carbon::THURSDAY)
        {
            $firstDayPrint = $dayPublished->addDays(7);
        }elseif ($dayPublished->dayOfWeek === Carbon::FRIDAY)
        {
            $firstDayPrint = $dayPublished->addDays(6);
        }elseif ($dayPublished->dayOfWeek === Carbon::SATURDAY)
        {
            $firstDayPrint = $dayPublished->addDays(5);
        }elseif ($dayPublished->dayOfWeek === Carbon::SUNDAY)
        {
            $firstDayPrint = $dayPublished->addDays(4);
        }

        for ($i=0; $i < 4; $i++){
            if ($i == 0){
                $dataArray[$i] = $firstDayPrint->toDateString();
            }else{
                $dataArray[$i] = $firstDayPrint->addDays(7)->toDateString();
            }
        }

        return $dataArray;
    }
}