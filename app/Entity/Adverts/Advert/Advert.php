<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/11/2018
 * Time: 15:17
 */

namespace App\Entity\Adverts\Advert;

use App\Entity\Adverts\Category;
use App\Entity\Adverts\Advert\Dialog\Dialog;
use App\Entity\Business\Company;
use App\Entity\Region;
use App\Entity\SystemBalance;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property int $user_id
 * @property int $category_id
 * @property int $region_id
 * @property string $title
 * @property string type
 * @property string $content
 * @property string $slug
 * @property int $price
 * @property int $phone
 * @property int $print_price
 * @property int $count_print
 * @property string $address
 * @property string $company_id
 * @property string $avatar
 * @property string $status
 * @property string $reject_reason
 * @property Carbon $update_at
 * @property Carbon $published_at
 * @property Carbon $expires_at
 *
 * @property Category $category
 * @property Region $region
 * @property Photo $photos
 * @property User $user
 * @property Value[] $values
 * @method Builder forUser(User $user)
 * @method Builder forCompany(Company $company)
 *
 * @method Builder forCategory(Category $category)
 * @method Builder forTitle($value)
 * @method Builder forRegion(Region $region)
 * @method Builder favoredByUser(User $user)
 * @method Builder active()
 */
class Advert extends Model
{
    public const STATUS_DRAFT = 'draft';
    public const STATUS_MODERATION = 'moderation';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_CLOSED = 'closed';

    protected $table = 'advert_adverts';

    protected $guarded = ['id'];

    protected $casts = [
        'published_at' => 'datetime',
        'expires_at' => 'datetime',
    ];

    public static function statusesList(): array
    {
        return [
            self::STATUS_CLOSED => 'closed',
            self::STATUS_DRAFT => 'draft',
            self::STATUS_MODERATION => 'moderation',
            self::STATUS_ACTIVE => 'active',
        ];
    }

    public static function typeListUser():array
    {
        return [
            'A',
            'Б',
            'В',
            'Г',
            'Д',
        ];
    }

    public static function typeListAdmin():array
    {
        return [
            'A',
            'Б',
            'В',
            'Г',
            'Д',
            'Е',
        ];
    }

    public static function timeListUser():array
    {
        return [
            '1 тиждень',
            '2 тижня',
            '3 тижня',
            '4 тижня',
        ];
    }

    public static function timeListAdmin():array
    {
        return [
            '1 тиждень',
            '2 тижня',
            '3 тижня',
            '4 тижня',
            '5 тижнів',
            '6 тижнів',
            '7 тижнів',
            '8 тижнів',
            '9 тижнів',
            '10 тижнів',
            '11 тижнів',
            '12 тижнів',
            '13 тижнів',
            '14 тижнів',
            '15 тижнів',
            '16 тижнів',
            '17 тижнів',
            '18 тижнів',
            '19 тижнів',
            '20 тижнів',
            '21 тиждень',
            '22 тижня',
            '23 тижня',
            '24 тижня',
            '25 тижня',
            '26 тижня',
            '27 тижня',
            '28 тижня',
            '29 тижня',
            '30 тижнів',
            '31 тижня',
            '32 тижня',
            '33 тижня',
            '34 тижня',
            '35 тижня',
            '36 тижня',
            '37 тижня',
            '38 тижня',
            '39 тижня',
            '40 тижнів',
            '41 тижня',
            '42 тижня',
            '43 тижня',
            '44 тижня',
            '45 тижня',
            '46 тижня',
            '47 тижня',
            '48 тижня',
            '49 тижня',
            '50 тижнів',
        ];
    }

    public function sendToModeration(): void
    {
        if (!$this->isDraft()){
            throw new \DomainException('Оголошення не знаходиться в статусі чорновик');
        }
//        if (!count($this->photos)){
//            throw new \DomainException('Upload photos');
//        }
        $this->update([
            'status' => self::STATUS_MODERATION,
            'reject_reason' => null,
            'published_at' => null,
            'expires_at' => null,
        ]);
    }
    public function moderate(Carbon $date): void
    {
        if ($this->status !== self::STATUS_MODERATION){
            flash('Оголошення не відправлено на модерацію ')->important()->info();
            throw new \DomainException('Оголошення не відправлено на модерацію');
        }

        $this->update([
            'published_at' => $date,
            'expires_at' => $date->copy()->addDays(7 * $this->count_print),
            'status' => self::STATUS_ACTIVE,
        ]);

    }

    public function moderateAdmin(Carbon $date): void
    {
        $category = Category::findOrFail($this->category_id);
        if ($this->isClosed()){
            $this->update([
                'published_at' => $date,
                'expires_at' => $date->copy()->addDays(7),
                'count_print' => 1,
                'status' => self::STATUS_ACTIVE,
            ]);

            $this->calcPrice($category);

        }else{
            $this->update([
                'published_at' => $date,
                'expires_at' => $date->copy()->addDays(7 * $this->count_print),
                'status' => self::STATUS_ACTIVE,
            ]);
        }

    }

    public function sendToDraft(): void
    {
        $this->update([
            'status' => self::STATUS_DRAFT,
        ]);
    }

    public function reject($reason): void
    {
        $this->update([
            'status' => self::STATUS_DRAFT,
            'reject_reason' => $reason,


        ]);
    }

    public function expire(): void
    {
        $this->update([
            'status' => self::STATUS_CLOSED,

        ]);
    }

    public function close(): void
    {
        $this->update([
            'status' => self::STATUS_CLOSED,

        ]);
    }

    public function writeClientMessage(int $fromId, string $message): void
    {
         $this->getOrCreateDialogWith($fromId)->writeMessage($fromId, $message);

    }

    public function writeOwnerMessage(int $toId, string $message): void
    {
        if ($this->isClosed()){
            flash('Оголошення закрите, активуйте оголошення щоб відповісти')->info();
            throw new \DomainException();
        }
        $this->getDialogWith($toId)->writeMessage(Auth::id(), $message);
    }

    public function readClientMessage(int $userId): void
    {
        $this->getDialogWith($userId)->readByClient();
    }

    public function readOwnerMessage(int $userId): void
    {
        $this->getDialogWith($userId)->readByOwner();
    }

    public function getDialogWith(int $userId): Dialog
    {
        $dialog = $this->dialogs()->where([
            'user_id' => $this->user_id,
            'client_id' => $userId,
        ])->first();
        if (!$dialog){
            flash('Діалог з даним користувачем не розпочатий')->info();
            throw new \DomainException();
        }
        return $dialog;
    }

    public function getOrCreateDialogWith(int $userId): Dialog
    {
        if ($userId === $this->user_id){
            flash('Ви намагаєтесь написати самому собі')->info();
        }

        return $this->dialogs()->firstOrCreate([

            'user_id' => $this->user_id,
            'advert_id' => $this->id,
            'client_id' => $userId,
        ]);

    }

    public function getValue($id)
    {
        foreach ($this->values as $value){
            if ($value->attribute_id === $id){
                return $value->value;
            }
        }
        return null;
    }

    public function isDraft(): bool
    {
        return $this->status === self::STATUS_DRAFT;
    }

    public function isClosed(): bool
    {
        return $this->status === self::STATUS_CLOSED;
    }

    public function hasPhone(): bool
    {
        return $this->phone !== null;
    }

    public function isOnModeration()
    {
        return $this->status === self::STATUS_MODERATION;
    }

    public function canAddPhoto()
    {
    $photos =  DB::table('advert_advert_photos')->where('advert_id', $this->id)->count();
    return $photos < 4;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isCompany(): bool
    {
        return $this->company_id !== null;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }

    public function values()
    {
        return $this->hasMany(Value::class, 'advert_id', 'id');
    }

    public function photos()
    {
        return $this->hasMany(Photo::class, 'advert_id', 'id');
    }

    public function scopeForUser(Builder $query, User $user)
    {
        return $query->where('user_id', $user->id);
    }

    public function scopeForCompany(Builder $query, Company $company)
    {
        return $query->where('company_id', $company->id);
    }

    public function scopeForTitle(Builder $query, $value)
    {

            return $query->where('title', 'like', '%' . $value . '%');
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }


    public function scopeForCategory(Builder $query, Category $category)
    {
        return $query->whereIn('category_id', array_merge(
            [$category->id],
            $category->descendants()->pluck('id')->toArray()
        ));
    }

    public function scopeForRegion(Builder $query, Region $region)
    {
        $ids = [$region->id];
        $childrenIds = $ids;
        while ($childrenIds = Region::where(['parent_id' => $childrenIds])->pluck('id')->toArray()){
            $ids = array_merge($ids, $childrenIds);
        }
        return $query->whereIn('region_id', $ids);
    }

    public function favorites()
    {
        return $this->belongsToMany(User::class, 'advert_favorites', 'advert_id', 'user_id');
    }

    public function dialogs()
    {
        return $this->hasMany(Dialog::class, 'advert_id', 'id');
    }

    public function scopeFavoredByUser(Builder $query, User $user)
    {
        return $query->whereHas('favorites', function (Builder $query) use ($user){
            $query->where('user_id', $user->id);
        });
    }

    public function calcPrice(Category $category){

            if ($this->type == 'A'){
                if ($category->getPrice() != 0){
                    $this->print_price = $this->count_print * $category->getPrice();
                    $this->save();
                }else{
                    $this->print_price = 0;
                    $this->save();
                }
            }elseif($this->type == 'Б'){
                $this->print_price = $this->count_print * 9;
                $this->save();
            }elseif($this->type == 'В'){
                $this->print_price = $this->count_print * 20;
                $this->save();
            }elseif($this->type == 'Г'){
                $this->print_price = $this->count_print * 25;
                $this->save();
            }elseif($this->type == 'Д'){
                $this->print_price = $this->count_print * 30;
                $this->save();
            }else{
                $this->print_price = 0;
                $this->save();
            }

        SystemBalance::create([
            'type' => SystemBalance::STATUS_ADVERT,
            'sum' => $this->print_price,
            'user_id' => \Auth::id(),
        ]);

        }



        public function dataPrint($data)
        {
            $dataArray =[];
            $dayPublished = Carbon::createFromFormat('Y-m-d H:s:i', $data);
            $standart = Carbon::createFromFormat('Y-m-d H:s:i', '2015-10-10 15:00:00');
            if ($dayPublished->dayOfWeek === Carbon::MONDAY) {
                $firstDayPrint = $dayPublished->addDays(3);
            }elseif ($dayPublished->dayOfWeek === Carbon::TUESDAY)
            {
                if ($standart->format('H:i') > $dayPublished->format('H:i')) {
                    $firstDayPrint = $dayPublished->addDays(2);
                }else{
                    $firstDayPrint = $dayPublished->addDays(9);
                }
            }elseif ($dayPublished->dayOfWeek === Carbon::WEDNESDAY)
            {
                $firstDayPrint = $dayPublished->addDays(8);
            }elseif ($dayPublished->dayOfWeek === Carbon::THURSDAY)
            {
                $firstDayPrint = $dayPublished->addDays(7);
            }elseif ($dayPublished->dayOfWeek === Carbon::FRIDAY)
            {
                $firstDayPrint = $dayPublished->addDays(6);
            }elseif ($dayPublished->dayOfWeek === Carbon::SATURDAY)
            {
                $firstDayPrint = $dayPublished->addDays(5);
            }elseif ($dayPublished->dayOfWeek === Carbon::SUNDAY)
            {
                $firstDayPrint = $dayPublished->addDays(4);
            }

            for ($i=0; $i < $this->count_print; $i++){
                if ($i == 0){
                    $dataArray[$i] = $firstDayPrint->toDateString();
                }else{
                    $dataArray[$i] = $firstDayPrint->addDays(7)->toDateString();
                }
            }

            return $dataArray;
        }

}