<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/11/2018
 * Time: 15:27
 */

namespace App\Entity\Adverts\Advert;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int $attribute_id
 * @property int $value
 */
class Value extends Model
{
    protected $table = 'advert_advert_values';

    public $timestamps = false;

    protected $fillable = ['attribute_id', 'value'];
}