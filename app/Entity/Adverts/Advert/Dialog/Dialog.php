<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 07/12/2018
 * Time: 00:32
 */

namespace App\Entity\Adverts\Advert\Dialog;


use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use App\User;
/**
 * @property int $id
 * @property int $user_id
 * @property int $advert_id
 * @property int $client_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $user_new_message
 * @property int $client_new_message
 * @method Builder forUser(User $user)
 */
class Dialog extends Model
{
    protected $table = 'advert_dialogs';

    protected $guarded = ['id'];

    public function writeMessage(int $userId, string $message): void
    {
        if ($userId !== $this->user_id && $userId !== $this->client_id){
            flash('Помилка. Невідомий системі діалог')->error();
            throw new \DomainException();
        }
        $this->messages()->create([
            'user_id' => $userId,
            'message' => $message,
        ]);
        if ($userId === $this->user_id){
            $this->user_new_message++;
        }
        if ($userId === $this->client_id){
            $this->client_new_message++;
        }
        $this->save();
    }

    public function readByOwner()
    {
        $this->user_new_message = null;
        $this->saveOrFail();
    }

    public function readByClient()
    {
        $this->client_new_message = null;
        $this->saveOrFail();
    }


    public function messages()
    {
        return $this->hasMany(Message::class, 'dialog_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo(User::class, 'client_id', 'id');
    }

    public function scopeForUser(Builder $query, User $user)
    {
        return $query->where('user_id', $user->id)->orWhere('client_id', $user->id);
    }

}