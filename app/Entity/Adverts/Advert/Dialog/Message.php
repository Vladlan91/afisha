<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 08/12/2018
 * Time: 15:34
 */

namespace App\Entity\Adverts\Advert\Dialog;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
/**
 * @property int $id
 * @property int $user_id
 * @property string $message
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Message extends Model
{
    protected $table = 'advert_dialog_messages';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}