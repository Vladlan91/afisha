<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 05/11/2018
 * Time: 15:33
 */

namespace App\Entity\Adverts\Advert;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $file
 */

class Photo extends Model
{
    protected $table = 'advert_advert_photos';

    public $timestamps = false;

    protected $fillable = ['advert_id','file'];
}
