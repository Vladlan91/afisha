<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 16/11/2018
 * Time: 16:01
 */

namespace App\Console\Commands\Advert;


use App\Entity\Adverts\Advert\Advert;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\UseCase\Adverts\AdvertService;

class ExpireCommand extends Command
{
    protected $signature = 'advert:expire';
    protected $description = 'Date description';

    private $service;

    public function __construct(AdvertService $service)
    {
        parent::__construct();
        $this->service = $service;

    }
    public function handle() : bool {

        $success = true;

        foreach (Advert::active()->where('expired_at', '<', Carbon::now())->cursor() as $advert){
            try{
                $this->service->expire($advert);
            }catch (\DomainException $e){
                $this->error($e->getMessage());
                $success = false;
            }
        }
        return $success;
    }
}