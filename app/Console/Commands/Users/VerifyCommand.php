<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 23/10/2018
 * Time: 17:18
 */

namespace App\Console\Commands\Users;


use App\UseCase\Auth\RegisterService;
use App\User;
use Illuminate\Console\Command;

class VerifyCommand extends Command
{
    private $service;
    protected $description = 'Command description';
    protected $signature = 'user:verify {email}';

    public function __construct(RegisterService $service)
    {
        parent::__construct();
        $this->service = $service;

    }

    public function handle() : bool {
        $email = $this->argument('email');

        if (!$user = User::where('email', $email)->first()){
            $this->error('Undefined user fore email' .- $email);
            return false;
        }

        $this->service->verify($user->id);
        $this->info('Success');
        return true;
    }
}