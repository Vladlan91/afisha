<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 23/10/2018
 * Time: 18:56
 */

namespace App\Console\Commands\Users;



use App\User;
use Illuminate\Console\Command;

class RoleCommand extends Command
{
    protected $description = 'Set role of user';
    protected $signature = 'user:role {email} {role}';

    public function handle() : bool {
        $email = $this->argument('email');
        $role = $this->argument('role');

        if (!$user = User::where('email', $email)->first()){
            $this->error('Undefined user fore email' .- $email);
            return false;
        }
        try{
            $user->changeRole($role);
        }catch (\DomainException $e){
            $this->error($e->getMessage());
            return false;
        }
        $this->info('Success');
        return true;
    }
}
