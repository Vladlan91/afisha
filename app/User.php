<?php

namespace App;

use App\Entity\Adverts\Advert\Advert;
use App\Entity\Order;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\User as Authenticatable;
/**
 * @property int $id
 * @property string $name
 * @property string $last_name
 * @property string $avatar
 * @property string $phone
 * @property bool $phone_verified
 * @property bool $phone_auth
 * @property string $email
 * @property string $provider
 * @property string $status
 * @property double $balance
 * @property string $password
 * @property string $verify_token
 * @property string $phone_verify_token
 * @property Carbon $phone_verify_token_expire
 * @property string $role
 *
*/
class User extends Authenticatable
{
    use Notifiable;

    public const STATUS_WAITE = 'waite';
    public const STATUS_ACTIV = 'activ';

    public const ROLE_ADMIN = 'admin';
    public const ROLE_MODERATOR = 'moderator';
    public const ROLE_USER = 'user';


    protected $fillable = [
        'id', 'name', 'last_name', 'balance','address','phone', 'email', 'password', 'status', 'verify_token', 'role', 'avatar', 'provider', 'provider_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'phone_verified' => 'boolean',
        'phone_verify_token_expire' => 'datetime',
        'phone_auth' => 'boolean',
    ];

    public static function rolesList(): array
    {
        return [
            self::ROLE_USER => 'User',
            self::ROLE_ADMIN => 'Admin',
            self::ROLE_MODERATOR => 'Moderator',
        ];
    }

    public static function register(string $name, string $email, string $password):self
    {
        return static::create([
            'name' => $name,
            'email' => $email,
//            'password' => bcrypt($password),
            'password' => bcrypt(bcrypt($password)),
            'verify_token' => Str::random(),
            'status' => self::STATUS_WAITE,
            'role' => self::ROLE_USER,
        ]);
    }

    public static function new($name, $email):self
    {
        return static::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt(Str::random()),
            'status' => self::STATUS_ACTIV,
            'role' => self::ROLE_USER,
        ]);
    }

    public function isWait() : bool
    {
        return $this->status === self::STATUS_WAITE;
    }
    public function isActive() : bool
    {
        return $this->status === self::STATUS_ACTIV;
    }

    public function verify() : void
    {
        if (!$this->isWait()){
            throw new \DomainException('User is already verified');
        }

        $this->update([
            'status' => self::STATUS_ACTIV,
            'verify_token' =>null,
        ]);
    }

    public  function isAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }

    public function isModerator(): bool
    {
        return $this->role === self::ROLE_MODERATOR;
    }

    public function changeRole($role): void
    {
        if (!\in_array($role, [self::ROLE_ADMIN, self::ROLE_USER], true)){
            throw new \InvalidArgumentException('Undefined role "' . $role . '"');
        }
        if ($this->role === $role){
            throw new \DomainException('Role is already assigned');
        }
        $this->update(['role'=> $role]);
    }

    public function unverifyPhone(){
        $this->phone_verified = false;
        $this->phone_verify_token = null;
        $this->phone_verify_token_expire = null;
        $this->saveOrFail();
    }

    public function requestPhoneVerification(Carbon $now)
    {
        if (empty($this->phone)){
            throw new \DomainException();
        }
        if (!empty($this->phone_verify_token) && $this->phone_verify_token_expire && $this->phone_verify_token_expire->gt($now)){
            throw new \DomainException();
        }
        $this->phone_verified = false;
        $this->phone_verify_token = (string)random_int(10000, 99999);
        $this->phone_verify_token_expire = $now->copy()->addSecond(300);
        $this->saveOrFail();

        return $this->phone_verify_token;
    }

    public function verifyPhone($token, Carbon $now) : void
    {
        if ($token !== $this->phone_verify_token){
            throw new \DomainException();
        }
        if ($this->phone_verify_token_expire->lt($now)){
            throw new \DomainException();
        }
        $this->phone_verified = true;
        $this->phone_verify_token = null;
        $this->phone_verify_token_expire = null;
        $this->saveOrFail();
    }

    public function isPhoneVerified(){
        return $this->phone_verified === true;
    }

    public function hasPhone(){
        return $this->phone !== null;
    }
    public function isFacebook(){
        return $this->provider === 'facebook';
    }
    public function isGoogle(){
        return $this->provider === 'google';
    }

    public function isPhoneAuthEnabled(){
        return $this->phone_auth === true;
    }

    public function hasMoney(){
        return $this->balance > 0;
    }

    public function disablePhoneAuth(){
        $this->phone_auth = false;
        $this->saveOrFail();
    }

    public function enablePhoneAuth(){
        $this->phone_auth = true;
        $this->saveOrFail();
    }

    public function hasFilledProfile(){
        if (empty($this->name)){
            flash('Заповніть будь ласка Ваше Ім\'я')->important()->error();
            return false;
        }elseif (empty($this->last_name)){
            flash('Заповніть будь ласка Ваше Прізвище')->important()->error();
            return false;
        }
        elseif (empty($this->email)){
            flash('Заповніть будь ласка Ваш емейл')->important()->error();
            return false;
        }
        elseif (!$this->hasPhone()){
            flash('Заповніть будь ласка Ваш телефон')->important()->error();
            return false;
        }else{
            return true;
        }
        return true;
    }

    public function favorites()
    {
        return $this->belongsToMany(Advert::class, 'advert_favorites', 'user_id', 'advert_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }

    public function addToFavorites($advertId)
    {
        if ($this->hasInFavorites($advertId)){
            throw new \DomainException('Оголошення вже знаходиться у вподобаних');
        }
        $this->favorites()->attach($advertId);
    }

    public function hasInFavorites($id): bool
    {
        return $this->favorites()->where('id', $id)->exists();
    }

    public function removeFromFavorites($advertId)
    {
        $this->favorites()->detach($advertId);
    }

    public function adverts()
    {
        return $this->hasMany(Advert::class, 'user_id', 'id');
    }

    public function haveMoney($price)
    {
        return $this->balance >= $price;
    }

    public function withdraw ($price)
    {
        $this->balance -= $price;
        $this->save();
    }

}
