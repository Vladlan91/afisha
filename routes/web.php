<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');

Route::get('cron/advert', 'CronController@advert')->name('cron.advert');
Route::get('cron/banner', 'CronController@banner')->name('cron.banner');
Route::post('/update', 'Admin\Adverts\AdvertController@update');
Route::post('/getCategory', 'Admin\Adverts\AdvertController@getCategory')->name('autocomplete.fetch');
Route::post('/moderationAdvert', 'Admin\Adverts\AdvertController@moderationAdvert')->name('moderation.advert');
Route::post('/orderBanner', 'Admin\Adverts\AdvertController@orderBanner')->name('order.banner');
Route::get('login/facebook', 'Auth\LoginController@redirectToFacebookProvider')->name('login.facebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderFacebookCallback');
Route::get('login/google', 'Auth\LoginController@redirectToGoogleProvider')->name('login.google');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderGoogleCallback');
Route::get('/download', function (){
        $file = public_path()."/afisha.xml";
        $headers = array(
            'Content-Type: text/plain'
        );
        return Response::download($file, 'Оголошення-афіша.xml', $headers);
});
Auth::routes();
Route::put('/message', 'HomeController@message')->name('message');
Route::get('/news/category/{category?}', 'NewsController@index')->name('news');
Route::get('/suggestions/show/{company}/{suggestions}', 'SuggestionsController@show')->name('suggestions.show');
Route::get('/company/{business?}/{region?}', 'CompanyController@index')->name('company');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');
Route::get('/banner/get', 'BannerController@get')->name('banner.get');
Route::get('/banner/{banner}/click', 'BannerController@click')->name('banner.click');
Route::get('/admin/regions/create/{id}', 'Admin\RegionController@create')->name('admin.regions.create');
Route::get('/admin/moderate', 'Admin\ModerationController@index')->name('admin.moderate.index');
Route::post('company/search', 'CompanyController@search')->name('company.search');
Route::group(
    [
        'prefix' => 'adverts',
        'as' => 'adverts.',
        'namespace' => 'Adverts',
    ],
    function (){
        Route::get('/{region?}', 'AdvertController@showTown')->name('showTown');
        Route::post('/search', 'AdvertController@search')->name('search');
        Route::post('/searchAtr/{category?}', 'AdvertController@searchAtr')->name('search.atr');
        Route::get('/show/{advert?}', 'AdvertController@show')->name('show');
        Route::get('/show/user/{user}/all', 'AdvertController@showuserall')->name('show.user.all');
        Route::get('/show/company/{company}/all', 'AdvertController@showcompanyall')->name('show.company.all');
        Route::get('/{advert}/photos', 'AdvertController@photos')->name('photos');
        Route::put('/{advert}/photos', 'AdvertController@save')->name('photos.save');
        Route::post('/show/{advert}/phone', 'AdvertController@phone')->name('phone');
        Route::get('/all/{category?}', 'AdvertController@all')->name('all');
        Route::get('/{region?}/{category?}', 'AdvertController@index')->name('index');
        Route::post('favorites/{advert}', 'FavoriteController@add')->name('favorites');
        Route::delete('favorites/{advert}', 'FavoriteController@remove')->name('favorites');

        Route::post('/message/{advert}', 'MessageController@sendmessage')->name('message');
        Route::post('/message/{advert}/send', 'MessageController@send')->name('message.send');
    }
);
Route::group(
    [
        'prefix' => 'cabinet',
        'as' => 'cabinet.',
        'namespace' => 'Cabinet',
        'middleware' => ['auth'],
    ],
    function (){
        Route::get('/', 'HomeController@index')->name('home');

        Route::group(['prefix' =>'profile', 'as' => 'profile.' ], function (){
            Route::get('/', 'ProfileController@index')->name('home');
            Route::get('/edit', 'ProfileController@edit')->name('edit');
            Route::put('/update', 'ProfileController@update')->name('update');
            Route::post('/phone', 'PhoneController@request');
            Route::get('/phone', 'PhoneController@form')->name('phone');
            Route::put('/phone', 'PhoneController@verify')->name('phone.verify');

            Route::post('/phone/auth', 'PhoneController@auth')->name('phone.auth');
        });
        Route::get('/favorites', 'FavoriteController@index')->name('favorites.index');
        Route::delete('favorites/{advert}', 'FavoriteController@remove')->name('favorites.remove');
        Route::resource('tickets','TicketController')->only(['index', 'show', 'create', 'store', 'destroy']);
        Route::post('tickets/{ticket}/message', 'TicketController@message')->name('tickets.message');
        Route::group(
            [
                'prefix' => 'company',
                'as' => 'company.',
                'namespace' => 'Company',
                'middleware' => [App\Http\Middleware\FilledProfile::class],
            ],
            function (){
                Route::get('/', 'CompanyController@index')->name('index');
                Route::get('/create/{business?}', 'CompanyController@business')->name('create');
                Route::get('/create/region/{business}/{region?}', 'CompanyController@region')->name('create.region');
                Route::get('/create/company/{business}/{region?}', 'CompanyController@company')->name('create.company');
                Route::post('/create/company/{business}/{region?}', 'CompanyController@store')->name('create.company.store');
                Route::get('/{company}/edit', 'CompanyController@editForm')->name('edit');
                Route::put('/{company}/edit', 'CompanyController@edit');
                Route::post('/{company}/send', 'CompanyController@send')->name('send');
                Route::delete('/{company}/destroy', 'CompanyController@destroy')->name('destroy');
            });
        Route::group(
            [
                'prefix' => 'adverts',
                'as' => 'adverts.',
                'namespace' => 'Adverts',
                'middleware' => [App\Http\Middleware\FilledProfile::class],
            ],
            function (){
                Route::get('/', 'AdvertController@index')->name('index');
                Route::get('/create/{category?}', 'CreateController@category')->name('create');
                Route::get('/create/region/{category}/{region?}', 'CreateController@region')->name('create.region');
                Route::get('/create/advert/{category}/{region?}', 'CreateController@advert')->name('create.advert');
                Route::post('/create/advert/{category}/{region?}', 'CreateController@store')->name('create.advert.store');
                Route::get('/{advert}/edit', 'ManageController@editForm')->name('edit');
                Route::put('/{advert}/edit', 'ManageController@edit');
                Route::get('/{advert}/photos', 'ManageController@photosForm')->name('photos');
                Route::post('/{advert}/photos', 'ManageController@photos');
                Route::get('/{advert}/company', 'ManageController@companyForm')->name('company');
                Route::put('/{advert}/company', 'ManageController@company');
                Route::get('/{advert}/attributes', 'ManageController@attributesForm')->name('attributes');
                Route::post('/{advert}/attributes', 'ManageController@attributes');
                Route::post('/{advert}/send', 'ManageController@send')->name('send');
                Route::post('/{advert}/close', 'ManageController@close')->name('close');
                Route::delete('/{advert}/destroy', 'ManageController@destroy')->name('destroy');
            });
        Route::group(
            [
                'prefix' => 'banners',
                'as' => 'banners.',
                'namespace' => 'Banners',
                'middleware' => [App\Http\Middleware\FilledProfile::class],
            ],
            function (){
                Route::get('/', 'BannerController@index')->name('index');
                Route::get('/create/{category?}', 'CreateController@category')->name('create');
                Route::get('/all/{category?}', 'CreateController@all')->name('create.reg');
                Route::get('/create/banner/all/{region?}', 'CreateController@bannerall')->name('create.banner.all');
                Route::post('/create/banner/all/{region?}', 'CreateController@storeall')->name('create.banner.storeall');
                Route::get('/create/region/{category?}/{region?}', 'CreateController@region')->name('create.region');
                Route::get('/create/banner/{category?}/{region?}', 'CreateController@banner')->name('create.banner');
                Route::post('/create/banner/{category?}/{region?}', 'CreateController@store')->name('create.banner.store');
                Route::get('/{banner}/show', 'BannerController@show')->name('show');
                Route::get('/{banner}/edit', 'BannerController@editForm')->name('edit');
                Route::put('/{banner}/edit', 'BannerController@edit');
                Route::get('/{banner}/file', 'BannerController@fileForm')->name('file');
                Route::put('/{banner}/file', 'BannerController@file');
                Route::post('/{banner}/send', 'BannerController@send')->name('send');
                Route::post('/{banner}/cancel', 'BannerController@cancel')->name('cancel');
                Route::post('/{banner}/order', 'BannerController@order')->name('order');
                Route::delete('/{banner}/destroy', 'BannerController@destroy')->name('destroy');
            });
        Route::group(
            [
                'prefix' => 'messages',
                'as' => 'messages.',
            ],
            function (){
                Route::get('/', 'MessageController@index')->name('index');
                Route::get('/message/show/{dialog}', 'MessageController@show')->name('show');
                Route::post('/message/{dialog}/send', 'MessageController@send')->name('send');
            });
    }
);
Route::group(
    [
        'prefix' => 'admin',
        'as' => 'admin.',
        'namespace' => 'Admin',
        'middleware' => ['auth', 'can:admin-panel'],
    ],
    function (){
        Route::post('/company/{business}/{company}/send', 'CompanyController@send')->name('company.send');
        Route::get('/', 'HomeController@index')->name('home');
        Route::get('removeUser', 'HomeController@removeUser')->name('home.removeUser');
        Route::get('deleteUser', 'HomeController@deleteUser')->name('home.deleteUser');
        Route::get('removeNews', 'HomeController@removeNews')->name('home.removeNews');
        Route::get('deleteNews', 'HomeController@deleteNews')->name('home.deleteNews');
        Route::get('removeCategory', 'HomeController@removeCategory')->name('home.removeCategory');
        Route::get('deleteCategory', 'HomeController@deleteCategory')->name('home.deleteCategory');
        Route::get('removeOrder', 'HomeController@removeOrder')->name('home.removeOrder');
        Route::get('deleteOrder', 'HomeController@deleteOrder')->name('home.deleteOrder');
        Route::post('/users/{user}/verify', 'UsersController@verify')->name('users.verify');
        Route::resource('users', 'UsersController');
        Route::get('/{user}/balance/add', 'UsersController@addMoneyForm')->name('user.balance.add');
        Route::put('/{user}/balance/add', 'UsersController@addMoney')->name('balance.add.save');
        Route::get('/{user}/balance/get', 'UsersController@getMoneyForm')->name('user.balance.get');
        Route::put('/{user}/balance/get', 'UsersController@getMoney')->name('balance.get.save');
        Route::get('/users/{user}/order', 'UsersController@order')->name('users.order');
        Route::resource('pages', 'PageController');
        Route::get('order', 'OrderController@index')->name('order.index');
        Route::get('order/week', 'HomeController@indexWeek')->name('order.indexWeek');
        Route::get('order/week/month', 'HomeController@indexMonth')->name('order.indexMonth');
        Route::resource('regions', 'RegionController');
        Route::resource('news', 'NewsController');
        Route::resource('newscat', 'NewscatController');
        Route::resource('afisha','AfishaController');
        Route::resource('business', 'BusinessController');
        Route::resource('company', 'CompanyController');
        Route::resource('messages', 'MessagesController');
        Route::group(
            [
                'prefix' => 'suggestions',
                'as' => 'suggestions.',
            ],
            function (){
                Route::get('/', 'SuggestionsController@index')->name('index');
                Route::get('/create/{company?}', 'SuggestionsController@create')->name('create');
                Route::post('/store/{company?}', 'SuggestionsController@store')->name('store');
                Route::get('/show/{suggestions?}', 'SuggestionsController@show')->name('show');
                Route::get('/edit/{suggestions?}', 'SuggestionsController@editForm')->name('edit');
                Route::put('/edit/{suggestions?}', 'SuggestionsController@edit');
                Route::get('/photos/{suggestions?}', 'SuggestionsController@photosForm')->name('photos');
                Route::put('/photos/{suggestions?}', 'SuggestionsController@photos');
                Route::delete('/{suggestions}/destroy', 'SuggestionsController@destroy')->name('destroy');
                Route::delete('/photoDelete/{suggestions}/{photo}', 'SuggestionsController@photoDelete')->name('photoDelete');
            });

        Route::group(
            [
                'prefix' => 'business{business}',
                'as' => 'business.',
            ],
            function (){
                Route::post('/first', 'BusinessController@first')->name('first');
                Route::post('/up', 'BusinessController@up')->name('up');
                Route::post('/down', 'BusinessController@down')->name('down');
                Route::post('/last', 'BusinessController@last')->name('last');
                Route::resource('company', 'CompanyController')->except('index');
            });
        Route::group(
            [
                'prefix' => 'pages{page}',
                'as' => 'pages.',
            ],
            function (){
                Route::post('/first', 'PageController@first')->name('first');
                Route::post('/up', 'PageController@up')->name('up');
                Route::post('/down', 'PageController@down')->name('down');
                Route::post('/last', 'PageController@last')->name('last');
            });
        Route::resource('adverts/categories', 'Adverts\CategoryController');
        Route::group(
            [
                'prefix' => 'adverts',
                'as' => 'adverts.',
                'namespace' => 'Adverts',
            ],
            function (){

                Route::get('/new', 'AdvertController@newForm')->name('new');
                Route::post('/new', 'AdvertController@storeNew')->name('new.store');
                Route::post('/{advert}/edit', 'AdvertController@edit');
                Route::resource('categories', 'CategoryController');
                Route::group(
                    [
                        'prefix' => 'categories{category}',
                        'as' => 'categories.',
                    ],
                    function (){
                Route::post('/first', 'CategoryController@first')->name('first');
                Route::post('/up', 'CategoryController@up')->name('up');
                Route::post('/down', 'CategoryController@down')->name('down');
                Route::post('/last', 'CategoryController@last')->name('last');
                Route::resource('attributes', 'AttributeController')->except('index');
                });
                Route::group(
                    [
                        'prefix' => 'adverts',
                        'as' => 'adverts.',
                    ],
                    function (){
                        Route::get('/', 'AdvertController@index')->name('index');
                        Route::get('/create/{user}/{category?}', 'CreateController@category')->name('create');
                        Route::get('/create/region/{user}/{category}/{region?}', 'CreateController@region')->name('create.region');
                        Route::get('/create/advert/{user}/{category}/{region?}', 'CreateController@advert')->name('create.advert');
                        Route::post('/create/advert/{user}/{category}/{region?}', 'CreateController@store')->name('create.advert.store');
                        Route::post('/{advert}/send', 'AdvertController@send')->name('send');
                        Route::get('/{advert}/edit', 'AdvertController@editForm')->name('edit');
                        Route::put('/{advert}/edit', 'AdvertController@edit');
                        Route::get('/{advert}/photos', 'AdvertController@photosForm')->name('photos');
                        Route::post('/{advert}/photos', 'AdvertController@photos');
                        Route::get('/{advert}/attributes', 'AdvertController@attributesForm')->name('attributes');
                        Route::post('/{advert}/attributes', 'AdvertController@attributes');
                        Route::post('/{advert}/moderate', 'AdvertController@moderate')->name('moderate');
                        Route::post('/{advert}/draft', 'AdvertController@sendToDraft')->name('draft');
                        Route::get('/{advert}/reject', 'AdvertController@rejectForm')->name('reject');
                        Route::post('/{advert}/reject', 'AdvertController@reject');
                        Route::delete('/{advert}/destroy', 'AdvertController@destroy')->name('destroy');
                    });
            });
        Route::group(
            [
                'prefix' => 'banners',
                'as' => 'banners.',
            ],
            function (){
                Route::get('/', 'BannerController@index')->name('index');
                Route::get('/{banner}/show', 'BannerController@show')->name('show');
                Route::get('/{banner}/edit', 'BannerController@editForm')->name('edit');
                Route::put('/{banner}/edit', 'BannerController@edit');
                Route::get('/{banner}/file', 'BannerController@fileForm')->name('file');
                Route::put('/{banner}/file', 'BannerController@file');
                Route::post('/{banner}/moderate', 'BannerController@moderateAdmin')->name('moderateAdmin');
                Route::get('/{banner}/reject', 'BannerController@rejectForm')->name('reject');
                Route::post('/{banner}/reject', 'BannerController@reject');
                Route::post('/{banner}/pay', 'BannerController@pay')->name('pay');
                Route::post('/{banner}/send', 'BannerController@send')->name('send');
                Route::post('/{banner}/order', 'BannerController@orderAdmin')->name('order.admin');
                Route::delete('/{banner}/destroy', 'BannerController@destroy')->name('destroy');
            });
        Route::group(
            [
                'prefix' => 'tickets',
                'as' => 'tickets.',
            ],
            function (){
                Route::get('/', 'TicketController@index')->name('index');
                Route::get('/{ticket}/show', 'TicketController@show')->name('show');
                Route::get('/{ticket}/edit', 'TicketController@editForm')->name('edit');
                Route::put('/{ticket}/edit', 'TicketController@edit');
                Route::get('/{ticket}/message', 'TicketController@message')->name('message');
                Route::post('/{ticket}/close', 'TicketController@close')->name('close');
                Route::post('/{ticket}/approve', 'TicketController@approve')->name('approve');
                Route::post('/{ticket}/reopen', 'TicketController@reopen')->name('reopen');
                Route::delete('/{ticket}/destroy', 'TicketController@destroy')->name('destroy');
            });
    }
);
Route::get('/business/{company_path}', 'CompanyController@show')->name('company.show')->where('company_path', '.+');
Route::get('/news/show/{news_path}', 'NewsController@show')->name('news.show')->where('news_path', '.+');
Route::get('/{page_path}', 'PageController@show')->name('page')->where('page_path', '.+');

