<?php

use App\User;
use App\Entity\Region;
use App\Entity\Page;
use App\Entity\Business\Business;
use App\Entity\Business\Company;
use App\Entity\Adverts\Category;
use App\Entity\Adverts\Advert\Advert;
use App\Entity\Banner\Banner;
use App\Entity\Ticket\Ticket;
use App\Router\AdvertsPath;
use App\Router\PagePath;
use \App\Entity\Adverts\Attribute;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator as Crumbs;

Breadcrumbs::register('home', function (Crumbs $crumbs) {
    $crumbs->push('Головна', route('home'));
});
Breadcrumbs::register('company', function (Crumbs $crumbs) {
    $crumbs->push('Бізнес-каталог', route('company'));
});

Breadcrumbs::register('admin', function (Crumbs $crumbs) {
    $crumbs->push('Головна', route('admin'));
});

Breadcrumbs::register('login', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Вхід', route('login'));
});

Breadcrumbs::register('register', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Реєстрація', route('register'));
});

Breadcrumbs::register('password.request', function (Crumbs $crumbs) {
    $crumbs->parent('login');
    $crumbs->push('Відновлення паролю', route('password.request'));
});

Breadcrumbs::register('password.reset', function (Crumbs $crumbs) {
    $crumbs->parent('password.request');
    $crumbs->push('Змінити', route('password.reset'));
});

/////////////////////////////////////////////////////////

Breadcrumbs::register('cabinet.home', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Кабінет', route('cabinet.home'));
});

Breadcrumbs::register('cabinet.profile.home', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.home');
    $crumbs->push('Профіль', route('cabinet.profile.home'));
});

Breadcrumbs::register('cabinet.favorites.index', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Вподобання', route('cabinet.favorites.index'));
});

Breadcrumbs::register('cabinet.profile.edit', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.profile.home');
    $crumbs->push('Змінити профіль', route('cabinet.profile.edit'));
});

Breadcrumbs::register('cabinet.profile.phone', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.profile.home');
    $crumbs->push('Підтвердження телефона', route('cabinet.profile.phone'));
});
/////////////////////////////////////////////////////////

Breadcrumbs::register('adverts.inner_region', function (Crumbs $crumbs, Region $region = null, Category $category = null) {
    if ($region && $parent = $region->parent){
        $crumbs->parent('adverts.inner_region', $parent, $region);
    }else{
        $crumbs->parent('home');
        //$crumbs->push('Оголошення', route('adverts.index'));
    }
    if ($region){
        $crumbs->push($region->name, route('adverts.index', $region, $category));
    }
});

//Breadcrumbs::register('adverts.inner_region', function (Crumbs $crumbs, AdvertsPath $path) {
//    if ($path->region && $parent = $path->region->parent){
//        $crumbs->parent('adverts.inner_region', $path->withRegion($parent));
//    }else{
//        $crumbs->parent('home');
//        $crumbs->push('Оголошення', route('adverts.index'));
//    }
//    if ($path->region){
//        $crumbs->push($path->region->name, route('adverts.index', $path));
//    }
//});

Breadcrumbs::register('adverts.inner_category', function (Crumbs $crumbs, Region $region = null, Category $category = null) {
    if ($category && $parent = $category->parent){
        $crumbs->parent('adverts.inner_category', $region, $parent);
    }else{
        $crumbs->parent('adverts.inner_region', $region, $category);
    }
    if ($category){
        $crumbs->push($category->name, route('adverts.all', $category));
    }
});
//
//Breadcrumbs::register('adverts.inner_category', function (Crumbs $crumbs, AdvertsPath $path, AdvertsPath $orig) {
//    if ($path->category && $parent = $path->category->parent){
//        $crumbs->parent('adverts.inner_category', $path->withCategory($parent), $orig);
//    }else{
//        $crumbs->parent('adverts.inner_region', $orig);
//    }
//    if ($path->category){
//        $crumbs->push($path->category->name, route('adverts.index', $path));
//    }
//});

Breadcrumbs::register('adverts.index', function (Crumbs $crumbs, Region $region = null, Category $category = null) {
    $crumbs->parent('adverts.inner_category', $region, $category);
});

//Breadcrumbs::register('adverts.index', function (Crumbs $crumbs, AdvertsPath $path = null) {
//    $path = $path ? : adverts_path(null, null);
//    $crumbs->parent('adverts.inner_category', $path, $path);
//});

Breadcrumbs::register('adverts.showTown', function (Crumbs $crumbs, Region $region = null) {
    $crumbs->parent('home');
    $crumbs->push($region->name, route('adverts.showTown', $region));
});

//Breadcrumbs::register('adverts.all', function (Crumbs $crumbs, Category $category) {
//    $crumbs->parent('home');
//    $crumbs->push($category->name, route('adverts.all'));
//});

Breadcrumbs::register('adverts.all', function (Crumbs $crumbs, Category $category, Region $region = null) {
    if ($category && $parent = $category->parent){
        $crumbs->parent('adverts.inner_category', $region, $parent);
    }else{
        $crumbs->parent('home');
    }
    $crumbs->push($category->name, route('adverts.all'));
});

Breadcrumbs::register('adverts.show', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('adverts.index', $advert->region, $advert->category);
    $crumbs->push($advert->title, route('adverts.show', $advert));
});

Breadcrumbs::register('adverts.photos', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('adverts.show', $advert);
    $crumbs->push($advert->title, route('adverts.photos', $advert));
});


Breadcrumbs::register('adverts.message', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('adverts.show', $advert);
    $crumbs->push('Відправка листа', route('adverts.show', $advert));
});


Breadcrumbs::register('cabinet.adverts.edit', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('adverts.index', $advert->region, $advert->category);
    $crumbs->push($advert->title, route('cabinet.adverts.edit', $advert));
});

Breadcrumbs::register('cabinet.adverts.company', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('adverts.index', $advert->region, $advert->category);
    $crumbs->push($advert->title. ' / Прикріплення компанії', route('cabinet.adverts.company', $advert));
});

Breadcrumbs::register('admin.adverts.adverts.edit', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('adverts.index', $advert->region, $advert->category);
    $crumbs->push($advert->title, route('admin.adverts.adverts.edit', $advert));
});

Breadcrumbs::register('admin.adverts.adverts.reject', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('adverts.index');
    $crumbs->push('Оголошення - ' . $advert->title, route('admin.adverts.adverts.reject', $advert));
});

////////////////////////////////////////////////////////

Breadcrumbs::register('cabinet.adverts.index', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.home');
    $crumbs->push('Оголошення', route('cabinet.adverts.index'));
});


Breadcrumbs::register('cabinet.adverts.create', function (Crumbs $crumbs) {
    $crumbs->parent('adverts.index');
    $crumbs->push('Створити оголошення', route('cabinet.adverts.create'));
});

Breadcrumbs::register('cabinet.adverts.create.region', function (Crumbs $crumbs, Category $category, Region $region = null) {
    $crumbs->parent('cabinet.adverts.create');
    $crumbs->push($category->name, route('cabinet.adverts.create.region', [$category, $region ]));
});

Breadcrumbs::register('page', function (Crumbs $crumbs, PagePath $path) {
    if ($parent = $path->page->parent){
        $crumbs->parent('page', $path->withPage($parent));
    }else{
        $crumbs->parent('home');
    }
    $crumbs->push($path->page->title, route('page', $path));
});

Breadcrumbs::register('news.show', function (Crumbs $crumbs, \App\Router\NewPath $path) {
    $crumbs->push($path->news->title, route('news', $path));
});

//Breadcrumbs::register('cabinet.adverts.create.region', function (Crumbs $crumbs, AdvertsPath $path) {
//    if ($parent = $path->region->parent){
//        $crumbs->parent('cabinet.adverts.create.region', $path->withRegion($parent));
//    }else{
//        $crumbs->parent('cabinet.adverts.create.category', $path);
//    }
//    $crumbs->push($path->category->name, route('cabinet.adverts.create.region', $path));
//});

Breadcrumbs::register('cabinet.adverts.create.advert', function (Crumbs $crumbs, Category $category, Region $region = null) {
    $crumbs->parent('cabinet.adverts.create.region', $category, $region);
    $crumbs->push($region ? $region->name : 'Івано-Франківська область', route('cabinet.adverts.create.advert', [$category, $region ]));
});

Breadcrumbs::register('cabinet.company.create', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.company.index');
    $crumbs->push('Створити оголошення', route('cabinet.company.create'));
});


Breadcrumbs::register('cabinet.company.create.region', function (Crumbs $crumbs, Business $business, Region $region = null) {
    $crumbs->parent('cabinet.company.index');
    $crumbs->push('Створити оголошення', route('cabinet.company.create.region', [$business, $region]));
});

Breadcrumbs::register('cabinet.company.create.company', function (Crumbs $crumbs, Business $business, Region $region = null) {
    $crumbs->parent('cabinet.company.index');
    $crumbs->push('Створити оголошення', route('cabinet.company.create.company', [$business, $region]));
});

Breadcrumbs::register('cabinet.company.edit', function (Crumbs $crumbs, Company $company){
    $crumbs->parent('home');
    $crumbs->push('Корегування оголошення', route('cabinet.company.edit', $company));
});

//Breadcrumbs::register('cabinet.adverts.create.advert', function (Crumbs $crumbs, AdvertsPath $path) {
//    $crumbs->parent('cabinet.adverts.create.region', $path);
//    $crumbs->push($path->region ? $path->region->name : 'All', route('cabinet.adverts.create.advert', $path));
//});
/////////////////////////////////////////////////////////

Breadcrumbs::register('admin.home', function (Crumbs $crumbs) {
    $crumbs->push('Адмін панель', route('admin.home'));
});

Breadcrumbs::register('admin.moderate.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Модерація Контенту', route('admin.moderate.index'));
});

Breadcrumbs::register('admin.users.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Користувачі', route('admin.users.index'));
});


Breadcrumbs::register('admin.adverts.adverts.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Оголошення', route('admin.adverts.adverts.index'));
});

Breadcrumbs::register('admin.users.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.users.index');
    $crumbs->push('Створити користувача', route('admin.users.create'));
});

Breadcrumbs::register('admin.adverts.new', function (Crumbs $crumbs) {
    $crumbs->parent('admin.adverts.adverts.index');
    $crumbs->push('Створити оголошення', route('admin.adverts.new'));
});

Breadcrumbs::register('admin.users.show', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('admin.users.index');
    $crumbs->push($user->name, route('admin.users.show', $user));
});

Breadcrumbs::register('admin.users.order', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('admin.users.show', $user);
    $crumbs->push('Історія платежів', route('admin.users.order', $user));
});

Breadcrumbs::register('admin.users.edit', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('admin.users.index', $user);
    $crumbs->push('Змінити користувача', route('admin.users.edit', $user));
});

/////////////////////////////////////////////////////////

Breadcrumbs::register('admin.regions.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Регіони', route('admin.regions.index'));
});

Breadcrumbs::register('admin.regions.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.regions.index');
    $crumbs->push('Створити регіон', route('admin.regions.create'));
});

Breadcrumbs::register('admin.regions.show', function (Crumbs $crumbs, Region $region) {
    if ($parent = $region->parent){
        $crumbs->parent('admin.regions.show', $parent);
    }else{
        $crumbs->parent('admin.regions.index');
    }
    $crumbs->push($region->name, route('admin.regions.show', $region));
});

Breadcrumbs::register('admin.regions.edit', function (Crumbs $crumbs, Region $region) {
    $crumbs->parent('admin.regions.index', $region);
    $crumbs->push('Змінити регіон', route('admin.regions.edit', $region));
});

/////////////////////////////////////////////////////////

Breadcrumbs::register('admin.adverts.categories.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Категорії', route('admin.adverts.categories.index'));
});

Breadcrumbs::register('admin.adverts.categories.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.adverts.categories.index');
    $crumbs->push('Створити категорію', route('admin.adverts.categories.create'));
});

Breadcrumbs::register('admin.adverts.categories.show', function (Crumbs $crumbs, Category $category) {
    if ($parent = $category->parent){
        $crumbs->parent('admin.adverts.categories.show', $parent);
    }else{
        $crumbs->parent('admin.adverts.categories.index');
    }
    $crumbs->push($category->name, route('admin.adverts.categories.show', $category));
});

Breadcrumbs::register('admin.adverts.categories.edit', function (Crumbs $crumbs, Category $category) {
    $crumbs->parent('admin.adverts.categories.index', $category);
    $crumbs->push('Змінити категорію', route('admin.adverts.categories.edit', $category));
});

/////////////////////////////////////////////////////////

Breadcrumbs::register('admin.adverts.categories.attributes.create', function (Crumbs $crumbs, Category $category) {
    $crumbs->parent('admin.adverts.categories.show', $category);
    $crumbs->push('Створити атрібути категорії', route('admin.adverts.categories.attributes.create', $category));
});

Breadcrumbs::register('admin.adverts.categories.attributes.show', function (Crumbs $crumbs, Category $category, Attribute $attribute) {
    $crumbs->parent('admin.adverts.categories.show', $category);
    $crumbs->push($attribute->name, route('admin.adverts.categories.attributes.show', [$category, $attribute]));
});

Breadcrumbs::register('admin.adverts.categories.attributes.edit', function (Crumbs $crumbs, Category $category, Attribute $attribute) {
    $crumbs->parent('admin.adverts.categories.index', $category, $attribute);
    $crumbs->push('Змінити атрібути категорії', route('admin.adverts.categories.edit', [$category, $attribute]));
});

//////////////////////////////////////////////////////////

Breadcrumbs::register('cabinet.banners.index', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.home');
    $crumbs->push('Банери', route('cabinet.banners.index'));
});



Breadcrumbs::register('cabinet.banners.show', function (Crumbs $crumbs, Banner $banner) {
    $crumbs->parent('cabinet.banners.index');
    $crumbs->push($banner->name, route('cabinet.banners.show', $banner ));
});

Breadcrumbs::register('cabinet.banners.edit', function (Crumbs $crumbs, Banner $banner) {
    $crumbs->parent('admin.banners.show', $banner);
    $crumbs->push('Корегувати', route('cabinet.banners.edit', $banner));
});

Breadcrumbs::register('cabinet.banners.file', function (Crumbs $crumbs, Banner $banner) {
    $crumbs->parent('admin.banners.show', $banner);
    $crumbs->push('Корегувати', route('cabinet.banners.file', $banner));
});

Breadcrumbs::register('cabinet.banners.create', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.banners.index');
    $crumbs->push('Створити банер', route('cabinet.banners.create'));
});

Breadcrumbs::register('cabinet.banners.create.region', function (Crumbs $crumbs, Category $category, Region $region = null) {
    $crumbs->parent('cabinet.banners.create');
    $crumbs->push($category->name, route('cabinet.banners.create.region', [$category, $region ]));
});
////
Breadcrumbs::register('cabinet.banners.create.reg', function (Crumbs $crumbs, Region $region = null) {
    $crumbs->parent('cabinet.banners.create');
    $crumbs->push('Всі категорії', route('cabinet.banners.create.reg', $region ));
});
////
Breadcrumbs::register('cabinet.banners.create.banner.all', function (Crumbs $crumbs, Region $region = null) {
    $crumbs->parent('cabinet.banners.create.reg', $region);
    $crumbs->push($region ? $region->name : 'Івано-Франківська область', route('cabinet.banners.create.banner.all', $region));
});

Breadcrumbs::register('cabinet.banners.create.banner', function (Crumbs $crumbs, Category $category, Region $region = null) {
    $crumbs->parent('cabinet.banners.create.region', $category, $region);
    $crumbs->push($region ? $region->name : 'Івано-Франківська область', route('cabinet.banners.create.banner', [$category, $region ]));
});

//////////////////////////////////////////////////////////

Breadcrumbs::register('admin.banners.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Банери', route('admin.banners.index'));
});

Breadcrumbs::register('admin.banners.show', function (Crumbs $crumbs, Banner $banner) {
    $crumbs->parent('admin.banners.index');
    $crumbs->push($banner->name, route('admin.banners.show', $banner ));
});

Breadcrumbs::register('admin.banners.edit', function (Crumbs $crumbs, Banner $banner) {
    $crumbs->parent('admin.banners.show', $banner);
    $crumbs->push('Корегувати', route('admin.banners.edit', $banner));
});

Breadcrumbs::register('admin.banners.file', function (Crumbs $crumbs, Banner $banner) {
    $crumbs->parent('admin.banners.show', $banner);
    $crumbs->push('Корегувати', route('admin.banners.file', $banner));
});

Breadcrumbs::register('admin.banners.reject', function (Crumbs $crumbs, Banner $banner) {
    $crumbs->parent('admin.banners.show', $banner);
    $crumbs->push('Відхилення', route('admin.banners.reject', $banner));
});

////////

Breadcrumbs::register('admin.pages.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Статичні сторінки', route('admin.pages.index'));
});

Breadcrumbs::register('admin.pages.show', function (Crumbs $crumbs, Page $page) {
    $crumbs->parent('admin.pages.index');
    $crumbs->push($page->title, route('admin.pages.show', $page));
});

Breadcrumbs::register('admin.pages.edit', function (Crumbs $crumbs, Page $page) {
    $crumbs->parent('admin.pages.show', $page);
    $crumbs->push('Корегування - ' . $page->title, route('admin.pages.edit', $page));
});

Breadcrumbs::register('admin.pages.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.pages.index');
    $crumbs->push('Створення сторінки', route('admin.pages.create'));
});


Breadcrumbs::register('admin.tickets.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Звернення', route('admin.tickets.index'));
});

Breadcrumbs::register('admin.tickets.show', function (Crumbs $crumbs, Ticket $ticket) {
    $crumbs->parent('admin.tickets.index');
    $crumbs->push($ticket->subject, route('admin.tickets.show', $ticket));
});

Breadcrumbs::register('admin.tickets.edit', function (Crumbs $crumbs,Ticket $ticket) {
    $crumbs->parent('admin.tickets.show', $ticket);
    $crumbs->push('Корегування - ' . $ticket->subject, route('admin.tickets.edit', $ticket));
});

Breadcrumbs::register('admin.tickets.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.tickets.index');
    $crumbs->push('Створення', route('admin.tickets.create'));
});


Breadcrumbs::register('cabinet.tickets.index', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.home');
    $crumbs->push('Звернення', route('cabinet.tickets.index'));
});

Breadcrumbs::register('cabinet.tickets.show', function (Crumbs $crumbs, Ticket $ticket) {
    $crumbs->parent('cabinet.tickets.index');
    $crumbs->push($ticket->subject, route('cabinet.tickets.show', $ticket));
});

Breadcrumbs::register('cabinet.tickets.edit', function (Crumbs $crumbs,Ticket $ticket) {
    $crumbs->parent('cabinet.tickets.show', $ticket);
    $crumbs->push('Корегування - ' . $ticket->subject, route('cabinet.tickets.edit', $ticket));
});

Breadcrumbs::register('cabinet.tickets.create', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.tickets.index');
    $crumbs->push('Створення', route('cabinet.tickets.create'));
});

Breadcrumbs::register('cabinet.messages.index', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.home');
    $crumbs->push('Повідомлення', route('cabinet.messages.index'));
});

Breadcrumbs::register('cabinet.messages.show', function (Crumbs $crumbs, \App\Entity\Adverts\Advert\Dialog\Dialog $dialog) {
    $crumbs->parent('cabinet.messages.index');
    $crumbs->push('Повідомлення', route('cabinet.messages.show', $dialog));
});

Breadcrumbs::register('cabinet.company.index', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.home');
    $crumbs->push('Список компаній', route('cabinet.company.index'));
});

Breadcrumbs::register('admin.business.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Бізнес каталог', route('admin.business.index'));
});

Breadcrumbs::register('admin.business.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.business.index');
    $crumbs->push('Cтворити', route('admin.business.create'));
});

Breadcrumbs::register('admin.messages.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Повідомлення', route('admin.messages.index'));
});

Breadcrumbs::register('admin.messages.show', function (Crumbs $crumbs, \App\Entity\Message $message) {
    $crumbs->parent('admin.messages.index');
    $crumbs->push('Повідомлення від '. $message->name, route('admin.messages.show', $message));
});

Breadcrumbs::register('admin.business.show', function (Crumbs $crumbs, Business $business) {
    $crumbs->parent('admin.business.index');
    $crumbs->push($business->name, route('admin.business.show', $business));
});

Breadcrumbs::register('admin.business.edit', function (Crumbs $crumbs, Business $business) {
    $crumbs->parent('admin.business.index');
    $crumbs->push($business->name, route('admin.business.edit', $business));
});

Breadcrumbs::register('admin.business.company.create', function (Crumbs $crumbs, Business $business) {
    $crumbs->parent('admin.business.index');
    $crumbs->push('Cтворити компанію у категорії -'. $business->name, route('admin.business.company.create', $business));
});


Breadcrumbs::register('admin.business.company.show', function (Crumbs $crumbs, Business $business, \App\Entity\Business\Company $company) {
    $crumbs->parent('admin.business.index');
    $crumbs->push($company->title, route('admin.business.company.show', [$business, $company]));
});

Breadcrumbs::register('admin.business.company.edit', function (Crumbs $crumbs, Business $business, \App\Entity\Business\Company $company) {
    $crumbs->parent('admin.business.index');
    $crumbs->push($company->title, route('admin.business.company.edit', [$business, $company]));
});

Breadcrumbs::register('admin.news.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Cписок новин', route('admin.news.index'));
});

Breadcrumbs::register('admin.news.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.news.index');
    $crumbs->push('Cписок новин', route('admin.news.create'));
});

Breadcrumbs::register('admin.news.show', function (Crumbs $crumbs, \App\Entity\News $news) {
    $crumbs->parent('admin.news.index');
    $crumbs->push($news->title, route('admin.news.show', $news));
});

Breadcrumbs::register('admin.afisha.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Банери Афіша', route('admin.afisha.index'));
});

Breadcrumbs::register('admin.afisha.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.afisha.index');
    $crumbs->push('Cтворення оголошення', route('admin.afisha.create'));
});

Breadcrumbs::register('admin.news.edit', function (Crumbs $crumbs, \App\Entity\News $news) {
    $crumbs->parent('admin.news.index');
    $crumbs->push($news->title, route('admin.news.edit', $news));
});

Breadcrumbs::register('admin.newscat.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Cписок категорії', route('admin.newscat.index'));
});

Breadcrumbs::register('admin.newscat.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.newscat.index');
    $crumbs->push('Cписок новин', route('admin.news.create'));
});

Breadcrumbs::register('admin.newscat.show', function (Crumbs $crumbs, \App\Entity\NewsCat $newscat) {
    $crumbs->parent('admin.newscat.index');
    $crumbs->push($newscat->title, route('admin.newscat.show', $newscat));
});

Breadcrumbs::register('admin.newscat.edit', function (Crumbs $crumbs, \App\Entity\NewsCat $newscat) {
    $crumbs->parent('admin.newscat.index');
    $crumbs->push($newscat->title, route('admin.newscat.edit', $newscat));
});

Breadcrumbs::register('admin.company.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Список компаній', route('admin.company.index'));
});

Breadcrumbs::register('admin.suggestions.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Створення пропозиції', route('admin.suggestions.create'));
});

Breadcrumbs::register('admin.suggestions.show', function (Crumbs $crumbs, App\Entity\Suggestions\Suggestions $suggestions) {
    $crumbs->parent('admin.company.index');
    $crumbs->push($suggestions->title, route('admin.suggestions.show'));
});

Breadcrumbs::register('admin.suggestions.edit', function (Crumbs $crumbs, App\Entity\Suggestions\Suggestions $suggestions) {
    $crumbs->parent('admin.home');
    $crumbs->push($suggestions->title, route('admin.suggestions.edit'));
});

Breadcrumbs::register('admin.suggestions.photos', function (Crumbs $crumbs, App\Entity\Suggestions\Suggestions $suggestions) {
    $crumbs->parent('admin.home');
    $crumbs->push($suggestions->title, route('admin.suggestions.photos'));
});

Breadcrumbs::register('suggestions.show', function (Crumbs $crumbs, Company $company, App\Entity\Suggestions\Suggestions $suggestions) {
    $crumbs->parent('company.show', company_path($company));
    $crumbs->push($suggestions->title, route('suggestions.show', [$company, $suggestions]));
});

Breadcrumbs::register('news', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Новини', route('news'));
});

Breadcrumbs::register('adverts.search', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Пошук оголошень', route('adverts.search'));
});

Breadcrumbs::register('company.search', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Пошук компаній', route('company.search'));
});


Breadcrumbs::register('company.show', function (Crumbs $crumbs, \App\Router\CompanyPath $path) {
    $crumbs->parent('home');
    $crumbs->push($path->company->title, route('company.show', $path));
});

Breadcrumbs::register('adverts.show.company.all', function (Crumbs $crumbs, Company $company) {
    $crumbs->parent('home');
    $crumbs->push('Оголошення компанії', route('adverts.show.company.all', $company));
});

Breadcrumbs::register('adverts.show.user.all', function (Crumbs $crumbs, User $avtor) {
    $crumbs->parent('home');
    $crumbs->push('Оголошення автора', route('adverts.show.user.all', $avtor));
});

Breadcrumbs::register('admin.adverts.adverts.create', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('admin.users.show', $user);
    $crumbs->push('Створити оголошення', route('admin.adverts.adverts.create', $user));
});

Breadcrumbs::register('admin.adverts.adverts.create.advert', function (Crumbs $crumbs, User $user, Category $category = null, Region $region = null) {
    $crumbs->parent('admin.users.show', $user);
    $crumbs->push('Створити оголошення від імені - '.$user->name, route('admin.adverts.adverts.create.advert', [$user, $category, $region]));
});

Breadcrumbs::register('admin.adverts.adverts.create.region', function (Crumbs $crumbs, User $user, Category $category = null, Region $region = null) {
    $crumbs->parent('admin.users.show', $user);
    $crumbs->push('Створити оголошення від імені - '.$user->name, route('admin.adverts.adverts.create.region', [$user, $category, $region]));
});

Breadcrumbs::register('admin.order.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Транзакції',  route('admin.order.index'));
});

Breadcrumbs::register('admin.order.indexWeek', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Транзакції',  route('admin.order.indexWeek'));
});

Breadcrumbs::register('admin.order.indexMonth', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Транзакції',  route('admin.order.indexMonth'));
});

Breadcrumbs::register('admin.user.balance.add', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('admin.users.show', $user);
    $crumbs->push('Поповнення рахунку ', route('admin.user.balance.add', $user));
});

Breadcrumbs::register('admin.user.balance.get', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('admin.users.show', $user);
    $crumbs->push('Списання з рахунку ', route('admin.user.balance.get', $user));
});