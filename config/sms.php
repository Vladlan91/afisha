<?php

return [
    'app_id' => env('SMS_UK_APP_ID'),
    'url' => env('SMS_UK_URL')
];