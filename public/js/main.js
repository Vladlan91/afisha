// var animateButton = function(e) {
//
//     e.preventDefault;
//     //reset animation
//     e.target.classList.remove('animate');
//
//     e.target.classList.add('animate');
//     setTimeout(function(){
//         e.target.classList.remove('animate');
//     },700);
// };
//
// var bubblyButtons = document.getElementsByClassName("bubbly-button");
//
// for (var i = 0; i < bubblyButtons.length; i++) {
//     bubblyButtons[i].addEventListener('click', animateButton, false);
// }


$('.banner').each(function () {
    var block = $(this);
    var url = block.data('url');
    var category = block.data('category');
    var region = block.data('region');
    var format = block.data('format');

    axios.get(url, {params:{
            format: format,
            category: category,
            region: region
        }})
        .then(function (response) {
            block.html(response.data);
        })
        .catch(function (error) {
            console.error(error);
        });
});

$(document).on('click', '.location-button', function () {
    var button = $(this);
    var target = $(button.data('target'));
    
     window.geocode_callback = function(response) {
        target.val(response.response.GeoObjectCollection.featureMember['0'].GeoObject.metaDataProperty.GeocoderMetaData.Address.formatted);
    };
    
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var location = position.coords.longitude + ',' + position.coords.latitude;
            var url = 'https://geocode-maps.yandex.ru/1.x/?format=json&lang=uk-UA&callback=geocode_callback&geocode=' + location;
            var script = $('<script>').appendTo($('body'));
            script.attr('src', url);

        }, function (error) {
            console.warn(error.message);
        });
    }else {
        alert(' Не можливо визначити вашу локацію!')
    }

});


