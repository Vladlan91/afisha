<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 23/10/2018
 * Time: 13:35
 */

namespace Tests\Unit\Entity\User;
use App\User;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function testRequest(): void {
        $user = User::register(
            $name = 'name',
            $password = 'password',
            $email = 'email'
        );
        self::assertNotEmpty($user);
        self::assertEquals($name, $user->name);
        self::assertEquals($email, $user->email);
        self::assertNotEmpty($user->password);
        self::assertNotEquals($password, $user->password);
        self::assertTrue($user->isWait());
        self::assertFalse($user->isActive());

    }

    public function testVerify(): void {
        $user = User::register('name', 'email', 'password');

        $user->verify();

        self::assertTrue($user->isWait());
        self::assertFalse($user->isActive());

    }

    public function testAlreadyVerify(): void {
        $user = User::register('name', 'email', 'password');

        $user->verify();
        $this->expectExceptionMessage('User is already verified');
        $user->verify();


    }

}