<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 23/10/2018
 * Time: 14:50
 */

namespace Tests\Unit\Entity\User;
use App\User;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use Illuminate\Foundation\Testing\DatabaseTransactions;

    public function testNew():void
    {
        $user = User::new(
            $name = 'name',
            $email = 'email'
        );

        self::assertNotEmpty($user);
        self::assertEquals($name, $user->name);
        self::assertEquals($email, $user->email);
        self::assertNotEmpty($user->password);
        self::assertTrue($user->isActive());

    }
}