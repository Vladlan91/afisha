@extends('layouts.admin')

@section('content')
    <p><a href="{{ route('admin.news.create') }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Cтворити новину</a></p>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-heading"  style="background-color: #8BC34A !important;" >
            <h2>Список сторінок</h2>
            <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span>
            </div>
        </div>
        <div class="panel-body no-padding" style="display: block;">
        <table class="table table-bordered table-striped pt-2">
            <tbody>
            <tr>
                <th>І'мя</th>
                <th>Посилання</th>
            </tr>
            </tbody>
            @foreach($news as $new)
                <tr>
                    <td>
                        <a href="{{ route('admin.news.show', $new) }}">{{$new->title}}</a>
                    </td>
                    <td>
                        {{$new->slug}}
                    </td>
                </tr>
            @endforeach
        </table>
        </div>
    </div>
@endsection
