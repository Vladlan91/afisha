@extends('layouts.admin')

@section('content')
    <p><div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.news.edit', $news) }}" class="btn btn-primary left">Корегувати</a>
        <form method="POST" action="{{ route('admin.news.destroy', $news) }}">
            {{ csrf_field() }}
            {{method_field('DELETE')}}
            <button class="btn btn-danger left">Видалити</button>
        </form>
    </div></p><br>
    <br>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th class="warning">ID</th> <td>{{$news->id}}</td>
                </tr>
                <tr>
                    <th class="warning">Назва</th> <td>{{$news->title}}</td>
                </tr>
                {{--<tr>--}}
                    {{--<th class="warning">Категорія</th> <td>{{$news->category->title}}</td>--}}
                {{--</tr>--}}
                <tr>
                    <th class="warning">Посилання</th> <td>{{$news->slug}}</td>
                </tr>
                <tr>
                    <th class="warning">Опис "SEO"</th> <td>{{$news->description}}</td>
                </tr>
                <tr>
                    <td colspan="2" class="warning">{!! $news->content !!}</td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
    <div class="card">

    </div>
@endsection