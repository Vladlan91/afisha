@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <form method="POST" action="{{ route('admin.news.update', $news) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{method_field('PUT')}}
        <div class="form-group">
            <label for="title">Назва</label>
            <input id="title" name="title" class="form-control{{ $errors->has('title') ? 'is-invalid': '' }}"
                   value="{{ old('title', $news->title) }}" required>
            @if( $errors->has('title'))
                <span class="invalid-feedback"><stron>{{ $errors->first('title') }}</stron></span>
            @endif
        </div>


        <div class="form-group">
            <img src="{{asset('app/'. $news->avatar)}}" alt="{{ $news->avatar }}" width="50" height="50" style="border-radius: 40px; float: left">
            <label for="uploadbtn" class="uploadButton">Загрузити фото</label>
            <input style="opacity: 0; z-index: -1;" type="file" name="avatar" id="uploadbtn">
        </div>

        <div class="form-group">
            <label for="slug">Посилання</label>
            <input id="slug" name="slug" class="form-control{{ $errors->has('slug') ? 'is-invalid': '' }}"
                   value="{{ old('slug', $news->slug) }}" required>
            @if( $errors->has('slug'))
                <span class="invalid-feedback"><stron>{{ $errors->first('slug') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <label for="parent">Батьківська сторінка</label>
            <select id="category_id" name="category_id" class="form-control{{ $errors->has('category_id') ? 'is-invalid': '' }}">
                <option value=""></option>
                @foreach($newsCategory as $parent)
                    <option value="{{ $parent->id }}"{{ $news->category_id === $parent->id ? 'selected': '' }}>
                        {{ $parent->title }}
                    </option>
                @endforeach
            </select>
            @if( $errors->has('parent'))
                <span class="invalid-feedback"><stron>{{ $errors->first('parent') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <label for="description">Опис "SEO"</label>
            <input id="description" name="description" class="form-control{{ $errors->has('description') ? 'is-invalid': '' }}"
                   value="{{ old('description', $news->description) }}">
            @if( $errors->has('description'))
                <span class="invalid-feedback"><stron>{{ $errors->first('description') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Контент</label>
            <textarea id="content" name="content" class="form-control{{ $errors->has('content') ? 'is-invalid': '' }}"
                      rows="10" required>{{ old('content', $news->content) }}</textarea>
            @if( $errors->has('content'))
                <span class="invalid-feedback"><stron>{{ $errors->first('content') }}</stron></span>
            @endif
        </div>
        @section('js')
            <script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js') }}"></script>
            <script>
                var editor_config = {
                    path_absolute : "{{URL::to('/')}}/",
                    selector: "textarea",
                    plugins: [
                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "emoticons template paste textcolor colorpicker textpattern"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                    relative_urls: false,
                    file_browser_callback : function(field_name, url, type, win) {
                        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                        if (type == 'image') {
                            cmsURL = cmsURL + "&type=Images";
                        } else {
                            cmsURL = cmsURL + "&type=Files";
                        }

                        tinyMCE.activeEditor.windowManager.open({
                            file : cmsURL,
                            title : 'Filemanager',
                            width : x * 0.8,
                            height : y * 0.8,
                            resizable : "yes",
                            close_previous : "no"
                        });
                    }
                };

                tinymce.init(editor_config);
            </script>
        @endsection

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Зберегти</button>
        </div>
    </form>
        </div>
    </div>
@endsection