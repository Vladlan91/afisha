@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <form action="{{ route('admin.afisha.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Назва</label>
                    <input id="title" name="title" class="form-control{{ $errors->has('title') ? 'is-invalid': '' }}"
                           value="{{ old('name') }}" required>
                    @if( $errors->has('title'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('title') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="url">Посилання</label>
                    <input id="url" name="title" class="form-control{{ $errors->has('url') ? 'is-invalid': '' }}"
                           value="{{ old('url') }}" required>
                    @if( $errors->has('url'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('url') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                        <label for="status">Статус</label>
                        <select id="status" class="form-control1 input-search" name="status">
                            @foreach($statuses as $value => $label)
                                <option value="{{ $value }}" {{ $value === request('status') ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                    @if( $errors->has('status'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('status') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="file">Фото</label>
                    <input id="file" name="file" type="file" class="form-control{{ $errors->has('file') ? 'is-invalid' : '' }}" value="{{ old('file') }}" required>
                    @if($errors->has('file'))
                        <span class="invalid-feedback"><strong>{{ $errors->first('file') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
@endsection