@extends('layouts.admin')

@section('content')
    <div style="background-color: azure; position: relative;">
        <div class="card" style="background-color: azure; margin-bottom: 20px">
            <div class="modal-header">
                Фільтер
                <div class="card-body">
                    <form action="?" method="GET">
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label for="id">ID</label>
                                <input class="form-control1 input-search" name="id" id="id" value="{{ request('id') }}">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="status">Статус</label>
                                <select id="status" class="form-control1 input-search" name="status">
                                    <option value=""></option>
                                    @foreach($statuses as $value => $label)
                                        <option value="{{ $value }}" {{ $value === request('status') ? 'selected' : '' }}>{{ $label }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="col-form-label"> </label><br />
                                <button type="submit" class="btn btn-primary">Пошук</button>
                                <a href="?" class="btn btn-primary">Очистити</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <p> <a href="{{ route('admin.afisha.create') }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Створити банер</a></p>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-heading"  style="background-color: #8BC34A !important;" >
            <h2>Список банерів</h2>
            <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
        </div>
        <div class="panel-body no-padding" style="display: block;">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Назва</th>
                    <th>Фото</th>
                    <th>Статус</th>
                    <th>Змінити статус</th>
                </tr>
                </thead>

                <tbody>
                @foreach($banners as $banner)
                    <tr>
                        <td>{{ $banner->id }}</td>
                        <td>{{ $banner->name }}</td>
                        update
                        <th>
                            <img src="{{asset('/app/'.$banner->file)}}" alt="" style="width: 250px; height: auto;">
                        </th>
                        <th>
                            @if($banner->isDraft())
                                <span class="success">Чорновик</span>
                            @else
                                <span class="primary">Активний</span>
                            @endif
                        </th>
                        <td><a href="{{route('admin.afisha.edit', $banner)}}"><i class="lnr lnr-history"  style="text-align: center;"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div style="margin-bottom: 40px;">
        {{ $banners->links() }}
    </div>
@endsection
