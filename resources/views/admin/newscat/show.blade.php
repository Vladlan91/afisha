@extends('layouts.admin')

@section('content')
    <p><div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.newscat.edit', $newscat) }}" class="btn btn-primary left">Корегувати</a>
        <form method="POST" action="{{ route('admin.newscat.destroy', $newscat) }}">
            {{ csrf_field() }}
            {{method_field('DELETE')}}
            <button class="btn btn-danger left">Видалити</button>
        </form>
    </div></p><br>
    <br>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th class="warning">ID</th> <td>{{$newscat->id}}</td>
                </tr>
                <tr>
                    <th class="warning">Назва</th> <td>{{$newscat->title}}</td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
    <div class="card">

    </div>
@endsection