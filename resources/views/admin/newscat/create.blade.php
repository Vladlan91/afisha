@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
    <form action="{{ route('admin.newscat.store') }}" method="POST" novalidate>
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Назва</label>
            <input id="title" name="title" class="form-control{{ $errors->has('title') ? 'is-invalid': '' }}"
                   value="{{ old('name') }}" required>
            @if( $errors->has('title'))
                <span class="invalid-feedback"><stron>{{ $errors->first('title') }}</stron></span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Зберегти</button>
        </div>
    </form>
        </div>
    </div>
@endsection