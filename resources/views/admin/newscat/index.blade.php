@extends('layouts.admin')

@section('content')
    <p><a href="{{ route('admin.newscat.create') }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Cтворити категорію</a></p>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-heading"  style="background-color: #8BC34A !important;" >
            <h2>Список категорїй</h2>
            <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span>
            </div>
        </div>
        <div class="panel-body no-padding" style="display: block;">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Назва</th>
                </tr>
                </thead>

                <tbody>
                @foreach($newscat as $cat)
                    <tr>
                        <td>{{ $cat->id }}</td>
                        <td><a href="{{ route('admin.newscat.show', $cat) }}" target="_self">{{ $cat->title }}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
