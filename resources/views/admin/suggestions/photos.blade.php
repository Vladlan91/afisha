@extends('layouts.admin')

@section('content')
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="" style="padding: 20px">
        <form action="{{ route('admin.suggestions.photos', $suggestions) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{method_field('PUT')}}

            <div class="form-group">
                <label for="uploadbtn" class="uploadButton">Загрузити фото</label>
                <input style="opacity: 0; z-index: -1;" type="file" name="file" id="uploadbtn">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Загрузити</button>
            </div>
        </form>
    </div>

@endsection