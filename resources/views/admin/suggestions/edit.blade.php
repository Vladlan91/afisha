@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <form action="{{ route('admin.suggestions.edit', $suggestions) }}" method="POST" enctype="multipart/form-data" novalidate>
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div class="form-group">
                    <label for="title">Назва</label>
                    <input id="title" name="title" class="form-control{{ $errors->has('title') ? 'is-invalid': '' }}"
                           value="{{ old('name', $suggestions->title) }}" required>
                    @if( $errors->has('title'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('title') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                    <img src="{{asset('app/'. $suggestions->avatar)}}" alt="{{ $suggestions->avatar }}" width="50" height="50" style="border-radius: 40px; float: left">
                    <label for="uploadbtn" class="uploadButton">Загрузити фото</label>
                    <input style="opacity: 0; z-index: -1;" type="file" name="avatar" id="uploadbtn">
                    @if($errors->has('avatar'))
                        <span class="invalid-feedback"><strong>{{ $errors->first('avatar') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="price">Ціна</label>
                    <input id="price" name="price" class="form-control{{ $errors->has('price') ? 'is-invalid': '' }}"
                           value="{{ old('price', $suggestions->price) }}" required>
                    @if( $errors->has('price'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('price') }}</stron></span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="address">Адреса</label>
                    <input id="address" name="address" class="form-control{{ $errors->has('address') ? 'is-invalid': '' }}"
                           value="{{ old('address', $suggestions->address) }}" required>
                    @if( $errors->has('address'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('address') }}</stron></span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="content">Контент</label>
                    <textarea id="content" name="content" class="form-control{{ $errors->has('content') ? 'is-invalid': '' }}"
                              rows="10" required>{{ old('content', $suggestions->content) }}</textarea>
                    @if( $errors->has('content'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('content') }}</stron></span>
                    @endif
                </div>
                @section('js')
                    <script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js') }}"></script>
                    <script>
                        var editor_config = {
                            path_absolute : "{{URL::to('/')}}/",
                            selector: "textarea",
                            language: 'uk',
                            plugins: [
                                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                "searchreplace wordcount visualblocks visualchars code fullscreen",
                                "insertdatetime media nonbreaking save table contextmenu directionality",
                                "emoticons template paste textcolor colorpicker textpattern"
                            ],
                            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                            relative_urls: false,
                            file_browser_callback : function(field_name, url, type, win) {
                                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                                if (type == 'image') {
                                    cmsURL = cmsURL + "&type=Images";
                                } else {
                                    cmsURL = cmsURL + "&type=Files";
                                }

                                tinyMCE.activeEditor.windowManager.open({
                                    file : cmsURL,
                                    title : 'Filemanager',
                                    width : x * 0.8,
                                    height : y * 0.8,
                                    resizable : "yes",
                                    close_previous : "no"
                                });
                            }
                        };

                        tinymce.init(editor_config);
                    </script>
                @endsection
                <div class="form-group">
                    <label for="movie">Відео</label>
                    <textarea id="movie" name="movie" class="form-control{{ $errors->has('movie') ? 'is-invalid': '' }}"
                              rows="10" required>{{ old('movie', $suggestions->movie) }}</textarea>
                    @if( $errors->has('movie'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('movie') }}</stron></span>
                    @endif
                </div>
                @section('js')
                    <script src="{{ URL::to('src/js/vendor/tinymce/js/tinymce/tinymce.min.js') }}"></script>
                    <script>
                        var editor_config = {
                            path_absolute : "{{URL::to('/')}}/",
                            selector: "textarea",
                            language: 'uk',
                            plugins: [
                                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                "searchreplace wordcount visualblocks visualchars code fullscreen",
                                "insertdatetime media nonbreaking save table contextmenu directionality",
                                "emoticons template paste textcolor colorpicker textpattern"
                            ],
                            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                            relative_urls: false,
                            file_browser_callback : function(field_name, url, type, win) {
                                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                                if (type == 'image') {
                                    cmsURL = cmsURL + "&type=Images";
                                } else {
                                    cmsURL = cmsURL + "&type=Files";
                                }

                                tinyMCE.activeEditor.windowManager.open({
                                    file : cmsURL,
                                    title : 'Filemanager',
                                    width : x * 0.8,
                                    height : y * 0.8,
                                    resizable : "yes",
                                    close_previous : "no"
                                });
                            }
                        };

                        tinymce.init(editor_config);
                    </script>
                @endsection
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
@endsection