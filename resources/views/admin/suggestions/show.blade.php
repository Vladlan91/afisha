@extends('layouts.admin')

@section('content')
    <a href="{{ route('admin.suggestions.edit', $suggestions) }}" class="btn btn-primary left"  style="margin-bottom: 20px;margin-top: 20px;">Корегувати</a>
    <a href="{{ route('admin.suggestions.photos', $suggestions) }}" class="btn btn-primary left"  style="margin-bottom: 20px;margin-top: 20px;">Додати фото</a>
    <form method="POST" action="{{ route('admin.suggestions.destroy', $suggestions) }}">
        {{ csrf_field() }}
        {{method_field('DELETE')}}
        <button class="btn btn-danger left"  style="margin-bottom: 20px;margin-top: 20px;">Видалити</button>
    </form>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <table class="table table-striped">
                <tbody>
                <tr class="">
                    <th class="warning">ID</th> <td>{{$suggestions->id}}</td>
                </tr>
                <tr>
                    <th class="warning">Назва</th><td>{{$suggestions->title}}</td>
                </tr>
                <tr>
                    <th class="warning">Компанія</th> <td>{{$suggestions->company->title}}</td>
                </tr>
                <tr>
                    <th class="warning">Адреса</th> <td>{{$suggestions->address}}</td>
                </tr>
                <tr>
                    <th class="warning">Логотип</th> <td><img width="100" height="50" src="{{ asset('/app/'. $suggestions->avatar ) }}" alt="{{$suggestions->title}}"></td>
                </tr>
                <tr>
                    <th class="warning">Koнтент</th> <td>{!!$suggestions->content!!}</td>
                </tr>
                <tr>
                    <th class="warning">Відео</th> <td>{!!$suggestions->movie!!}</td>
                </tr>
                <tr>
                    <th class="warning">Фото</th>
                    <td>
                    @foreach($suggestions->photo()->orderBy('id')->get() as $photo)
                        <div class="col-md-2">
                            <img width="100" height="50" src="{{ asset('/app/'. $photo->file ) }}" alt="{{$suggestions->title}}">
                            <form method="POST" action="{{ route('admin.suggestions.photoDelete', [$suggestions, $photo->id]) }}">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button class="btn btn-danger"  style=" border-radius: 20px; top: -24px; left: -5px; position: relative;">x</button>
                            </form>
                        </div>
                    @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection