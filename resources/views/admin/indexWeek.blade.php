@extends('layouts.admin')

@section('content')
    <div id="page-wrapper" style="">
        <div class="graphs">
            <div class="col-md-4 widget">
                <a href="{{route('admin.home')}}">
                    <div class="r3_counter_box">
                        <div class="stats">
                            <h5 style="">За сьогодні</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 widget">
                <a href="{{ route('admin.order.indexWeek') }}">
                    <div class="r3_counter_box">
                        <div class="stats"  style="background: white!important;">
                            <h5 style=";">За тиждень</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 widget">
                <a href="{{ route('admin.order.indexMonth') }}">
                    <div class="r3_counter_box">
                        <div class="stats">
                            <h5>За місяць</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-mail-forward"></i>
                    <div class="stats">
                        <h5>{{$allSum}} <span>грн</span></h5>
                        <div class="grow" style="width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Поповнено баланс</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-mail-forward"></i>
                    <div class="stats">
                        <h5>{{$allSumUser}}<span>грн</span></h5>
                        <div class="grow" style="width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Поповнено користувачем</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-mail-forward"></i>
                    <div class="stats">
                        <h5>{{$allSumAdmin}} <span>грн</span></h5>
                        <div class="grow" style="width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Поповнено адміністратором</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-users"></i>
                    <div class="stats">
                        <h5>{{$writtenOff}}<span>грн</span></h5>
                        <div class="grow grow1" style="width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Списано з рахунків</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-users"></i>
                    <div class="stats">
                        <h5>{{$writtenOffUser}}<span>грн</span></h5>
                        <div class="grow grow1" style="width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Списано з рахунків користувачем</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-users"></i>
                    <div class="stats">
                        <h5>{{$writtenOffAdmin}}<span>грн</span></h5>
                        <div class="grow grow1" style="width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Списано з рахунків адміністратором</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-eye"></i>
                    <div class="stats">
                        <h5>{{$userAdmin + $userUser}} <span>грн</span></h5>
                        <div class="grow grow3" style="width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Залишок на рахунках</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-eye"></i>
                    <div class="stats">
                        <h5>{{$userUser}}<span>грн</span></h5>
                        <div class="grow grow3" style="width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Залишок на рахунках користувачів</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-eye"></i>
                    <div class="stats">
                        <h5>{{$userAdmin}} <span>грн</span></h5>
                        <div class="grow grow3" style="width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Залишок на рахунках адміністраторів</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-credit-card" style="color: #ff30bb"></i>
                    <div class="stats">
                        <h5>{{$sumBanner}}<span>грн</span></h5>
                        <div class="grow" style="width: 270px!important; left: 18%!important;background-color: #ff30bb">
                            <p style="font-weight: 900;">Оплата публікації банерів</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-bullhorn"></i>
                    <div class="stats">
                        <h5>{{$sumAdvert}}<span>грн</span></h5>
                        <div class="grow grow1" style="background-color: #2d2c2e;width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Оплата публікацій оголошень</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-gears" style="color: #ff710d"></i>
                    <div class="stats">
                        <h5>{{$sumPackage}} <span>грн</span></h5>
                        <div class="grow grow3" style="background-color: #ff710d; width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Оплата тарифних планів</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-credit-card" style="color: #ff30bb"></i>
                    <div class="stats">
                        <h5>{{$systemBalanceBanner}}<span>грн</span></h5>
                        <div class="grow" style="width: 270px!important; left: 18%!important;background-color: #ff30bb">
                            <p style="font-weight: 900;">Створено банерів на суму</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-bullhorn"></i>
                    <div class="stats">
                        <h5>{{$systemBalanceAdverts}}<span>грн</span></h5>
                        <div class="grow grow1" style="background-color: #2d2c2e;width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Створено оголошень на суму</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-gears" style="color: #ff710d"></i>
                    <div class="stats">
                        <h5>{{$sumPackage}} <span>грн</span></h5>
                        <div class="grow grow3" style="background-color: #ff710d; width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Створено тарифних планів на суму</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-credit-card" style="color: #ff30bb"></i>
                    <div class="stats">
                        <h5>{{$sumBanner}}<span>грн</span></h5>
                        <div class="grow" style="width: 270px!important; left: 18%!important;background-color: #ff30bb">
                            <p style="font-weight: 900;">Небаланс по банерам на суму</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-bullhorn"></i>
                    <div class="stats">
                        <h5>{{$sumAdvert}}<span>грн</span></h5>
                        <div class="grow grow1" style="background-color: #2d2c2e;width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Небаланс по оголошень на суму</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 widget">
                <div class="r3_counter_box">
                    <i class="fa fa-gears" style="color: #ff710d"></i>
                    <div class="stats">
                        <h5>{{$sumPackage}} <span>грн</span></h5>
                        <div class="grow grow3" style="background-color: #ff710d; width: 270px!important; left: 18%!important;">
                            <p style="font-weight: 900">Небаланс по  тарифним планам на суму</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div style="margin-bottom:100px; ">

    </div>
    {{--<a style="background-color: red; border-radius: 20px; color: white; margin-top: 20px; padding: 20px;" href="{{route('admin.home.removeUser')}}">Завантаження користувачів</a>--}}
    {{--<a style="background-color: red; border-radius: 20px; color: white; margin-top: 20px; padding: 20px;" href="{{route('admin.home.deleteUser')}}">Видалити користувачів</a>--}}
    {{--<a style="background-color: red; border-radius: 20px; color: white; margin-top: 20px; padding: 20px;" href="{{route('admin.home.removeOrder')}}">Завантаження транзакції</a>--}}
    {{--<a style="background-color: red; border-radius: 20px; color: white; margin-top: 20px; padding: 20px;" href="{{route('admin.home.deleteOrder')}}">Видалити транзакції</a>--}}
@endsection