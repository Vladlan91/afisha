@extends('layouts.admin')

@section('content')
    <p><a href="{{ route('admin.tickets.edit', $ticket) }}" class="btn btn-success" style="float: left;">Корегувати</a></p>
    <p><div class="d-flex flex-row mb-3">
        @if($ticket->isOpen())
            <form  method="POST" action="{{ route('admin.tickets.approve', $ticket) }}">
                {{ csrf_field() }}
                <button class="btn btn-success" style="float: left; margin-left: 5px;">Прийняти</button>
            </form>
        @endif
        @if(!$ticket->isClosed())
            <form  method="POST" action="{{ route('admin.tickets.close', $ticket) }}">
                {{ csrf_field() }}
                <button class="btn btn-success" style="float: left; margin-left: 5px;">Закрити</button>
            </form>
        @endif
        @if($ticket->isClosed())
            <form  method="POST" action="{{ route('admin.tickets.reopen', $ticket) }}">
                {{ csrf_field() }}
                <button class="btn btn-success" style="float: left; margin-left: 5px;">Відкрити</button>
            </form>
        @endif
            <form  method="POST" action="{{ route('admin.tickets.destroy', $ticket) }}">
                {{ csrf_field() }}
                {{method_field('DELETE')}}
                <button class="btn btn-success" style="float: left; margin-left: 5px;">Видалити</button>
            </form>

    </div></p><br>
    <br>
<div class="row">
    <div class="col-md-6">
        <div class="grid_3 grid_5">
            <h3>{{ $ticket->subject }}</h3>
            <div class="but_list">
                <div class="well">
                    {!! nl2br(e($ticket->content)) !!}
                </div>
            </div>
        </div>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <table class="table table-bordered table-striped pt-2">
                <tbody>
                <tr>
                    <th class="warning">ІD</th> <td>{{$ticket->id}}</td>
                </tr>
                <tr>
                    <th class="warning">Створено</th> <td>{{ $ticket->created_at }}</td>
                </tr>
                <tr>
                    <th class="warning">Змінено</th> <td>{{ $ticket->updated_at }}</td>
                </tr>
                <tr>
                    <th class="warning">Тема</th> <td>{{$ticket->subject}}</td>
                </tr>
                <tr>
                    <th class="warning">Користувач</th> <td><a href="{{route('admin.users.show', $ticket->user)}}" target="_blank">{{$ticket->user->name}}</a></td>
                </tr>
                <tr>
                    <th  class="warning">Статус</th>
                    @if($ticket->isOpen())
                        <td><span class="info">Відкритий</span></td>
                    @elseif($ticket->isApproved())
                        <td><span class="primary">Активований</span></td>
                    @elseif($ticket->isClosed())
                        <td><span class="errorss">Закритий</span></td>
                    @endif
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    </div>
    <div class="col-md-6 email-list1">
        <ul class="collection">
            @foreach($ticket->statuses()->orderBy('id')->with('user')->get() as $status)
            <li class="collection-item avatar email-unread">
                <img src="{{ asset('app/'. $status->user->avatar )}}" alt="{{ $status->user->name }}" width="50" height="50" style="border-radius: 40px; float: left; margin-right: 10px;">
                <div class="avatar_left">
                    <span class="email-title">{{ $status->user->name }}</span>
                    <p class="truncate grey-text ultra-small">{{ $ticket->subject }}</p>
                </div>
                @if($status->isOpen())
                    <a href="{{ route('admin.tickets.show', $ticket) }}" class="secondary-content"><span class="new badge1 blue">Відкритий</span></a>
                @elseif($status->isApproved())
                    <a href="{{ route('admin.tickets.show', $ticket) }}" class="secondary-content"><span class="new badge1 blue1">Прийнятий</span></a>
                @elseif($status->isClosed())
                    <a href="{{ route('admin.tickets.show', $ticket) }}" class="secondary-content"><span class="new badge1 red">Закритий</span></a>
                @endif
                <div class="clearfix"> </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
    <br>
    <div class="widget_bottom" style="margin-bottom: 70px;">
        <div class="col-md-12 widget_bottom_left">
            <div class="banner-bottom-video-grid-left">
                @php( $i = 1)
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach($ticket->messages()->orderBy('id')->with('user')->get() as $message)
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading{{ $i }}">
                                <h4 class="panel-title asd">
                                    <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $i }}" aria-expanded="false" aria-controls="collapse{{ $i }}">
                                        <label>{{ $message->created_at }} - {{ $message->user->name }}</label>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{ $i }}" class="panel-collapse collapse {{ $i === 1 ? 'in' : '' }}" role="tabpanel" aria-labelledby="heading{{ $i }}">
                                <div class="panel-body panel_text">
                                    {!! nl2br(e($message->message)) !!}
                                </div>
                            </div>
                        </div>
                        @php( $i++)
                @endforeach
                </div>
            </div>
        </div>
    @if($ticket->allowsMessage())
        <form action="{{ route('admin.tickets.message', $ticket) }}" method="'GET">
            {{ csrf_field() }}
        <div class="form-group">
            <textarea name="message" id="message" cols="30" rows="4" class="form-control{{ $errors->has('message') ? 'is-invalid' : '' }}" required>{{ old('message') }}</textarea>
        @if($errors->has('message'))
            <span class="invalid-feedback"><strong>{{ $errors->first('message') }}</strong></span>
        @endif
        </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary" style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px; border: none;">Відправити</button>
            </div>
        </form>
    @endif
@endsection