@extends('layouts.admin')

@section('content')
    <div style="background-color: azure; position: relative;">
        <div class="card" style="background-color: azure; margin-bottom: 20px">
            <div class="modal-header">
                Фільтер
                <div class="card-body">
                    <form action="?" method="GET">
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label for="id">ID</label>
                                <input class="form-control1 input-search" name="id" id="id" value="{{ request('id') }}">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="user">Користувач</label>
                                <input class="form-control1 input-search" name="user" id="user" value="{{ request('user') }}">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="status">Статус</label>
                                <select id="status" class="form-control1 input-search" name="status">
                                    <option value=""></option>
                                    @foreach($statuses as $value => $label)
                                        <option value="{{ $value }}" {{ $value === request('status') ? 'selected' : '' }}>{{ $label }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="col-form-label"> </label><br />
                                <button type="submit" class="btn btn-success" style="float: left; margin-left: 5px;">Пошук</button>
                                <a href="?" class="btn btn-warning" style="float: left; margin-left: 5px;">Очистити</a>
                                <a href="{{ route('cabinet.tickets.create') }}" class="btn btn-success" style="float: left; margin-left: 5px;">Створити</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
    </div>
    <p></p>
    <div style="position: relative">
        <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
            <div class="panel-heading"  style="background-color: #8BC34A !important;" >
                <h2>Список користувачів</h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
            </div>
            <div class="panel-body no-padding" style="display: block;">
             <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Створено</th>
            <th>Змінено</th>
            <th>Тема листа</th>
            <th>Користувач</th>
            <th>Статус</th>
        </tr>
        </thead>

        <tbody>
        @foreach($tickets as $ticket)
            <tr>
                <td>{{ $ticket->id }}</td>
                <td>{{ $ticket->created_at }}</td>
                <td>{{ $ticket->updated_at }}</td>
                <td><a href="{{ route('admin.tickets.show', $ticket) }}">{{ $ticket->subject }}</a></td>
                <td>{{ $ticket->user->id }}-{{ $ticket->user->name }}</td>
                @if($ticket->isOpen())
                    <td><span class="info">Відкритий</span></td>
                @elseif($ticket->isApproved())
                    <td><span class="primary">Активований</span></td>
                @elseif($ticket->isClosed())
                    <td><span class="errorss">Закритий</span></td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
            </div>
        </div>
    </div>
    {{--{{ $tickets->links() }}--}}
@endsection