@extends('layouts.admin')

@section('content')
    <form method="POST" action="{{ route('admin.messages.destroy', $message) }}">
        {{ csrf_field() }}
        {{method_field('DELETE')}}
        <button class="btn btn-danger left"  style="margin-bottom: 20px;margin-top: 20px;">Видалити</button>
    </form>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th class="warning">Емейл</th> <td>{{$message->email}}</td>
                </tr>
                <tr>
                    <th class="warning">Ім'я</th> <td>{{$message->name}}</td>
                </tr>
                <tr>
                    <th class="warning">Дата відправлення</th> <td>{{$message->created_at}}</td>
                </tr>
                <tr>
                    <th class="warning">Текст</th> <td>{{$message->text}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection