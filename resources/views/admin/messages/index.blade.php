@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="graphs">
            <h3 class="blank1">Отримані листи</h3>
            <div class="xs">
                <div class="col-md-12">
                    <form action="?" method="GET">
                        <div class="input-group input-group-ind">
                            <input type="text" name="name" class="form-control1 input-search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="submit"><i class="fa fa-search icon-ser"></i></button>
                            </span>
                        </div>
                    </form>
                    <div class="mailbox-content">
                        <table class="table table-fhr">
                            <tbody>
                            @foreach($messages as $item)
                                <td class="hidden-xs">
                                    @if($item->isView())
                                        <i class="fa fa-star icon-state-warning"></i>
                                    @else
                                        <i class="fa fa-star"></i>
                                    @endif
                                </td>
                                <td class="hidden-xs">
                                    <a href="{{ route('admin.messages.show', $item) }}">{{ $item->name }}</a>
                                </td>
                                <td class="hidden-xs">
                                    <a href="{{ route('admin.messages.show', $item) }}">{{ $item->email }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.messages.show', $item) }}">{!! \Illuminate\Support\Str::limit($item->text,30)  !!}</a>
                                </td>
                                <td>
                                    {{ $item->created_at }}
                                </td>
                            </tr>
                                </a>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 40px;">
        {{ $messages->links() }}
    </div>
@endsection