@extends('layouts.admin')

@section('content')
<p><div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.banners.edit', $banner) }}" class="btn btn-primary"  style="float: left; margin-left: 5px;">Корегувати</a>
    <a href="{{ route('admin.banners.file', $banner) }}" class="btn btn-primary"  style="float: left; margin-left: 5px;">Змінити формат</a>
    @if($banner->isOnModeration())
        <form  method="POST" action="{{ route('admin.banners.moderateAdmin', $banner) }}">
            {{ csrf_field() }}
            <button class="btn btn-primary" style="float: left; margin-left: 5px;">Відмодерувати</button>
        </form>
    @endif
    @if($banner->isDraft())
        <form method="POST" action="{{ route('admin.banners.send', $banner) }}">
            {{ csrf_field() }}
            <button class="btn btn-primary" style="float: left; margin-left: 5px;">Надіслати на модерацію</button>
        </form>
    @endif
    @if($banner->isModerated())
        <form  method="POST" action="{{ route('admin.banners.order.admin', $banner) }}">
            {{ csrf_field() }}
            <button class="btn btn-primary"  style="float: left; margin-left: 5px;">Активувати</button>
        </form>
    @endif
        <form  method="POST" action="{{ route('admin.banners.destroy', $banner) }}">
            {{ csrf_field() }}
            {{method_field('DELETE')}}
            <button class="btn btn-primary"  style="float: left; margin-left: 5px;">Видалити</button>
        </form>

</div></p><br>
<br>
<div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
    <div class="panel-body no-padding">
        <table class="table table-striped">
        <tbody>
        <tr>
            <th>ІD</th> <td>{{$banner->id}}</td>
        </tr>
        <tr>
            <th>Назва</th> <td>{{$banner->name}}</td>
        </tr>
        <tr>
            <th>Місто</th>
            @if($banner->region)
                <td>{{$banner->region->name}}</td>
            @else
                <td>Івано-Франківська область</td>
            @endif
        </tr>
        <tr>
            <th>Категорія</th>
            @if($banner->category)
                <td>{{$banner->category->name}}</td>
            @else
                <td>Всі категорії</td>
            @endif
        </tr>
        <tr>
            <th>Статус</th>
            <td>
                @if($banner->isDraft())
                    <span class="success">Чорновик</span>
                @elseif($banner->isOnModeration())
                    <span class="primary">На модерації</span>
                @elseif($banner->isModerated())
                    <span class="primary">Готовий до оплати</span>
                @elseif($banner->isOrdered())
                    <span class="primary">Очікує оплати</span>
                @elseif($banner->isActive())
                    <span class="primary">Активний</span>
                @elseif($banner->isClosed())
                    <span class="primary">Закритий</span>
                @endif
            </td>
        </tr>
        <tr>
            <th>Дата публікації</th>
            <td>{{$banner->published_at}}</td>
        </tr>
        </tbody>
    </table>
    <div class="card">
        <div class="card-body">
            <img src="{{asset('app/'. $banner->file)}}" alt="{{ $banner->name }}">
        </div>
    </div>
    </div>
</div>
@endsection