@extends('layouts.admin')

@section('content')
    <div style="background-color: azure; position: relative;">
        <div class="card" style="background-color: azure; margin-bottom: 20px">
            <div class="modal-header">
                Фільтер
                <div class="card-body">
                    <form action="?" method="GET">
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label for="id">ID</label>
                                <input class="form-control1 input-search" name="id" id="id" value="{{ request('id') }}">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="user">Користувач</label>
                                <input class="form-control1 input-search" name="user" id="user" value="{{ request('user') }}">
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label for="region">Місто</label>
                                <input class="form-control1 input-search" name="region" id="region" value="{{ request('region') }}">
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label for="category">Категорія</label>
                                <input class="form-control1 input-search" name="category" id="category" value="{{ request('category') }}">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="status">Статус</label>
                                <select id="status" class="form-control1 input-search" name="status">
                                    <option value=""></option>
                                    @foreach($statuses as $value => $label)
                                        <option value="{{ $value }}" {{ $value === request('status') ? 'selected' : '' }}>{{ $label }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="col-form-label"> </label><br />
                                <button type="submit" class="btn btn-primary">Пошук</button>
                                <a href="?" class="btn btn-primary">Очистити</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
    </div>
    <p> <a href="{{ route('cabinet.banners.create') }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Створити банер</a></p>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-heading"  style="background-color: #8BC34A !important;" >
            <h2>Список банерів</h2>
            <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
        </div>
        <div class="panel-body no-padding" style="display: block;">
            <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Назва</th>
            <th>Користувач</th>
            <th>Місто</th>
            <th>Категорія</th>
            <th>Статус</th>
        </tr>
        </thead>

        <tbody>
        @foreach($banners as $banner)
            <tr>
                <td>{{ $banner->id }}</td>
                <td><a href="{{ route('admin.banners.show', $banner) }}" target="_self">{{ $banner->name }}</a></td>
                <td>{{ $banner->user->id }}-{{ $banner->user->name }}</td>
                @if($banner->region)
                    <td>{{ $banner->region->name }}</td>
                @else
                    <td>Івано-Франківська область</td>
                @endif
                @if($banner->category)
                    <td>{{ $banner->category->name }}</td>
                @else
                    <td>Всі категорії</td>
                @endif
                <th>
                @if($banner->isDraft())
                        <span class="success">Чорновик</span>
                @elseif($banner->isOnModeration())
                        <span class="primary">На модерації</span>
                @elseif($banner->isModerated())
                    <span class="primary">Готовий до оплати</span>
                @elseif($banner->isOrdered())
                    <span class="primary">Очікує оплати</span>
                @elseif($banner->isActive())
                    <span class="primary">Активний</span>
                @elseif($banner->isClosed())
                    <span class="primary">Закритий</span>
                @endif
                </th>
            </tr>
        @endforeach
        </tbody>
    </table>
        </div>
    </div>
    <div style="margin-bottom: 40px;">
        {{ $banners->links() }}
    </div>
@endsection
