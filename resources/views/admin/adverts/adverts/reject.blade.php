@extends('layouts.admin')

@section('content')
    <form action="?" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="reason">Причина відхилення</label>
            <input id="reason" name="reason" style="height: 200px;" class="form-control{{ $errors->has('reason') ? 'is-invalid': '' }}"
                   value="{{ old('reason') }}" required>
            @if( $errors->has('reason'))
                <span class="invalid-feedback"><stron>{{ $errors->first('reason') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Зберегти</button>
        </div>
    </form>
@endsection