@extends('layouts.admin')

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"> </script>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <form action="{{route('admin.adverts.new.store')}}" method="POST" enctype="multipart/form-data" novalidate>
                {{ csrf_field() }}
                <div class="col-md-12">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title">Назва</label>
                        <input id="title" name="title" class="form-control1" value="{{ old('title') }}" >
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price">Ціна</label>
                            <input id="price" name="price" class="form-control1" value="{{ old('price') }}">
                            @if ($errors->has('price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" id="category">
                            <label for="type">Тип оголошення</label>
                            <select class="form-control1 input-search" name="type" id="type">
                                @foreach($types as $parent)
                                    <option value="{{ $parent }}"{{ $parent == old('parent')? 'selected': '' }}>
                                        {{ $parent}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                            <label for="user_id">ID користувача</label>
                            <input id="user_id" name="user_id" class="form-control1{{ $errors->has('user_id') ? 'is-invalid': '' }}"
                                   value="{{ old('user_id') }}">
                            @if ($errors->has('user_id'))
                                <span class="help-block">
                                <strong>{{ $errors->first('user_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group" id="category">
                            <label for="category">Категорія</label>
                            <select class="form-control1 input-search" name="category" id="category" required>
                                <option value="">Обрати категорію</option>
                                @foreach($categories as $parent)
                                    <option value="{{ $parent->id }}"{{ $parent->id == old('category') ? 'selected': '' }}>
                                        @for($i = 0; $i < $parent->depth; $i++) &mdash; @endfor
                                        {{$parent->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" id="region">
                            <label for="region">Міста</label>
                            <select class="form-control1 input-search" name="region" id="region" required>
                                <option value="">Обрати місто</option>
                                @foreach($regions as $parent)
                                    <option value="{{ $parent->id }}"{{ $parent->id == old('region')? 'selected': '' }}>
                                        @for($i = 0; $i < $parent->depth; $i++) &mdash; @endfor
                                        {{ $parent->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 new_phone">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="new_phone">Оберіть телефон</label>
                            <select id="new_phone" class="form-control1">
                                <option data-phone="1">Телефон профіля</option>
                                <option data-phone="2" {{$errors->has('phone')  ? 'selected' : ''}}>Новий телефон</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4" id="pnones" style="{{$errors->has('phone')  ? '' : 'display: none'}}">
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone">Новий телефон</label>
                            <input id="phone" name="{{ $errors->first('phone') ? 'phone': '' }}" class="form-control1" value="{{ old('phone') }}" placeholder="Формат: 0963544614">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="count_print">Кількість тижнів публікації</label>
                            <select id="count_print" class="form-control1{{ $errors->has('count_print' ) ? 'is-invalid' : ''  }}" name="count_print">
                                @php($i=1)
                                @foreach($times as $variant)
                                    <option data-time="{{ $i }}" value="{{ $i }}"{{ $i == old('count_print')? 'selected': '' }}>{{ $variant }}</option>
                                    @php($i++)
                                @endforeach
                            </select>
                            @if($errors->has('count_print'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('count_print') }}</strong></span>
                            @endif
                        </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function(){
                        $('.new_phone select').change(function(){
                            var pnone = $('select#new_phone option:selected').attr('data-phone');
                            if (pnone == 1) {
                                $('#pnones').hide('slow');
                                document.getElementById('phone').name = ''
                            }
                            if (pnone == 2) {
                                $('#pnones').show();
                                document.getElementById('phone').name = 'phone'

                                $('input#phone').name('phone');
                            }
                        })
                    })
                </script>

                <div class="col-md-12" >
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-9">
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address">Адреса</label>
                                <input id="location" name="address" class="form-control1" value="">
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-10 col-xs-3">
                            <label for="location" style="color: #f5f5f5;">Авто</label>
                            <span class="location-button" data-target="#location"><img style="height: 30px; width: 30px; cursor: pointer;" src="{{asset('images/map-location.png')}}" alt=""></span>
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                    <label for="content">Контент</label>
                    <textarea id="content" name="content" class="form-control"
                          rows="5" >{{ old('content') }}</textarea>
                    @if ($errors->has('content'))
                        <span class="help-block">
                                <strong>{{ $errors->first('content') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group" id="category">
                    <label for="attribut">Атрібути</label>
                    <div id="countryList">
                    </div>
                    {{ csrf_field() }}
                </div>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script>
                    $(document).ready(function() {
                        $('.input-search').change(function () {
                            if ($(this).val() != '') {
                                var select = $(this).attr("id");
                                var value = $(this).val();
                                var _token = $('input[name="_token"]').val();

                                $.ajax({
                                    url: "{{ route('autocomplete.fetch') }}",
                                    method: "POST",
                                    data: {select: select, value: value, _token:_token},
                                    success:function (result) {
                                        $('#countryList').fadeIn();
                                        $('#countryList').html(result);
                                    }
                                })
                            }
                        });
                    });
                </script>
                <div class="form-group">
                    <label for="avatar">Фото</label>
                    <input id="avatar" name="avatar" type="file" class="form-control1{{ $errors->has('avatar') ? 'is-invalid' : '' }}" value="{{ old('avatar') }}" >
                    @if($errors->has('avatar'))
                        <span class="invalid-feedback"><strong>{{ $errors->first('avatar') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
@endsection
