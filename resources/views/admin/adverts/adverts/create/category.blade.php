@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="sect" style="background-color: #fd4235; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: white; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">1 крок</h1>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: #999999; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">2 крок</h1>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: #999999; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">3 крок</h1>
            </div>
        </div>
        <div class="col-md-9">
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #999999">
                <h1  style="font-size: 15px; margin-top: 0!important; padding-top: 6px; padding-bottom: 6px; margin-bottom: 0!important;">Категорії</h1>
            </div>
            <div class="col-md-12" style="    background-color: #f5f5f5; padding: 15px; border-radius: 20px;">
                <div class="maincategories">
                    <div class="maincategories-list clr">
                        @foreach($categories as $category)
                        <div class="li fleft" style="margin-top: 5px;">
                            <div class="item">
                                <a href="{{ route('admin.adverts.adverts.create', [$user, $category]) }}" data-id="36" class="link parent   ">
                                    <span style="font-size: 12px;">{{ $category->name }}</span>
                                    <span class="cat-icon icon-home cat-icon-36" style="color: red; font-size: 24px;"></span>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection