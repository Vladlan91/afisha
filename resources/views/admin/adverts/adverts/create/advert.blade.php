@extends('layouts.app')

@section('content')
    <div class="row time">
        <div class="col-md-3">
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: #999999; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">1 крок</h1>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: #999999; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">2 крок</h1>
            </div>
            <div class="sect" style="background-color: #fd4235; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: white; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">3 крок</h1>
            </div>
        </div>
        <div class="col-md-9">
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #000000">
                <div class="row">
                    <div class="col-md-6">
                        <h1 style="font-size: 15px; color: #0098d0; padding-top: 6px; float: right; margin-top: 0!important; margin-bottom: 0!important;">{{ $category->getPa() }}</h1>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('admin.adverts.adverts.create', $user) }}" style="text-decoration: none;"><p style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px;">Змінити</p></a>
                    </div>
                </div>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #000000">
                <div class="row">
                    <div class="col-md-6">
                        <h1 style="font-size: 15px; color: #0098d0; padding-top: 6px;  float: right; margin-top: 0!important; margin-bottom: 0!important;">{{ $region ? $region->getAddress() : 'Івано-Франківська область' }}</h1>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('admin.adverts.adverts.create.region' , [$user, $category]) }}" style="text-decoration: none;"><p style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center; margin-top: 5px;">Змінити</p></a>
                    </div>
                </div>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #999999">
                <h1  style="font-size: 15px; margin-top: 0!important; padding-top: 6px; padding-bottom: 6px; margin-bottom: 0!important;">Зміст оголошення</h1>
            </div>
            <div class="col-md-12" style="background-color: #f5f5f5; padding: 15px; border-radius: 20px;">
                <div style="background-color: white; padding: 20px; border-radius: 60px;">
                @if($user->avatar)
                    <img src="{{asset('app/'. $user->avatar)}}" alt="{{ $user->name }}" width="75" height="75" style="border-radius: 60px; margin-top: -9px; margin-left: -6px; margin-right:10px; float: left; z-index: -100">
                @else
                    <img src="{{asset('images/no-avatar.jpg')}}" width="75" height="75" style="border-radius: 40px; float: left; margin-top: -9px; margin-left: -6px; margin-right:10px;z-index: -100">
                @endif
                    <h3  style="font-size: 16px; font-weight:900; margin-top: 0!important; padding-top: 6px; padding-bottom: 6px; margin-bottom: 0!important;">Автор Оголошення</h3>
                    <h3  style="font-size: 16px; font-weight:900;  margin-top: 0!important; padding-top: 6px; padding-bottom: 6px; margin-bottom: 0!important; padding-left: 20px;">{{ ' '.$user->name .' '}}{{ $user->last_name }}</h3>
                </div>
                <form method="POST" action="{{ route('admin.adverts.adverts.create.advert.store', [$user, $category, $region]) }}" enctype="multipart/form-data" novalidate>
                    {{ csrf_field() }}
                    <div class="">
                        <div class="">
                            <div class="row">
                                <div id="one" class="col-md-12" style="">
                                    <div class="col-md-6">
                                        <img src="{{asset('images/type1.jpg')}}" alt="" style="padding: 20px;">
                                        <img src="{{asset('images/typearrow.png')}}" alt="" style="float: right; top: 50%; right:5px; position: absolute;">
                                    </div>
                                    <div class="col-md-6">
                                        <p style="padding-top: 10%;">Залежно від рубрики та підрубрики може коштувати 6 грн або бути безкоштовним. Експортується в газету звичайними оголошеннями. На сайті сортується по даті подання. Не перебуває в ротації на головній сторінці.</p>
                                        <p style="padding-top: 2%; font-weight: bold;">Вартість: 0 грн. або 6 грн.</p>
                                        <p style="">(платні рубрики: Нерухомість, Будівельні та ремонтні роботи, Столярні роботи, Бюро послуг, Пропоную роботу)</p>
                                    </div>
                                </div>
                                <div id="two" class="col-md-12" style="display: none">
                                    <div class="col-md-6">
                                        <img src="{{asset('images/type2.jpg')}}" alt="" style="padding: 20px;">
                                        <img src="{{asset('images/typearrow.png')}}" alt="" style="float: right; top: 50%; right:5px; position: absolute;">
                                    </div>
                                    <div class="col-md-6" >
                                        <p style="padding-top: 10%;">Вартість даного оголошення становить 9 грн. Експортується в газету у світлій рамці та з напівжирним шрифтом. На сайті у своїй рубриці відображується жирним шрифтом. Сортується по даті подання. Не перебуває в ротації на головній сторінці.</p>
                                        <p style="padding-top: 2%; font-weight: bold;">Вартість: 9 грн.</p>
                                    </div>
                                </div>
                                <div  id="three" class="col-md-12"  style="display: none">
                                    <div class="col-md-6">
                                        <img src="{{asset('images/type3.jpg')}}" alt="" style="padding: 20px;">
                                        <img src="{{asset('images/typearrow.png')}}" alt="" style="float: right; top: 50%; right:5px; position: absolute;">
                                    </div>
                                    <div class="col-md-6">
                                        <p style="padding-top: 10%;">Вартість даного оголошення становить 20 грн. Експортується в газету у рамці 4х4. На сайті у своїй рубриці відображується жирним шрифтом та в рамці. Перебуває в ротації (тобто змінюють своє місце у списку відображення за певним алгоритмом протягом часу свого розміщення).</p>
                                        <p style="padding-top: 2%; font-weight: bold;">Вартість: 20 грн.</p>
                                    </div>
                                </div>
                                <div  id="fore" class="col-md-12"  style="display: none">
                                    <div class="col-md-6">
                                        <img src="{{asset('images/type4.jpg')}}" alt="" style="padding: 20px;">
                                        <img src="{{asset('images/typearrow.png')}}" alt="" style="float: right; top: 50%; right:5px; position: absolute;">
                                    </div>
                                    <div class="col-md-6">
                                        <p style="padding-top: 10%;">Вартість даного оголошення становить 25 грн. Експортується в газету у рамці 4х4 з темним фоном. На сайті у своїй рубриці відображується з темним фоном. Перебувають в ротації (тобто змінюють своє місце у списку відображення за певним алгоритмом протягом часу свого розміщення).</p>
                                        <p style="padding-top: 2%; font-weight: bold;">Вартість: 25 грн.</p>
                                    </div>
                                </div>
                                <div  id="five" class="col-md-12"  style="display: none">
                                    <div class="col-md-6">
                                        <img src="{{asset('images/type5.jpg')}}" alt="" style="padding: 20px;">
                                        <img src="{{asset('images/typearrow.png')}}" alt="" style="float: right; top: 50%; right:5px; position: absolute;">
                                    </div>
                                    <div class="col-md-6">
                                        <p style="padding-top: 10%;">Вартість даного оголошення становить 30 грн. Експортується в газету у рамці 4х6. На сайті у своїй рубриці відображується в збільшеній (вищій) рамці. Розташовані у верхній позиції на сайті у своїй рубриці. Перебувають в ротації (тобто змінюють своє місце у списку відображення за певним алгоритмом протягом часу свого розміщення). Поширюється адміністратором в соцмережі (групі) Фейсбук</p>
                                        <p style="padding-top: 2%; font-weight: bold;">Вартість: 30 грн.</p>
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-6 calc">
                                    <div class="">
                                        <div class="form-group">
                                            <label for="type">Тип оголошення</label>
                                            <select id="type" class="form-control{{ $errors->has('type' ) ? 'is-invalid' : ''  }}" name="type">
                                                @php($i=1)
                                                @php($b=0)
                                                @foreach($types as $variant)
                                                    @if($i == 1)
                                                        @php($b=0)
                                                    @elseif($i == 2)
                                                        @php($b=9)
                                                    @elseif($i == 3)
                                                        @php($b=20)
                                                    @elseif($i == 4)
                                                        @php($b=25)
                                                    @else
                                                        @php($b=30)
                                                    @endif
                                                    <option data-type="{{ $i }}" data-price="{{ $b }}" value="{{ $variant }}">{{ $variant }}</option>
                                                    @php($i++)
                                                @endforeach
                                            </select>
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                        <label for="title">Назва</label>
                                        <input id="title" name="title" class="form-control" value="{{ old('title') }}" >
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                        <label for="price">Ціна</label>
                                        <input id="price" name="price" class="form-control" value="{{ old('price') }}">
                                        @if ($errors->has('price'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6" >
                                    <div class="row">
                                        <div class="col-md-10 col-sm-10 col-xs-9">
                                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                                <label for="address">Адреса</label>
                                                <input id="address" name="address" class="form-control" value="{{ old('address', $region ? $region->getAddress() : 'Івано-Франківська область') }}" >
                                                @if ($errors->has('address'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-10 col-xs-3">
                                            <label for="address" style="color: #f5f5f5;">Авто</label>
                                            <span class="location-button" data-target="#address"><img style="height: 30px; width: 30px; cursor: pointer;"
                                                        src="{{asset('images/map-location.png')}}" alt=""></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                                        <label for="content">Опис</label>
                                        <textarea id="content" name="content" rows="4" cols="50" class="form-control" >{{ old('content') }}</textarea>
                                        @if ($errors->has('content'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('content') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div class="row">
                        @foreach($category->allAttributes() as $attribute)
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="attribute_{{ $attribute->id }}">{{ $attribute->name }}</label>
                                    @if($attribute->isSelect())
                                        <select id="attribute_{{ $attribute->id }}" class="form-control{{ $errors->has('attributes.' . $attribute->id) ? 'is-invalid' : ''  }}" name="attributes[{{ $attribute->id }}]">
                                            <option value=""></option>
                                            @foreach($attribute->variants as $variant)
                                                <option value="{{ $variant }}" {{ $variant === old('attributes.' . $attribute->id) ? 'selected' : ''}}>{{ $variant }}</option>
                                            @endforeach
                                        </select>
                                    @elseif($attribute->isCheckbox())
                                            <input type="checkbox"  id="attribute_{{ $attribute->id }}" name="attributes[{{ $attribute->id }}]">
                                    @elseif($attribute->isNumber())
                                        <input id="attribute_{{ $attribute->id }}" type="number" class="form-control{{ $errors->has('attributes.' . $attribute->id) ? 'is-invalid' : ''  }}" name="attributes[{{ $attribute->id }}]">
                                    @else
                                        <input id="attribute_{{ $attribute->id }}" type="text" class="form-control{{ $errors->has('attributes.' . $attribute->id) ? 'is-invalid' : ''  }}" name="attributes[{{ $attribute->id }}]">
                                    @endif

                                    @if($errors->has('parent'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('attributes.' . $attribute->id) }}</strong></span>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="col-md-12">
                        <div class="file-upload">
                            <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Додати фото</button>

                            <div class="image-upload-wrap">
                                <input class="file-upload-input" id="avatar" name="avatar" type='file' onchange="readURL(this);" accept="image/*" />
                                <div class="drag-text">
                                    <h3>Перетягніть файл або виберіть Додати зображення</h3>
                                </div>
                            </div>
                            <div class="file-upload-content">
                                <img class="file-upload-image" src="#" alt="your image" />
                                <div class="image-title-wrap">
                                    <button type="button" onclick="removeUpload()" class="remove-image">Видалити <span class="image-title">Завантажено зображення</span></button>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('avatar'))
                            <span class="invalid-feedback"><strong style="color: red">{{ $errors->first('avatar') }}</strong></span>
                        @endif
                    </div>
                    <div class="col-md-12 new_phone">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="new_phone">Оберіть телефон</label>
                                <select id="new_phone" class="form-control">
                                        <option data-phone="1">Телефон профіля</option>
                                        <option data-phone="2" {{$errors->has('phone')  ? 'selected' : ''}}>Новий телефон</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8" id="pnones" style="{{$errors->has('phone')  ? '' : 'display: none'}}">
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone">Новий телефон</label>
                                <input id="phone" name="{{ $errors->first('phone') ? 'phone': '' }}" class="form-control" value="{{ old('phone') }}" placeholder="Формат: 0963544614">
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 time">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="count_print">Кількість тижнів публікації</label>
                                <select id="time" class="form-control{{ $errors->has('count_print' ) ? 'is-invalid' : ''  }}" name="count_print">
                                    @php($i=1)
                                    @foreach($times as $variant)
                                        <option data-time="{{ $i }}" value="{{ $i }}">{{ $variant }}</option>
                                        @php($i++)
                                    @endforeach
                                </select>
                                @if($errors->has('count_print'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('count_print') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8" style="margin-top: 15px;">
                            @php($i = 1)
                            @foreach($category->dataPrint(\Carbon\Carbon::now()) as $k => $item)
                            <p id="{{$k}}" style="float: left; background-color: red; border-radius: 20px; padding: 7px; color: white; font-weight: 900; margin-right: 5px; {{ $i ? '': 'display: none' }}">{{$item}}</p>
                        @php($i = null)
                            @endforeach
                        </div>
                    </div>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"> </script>
                    <script>
                        function readURL(input) {
                            if (input.files && input.files[0]) {

                                var reader = new FileReader();

                                reader.onload = function(e) {
                                    $('.image-upload-wrap').hide();

                                    $('.file-upload-image').attr('src', e.target.result);
                                    $('.file-upload-content').show();

                                    $('.image-title').html(input.files[0].name);
                                };

                                reader.readAsDataURL(input.files[0]);

                            } else {
                                removeUpload();
                            }
                        }

                        function removeUpload() {
                            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                            $('.file-upload-content').hide();
                            $('.image-upload-wrap').show();
                        }
                        $('.image-upload-wrap').bind('dragover', function () {
                            $('.image-upload-wrap').addClass('image-dropping');
                        });
                        $('.image-upload-wrap').bind('dragleave', function () {
                            $('.image-upload-wrap').removeClass('image-dropping');
                        });
                    </script>
                    <script>
                        $(document).ready(function(){
                            $('.time select').change(function(){
                                var type_format = $('select#type option:selected').attr('data-type');
                                if (type_format == 1) {
                                    $('#one').show();
                                    $('#two').hide('slow');
                                    $('#three').hide('slow');
                                    $('#fore').hide('slow');
                                    $('#five').hide('slow');
                                    console.log(type_format);
                                }
                                if (type_format == 2) {
                                    $('#one').hide('slow');
                                    $('#two').show();
                                    $('#three').hide('slow');
                                    $('#fore').hide('slow');
                                    $('#five').hide('slow');
                                    console.log(type_format);
                                }
                                if (type_format == 3) {
                                    $('#one').hide('slow');
                                    $('#two').hide('slow');
                                    $('#three').show();
                                    $('#fore').hide('slow');
                                    $('#five').hide('slow');
                                    console.log(type_format);
                                }
                                if (type_format == 4) {
                                    $('#one').hide('slow');
                                    $('#two').hide('slow');
                                    $('#three').hide('slow');
                                    $('#fore').show();
                                    $('#five').hide('slow');
                                    console.log(type_format);
                                }
                                if (type_format == 5) {
                                    $('#one').hide('slow');
                                    $('#two').hide('slow');
                                    $('#three').hide('slow');
                                    $('#fore').hide('slow');
                                    $('#five').show();
                                }
                            })
                        })
                    </script>
                    <script>
                        $(document).ready(function(){
                            $('.time select').change(function(){
                                var time_format = $('select#time option:selected').attr('data-time');
                                var type_format = $('select#type option:selected').attr('data-price');
                                if (time_format == 1) {
                                    $('#0').show();
                                    $('#1').hide('slow');
                                    $('#2').hide('slow');
                                    $('#3').hide('slow');

                                }
                                if (time_format == 2) {
                                    $('#0').show();
                                    $('#1').show();
                                    $('#2').hide('slow');
                                    $('#3').hide('slow');


                                }
                                if (time_format == 3) {
                                    $('#0').show();
                                    $('#1').show();
                                    $('#2').show();
                                    $('#3').hide('slow');

                                }
                                if (time_format == 4) {
                                    $('#0').show();
                                    $('#1').show();
                                    $('#2').show();
                                    $('#3').show();


                                }

                                var final_prices = parseInt(type_format) * parseInt(time_format);
                                $('span#final-price').text(final_prices.toFixed(2));
                            })
                        })
                    </script>
                    <script>
                        $(document).ready(function(){
                            $('.new_phone select').change(function(){
                                var pnone = $('select#new_phone option:selected').attr('data-phone');
                                if (pnone == 1) {
                                    $('#pnones').hide('slow');
                                    document.getElementById('phone').name = ''
                                }
                                if (pnone == 2) {
                                    $('#pnones').show();
                                    document.getElementById('phone').name = 'phone'

                                    $('input#phone').name('phone');
                                }
                            })
                        })
                    </script>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div style="font-weight: 900; color: black;">
                            <p style="font-weight: 900; color: grey;">Вартість категорії - {{$category->getPrice()}}&nbsp;грн</p>
                            <p style="font-weight: 900; font-size: 20px; color: grey;">Всього до сплати: <span style="color: #ed5565" id="final-price">0</span>&nbsp;грн</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" id="price" data-price="{{$category->getPrice()}}">
                        <button type="submit" class="btn btn-primary" style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px; border: none;">Зберегти</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
