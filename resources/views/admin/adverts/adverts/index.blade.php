@extends('layouts.admin')

@section('content')
    <script>
        function toggle(source){
            checkboxes = document.getElementsByName('update[]');
            for (var i = 0, n = checkboxes.length; i<n; i++) {
                checkboxes[i].checked = source.checked;
            }
        }
    </script>
    <div style="background-color: azure; position: relative;">
        <div class="card" style="background-color: azure; margin-bottom: 20px">
            <div class="modal-header">
                Фільтер
                <a href="{{route('admin.adverts.new')}}" class="btn btn-primary" style="float: right">Створити оголошення</a>
                <div class="card-body">
                    <form action="?" method="GET">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="id">ID</label>
                                <input class="form-control1 input-search" name="id" id="id" value="{{ request('id') }}">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="title">Назва</label>
                                <input class="form-control1 input-search" name="title" id="title" value="{{ request('title') }}">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="address">Вулиця</label>
                                <input class="form-control1 input-search" name="address" id="address" value="{{ request('address') }}">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="user">Автор</label>
                                <input class="form-control1 input-search" name="user" id="user" value="{{ request('user') }}">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="user">Тип</label>
                                <select class="form-control1 input-search" name="type" id="type">
                                    <option value="">Обрати тип </option>
                                    @foreach($types as $type)
                                        <option value="{{ $type }}">{{$type}}</option>
                                    @endforeach
                                    <option value="А+Б+В">А+Б+В</option>
                                    <option value="Г+Д">Г+Д</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="region">Місто</label>
                                <select class="form-control1 input-search" name="region" id="region">
                                    <option value="">Обрати місто</option>
                                    @foreach($regions as $region)
                                        <option value="{{ $region->id }}" {{ $region === request('region') ? 'selected' : '' }}>{{$region->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="category">Категорія</label>
                                <select class="form-control1 input-search" name="category" id="category">
                                    <option value="">Обрати категорію</option>
                                    {{--@foreach($categories as $category)--}}
                                    {{--<option value="{{ $category->id }}" {{ $category === request('category') ? 'selected' : '' }}>{{$category->name}}</option>--}}
                                    {{--@endforeach--}}
                                    @foreach($categories as $parent)
                                        <option value="{{ $parent->id }}"{{ $parent->id === old('parent')? 'selected': '' }}>
                                            @for($i = 0; $i < $parent->depth; $i++) &mdash; @endfor
                                            {{ $parent->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        {{--<div class="col-sm-3">--}}
                        {{--<div class="form-group">--}}
                        {{--<label for="category">Категорія</label>--}}
                        {{--<input class="form-control1 input-search" name="category" id="category"/>--}}
                        {{--<div id="countryList"></div>--}}
                        {{--{{ csrf_field() }}--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                        <script>
                            $(document).ready(function () {
                                $('#category').keyup(function(){
                                    var query = $(this).val();
                                    if (query != ''){
                                        var _token = $('input[name="_token"]').val();
                                        $.ajax({
                                            url:"{{ route('autocomplete.fetch') }}",
                                            method:"POST",
                                            data:{query:query,_token:_token},
                                            success:function (data) {
                                                $('#countryList').fadeIn();
                                                $('#countryList').html(data);
                                            }
                                        });
                                    }
                                });
                                $(document).on('click','li', function () {
                                    $('#category').val($(this).text());
                                    $('#countryList').fadeOut();
                                })
                            });
                        </script>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="error">Помилки</label>
                                <select class="form-control1 input-search" name="error" id="error">
                                    <option value="1" {{1 === request('error') ? 'selected' : ''}}>Помилки усунуто</option>
                                    <option value="{{null}}" {{null === request('error') ? 'selected' : ''}}>Помилки не усунуто</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="start">Пошук по даті від:</label>
                                <input type="date" id="start" name="trip-start"
                                       value="{{request('trip-start')}}"
                                       min="" max="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="start">Пошук по даті по:</label>
                                <input type="date" id="finish" name="trip-finish"
                                       value="{{request('trip-finish')}}"
                                       {{--@php(\Carbon\Carbon::now()->setTime(0,0)->format('Y-m-d H:i:s'))--}}
                                       {{--value="{{\Carbon\Carbon::now()->toDateString()}}"--}}
                                       min="" max="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="status">Статус</label>
                                <select id="status" class="form-control1 input-search" name="status">
                                    <option value=""></option>
                                    @foreach($statuses as $value => $label)
                                        @if($label =='draft')
                                            <option value="{{ $value }}" {{ $value === request('status') ? 'selected' : '' }}>Чорновик</option>
                                        @elseif($label =='closed')
                                            <option value ="{{ $value }}" {{ $value === request('status') ? 'selected' : '' }}>Закриті</option>
                                        @elseif($label =='moderation')
                                            <option value ="{{ $value }}" {{ $value === request('status') ? 'selected' : '' }}>На модерації</option>
                                        @elseif($label =='active')
                                            <option value ="{{ $value }}" {{ $value === request('status') ? 'selected' : '' }}>Активні</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="col-form-label"> </label><br />
                                <button type="submit" class="btn btn-primary">Пошук</button>
                                <a href="?" class="btn btn-primary">Очистити</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <div style=" position: relative">
        <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
            <div class="panel-heading"  style="background-color: #8BC34A !important;" >
                <h2>Список оголошень</h2>
               <a style="float: right; margin-top: -23px; color: white; border-radius:20px; background-color: #0a90eb; padding: 5px;" href="/download"> <span class="lnr lnr-download" style="margin-right: 5px;"></span>Скачати</a>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
            </div>
            <div class="panel-body no-padding" style="display: block;">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Обновлено</th>
                        <th>Назва</th>
                        <th>Автор</th>
                        <th>Тип</th>
                        <th>Категорія</th>
                        <th>Статус</th>
                        <th><input type="checkbox" onclick="toggle(this)">&nbsp;Всі</th>
                    </tr>
                    </thead>
                    <tbody>
                    <form action="{{url('/update')}}" method="POST">
                        {{ csrf_field() }}
                    @foreach($adverts as $advert)
                        <tr>
                            <td>{{ $advert->id }}</td>
                            <td>{{ $advert->created_at }}</td>
                            <td width="30%"><a href="{{ route('adverts.show', $advert) }}" target="_self">{{ $advert->title }}</a>
                            <p>{{ $advert->content }}</p></td>
                            <td>{{ $advert->user->name }}</td>
                            <td>{{ $advert->type }}</td>
                            <td>{{ $advert->category->name }}</td>
                            <td>
                                @if($advert->isDraft())
                                    <span class="info">Чорновик</span>
                                @elseif($advert->isOnModeration())
                                    <span class="primary">На модерації</span>
                                @elseif($advert->isActive())
                                    <span class="primary">Активний</span>
                                @elseif($advert->isClosed())
                                    <span class="error">Закрити</span>
                                @endif
                            </td>
                            <td><input type="checkbox" name="update[]" value="{{$advert->id}}"></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <button type="submit" class="btn btn-success" style="float: right" >Активувати оголошення</button>
                </form>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 40px;">
        {{ $adverts->links() }}
    </div>
@endsection