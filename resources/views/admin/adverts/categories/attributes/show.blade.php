@extends('layouts.admin')

@section('content')
    <p><div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.adverts.categories.attributes.edit', [$category, $attribute]) }}" class="btn btn-primary left">Корегувати</a>
        <form method="POST" action="{{ route('admin.adverts.categories.attributes.destroy', [$category, $attribute]) }}">
            {{ csrf_field() }}
            {{method_field('DELETE')}}
            <button class="btn btn-danger left">Видалити</button>
        </form>
    </div></p><br>
    <br>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th class="warning">ID</th> <td>{{$attribute->id}}</td>
                </tr>
                <tr>
                    <th class="warning">Назва</th> <td>{{$attribute->name}}</td>
                </tr>
                <tr>
                    <th class="warning">Тип</th> <td>{{$attribute->type}}</td>
                </tr>
                <tr>
                    <th class="warning">Позиція</th> <td>{{$attribute->sort}}</td>
                </tr>
                <tr>
                    <th class="warning">Обов'язковий</th>  <td>{{$attribute->required ? 'Yes': ''}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection