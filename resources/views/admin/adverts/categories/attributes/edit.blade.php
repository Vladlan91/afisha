@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <form action="{{ route('admin.adverts.categories.attributes.update', [$category, $attribute] )}}" method="POST">
        {{ csrf_field() }}
        {{method_field('PUT')}}
        <div class="form-group">
            <label for="name">Назва</label>
            <input id="name" name="name" type="text" class="form-control{{ $errors->has('name') ? 'is-invalid': '' }}"
                   value="{{ old('name', $attribute->name) }}" required>
            @if( $errors->has('name'))
                <span class="invalid-feedback"><stron>{{ $errors->first('name') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <label for="sort">Позиція</label>
            <input id="sort" name="sort"  type="text" class="form-control{{ $errors->has('sort') ? 'is-invalid': '' }}"
                   value="{{ old('sort', $attribute->sort) }}" required>
            @if( $errors->has('sort'))
                <span class="invalid-feedback"><stron>{{ $errors->first('sort') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <label for="type">Тип</label>
            <select id="type" name="type" class="form-control{{ $errors->has('type') ? 'is-invalid': '' }}">
                <option value=""></option>
                @foreach($types as $type => $label)
                    <option value="{{ $label }}"{{ $label === $attribute->type ? 'selected': '' }}>
                        {{ $label }}
                    </option>
                @endforeach
            </select>
            @if( $errors->has('type'))
                <span class="invalid-feedback"><stron>{{ $errors->first('type') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <label for="variants">Варіанти</label>
            <textarea id="variants" name="variants" class="form-control{{ $errors->has('type') ? 'is-invalid': '' }}">{{ $result }}</textarea>
        </div>

        <div class="form-group">
            <input name="required"  type="hidden" value="0">
            <div class="checkbox-inline1">
                <label><input  name="required" type="checkbox" {{ $attribute->required ? 'checked': '' }}> Обов'язковий</label>
            </div>
            @if( $errors->has('required'))
                <span class="invalid-feedback"><stron>{{ $errors->first('required') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
        </div>
    </div>
@endsection