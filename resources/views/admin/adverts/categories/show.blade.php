@extends('layouts.admin')

@section('content')
    <p><div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.adverts.categories.edit', $category) }}" class="btn btn-primary left">Корегувати</a>
        <form method="POST" action="{{ route('admin.adverts.categories.destroy', $category) }}">
            {{ csrf_field() }}
            {{method_field('DELETE')}}
            <button class="btn btn-danger left">Видалити</button>
        </form>
    </div></p><br>
    <br>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <table class="table table-striped">
                <tbody>
                <tr class="">
                    <th class="warning">ID</th> <td>{{$category->id}}</td>
                </tr>
                <tr>
                    <th class="warning">Назва</th> <td>{{$category->name}}</td>
                </tr>
                <tr>
                    <th class="warning">Посилання</th> <td>{{$category->slug}}</td>
                </tr>
                <tr>
                    <th class="warning">Ціна категорії</th> <td>{{$category->getPrice()}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <a href="{{ route('admin.adverts.categories.attributes.create', $category) }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Cтворити атрібут</a>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
    <table class="table table-bordered table-striped pt-2">
        <thead>
        <tr>
            <th  class="warning">Сортування</th>
            <th  class="warning">Назві</th>
            <th  class="warning">Тип</th>
            <th  class="warning">Обов'язковий</th>
        </tr>
        </thead>

        <tbody>
        <tr><th colspan="4">Батьківські атрібути</th></tr>
        @forelse($parentAttributes as $attribute)
            <tr>
                <td>{{$attribute->sort}}</td>
                <td>
                    {{$attribute->name}}
                </td>
                <td>{{$attribute->type}}</td>
                <td>{{$attribute->required ? 'Yes': ''}}</td>
            </tr>
        @empty
            <tr><th colspan="4">None</th></tr>
        @endforelse

        <tr><th colspan="4">Власті атрібути</th></tr>
        @forelse($attributes as $attribute)
            <tr>
                <td>{{$attribute->sort}}</td>
                <td>
                    <a href="{{ route('admin.adverts.categories.attributes.show', [$category, $attribute]) }}">{{$attribute->name}}</a>
                </td>
                <td>{{$attribute->type}}</td>
                <td>{{$attribute->required ? 'Yes': ''}}</td>
            </tr>
        @empty
            <tr><th colspan="4">Відсутні</th></tr>
        @endforelse
        </tbody>
    </table>
    </div>
@endsection