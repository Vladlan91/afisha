@extends('layouts.admin')

@section('content')
    <p><a href="{{ route('admin.adverts.categories.create') }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Створити категорію</a></p>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-heading"  style="background-color: #8BC34A !important;" >
            <h2>Список категорій</h2>
            <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
        </div>
        <div class="panel-body no-padding" style="display: block;">
            <table class="table table-striped">
                <thead>
                <tr class="warning">
                    <th>Назва</th>
                    <th>Посилання</th>
                    <th>Переміщення</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>
                            @for($i = 0; $i < $category->depth; $i++) &mdash; @endfor
                            <a href="{{ route('admin.adverts.categories.show', $category) }}">{{$category->name}}</a>
                        </td>
                        <td>
                            {{$category->slug}}
                        </td>
                        <td>
                            <div class="row"></div>
                            <form action="{{ route('admin.adverts.categories.first', $category) }}" method="POST" class="col-sm-2">
                                {{ csrf_field() }}
                                <button class="btn btn-sm btn-primary" style="border-radius: 50px;"><span class="lnr lnr-arrow-up-circle" style="font-weight: 900; font-size: 16px"></span></button>
                            </form>
                            <form action="{{ route('admin.adverts.categories.up', $category) }}" method="POST" class="col-sm-2">
                                {{ csrf_field() }}
                                <button class="btn btn-sm btn-primary" style="border-radius: 50px;"><span class="lnr lnr-chevron-up-circle" style="font-weight: 900; font-size: 16px"></span></button>
                            </form>
                            <form action="{{ route('admin.adverts.categories.down', $category) }}" method="POST" class="col-sm-2">
                                {{ csrf_field() }}
                                <button class="btn btn-sm btn-primary" style="border-radius: 50px;"><span class="lnr lnr-chevron-down-circle" style="font-weight: 900; font-size: 16px"></span></button>
                            </form>
                            <form action="{{ route('admin.adverts.categories.last', $category) }}" method="POST" class="col-sm-2">
                                {{ csrf_field() }}
                                <button class="btn btn-sm btn-primary" style="border-radius: 50px;"><span class="lnr lnr-arrow-down-circle" style="font-weight: 900; font-size: 16px"></span></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div style="margin-bottom: 40px;">
    </div>
@endsection
