@extends('layouts.admin')

@section('content')
    <div style="background-color: azure; position: relative;">
        <div class="card" style="background-color: azure; margin-bottom: 20px">
            <div class="modal-header">
                Фільтер
                <div class="card-body">
                    <form action="?" method="GET">
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label for="id">ID</label>
                                <input class="form-control1 input-search" name="id" id="id" value="{{ request('id') }}">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="title">Назва</label>
                                <input class="form-control1 input-search" name="title" id="title" value="{{ request('title') }}">
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label for="user">Автор</label>
                                <input class="form-control1 input-search" name="user" id="user" value="{{ request('user') }}">
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label for="region">Місто</label>
                                <input class="form-control1 input-search" name="region" id="region" value="{{ request('region') }}">
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label for="category">Категорія</label>
                                <input class="form-control1 input-search" name="category" id="category" value="{{ request('category') }}">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="col-form-label"> </label><br />
                                <button type="submit" class="btn btn-primary">Пошук</button>
                                <a href="?" class="btn btn-primary">Очистити</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div style="position: relative">
        <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
            <div class="panel-heading"  style="background-color: #8BC34A !important;" >
                <h2>Список компаній</h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
            </div>
            <div class="panel-body no-padding" style="display: block;">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Назва</th>
                        <th>Автор</th>
                        <th>Місто</th>
                        <th>Категорія</th>
                        <th>Статус</th>
                        <th>Обновлено</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $advert)
                        <tr>
                            <td>{{ $advert->id }}</td>
                            <td><a href="{{ route('admin.business.company.show', [$advert->business->id, $advert]) }}">{{$advert->title}}</a></td>
                            <td>{{ $advert->user->id }}-{{ $advert->user->name }}</td>
                            @if($advert->region)
                                <td>{{ $advert->region->id }}-{{ $advert->region->name }}</td>
                            @else
                                <td>All region</td>
                            @endif
                            <td>{{ $advert->business->name }}</td>
                            <td>
                                @if($advert->isDraft())
                                    <span class="info">Draft</span>
                                @elseif($advert->isOnModeration())
                                    <span class="primary">Moderation</span>
                                @elseif($advert->isActive())
                                    <span class="primary">Active</span>
                                @elseif($advert->isClosed())
                                    <span class="error">Closed</span>
                                @endif
                            </td>
                            <td>{{ $advert->updated_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 40px;">
        {{ $companies->links() }}
    </div>
@endsection