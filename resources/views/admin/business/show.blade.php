@extends('layouts.admin')

@section('content')
    <p><div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.business.edit', $business) }}" class="btn btn-primary left">Корегувати</a>
        <form method="POST" action="{{ route('admin.business.destroy', $business) }}">
            {{ csrf_field() }}
            {{method_field('DELETE')}}
            <button class="btn btn-danger left">Видалити</button>
        </form>
    </div></p><br>
    <br>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <table class="table table-striped">
        <tbody>
            <tr>
                <th class="warning">ID</th> <td>{{$business->id}}</td>
            </tr>
            <tr>
                <th class="warning">Назва</th> <td>{{$business->name}}</td>
            </tr>
            <tr>
                <th class="warning">Посилання</th> <td>{{$business->slug}}</td>
            </tr>
        </tbody>
    </table>
        </div>
    </div>
    <a href="{{ route('admin.business.company.create', $business) }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Створити компанію</a>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-heading"  style="background-color: #8BC34A !important;" >
            <h2>Список компаній</h2>
            <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
        </div>
        <div class="panel-body no-padding" style="display: block;">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th  class="warning">Назва</th>
                    <th class="warning">Категорія</th>
                    <th class="warning">Посилання</th>
                    <th class="warning">Статус</th>
                </tr>
                </thead>
                <tbody>
                @foreach($companies as $company)
                    <tr>
                        <td><a href="{{ route('admin.business.company.show', [$business, $company]) }}">{{$company->title}}</a></td>
                        <td>{{$business->name}}</td>
                        <td>{{$company->slug}}</td>
                        <td>{{$company->status}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection