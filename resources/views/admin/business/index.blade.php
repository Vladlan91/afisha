@extends('layouts.admin')

@section('content')
    <p><a href="{{ route('admin.business.create') }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Створити бізнес-категорію</a></p>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-heading"  style="background-color: #8BC34A !important;" >
            <h2>Список бізнес-категорій</h2>
            <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
        </div>
        <div class="panel-body no-padding" style="display: block;">
    <table class="table table-bordered table-striped pt-2">
        <tbody>
        <tr>
            <th>Назва</th>
            <th>Посилання</th>
            <th>Переміщення</th>
        </tr>
        </tbody>
        @foreach($business as $item)
            <tr>
                <td>
                    @for($i = 0; $i < $item->depth; $i++) &mdash; @endfor
                    <a href="{{ route('admin.business.show', $item) }}">{{$item->name}}</a>
                </td>
                <td>
                    {{$item->slug}}
                </td>
                <td>
                    <div class="row"></div>
                    <form action="{{ route('admin.business.first', $item) }}" method="POST" class="col-sm-2">
                        {{ csrf_field() }}
                        <button class="btn btn-sm btn-primary" style="border-radius: 50px;"><span class="lnr lnr-arrow-up-circle" style="font-weight: 900; font-size: 16px"></span></button>
                    </form>
                    <form action="{{ route('admin.business.up', $item) }}" method="POST" class="col-sm-2">
                        {{ csrf_field() }}
                        <button class="btn btn-sm btn-primary" style="border-radius: 50px;"><span class="lnr lnr-chevron-up-circle" style="font-weight: 900; font-size: 16px"></span></button>
                    </form>
                    <form action="{{ route('admin.business.down', $item) }}" method="POST" class="col-sm-2">
                        {{ csrf_field() }}
                        <button class="btn btn-sm btn-primary" style="border-radius: 50px;"><span class="lnr lnr-chevron-down-circle" style="font-weight: 900; font-size: 16px"></span></button>
                    </form>
                    <form action="{{ route('admin.business.last', $item) }}" method="POST" class="col-sm-2">
                        {{ csrf_field() }}
                        <button class="btn btn-sm btn-primary" style="border-radius: 50px;"><span class="lnr lnr-arrow-down-circle" style="font-weight: 900; font-size: 16px"></span></button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    </div>
    </div>
@endsection
