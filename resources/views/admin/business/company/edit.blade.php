@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <form action="{{ route('admin.business.company.update', [$company->category_id, $company]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div class="form-group">
                    <label for="title">Назва</label>
                    <input id="title" name="title" class="form-control{{ $errors->has('title') ? 'is-invalid': '' }}"
                           value="{{ old('title', $company->title) }}" required>
                    @if( $errors->has('title'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('title') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="phone">Телефон</label>
                    <input id="phone" name="phone" class="form-control{{ $errors->has('phone') ? 'is-invalid': '' }}"
                           value="{{ old('phone', $company->phone) }}" required>
                    @if( $errors->has('phone'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('phone') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="site">Посилання на сайт</label>
                    <input id="site" name="site" class="form-control{{ $errors->has('site') ? 'is-invalid': '' }}"
                           value="{{ old('site', $company->site) }}" required>
                    @if( $errors->has('site'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('site') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="email">Пошта</label>
                    <input id="email" name="email" class="form-control{{ $errors->has('email') ? 'is-invalid': '' }}"
                           value="{{ old('email', $company->email) }}" required>
                    @if( $errors->has('email'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('email') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="category_id">Категорія</label>
                    <select id="category_id" name="category_id" class="form-control{{ $errors->has('category_id') ? 'is-invalid': '' }}">
                        @foreach($category as $parent)
                            <option value="{{ $parent->id }}"{{ $parent->id === $company->category_id ? 'selected': '' }}>
                                @for($i = 0; $i < $parent->depth; $i++) &mdash; @endfor
                                {{ $parent->name }}
                            </option>
                        @endforeach
                    </select>
                    @if( $errors->has('category_id'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('parent') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="region_id">Місто</label>
                    <select id="region_id" name="region_id" class="form-control{{ $errors->has('region_id') ? 'is-invalid': '' }}">
                            @foreach($regions as $par)
                                <option value="{{ $par->id }}"{{ $par->id === $company->region_id ? 'selected': '' }}>
                                    {{ $par->name }}
                                </option>
                            @endforeach
                    </select>
                    @if( $errors->has('region_id'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('region_id') }}</stron></span>
                    @endif
                </div>
                <div class="col-md-12" >
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-9">
                            <div class="form-group">
                                <label for="location">Адреса</label>
                                <input id="location" name="location" class="form-control{{ $errors->has('location') ? 'is-invalid' : '' }}" value="{{$company->location}}" required>
                                @if($errors->has('location'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('location') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-10 col-xs-3">
                            <label for="location" style="color: #f5f5f5;">Авто</label>
                            <span class="location-button" data-target="#location"><img style="height: 30px; width: 30px; cursor: pointer;" src="{{asset('images/map-location.png')}}" alt=""></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="content">Контент</label>
                    <textarea id="content" name="content" class="form-control{{ $errors->has('content') ? 'is-invalid': '' }}"
                              rows="10" required>{{ old('content', $company->content) }}</textarea>
                    @if( $errors->has('content'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('content') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="address">Контакти</label>
                    <textarea id="address" name="address" class="form-control{{ $errors->has('address') ? 'is-invalid': '' }}"
                              rows="10" required>{{ old('address', $company->address) }}</textarea>
                    @if( $errors->has('address'))
                        <span class="invalid-feedback"><stron>{{ $errors->first('address') }}</stron></span>
                    @endif
                </div>
                <div class="form-group">
                    <img src="{{asset('app/'. $company->logo)}}" alt="{{ $company->logo }}" width="50" height="50" style="border-radius: 40px; float: left">
                    <label for="uploadbtn" class="uploadButton">Загрузити фото</label>
                    <input style="opacity: 0; z-index: -1;" type="file" name="logo" id="uploadbtn">
                    @if($errors->has('logo'))
                        <span class="invalid-feedback"><strong>{{ $errors->first('logo') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
@endsection