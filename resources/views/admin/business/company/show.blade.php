@extends('layouts.admin')

@section('content')
    <a href="{{ route('admin.business.company.edit', [$company->category_id, $company]) }}" class="btn btn-primary left"  style="margin-bottom: 20px;margin-top: 20px;">Корегувати</a>
    @if($company->isOnModeration())
        <form  method="POST" action="{{route('admin.company.send', [$company->category_id, $company])}}">
            {{ csrf_field() }}
            <button class="btn btn-success" style="float: left; margin-left: 5px; margin-top: 20px;">Відмодерувати</button>
        </form>
    @endif
    <form method="POST" action="{{ route('admin.business.company.destroy', [$company->category_id, $company]) }}">
        {{ csrf_field() }}
        {{method_field('DELETE')}}
        <button class="btn btn-danger left"  style="margin-bottom: 20px;margin-top: 20px;">Видалити</button>
    </form>
    <a href="{{ route('admin.business.company.create', $business) }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Створити оголошення</a>
    <a href="{{ route('admin.suggestions.create', $company) }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Створити пропозицію</a>
<div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
    <div class="panel-body no-padding">
        <table class="table table-striped">
            <tbody>
            <tr class="">
                <th class="warning">ID</th> <td>{{$company->id}}</td>
            </tr>
            <tr>
                <th class="warning">Назва</th><td><a href="{{ route('company.show', $company) }}">{{$company->title}}</a></td>
            </tr>
            <tr>
                <th class="warning">Категорія</th> <td>{{$business->name}}</td>
            </tr>
            <tr>
                <th class="warning">Статус</th>
                <td>
                    @if($company->isDraft())
                        <span class="info">Чорновик</span>
                    @elseif($company->isOnModeration())
                        <span class="primary">На модерації</span>
                    @elseif($company->isActive())
                        <span class="primary">Активний</span>
                    @elseif($company->isClosed())
                        <span class="error">Закрити</span>
                    @endif
                </td>
            </tr>
            <tr>
                <th class="warning">Адреса</th> <td>{{$company->location}}</td>
            </tr>
            <tr>
                <th class="warning">Автор</th> <td><a href="{{ route('admin.users.show', $company->user->id) }}">{{$company->user->name}}</a></td>
            </tr>
            <tr>
                <th class="warning">Логотип</th> <td><img width="100" height="50" src="{{ asset('/app/'. $company->logo ) }}" alt="{{$company->title}}"></td>
            </tr>
            <tr>
                <th class="warning">Koнтент</th> <td>{{$company->content}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div style="margin-top: 10px; position: relative" class="col-md-6">
        <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
            <div class="panel-heading"  style="background-color: #8BC34A !important;" >
                <h2>Список пропозицій</h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
            </div>
            <div class="panel-body no-padding" style="display: block;">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Назва</th>
                        <th>Фото</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($suggestions as $item)
                        <tr>
                            <td><a href="{{ route('admin.suggestions.show', $item) }}">{{$item->title}}</a></td>
                           <td><img src="{{asset('app/'. $item->avatar)}}" alt="{{ $item->avatar }}" width="50" height="50" style="border-radius: 40px"></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div style="margin-top: 10px; position: relative" class="col-md-6">
        <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
            <div class="panel-heading"  style="background-color: #8BC34A !important;" >
                <h2>Список оголошень</h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
            </div>
            <div class="panel-body no-padding" style="display: block;">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Назва</th>
                        <th>Фото</th>
                        <th>Статус</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($adverts as $advert)
                    <tr>
                    <td><a href="{{ route('adverts.show', $advert) }}">{{ $advert->title }}</td>
                    <td><img src="{{asset('app/'. $advert->avatar)}}" width="50" height="50" style="border-radius: 40px"></td>
                    <td>
                    @if($advert->isDraft())
                    <span class="info">Draft</span>
                    @elseif($advert->isOnModeration())
                    <span class="primary">Moderation</span>
                    @elseif($advert->isActive())
                    <span class="primary">Active</span>
                    @elseif($advert->isClosed())
                    <span class="error">Closed</span>
                    @endif
                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection