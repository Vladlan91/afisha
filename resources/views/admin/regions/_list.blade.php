<div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
    <div class="panel-heading"  style="background-color: #8BC34A !important;" >
        <h2>Список регіонів</h2>
        <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
    </div>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Назва</th>
        <th>Посилання</th>
    </tr>
    </thead>
    <tbody>
    @foreach($regions as $region)
        <tr>
            <th>{{$region->id}}</th>
            <th><a href="{{ route('admin.regions.show', $region) }}">{{$region->name}}</a></th>
            <th>{{$region->slug}}</th>
        </tr>
    @endforeach
    </tbody>
</table>
</div>