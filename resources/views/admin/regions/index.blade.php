@extends('layouts.admin')

@section('content')
    <p><a href="{{ route('admin.regions.create') }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Створити місто</a></p>
   @include('admin.regions._list', ['regions' =>$regions])
    <div style="margin-bottom: 40px;">
    </div>
@endsection
