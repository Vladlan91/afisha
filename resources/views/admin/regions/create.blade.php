@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <form action="{{ route('admin.regions.store', ['parent' => $parent ? $parent->id : null]) }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Назва</label>
            <input id="name" name="name" class="form-control{{ $errors->has('name') ? 'is-invalid': '' }}"
                   value="{{ old('name') }}" required>
            @if( $errors->has('name'))
                <span class="invalid-feedback"><stron>{{ $errors->first('name') }}</stron></span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Зберегти</button>
        </div>
    </form>
        </div>
    </div>
@endsection