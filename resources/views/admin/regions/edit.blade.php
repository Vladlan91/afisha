@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
         <form method="POST" action="{{ route('admin.regions.update', $region) }}">
        {{ csrf_field() }}
        {{method_field('PUT')}}
        <div class="form-group">
            <label for="name">Назва</label>
            <input id="name" name="name" class="form-control{{ $errors->has('name') ? 'is-invalid': '' }}"
                   value="{{ $region->name }}" required>
            @if( $errors->has('name'))
                <span class="invalid-feedback"><stron>{{ $errors->first('name') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <label for="slug">Посилання</label>
            <input id="slug" name="slug" class="form-control{{ $errors->has('slug') ? 'is-invalid': '' }}"
                   value="{{ $region->slug }}" required>
            @if( $errors->has('slug'))
                <span class="invalid-feedback"><stron>{{ $errors->first('slug') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Зберегти</button>
        </div>
    </form>
        </div>
    </div>
@endsection