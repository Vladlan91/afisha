@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <form action="{{ route('admin.users.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">I`мя</label>
            <input id="name" name="name" class="form-control{{ $errors->has('name') ? 'is-invalid': '' }}"
                   value="{{ old('name') }}" required>
            @if( $errors->has('name'))
                <span class="invalid-feedback"><stron>{{ $errors->first('name') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input id="email" name="email" class="form-control{{ $errors->has('email') ? 'is-invalid': '' }}"
                   value="{{ old('email') }}" required>
            @if( $errors->has('email'))
                <span class="invalid-feedback"><stron>{{ $errors->first('email') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Зберегти</button>
        </div>
    </form>
        </div>
    </div>
@endsection