@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
    <form method="POST" action="{{ route('admin.users.update', $user) }}" class="form-horizontal">
        {{ csrf_field() }}
        {{method_field('PUT')}}
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">І'мя</label>
            <div class="col-sm-8">
            <input id="name" name="name" class="form-control1{{ $errors->has('name') ? 'is-invalid': '' }}"
                   value="{{ old('name', $user->name) }}" required>
            @if( $errors->has('name'))
                <span class="invalid-feedback"><stron>{{ $errors->first('name') }}</stron></span>
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Емейл</label>
            <div class="col-sm-8">
            <input id="email" name="email" class="form-control1{{ $errors->has('email') ? 'is-invalid': '' }}"
                   value="{{ old('email', $user->email) }}" required>
            @if( $errors->has('email'))
                <span class="invalid-feedback"><stron>{{ $errors->first('email') }}</stron></span>
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Статус</label>
            <div class="col-sm-8">
            <select id="status" name="status" class="form-control1{{ $errors->has('status') ? 'is-invalid': '' }}">
                @foreach($statuses as $value => $label)
                    <option value="{{ $value }}"{{ $value === old('status', $user->status) ? 'selected' : '' }}>{{ $label }}</option>
                @endforeach
            </select>
            @if( $errors->has('status'))
                <span class="invalid-feedback"><stron>{{ $errors->first('status') }}</stron></span>
            @endif
            </div>

        </div>

        <div class="form-group">
            <label for="role" class="col-sm-2 control-label">Роль</label>
            <div class="col-sm-8">
            <select id="role" name="role" class="form-control1{{ $errors->has('role') ? 'is-invalid': '' }}">
                @foreach($roles as $value => $label)
                    <option value="{{ $value }}"{{ $value === old('role', $user->role) ? 'selected' : '' }}>{{ $label }}</option>
                @endforeach
            </select>
            @if( $errors->has('role'))
                <span class="invalid-feedback"><stron>{{ $errors->first('role') }}</stron></span>
            @endif
            </div>

        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Зберегти</button>
        </div>
    </form>
        </div>
    </div>
@endsection