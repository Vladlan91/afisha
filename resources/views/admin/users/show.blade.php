@extends('layouts.admin')

@section('content')
    <p style="padding-bottom: 30px;position: absolute;"><div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-primary left">Корегувати</a>
        <a href="{{route('admin.user.balance.add', $user)}}" class="btn btn-primary left">Поповнити баланс</a>
        @if($user->hasMoney())
            <a href="{{route('admin.user.balance.get', $user)}}" class="btn btn-primary left">Списати з балансу</a>
        @endif
        <a href="{{route('admin.users.order', $user)}}" class="btn btn-primary left">Історія транзакцій</a>
        <a href="{{route('admin.adverts.adverts.create', $user)}}" class="btn btn-primary left">Подати оголошення</a>
        @if($user->isWait())
        <form method="POST" action="{{ route('admin.users.verify', $user) }}">
            {{ csrf_field() }}
            <button class="btn btn-success left">Активувати</button>
        </form>
        @endif
        <form method="POST" action="{{ route('admin.users.destroy', $user) }}">
            {{ csrf_field() }}
            {{method_field('DELETE')}}
            <button class="btn btn-danger left">Видалити</button>
        </form>
    </div></p>
    <br>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
        <table class="table table-striped">
        <tbody>
            <tr class="">
                <th class="warning">ID</th> <td>{{$user->id}}</td>
            </tr>
            <tr>
                <th class="warning">І'мя</th> <td>{{$user->name}}</td>
            </tr>
            <tr>
                <th class="warning">Email</th> <td>{{$user->email}}</td>
            </tr>
            <tr>
                <th class="warning">Баланс</th>
                @php($res = substr($user->balance, 0,1))
                @if($res === '-')
                    <td><p style="background-color: #ff738e; color: black; font-weight: 900; text-align: center;">{{$user->balance}}&nbsp;грн.</p></td>
                @elseif($res === '0')
                    <td><p style="background-color: #ffef37; color: black; font-weight: 900; text-align: center;">{{$user->balance}}&nbsp;грн.</p></td>
                @else
                    <td><p style="background-color: aquamarine; color: black; font-weight: 900; text-align: center;">{{$user->balance}}&nbsp;грн.</p></td>
                @endif
            </tr>
            <tr>
                <th class="warning">Статус</th>
                <td>
                    @if($user->isWait())
                        <span class="success">Очікує підтвердження емейлу</span>
                    @endif
                    @if($user->isActive())
                        <span class="primary">Активний</span>
                    @endif
                </td>
            </tr>
            <tr>
                <th class="warning">Роль</th>
                <td>
                    @if($user->isAdmin())
                        <span class="error">Адміністратор</span>
                    @else
                        <span class="info">Звичайний користувач</span>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
    </div>
    </div>
    <div style="position: relative">
        <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
            <div class="panel-heading"  style="background-color: #8BC34A !important;" >
                <h2>Список оголошень</h2>
            </div>
            <div class="panel-body no-padding" style="display: block;">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Обновлено</th>
                        <th>Назва</th>
                        <th>Автор</th>
                        <th>Місто</th>
                        <th>Категорія</th>
                        <th>Статус</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($adverts as $advert)
                        <tr>
                            <td>{{ $advert->id }}</td>
                            <td>{{ $advert->created_at }}</td>
                            <td><a href="{{ route('adverts.show', $advert) }}" target="_self">{{ $advert->title }}</a></td>
                            <td>{{ $advert->user->name }}</td>
                            @if($advert->region)
                                <td>{{ $advert->region->name }}</td>
                            @else
                                <td>All region</td>
                            @endif
                            <td>{{ $advert->category->name }}</td>
                            <td>
                                @if($advert->isDraft())
                                    <span class="info">Чорновик</span>
                                @elseif($advert->isOnModeration())
                                    <span class="primary">На модерації</span>
                                @elseif($advert->isActive())
                                    <span class="primary">Активний</span>
                                @elseif($advert->isClosed())
                                    <span class="error">Закрити</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 40px;">
        {{ $adverts->links() }}
    </div>
@endsection