@extends('layouts.admin')

@section('content')
    <div style="background-color: azure; position: relative;">
        <div class="card" style="background-color: azure; margin-bottom: 20px">
            <div class="modal-header">
                Фільтер
            </div>
            <div class="card-body">
                <form action="?" method="GET">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="id">ID</label>
                            <input class="form-control1 input-search" name="id" id="id" value="{{ request('id') }}">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="type">Тип</label>
                            <select class="form-control1 input-search" name="type" id="type">
                                <option value="">Тип платежу</option>
                                @foreach($types as $type)
                                    @if($type === 'EasyPay')
                                        <option value="{{ $type }}">Поповнення платіжною системою</option>
                                    @elseif($type === 'error')
                                        <option value="{{ $type }}">Помилка системи</option>
                                    @elseif($type === 'userpanel')
                                        <option value="{{ $type }}">Списання з рахунку</option>
                                    @else
                                        <option value="{{ $type }}">Дії адміністратора</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="action">Тип</label>
                            <select class="form-control1 input-search" name="action" id="action">
                                <option value="">Тип послуги </option>
                                @foreach($actionList as $action)
                                    @if($action === 'banners')
                                        <option value="{{ $action }}">Проплата за банер</option>
                                    @elseif($action === 'adverts')
                                        <option value="{{ $action }}">Проплата за оголошення</option>
                                    @else
                                        <option value="{{ $action }}">Проплата за тарифний пакет</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="start">Пошук по даті від:</label>
                            <input type="date" id="start" name="trip-start"
                                   value="{{request('trip-start')}}"
                                   min="" max="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="start">Пошук по даті по:</label>
                            <input type="date" id="finish" name="trip-finish"
                                   value="{{request('trip-finish')}}"
                                   {{--@php(\Carbon\Carbon::now()->setTime(0,0)->format('Y-m-d H:i:s'))--}}
                                   {{--value="{{\Carbon\Carbon::now()->toDateString()}}"--}}
                                   min="" max="">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label"> </label><br />
                            <button type="submit" class="btn btn-primary">Пошук</button>
                            <a href="?" class="btn btn-primary">Очистити</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="position: relative; margin-top: 190px;">
        <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
            <div class="panel-heading"  style="background-color: #8BC34A !important;" >
                <h2>Список транзакцій</h2>
            </div>
            <div class="panel-body no-padding" style="display: block;">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Cтворено</th>
                        <th>Тип платежу</th>
                        <th>Тип послуги</th>
                        <th>Сума</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->created_at }}</td>
                            @if($item->system === 'EasyPay')
                                <td  style="color: green; font-weight: 900; "Поповнення платіжною системою</td>
                                @elseif($item->system === 'error')
                                <td style="color: red; font-weight: 900; ">Помилка системи</td>
                            @elseif($item->system === 'userpanel')
                                <td  style="color: #676767; font-weight: 900; ">Списання з рахунку</td>
                                @else
                                <td  style="color: blue; font-weight: 900; ">Дії адміністратора</td>
                            @endif
                            @if($item->action === 'banners')
                                <td >Проплата за банер</td>
                            @elseif($item->action === 'adverts')
                                <td>Проплата за оголошення</td>
                            @else
                                <td>Проплата за тарифний пакет</td>
                            @endif
                            @php($res = substr($item->sum, 0,1))
                            @if($res !== '-')
                            <td><p style="background-color: grey; color: white; border-radius: 20px; padding-right: 5px; padding-left: 5px; text-align: center">{{ $item->sum }}</p></td>
                            @else
                                <td><p style="background-color: rgba(24,24,24,0.97); color: red; border-radius: 20px; padding-right: 5px; padding-left: 5px; text-align: center">{{ $item->sum }}</p></td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 40px;">
        {{ $order->links() }}
    </div>
@endsection