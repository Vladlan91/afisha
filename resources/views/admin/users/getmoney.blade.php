@extends('layouts.admin')

@section('content')
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-body no-padding">
            <table class="table table-striped">
                <tbody>
                <tr class="">
                    <th class="warning">ID</th> <td>{{$user->id}}</td>
                </tr>
                <tr>
                    <th class="warning">І'мя</th> <td>{{$user->name}}</td>
                </tr>
                <tr>
                    <th class="warning">Email</th> <td>{{$user->email}}</td>
                </tr>
                <tr>
                    <th class="warning">Баланс</th>
                    @php($res = substr($user->balance, 0,1))
                    @if($res === '-')
                        <td><p style="background-color: #ff738e; color: black; font-weight: 900; text-align: center;">{{$user->balance}}&nbsp;грн.</p></td>
                    @elseif($res === '0')
                        <td><p style="background-color: #ffef37; color: black; font-weight: 900; text-align: center;">{{$user->balance}}&nbsp;грн.</p></td>
                    @else
                        <td><p style="background-color: aquamarine; color: black; font-weight: 900; text-align: center;">{{$user->balance}}&nbsp;грн.</p></td>
                    @endif
                </tr>
                <tr>
                    <th class="warning">Статус</th>
                    <td>
                        @if($user->isWait())
                            <span class="success">Очікує підтвердження емейлу</span>
                        @endif
                        @if($user->isActive())
                            <span class="primary">Активний</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="warning">Роль</th>
                    <td>
                        @if($user->isAdmin())
                            <span class="error">Адміністратор</span>
                        @else
                            <span class="info">Звичайний користувач</span>
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div style="position: relative">
        <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
            <div class="panel-heading"  style="background-color: #8BC34A !important;" >
                <h2>Поповнення</h2>
            </div>
            <div class="panel-body no-padding" style="display: block;">
                <form action="{{route('admin.balance.get.save', $user)}}" method="POST">
                    {{ csrf_field() }}
                    {{method_field('PUT')}}
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('balance') ? ' has-error' : '' }}">
                            <label for="balance">Сума списання</label>
                            <input id="balance" name="balance" class="form-control" value="{{ old('balance') }}" >
                            @if ($errors->has('balance'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('balance') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('action') ? ' has-error' : '' }}">
                            <label for="action">Тип платежу</label>
                            <select id="action" name="action" class="form-control" value="{{ old('action') }}" >
                                <option value="">Обрати ціль списання</option>
                                @foreach($type as $item)
                                    @if($item === 'banners')
                                        <option value="{{$item}}">Для подачі банера</option>
                                    @elseif($item === 'adverts')
                                        <option value="{{$item}}">Для подачі оголошення</option>
                                    @else
                                        <option value="{{$item}}">Для придбання тарифного пакету</option>
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('action'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('action') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3" style="margin-top: 30px;">
                        <button class="btn btn-success" type="submit">Списати</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection