@extends('layouts.admin')

@section('content')
   <p> <a href="{{ route('admin.users.create') }}" class="btn btn-success " style="margin-bottom: 20px;margin-top: 20px;">Створити Користувача</a></p>
    <div class="card">
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" id="id" name="id" class="form-control1 input-search" placeholder="Пошук по ID">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" id="name" class="form-control1 input-search" name="name" value="{{ request('name') }}" placeholder="Пошук по Імені">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" id="email" class="form-control1 input-search" name="email" value="{{ request('email') }}" placeholder="Пошук по Eмейл">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" id="phone" class="form-control1 input-search" name="phone" value="{{ request('phone') }}" placeholder="Пошук по Телефону">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Пошук</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
        <div class="panel-heading"  style="background-color: #8BC34A !important;" >
            <h2>Список користувачів</h2>
            <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
        </div>
        <div class="panel-body no-padding" style="display: block;">
            <table class="table table-striped">
                <thead>
                <tr class="warning">
                    <th>ID</th>
                    <th>І'мя</th>
                    <th>Емейл</th>
                    <th>Кількість оголошень</th>
                    <th>Телефон</th>
                    <th>Рахунки</th>
                    <th>Роль</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <th>{{$user->id}}</th>
                        <th><a href="{{ route('admin.users.show', $user) }}">{{$user->name}}</a></th>
                        <th>{{$user->email}}</th>
                        <th>{{$user->adverts->count()}}</th>
                        <th>{{$user->phone}}</th>
                        <th>{{$user->orders->count()}}</th>
                        <th>
                            @if($user->isAdmin())
                                <span class="errorss">Адміністратор</span>
                            @else
                                <span class="info">Користувач</span>
                            @endif
                        </th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
   <div style="margin-bottom: 40px;">
   {{ $users->links() }}
   </div>
@endsection
