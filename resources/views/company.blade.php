@extends('layouts.app')
@section('content')
    <div class="s131">
        <form action="{{route('company.search')}}" method="POST">
            {{ csrf_field() }}
            <div class="inner-form">
                <div class="input-field first-wrap">
                    <input name="title" id="title"  type="text" placeholder="Пошук по тексту" />
                </div>
                <div class="input-field second-wrap">
                    <div class="input-select">
                        <select data-trigger=""  id="category"  name="category">
                                <option  value="{{ null }}">Категорії</option>
                            @if(isset($category))
                                @foreach($listCategories as $item )
                                    <option  value="{{ $item->id }}"{{ $item->id === $category->id ? 'selected': '' }}>{{ $item->name }}</option>
                                @endforeach
                            @else
                                @foreach($listCategories as $item )
                                    <option  value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="input-field second-wrap">
                    <div class="input-select">
                        <select data-trigger=""  id="region"  name="region">
                                <option value="{{ null }}">Міста</option>
                            @if(isset($region))
                                @foreach($listRegion as $item )
                                    <option  value="{{ $item->id }}"{{ $item->id === $region->id ? 'selected': '' }}>{{ $item->name }}</option>
                                @endforeach
                            @else
                                @foreach($listRegion as $item )
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="input-field third-wrap">
                    <button class="btn-search" type="">Пошук</button>
                </div>
            </div>
    </div>
    </form>
    </div>
    <div id="mobileAppsbadge" class="fix" style="display: block">
        <a href="{{ route('about') }}" class="icon tdnone abs" style="background:url(../images/icons2.png) no-repeat;"></a>
        <a href="#" id="mobileAppsbadgeClose" onclick="myFunction()" class="tdnone abs" title="Закрити"></a>
    </div>
    <script>
        function myFunction() {
            document.getElementById("mobileAppsbadge").style.display = "none";
        }
    </script>
    <div class="card">
        <div class="modal-header">
        </div>
        <div class="card-body" style="padding: 25px;">
            <div class="row">
                @foreach(array_chunk($categories, 3) as $chunk)
                    <div class="col-md-3">
                        <ul class="list-unstyled">
                            @foreach( $chunk as $current)
                                <li><a href="{{ route('company',  $current) }}">{{ $current->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="float: right">
            <div class="col-md-offset-10" style="font-size: 16px;">
                <a href="{{ route('cabinet.company.create') }}" style="text-align: center; color: #333e48">
                    <div class="miniCartWrap">
                        <div class="mini-maincart" style="font-size: 13px; color: #333333">
                            <div class="cartSummary">
                                <div class="icon-plus" style="margin: auto;display: block; font-size: 20px">
                                </div>
                            </div>
                            Додати компанію
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-9">
            <h4 style="font-weight: 900;">Компанії</h4>
            <ul class="tabsContent block-white">
                @foreach($companies as $advert)
                <li style="border-bottom: 1px solid #ccc !important;">
                    <a href="{{ route('company.show', company_path($advert)) }}" class="list_item clearfix trackable" data-info="{&quot;event_type&quot;: &quot;publisher&quot;, &quot;campaign&quot;: &quot;shop_listing&quot;, &quot;shop_cat&quot;: &quot;immobilier&quot;, &quot;shop_region&quot;: &quot;bourgogne&quot;, &quot;shop_subregion&quot;: &quot;saone_et_loire&quot;, &quot;shop_city&quot;: &quot;chalon_sur_saone&quot;, &quot;shop_id&quot;: &quot;39200&quot;, &quot;shop_name&quot;: &quot;neyrat_immobilier&quot;}">
                        <div class="item_image">
                            <span class="item_imagePic">
                                <span class="lazyload loaded" style="display:block; width:100%; height:100%;" data-imgsrc="{{  asset('/app/'. $advert->logo ) }}" data-imgalt="l'Immobilier réinventé"><img itemprop="image" content="https://img7.leboncoin.fr/bo-thumb/084730036580176.jpg" src="{{  asset('/app/'. $advert->logo ) }}" alt="l'Immobilier réinventé" style="height: 150px;"></span>
                            </span>
                        </div>
                        @if($advert->type === 'A')
                        <div style="position: absolute; left: 0;top: -12px;">
                            <h5 style="background-color: #ea1b25; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>А</h5>
                        </div>
                        @elseif($advert->type === 'Б')
                            <div style="position: absolute; left: 0;top: -12px;">
                                <h5 style="background-color: #4387f4; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Б</h5>
                            </div>
                        @elseif($advert->type === 'В')
                            <div style="position: absolute; left: 0;top: -12px;">
                                <h5 style="background-color: #45c639; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>В</h5>
                            </div>
                        @else
                            <div style="position: absolute; left: 0;top: -12px;">
                                <h5 style="background-color: #aaaaaa; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                            </div>
                        @endif
                        <section class="item_infos">
                            <h2 class="item_title mb0">
                                {{ \Illuminate\Support\Str::limit($advert->title, 20) }}
                            </h2>
                            <aside>
                                <p style="line-height: 1.5!important; margin-top: 0 !important;" class="item_slogan small-hidden tiny-hidden">{{ \Illuminate\Support\Str::limit($advert->content,30) }}</p>
                                <p style="line-height: 1.5!important; margin-top: 0 !important;"class="item_nbAds"><b>{{ $advert->adverts()->count() }} оголошень</b></p>
                                <p style="line-height: 1.5!important; margin-top: 0 !important;"class="item_nbAds"><b>{{ $advert->suggestion()->count() }} пропозицій</b></p>
                                <p style="line-height: 1.5!important; margin-top: 0 !important;"class="item_supp small-hidden tiny-hidden">{{ $advert->region ?  $advert->region->name : 'Всі міста'}}</p>
                                <p style="line-height: 1.5!important; margin-top: 0 !important;"class="item_supp">{{ $advert->business->name }}</p>
                            </aside>
                        </section>
                    </a>
                </li>
                @endforeach
            </ul>
            {{$companies->links()}}
        </div>
        <div class="col-md-3">
            <h4 style="font-weight: 900;">Пропозиції</h4>
            <ul class="">
                @foreach($suggestions as $suggest)
                    <li itemscope="" itemtype="http://schema.org/Offer" class="_3eDdy">
                        <a title="Maserati Granturismo 4.2" class="_2fKRW" data-qa-id="aditem_container" href="{{ route('suggestions.show', [$suggest->company_id, $suggest]) }}">
                            <div class="_2-jsN"><div class="LazyLoad is-visible">
                                    <div>
                                        <div class="_6ntGx" style="background-image: url(&quot;https://img1.leboncoin.fr/ad-image/fd96e60b12d1c41565d4c3ffd45db7e4a01c2bb6.jpg&quot;);"></div>
                                        <img src="{{asset('app/'. $suggest->avatar)}}" itemprop="image" content="https://img1.leboncoin.fr/ad-image/fd96e60b12d1c41565d4c3ffd45db7e4a01c2bb6.jpg" alt="subject"></div>
                                </div>
                                <span class="_1sbqp">
                                <div class="_3jAsY">
                                    <div class="_3KcVT">
                                        <span class="_1vK7W" name="spotlight">
                                            <svg viewBox="0 0 24 24" data-name="Calque 1">
                                                <path d="M18.43 0H5.57A2.63 2.63 0 0 0 3 2.67V24l9-4 9 4V2.67A2.63 2.63 0 0 0 18.43 0z"></path>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="_1MBDf">{{$suggest->company->title}}</div>
                                </div>
                            </span>
                            </div>
                            <div class="_3beID">
                                <section class="irAof">
                                    <p class="_3ZfBw">
                                        <span itemprop="name" data-qa-id="aditem_title">{{ \Illuminate\Support\Str::limit($suggest->title,30) }}</span>
                                    </p>
                                    <p class="_1s5WJ" itemprop="availableAtOrFrom" data-qa-id="aditem_location"><!-- react-text: 2239 -->{!! \Illuminate\Support\Str::limit($suggest->content,30) !!} <!-- /react-text --></p>
                                    <div class="CeFtS" itemprop="priceSpecification" itemscope="" itemtype="http://schema.org/PriceSpecification" data-qa-id="aditem_price">
                                        <meta itemprop="priceCurrency" content="EUR">
                                        <span class="_1_bNq">
                                        <span itemprop="price">{{$suggest->price}}</span><!-- react-text: 2244 -->&nbsp;UAH<!-- /react-text --></span>
                                    </div>
                                </section>
                                <div class="_3A9T7"></div>
                            </div>
                        </a>
                        <div class="_3Zm0x" data-qa-id="listitem_save_ad">
                            <div>
                                <div class="_3C4to">
                                    <div class="" data-tip="Sauvegarder l'annonce" data-place="left" data-for="toggleSavedFeaturedAd_1554842308" currentitem="false">
                                    <span class="_1vK7W" name="heartoutline">
                                        <svg viewBox="0 0 24 24" data-name="Calque 1">
                                            <path d="M21.19 2.24A6.76 6.76 0 0 0 12 3.61a6.76 6.76 0 0 0-9.19-1.37A6.89 6.89 0 0 0 0 7.58c-.16 4.84 4 8.72 10.26 14.66l.12.12a2.32 2.32 0 0 0 3.23 0l.13-.12C20 16.29 24.15 12.41 24 7.57a6.89 6.89 0 0 0-2.81-5.33zm-9.07 18.15l-.12.12-.12-.12C6.17 15 2.4 11.46 2.4 7.86a4.18 4.18 0 0 1 4.2-4.37 4.68 4.68 0 0 1 4.28 3h2.25a4.66 4.66 0 0 1 4.27-3 4.18 4.18 0 0 1 4.2 4.37c0 3.6-3.77 7.14-9.48 12.53z"></path>
                                        </svg>
                                    </span>
                                    </div>
                                    <div class="sc-fjdhpX bpzbGb">
                                        <div class="__react_component_tooltip place-top type-dark " id="toggleSavedFeaturedAd_1554842308" data-id="tooltip"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
