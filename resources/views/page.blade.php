@extends('layouts.app')

@section('meta')
    <meta name="description" content="{{ $page->description }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            @if($page->children)
                <ul class="list-unstyled">
                    @foreach($page->children as $child)
                        {{--<li style="padding: 5px !important;"><a style="font-size: 16px !important; " href="{{ route('page.show', $child) }}">{{ $child->title }}</a></li>--}}
                        <li style="padding: 5px !important;"><a style="font-size: 16px !important; " href="{{ route('page', page_path($child)) }}">{{ $child->title }}</a></li>
                    @endforeach
                </ul>
            @endif
        </div>
        <div class="col-md-9">
            <h1>{{ $page->title }}</h1>
            {!! clean($page->content) !!}
        </div>
    </div>

@endsection