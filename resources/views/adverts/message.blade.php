@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-9">
            <div class="col-md-8">
                <h1 style="float: left;">{{$advert->title}}</h1>
                <form action="{{ route('adverts.message.send', $advert) }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="message">Повідомлення</label>
                        <textarea id="message" name="message" class="form-control" rows="6" value="" required=""></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary" style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center; margin-top: 5px; border: none;">Надіслати</button>
                </form>
            </div>
            <div class="col-md-4">
                <div style="background: #f6f6f6; border-top: 7px solid red; border-radius: 8px;">
                    <p  style="font-size: 36px; padding: 10px;">{{ $advert->price }} грн.</p>
                </div>
                <span class="btn btn-primary phone-button " style="position: center; border-color: red !important; background-color: red; margin-top: 2%; margin-bottom: 2%; width: 100%; font-weight: 900" data-source="{{ route('adverts.phone', $advert) }}"><span class="number">+38xxx-xxx-xx-xx Показати</span></span>
                <div class="offer-sidebar__box">
                    <div class="offer-user__location">
                        <div class="offer-user__address">
                            <span class="lnr lnr-map-marker" style="color: red; font-weight: 900; font-size: 25px; "></span>
                            <address>
                                <p>{{ $advert->address }}</p>
                            </address>
                        </div>
                        <a href="https://ruslanif.olx.ua" class="offer-user__image-link">
                            <div class="offer-user__image">
                                @if($advert->isCompany())
                                    <img src="{{asset('app/'. $advert->company->logo)}}" alt="{{ $advert->user->name }}" width="75" height="75" style="border-radius: 40px">
                                @else
                                    @if($advert->user->avatar)
                                        <img src="{{asset('/app/'. $advert->user->avatar)}}" alt="{{ $advert->user->name }}" width="75" height="75" style="border-radius: 40px">
                                    @else
                                        <img src="{{asset('images/no-avatar.jpg')}}" width="75" height="75" style="border-radius: 40px">
                                    @endif
                                @endif
                            </div>
                        </a>
                    </div>
                    <div class="offer-user__details ">
                        <h4>
                            @if($advert->isCompany())
                                <a href="">{{ $advert->company->title }}</a>
                            @else
                                <a href="">{{ $advert->user->name }}</a>
                            @endif
                        </h4>
                        <span class="user-since">на Afisha з  {{ Carbon\Carbon::parse($advert->user->created_at)->format('Y') }} року</span>
                        @if($advert->isCompany())
                            <a class="user-offers" style="border: 2px solid red; color: red;" href="{{ route('adverts.show.company.all', $advert->company->id) }}">Оголошення компанії</a>
                        @else
                            <a class="user-offers" style="border: 2px solid red; color: red;" href="{{ route('adverts.show.user.all', $advert->user->id) }}">Оголошення автора</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3"  style="margin-top: 20px;">
            <div class="banner" data-format="240x440" data-category="{{ $advert->category ?  $advert->category->id : '' }}" data-region="{{ $advert->region ? $advert->region->id : '' }}" data-url="{{ route('banner.get') }}">

            </div>
            <div class="clearfix" style="margin-top: 30px;"></div>
        </div>
    </div>
@endsection