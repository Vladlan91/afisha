@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('cabinet.adverts.company', $advert) }}">
        {{ csrf_field() }}
        {{method_field('PUT')}}

        <div class="form-group">
            <label for="company_id">Категорія</label>
            <select id="company_id" name="company_id" class="form-control{{ $errors->has('company_id') ? 'is-invalid': '' }}">
                @foreach($company as $parent)
                    <option value="{{ $parent->id }}"{{ $parent->id === $advert->company_id ? 'selected': '' }}>
                        {{ $parent->title }}
                    </option>
                @endforeach
            </select>
            @if( $errors->has('company_id'))
                <span class="invalid-feedback"><stron>{{ $errors->first('company_id') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>

@endsection