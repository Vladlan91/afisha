@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div style="border: 2px solid #0198d0; red; padding: 20px; margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-3">
                        <div class="log-company" style="margin: 20px;">
                            <a href="{{route('company.show', company_path($company))}}"><p style="font-size: 24px; line-height: 28px; font-weight: 700; display: inline-block; max-width: 300px;">{{$company->title}}</p></a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5"  style="margin: 20px;">
                                <span class="lnr lnr-map-marker"  style="color: #0a90eb; font-size: 14px; font-weight: 700; margin-right: 2px;"></span>{{$company->location}}
                            </div>
                            <div class="col-md-5"  style="margin: 20px;">
                                <span class="lnr lnr-phone" style="color: #0a90eb; font-size: 14px; font-weight: 700; margin-right: 2px;"></span>{{$company->phone}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div>
                            <img  class="center" src="{{  asset('/app/'. $company->logo ) }}" alt="телевизор &quot;TELEFUNKEN&quot; 26 LCD" height="70" style="text-align:right; display:block; margin:0 auto;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <ul class="_1rXe6">
                @foreach($adverts as $advert)
                    <li>
                        <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            @if($advert->type === 'A')
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #aaaaaa; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>А</h5>
                                </div>
                            @elseif($advert->type === 'Б')
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #45c639; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Б</h5>
                                </div>
                            @elseif($advert->type === 'В')
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>В</h5>
                                </div>
                            @elseif($advert->type === 'Г')
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                                </div>
                            @elseif($advert->type === 'Д')
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #fed700; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                                </div>
                            @else
                                <div style="position: absolute; left: 0;top: -12px; z-index: 1;">
                                    <h5 style="background-color: #ea1b25; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Е</h5>
                                </div>
                            @endif
                            <div class="item_image">
                                <span class="item_imagePic">
                                        <img src="{{  asset('/app/'. $advert->avatar ) }}" alt="Appartement 4 pièces 101 m²" style="height: 150px;">
                                </span>
                                <span class="item_imageNumber">
                        	<i class="icon-camera icon-2x nomargin"></i>
                        	<span>3</span>
                      	</span>
                            </div>
                            <div title="" class="saveAd" data-savead-id="1557273607">
                                @if($advert->isCompany())
                                    <img src="{{asset('app/'. $advert->company->logo)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                @else
                                    @if($advert->user->avatar)
                                        <img src="{{asset('app/'. $advert->user->avatar)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                    @else
                                        <img src="{{asset('images/no-avatar.jpg')}}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                    @endif
                                @endif
                            </div>
                            <section class="item_infos">
                                <h2 class="item_title">
                                    {{ \Illuminate\Support\Str::limit($advert->title,30) }}
                                </h2>
                                <h3 class="item_price">
                                    {{ $advert->price }}&nbsp;UAH
                                </h3>
                                <aside>
                                    <p class="item_supp">
                                        {{ $advert->category->name }}
                                        /
                                        {{ $advert->region ?  $advert->region->name : 'Всі міста'}}
                                    </p>
                                    <p class="item_supp item_suppDate">
                                        {{ $advert->user->name }}
                                    </p>
                                </aside>
                            </section>
                            @if(\Illuminate\Support\Facades\Auth::user() && !\Illuminate\Support\Facades\Auth::user()->hasInFavorites($advert->id) )
                                <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                    {{ csrf_field() }}
                                    <button class="" style="margin-right: 10px; color: black; border-radius: 20px; padding: 5px;border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-heart" style="font-size: 20px; font-weight: 900"></span></button>
                                </form>
                            @elseif(\Illuminate\Support\Facades\Auth::user())
                                <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                    {{ csrf_field() }}
                                    {{method_field('DELETE')}}
                                    <button class="" style="margin-right: 10px;  background-color: white; color: black; border-radius: 20px; padding: 5px; border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-unlink" style="font-size: 20px; font-weight: 900"></span></button>
                                </form>
                            @endif
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-3"  style="margin-top: 20px;">
            <div class="offer-sidebar__box">
                <div class="offer-user__location-two">
                    <div class="offer-user__address">
                        <span class="lnr lnr-user" style="color: red; font-weight: 900; font-size: 25px; "></span>
                        <address>
                            <p style="font-weight: 900; padding-left: 5px; padding-top: 4px;">Ваш рахунок: 0 грн.</p>
                            <br>
                            <p>Для подальшої подачі та реклами оголошень</p>
                        </address>
                        <a class="user-offers" href="https://ruslanif.olx.ua">Поповніть рахунок!</a>
                    </div>
                </div>
            </div>
            <h5 style="font-weight: 900; font-size: 14px; text-align: center;">Реклама</h5>
            <div class="banner" data-format="240x440" data-category="{{  $company->category ? $company->category->id : '' }}" data-region="{{ $company->region ? $company->region->id : '' }}" data-url="{{ route('banner.get') }}">
            </div>
            <div class="clearfix" style="margin-top: 30px;"></div>
        </div>
@endsection