@extends('layouts.app')
@section('content')
    @include('layouts.search._search', ['categories' => $categories, 'regions' => $regions ])
    <div class="card">
        <div class="modal-header">
        </div>
    </div>
        <div class="col-md-3">
            <h4 style="font-weight: 900">Категорії</h4>
            <p style="width: 100px; background-color: red; height: 2px;"></p>
            @foreach($categories as $chunk)
                    <ul class="list-unstyled">
                            <li  style="text-transform: uppercase !important;"><a href="{{ route('adverts.all',  [$chunk]) }}">{{ $chunk->name }}</a></li>
                    </ul>
            @endforeach
                <form action="{{route('adverts.search.atr', $category)}} " method="POST">
                    {{ csrf_field() }}
                    @if($category)
                    @foreach($category->allAttributes() as $attribute)
                        <div class="form-group">
                            <label for="attribute_{{ $attribute->id }}">{{ $attribute->name }}</label>
                            @if($attribute->isSelect())
                                <select id="attribute_{{ $attribute->id }}" class="form-control{{ $errors->has('attributes.' . $attribute->id) ? 'is-invalid' : ''  }}" name="attributes[{{ $attribute->id }}]">
                                    <option value=""></option>
                                    @foreach($attribute->variants as $variant)
                                        <option value="{{ $variant }}" {{ $variant === old('attributes.' . $attribute->id) ? 'selected' : ''}}>{{ $variant }}</option>
                                    @endforeach
                                </select>
                            @elseif($attribute->isNumber())
                                <input id="attribute_{{ $attribute->id }}" type="number" class="form-control{{ $errors->has('attributes.' . $attribute->id) ? 'is-invalid' : ''  }}" name="attributes[{{ $attribute->id }}]">
                            @else
                                <input id="attribute_{{ $attribute->id }}" type="text" class="form-control{{ $errors->has('attributes.' . $attribute->id) ? 'is-invalid' : ''  }}" name="attributes[{{ $attribute->id }}]">
                            @endif

                            @if($errors->has('parent'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('attributes.' . $attribute->id) }}</strong></span>
                            @endif
                        </div>
                    @endforeach
                    @endif
                    <div class="">
                        <button style="float: right; border-radius: 20px; background-color: #fd4235; padding-left: 30px; padding-right: 30px; padding-top: 10px; padding-bottom: 10px; color: white; margin-top: 10px;" type="submit">Пошук</button>
                    </div>
                </form>
        </div>
        <div class="col-md-9">
        <h4 style="font-weight: 900;">Оголошення</h4>
        <div class="banner" data-format="820x143" data-category="" data-region="" style="0.75em;">
            @foreach($banner1 as $ban)
                <a href="{{ route('banner.click', $ban) }}" target="_blank">
                    <img width="850"
                         height="143" src="{{ asset('/app/'. $ban->file ) }}" alt="{{ $ban->name }}">
                </a>
            @endforeach
        </div>
        <ul class="">
            @php($i = 1)
            @php($b = 0)
            @if($adverts->count() > 1)
                @foreach($adverts as $advert)
                    @if($i == 6)
        </ul>
            <div class="banner" data-format="820x143" data-category="" data-region="" style="margin-top: 10px;">
                @foreach($banner2 as $ban)
                    <a href="{{ route('banner.click', $ban) }}" target="_blank">
                        <img width="850"
                             height="143" src="{{ asset('/app/'. $ban->file ) }}" alt="{{ $ban->name }}">
                    </a>
                @endforeach
            </div>
            <ul class="">
                @elseif($i == 17 or $i == 27 or $i == 37 )
            </ul>
            <div class="banner" data-format="820x143" data-category="null" data-region="null" data-url="{{ route('banner.get') }}"  style="margin-top: 0.75em;">

            </div>
            <ul class="">
                @else
                    @if($advert->type === 'Е')
                        @php($b++ )
                    @endif
                    <li style="margin-top: 10px;">
                        @if($b < 6)
                            @if($advert->type === 'A')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" style="border: 1px solid #ccc !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                <h5 style="background-color: #aaaaaa; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Д</h5>
                            </div>
                            @elseif($advert->type === 'Б')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" style="border: 1px solid #45c639 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                <h5 style="background-color: #45c639; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                            </div>
                            @elseif($advert->type === 'В')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #219fde !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>В</h5>
                            </div>
                            @elseif($advert->type === 'Г')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #219fde !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Б</h5>
                            </div>
                            @elseif($advert->type === 'Д')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #fed700 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                <h5 style="background-color: #fed700; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>А</h5>
                            </div>
                            @else
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #ea1b25 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index: 1;">
                                <h5 style="background-color: #ea1b25; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Р</h5>
                            </div>
                            @endif
                            <div class="item_image">
                            <span class="item_imagePic">
                                    <img src="{{  asset('/app/'. $advert->avatar ) }}" alt="Appartement 4 pièces 101 m²" style="height: 150px;">
                            </span>
                            <span class="item_imageNumber">
                        	<i class="icon-camera icon-2x nomargin"></i>
                        	<span>3</span>
                      	</span>
                    </div>
                        <div title="" class="saveAd" data-savead-id="1557273607">
                            @if($advert->isCompany())
                                <img src="{{asset('app/'. $advert->company->logo)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                            @else
                                @if($advert->user->avatar)
                                    <img src="{{asset('app/'. $advert->user->avatar)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                @else
                                    <img src="{{asset('images/no-avatar.jpg')}}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                @endif
                            @endif
                        </div>
                        <section class="item_infos">
                            <h2 class="item_title">
                                {{ \Illuminate\Support\Str::limit($advert->title,30) }}
                            </h2>
                            <h3 class="item_price">
                                {{ $advert->price }}&nbsp;UAH
                            </h3>
                            <aside>
                                <p class="item_supp">
                                    {{ $advert->category->name }}
                                    /
                                    {{ $advert->region ?  $advert->region->name : 'Всі міста'}}
                                </p>
                                <p class="item_supp item_suppDate">
                                    {{ $advert->user->name }}
                                </p>
                            </aside>
                        </section>
                        @if(\Illuminate\Support\Facades\Auth::user() && !\Illuminate\Support\Facades\Auth::user()->hasInFavorites($advert->id) )
                            <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                {{ csrf_field() }}
                                <button class="" style="margin-right: 10px; color: black; border-radius: 20px; padding: 5px;border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-heart" style="font-size: 20px; font-weight: 900"></span></button>
                            </form>
                        @elseif(\Illuminate\Support\Facades\Auth::user())
                            <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button class="" style="margin-right: 10px;  background-color: white; color: black; border-radius: 20px; padding: 5px; border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-unlink" style="font-size: 20px; font-weight: 900"></span></button>
                            </form>
                        @endif
                    </a>
                    </li>
                    @else
                        @if($advert->type === 'Е')
                            @php($i--)
                        @else
                            @if($advert->type === 'A')
                                <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" style="border: 1px solid #ccc !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #aaaaaa; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Д</h5>
                                </div>
                                @elseif($advert->type === 'Б')
                                <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" style="border: 1px solid #45c639 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #45c639; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                                </div>
                                @elseif($advert->type === 'В')
                                <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #219fde !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>В</h5>
                                </div>
                                @elseif($advert->type === 'Г')
                                <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #219fde !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Б</h5>
                                </div>
                                @elseif($advert->type === 'Д')
                                <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #fed700 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #fed700; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>А</h5>
                                </div>
                                @else
                                <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #ea1b25 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index: 1;">
                                    <h5 style="background-color: #ea1b25; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Р</h5>
                                </div>
                                @endif
                                <div class="item_image">
                            <span class="item_imagePic">
                                    <img src="{{  asset('/app/'. $advert->avatar ) }}" alt="Appartement 4 pièces 101 m²" style="height: 150px;">
                            </span>
                            <span class="item_imageNumber">
                        	    <i class="icon-camera icon-2x nomargin"></i>
                        	<span>3</span>
                      	</span>
                        </div>
                        <div title="" class="saveAd" data-savead-id="1557273607">
                            @if($advert->isCompany())
                                <img src="{{asset('app/'. $advert->company->logo)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                            @else
                                @if($advert->user->avatar)
                                    <img src="{{asset('app/'. $advert->user->avatar)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                @else
                                    <img src="{{asset('images/no-avatar.jpg')}}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                @endif
                            @endif
                        </div>
                        <section class="item_infos">
                            <h2 class="item_title">
                                {{ \Illuminate\Support\Str::limit($advert->title,30) }}
                            </h2>
                            <h3 class="item_price">
                                {{ $advert->price }}&nbsp;UAH
                            </h3>
                            <aside>
                                <p class="item_supp">
                                    {{ $advert->category->name }}
                                    /
                                    {{ $advert->region ?  $advert->region->name : 'Всі міста'}}
                                </p>
                                <p class="item_supp item_suppDate">
                                    {{ $advert->user->name }}
                                </p>
                            </aside>
                        </section>
                        @if(\Illuminate\Support\Facades\Auth::user() && !\Illuminate\Support\Facades\Auth::user()->hasInFavorites($advert->id) )
                            <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                {{ csrf_field() }}
                                <button class="" style="margin-right: 10px; color: black; border-radius: 20px; padding: 5px;border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-heart" style="font-size: 20px; font-weight: 900"></span></button>
                            </form>
                        @elseif(\Illuminate\Support\Facades\Auth::user())
                            <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button class="" style="margin-right: 10px;  background-color: white; color: black; border-radius: 20px; padding: 5px; border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-unlink" style="font-size: 20px; font-weight: 900"></span></button>
                            </form>
                        @endif
                    </a>
                    </li>
                @endif
                @endif
                @endif
                @php($i++)
                @endforeach
                @else
                    <h4 style="text-align: center; color: #ff7263; font-weight: 900; margin-top: 70px;">В ДАНІЙ КАТЕГОРІЇ ОГОЛОШЕННЯ ВІДСУТНІ...</h4>
                    <p style="background-color: #ff7263; display: block; margin: 0 auto;width: 100px; height: 4px; margin-top: 20px;"></p>
                    <img style="display: block; margin: 0 auto; width: 250px;" src="{{asset('images/non-pro.png')}}" alt="">
            @endif
        </ul>
        {{$adverts->links()}}
    </div>
    </div>
@endsection
