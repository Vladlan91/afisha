@extends('layouts.app')
@section('content')
    <!-- jQuery 2.1.4 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"> </script>
    <div class="modal fade" id="myModal"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title" style="font-weight: 900">Публікація оголошення</h4>
                    <h6 class="modal-title" style="font-weight: 900; font-size: 14px; color: red;">Недостатньо коштів на рахунку</h6>
                    <button type="button" class="close" style="margin-top: -33px!important;" data-dismiss="modal">×</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <p style="line-height: 0.5;">Вартість публікації оголошення складає - <span style="font-size: 20px !important;">{{$advert->print_price}}&nbsp;грн</span></p>
                    <p style="line-height: 0.5;">Ваш баланс - <span style="font-size: 20px !important;">{{Auth::user()->balance}}&nbsp;грн</span></p>
                    <p style="line-height: 0.5;">Для подальшої публікації поповніть Ваш рахунок на суму - <span style="font-size: 20px !important;">{{$advert->print_price - Auth::user()->balance}}&nbsp;грн</span></p>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer" style="padding-bottom: 4px !important;">
                    {!! $html !!}
                </div>

            </div>
            {{ csrf_field() }}
        </div>
    </div>
    <script>
            $(document).on('click','.add-to-card-link', function () {
                var id = '{{Auth::id()}}';
                var advert = '{{$advert->print_price}}';
                var advertid = '{{$advert->id}}';
                var _token = $('input[name="_token"]').val();
                    console.log(id, advert );
                $.ajax({
                    url: "{{ route('moderation.advert') }}",
                    method: "POST",
                    data: {id: id, price: advert, advertid: advertid, _token:_token},
                    cache: false, success: function(result){
                        location.href = '{{route('adverts.show', $advert)}}';
                    },
                    error: function () {
                        $("#myModal").modal("show");
                    }

                });
            });
    </script>
    <div class="row">
        <div class="col-md-9">
            @can('manage-adverts', $advert)
                @if($advert->isOnModeration())
                    <form method="POST" action="{{ route('admin.adverts.adverts.moderate', $advert) }}">
                        {{ csrf_field() }}
                        <button class="bubbly-button-gre" style="float: left; margin-right: 10px;"  title="Відмодерувати"><span class="lnr lnr-star" style="font-size: 24px; font-weight: 900"></span></button>
                    </form>
                @endif
                @if(!$advert->isDraft())
                    <form method="POST" action="{{ route('admin.adverts.adverts.draft', $advert) }}">
                        {{ csrf_field() }}
                        <button class="bubbly-button-yellow"  title="В чорновик!" style="float: left;  margin-right: 10px;"><span class="lnr lnr-book" style="font-size: 24px; font-weight: 900"></span></button>
                    </form>
                @endif
                @if($advert->isDraft())
                        <form method="POST" action="{{ route('admin.adverts.adverts.send', $advert) }}">
                            {{ csrf_field() }}
                            <button class="bubbly-button-gre"   title="На модерацію!" style="float: left; margin-right: 10px;"><span class="lnr lnr-bullhorn" style="font-size: 24px; font-weight: 900"></span></button>
                        </form>
                @endif
                @if(!$advert->isDraft())
                    <a href="{{ route('admin.adverts.adverts.reject', $advert) }}" class="btn btn-warning btns-error" title="Відхилити!"><span class="lnr lnr-cross-circle" style="font-size: 24px; font-weight: 900"></span></a>
                @endif
                <form method="POST" action="{{ route('admin.adverts.adverts.destroy', $advert) }}">
                    {{ csrf_field() }}
                    {{method_field('DELETE')}}
                    <button class="bubbly-button"  title="Видалити!" style="float: left;  margin-right: 10px;"><span class="lnr lnr-unlink" style="font-size: 24px; font-weight: 900"></span></button>
                </form>
                <a href="{{ route('cabinet.adverts.company', $advert) }}" class="btn btn-info btns-info"  title="Закріпити за компанією"><span class="lnr lnr-apartment" style="font-size: 24px; font-weight: 900"></span></a>
                <a href="{{ route('admin.adverts.adverts.edit', $advert) }}" class="btn btn-primary btns-worning" title="Корегувати"><span class="lnr lnr-pencil"  style="font-size: 24px; font-weight: 900"></span></a>
                @if($advert->canAddPhoto())
                    <a href="{{ route('adverts.photos', $advert) }}" class="btn btn-info btns-info" title="Додати фото!"><span class="lnr lnr-picture" style="font-size: 24px; font-weight: 900"></span></a>
                @endif
            @endcan
        @if(!$advert->isActive())
            @can('manage-own-advert', $advert)
                    <a href="{{ route('cabinet.adverts.company', $advert) }}" class="btn btn-info btns-info"  title="Закріпити за компанією"><span class="lnr lnr-apartment" style="font-size: 24px; font-weight: 900"></span></a>
                @if($advert->isDraft())
                            @if(!Auth::user()->isAdmin())
                                <button class="bubbly-button-gre  add-to-card-link"  title="На модерацію!" style="float: left; margin-right: 10px;"><span class="lnr lnr-bullhorn" style="font-size: 24px; font-weight: 900"></span></button>
                            @endif
                    <form method="POST" action="{{ route('cabinet.adverts.destroy', $advert) }}" >
                        {{ csrf_field() }}
                        {{method_field('DELETE')}}
                        <button class="bubbly-button"title="Видалити!" style="float: left; margin-right: 10px;"><span class="lnr lnr-unlink" style="font-size: 24px; font-weight: 900"></span></button>
                    </form>
                        <a href="{{ route('cabinet.adverts.edit', $advert) }}" class="btn btn-primary btns-worning"  style="float: left;  margin-right: 10px;"title="Корегувати"><span class="lnr lnr-pencil" style="font-size: 24px; font-weight: 900"></span></a>
                    @if($advert->canAddPhoto())
                        <a href="{{ route('adverts.photos', $advert) }}" class="btn btn-info btns-info"  style="float: left;  margin-right: 10px;" title="Додати фото!"><span class="lnr lnr-picture" style="font-size: 24px; font-weight: 900"></span></a>
                    @endif
                @endif
            @endcan
        @endif
        </div>
        <div class="col-md-9">
            <div style="margin-top: 20px;">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="favoriet" style="position: absolute; top: 5%; left: 5%;">
                            @if($user && !$user->hasInFavorites($advert->id) )
                                <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                    {{ csrf_field() }}
                                    <button class="bubbly-button-gre" style="margin-right: 10px;"><span class="lnr lnr-heart" style="font-size: 20px; font-weight: 900"></span></button>
                                </form>
                            @elseif($user)
                                <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                    {{ csrf_field() }}
                                    {{method_field('DELETE')}}
                                    <button class="bubbly-button" style="margin-right: 10px;"><span class="lnr lnr-unlink" style="font-size: 20px; font-weight: 900"></span></button>
                                </form>
                            @endif
                            </div>
                            <div style="text-align: center;height: 493px; width: 100%; background: #f6f6f6; border-top: 7px solid red; border-radius: 8px;">
                                <img src="{{  asset('/app/'. $advert->avatar ) }} " alt="" style=" height: 100%;">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div style="background: #f6f6f6; border-top: 7px solid red; border-radius: 8px;">
                                @if(!empty($advert->price))
                                <p  style="font-size: 36px; padding: 10px;">{{ $advert->price }} грн.</p>
                                    @else
                                    <p  style="font-size: 20px; padding: 10px; color: black">ціна не вказана</p>
                                    @endif
                            </div>
                            <form method="POST" action="{{ route('adverts.message', $advert) }}">
                                {{ csrf_field() }}
                                <button class="btn btn-primary" style="position: center; border-color: red !important; background-color: red; margin-top: 2%; margin-bottom: 2%; width: 100%; font-weight: 900">Відправити повідомлення</button>
                            </form>
                            <span class="btn btn-primary phone-button " style="position: center; border-color: red !important; background-color: red; margin-top: 2%; margin-bottom: 2%; width: 100%; font-weight: 900" data-source="{{ route('adverts.phone', $advert) }}"><span class="number">+38xxx-xxx-xx-xx Показати</span></span>
                            <div class="offer-sidebar__box">
                                <div class="offer-user__location">
                                    <div class="offer-user__address">
                                        <span class="lnr lnr-map-marker" style="color: red; font-weight: 900; font-size: 25px; "></span>
                                        <address>
                                            <p>{{ $advert->address }}</p>
                                        </address>
                                    </div>
                                    <a href="https://ruslanif.olx.ua" class="offer-user__image-link">
                                        <div class="offer-user__image">
                                            @if($advert->isCompany())
                                                <img src="{{asset('app/'. $advert->company->logo)}}" alt="{{ $advert->user->name }}" width="75" height="75" style="border-radius: 40px">
                                            @else
                                                @if($advert->user->avatar)
                                                    <img src="{{asset('app/'. $advert->user->avatar)}}" alt="{{ $advert->user->name }}" width="75" height="75" style="border-radius: 40px">
                                                @else
                                                    <img src="{{asset('images/no-avatar.jpg')}}" width="75" height="75" style="border-radius: 40px">
                                                @endif
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <div class="offer-user__details ">
                                    <h4>
                                        @if($advert->isCompany())
                                            <a href="">{{ $advert->company->title }}</a>
                                        @else
                                            <a href="">{{ $advert->user->name }}</a>
                                        @endif
                                    </h4>
                                    <span class="user-since">на Afisha з  {{ Carbon\Carbon::parse($advert->user->created_at)->format('Y') }} року</span>
                                    @if($advert->isCompany())
                                        <a class="user-offers" style="border: 2px solid red; color: red;" href="{{ route('adverts.show.company.all', $advert->company->id) }}">Оголошення компанії</a>
                                    @else
                                        <a class="user-offers" style="border: 2px solid red; color: red;" href="{{ route('adverts.show.user.all', $advert->user->id) }}">Оголошення автора</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-md-12">
                @foreach($advert->photos()->orderBy('id')->get() as $photos)
                    <div class="col-md-3">
                        <img src="{{  asset('/app/'. $photos->file ) }} " alt="" style="height: 150px; width: auto; text-align: center;">
                    </div>
                @endforeach
            </div>
            <script type="text/javascript">
                $(function() {

//Set the default directory to find the images needed
//by the plugin (closebtn.png, blank.gif, loading images ....)
                    $.fn.fancyzoom.defaultsOptions.imgDir='../images/';//very important must finish
                    with a /

// Select all links in object with gallery ID using the defaults options
                    $('#gallery a').fancyzoom();

// Select all links with tozoom class, set the open animation time to 1000
                    $('a.tozoom').fancyzoom({Speed:1000});

// Select all links set the overlay opacity to 80%
                    $('a').fancyzoom({overlay:0.8});

//New, you can now apply the fancy zoom effect on an image
//apply the fancyzoom effect on all images that have the fancyzoom class
                    $("img.fancyzoom").fancyzoom();

                });
            </script>
            <h2 style="margin-top: 10px;">{{ $advert->title }}</h2>
            <p style="background-color: #f5f5f5; padding: 10px; border-radius: 20px;">
                <span class="lnr lnr-history" style="font-weight: 900; margin-right: 10px; color: #4387f4"></span>Дата публікації: {{ $advert->published_at }} <span class="lnr lnr-graduation-hat" style="font-weight: 900; margin-right: 10px; margin-left: 20px; color: #4387f4; font-size: 18px"></span>
                    @if($advert->isCompany())
                        Автор: {{ $advert->company->title }}
                    @else
                        Автор: {{ $advert->user->name }}
                    @endif

                @if($advert->expires_at)
                    &nbsp;Дата закриття: {{ $advert->expires_at }}
                @endif
            </p>
            <p>{!! nl2br(e($advert->content)) !!}</p>
            <p style="background-color: #f5f5f5; padding: 10px; border-radius: 20px;"><span class="lnr lnr-map" style="font-weight: 900; margin-right: 10px; color: #4387f4"></span>
                Адреса: {{ $advert->address }}</p>
            <table class="table table-bordered" style="background-color: #f6f6f6;">
                <tbody>

                     @foreach($advert->category->allAttributes() as $attribute)
                         <div class="col-md-6">
                             @if($advert->getValue($attribute->id))
                                 <p style="color: darkgrey; text-align: center; font-weight: 900; margin-left: 10px;">{{ $attribute->name }}
                                     @if($attribute->isCheckbox())
                                         <img src="{{asset('images/confirm.png')}}" alt="" style="width: 20px; height: 20px;">
                                     @else
                                         <span style="color: #4387f4; margin-left: 15px;">{{ $advert->getValue($attribute->id) }}</span>
                                     @endif
                             </span>
                             @endif
                         </div>
                     @endforeach
                </tbody>
            </table>
            <hr/>
            <div style="background: black; background-image: url('/images/map.png')">
                <div id="map" style="width: 100%; height: 250px;"></div>
                @section('script')
                    <script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=uk-UA" type="text/javascript"></script>

                    <script type='text/javascript'>
                        ymaps.ready(init);
                        function init(){
                            var geocoder = new ymaps.geocode(
                                // Строка с адресом, который нужно геокодировать
                                '{{ $advert->address }}',
                                // требуемое количество результатов
                                { results: 1 }
                            );
                            // После того, как поиск вернул результат, вызывается callback-функция
                            geocoder.then(
                                function (res) {
                                    // координаты объекта
                                    var coord = res.geoObjects.get(0).geometry.getCoordinates();
                                    var map = new ymaps.Map('map', {
                                        // Центр карты - координаты первого элемента
                                        center: coord,
                                        // Коэффициент масштабирования
                                        zoom: 7,
                                        // включаем масштабирование карты колесом
                                        behaviors: ['default', 'scrollZoom'],
                                        controls: ['mapTools']
                                    });
                                    // Добавление метки на карту
                                    map.geoObjects.add(res.geoObjects.get(0));
                                    // устанавливаем максимально возможный коэффициент масштабирования - 1
                                    map.zoomRange.get(coord).then(function(range){
                                        map.setCenter(coord, range[1] - 1)
                                    });
                                    // Добавление стандартного набора кнопок
                                    map.controls.add('mapTools')
                                    // Добавление кнопки изменения масштаба
                                        .add('zoomControl')
                                        // Добавление списка типов карты
                                        .add('typeSelector');
                                }
                            );
                        }
                    </script>
                @endsection
            </div>
            <div class="h3">Схожі оголошення</div>
            <div class="row">
                @foreach($adverts as $item)
                    <div class="col-sm-6 col-md-4">
                        <ul class="">
                            <li itemscope="" itemtype="http://schema.org/Offer" class="_3eDdy">
                                <a title="{{ $item->title }}" class="_2fKRW" data-qa-id="aditem_container" href="{{ route('adverts.show', $item) }}">
                                    <div class="_2-jsN"><div class="LazyLoad is-visible">
                                            <div>
                                                <div class="_6ntGx" style="background-image: url(&quot;https://img1.leboncoin.fr/ad-image/fd96e60b12d1c41565d4c3ffd45db7e4a01c2bb6.jpg&quot;);"></div>
                                                <img src="{{  asset('/app/'. $item->avatar ) }}" itemprop="image" content="{{  asset('/app/'. $item->avatar ) }}" alt="subject"></div>
                                        </div>
                                        <span class="_1sbqp">
                                <div class="_3jAsY">
                                    <div class="_3KcVT">
                                        <span class="_1vK7W" name="spotlight">
                                            <svg viewBox="0 0 24 24" data-name="Calque 1">
                                                <path d="M18.43 0H5.57A2.63 2.63 0 0 0 3 2.67V24l9-4 9 4V2.67A2.63 2.63 0 0 0 18.43 0z"></path>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="_1MBDf">Оголошення</div>
                                </div>
                            </span>
                                    </div>
                                    <div class="_3beID">
                                        <section class="irAof">
                                            <p class="_3ZfBw">
                                                <span itemprop="name" data-qa-id="aditem_title">{{ $item->title }}</span>
                                            </p>
                                            <p class="_1s5WJ" itemprop="availableAtOrFrom" data-qa-id="aditem_location"><!-- react-text: 2239 -->{{ \Illuminate\Support\Str::limit($item->content,30) }}<!-- /react-text --></p>
                                            <div class="CeFtS" itemprop="priceSpecification" itemscope="" itemtype="http://schema.org/PriceSpecification" data-qa-id="aditem_price">
                                                <meta itemprop="priceCurrency" content="UAH">
                                                <span class="_1_bNq">
                                        <span itemprop="price">{{ $item->price }}</span><!-- react-text: 2244 -->&nbsp;UAH<!-- /react-text --></span>
                                            </div>
                                        </section>
                                        <div class="_3A9T7"></div>
                                    </div>
                                </a>
                                <div class="_3Zm0x" data-qa-id="listitem_save_ad">
                                    <div>
                                        <div class="_3C4to">
                                            <div class="" data-tip="Sauvegarder l'annonce" data-place="left" data-for="toggleSavedFeaturedAd_1554842308" currentitem="false">
                                    <span class="_1vK7W" name="heartoutline">
                                        <svg viewBox="0 0 24 24" data-name="Calque 1">
                                            <path d="M21.19 2.24A6.76 6.76 0 0 0 12 3.61a6.76 6.76 0 0 0-9.19-1.37A6.89 6.89 0 0 0 0 7.58c-.16 4.84 4 8.72 10.26 14.66l.12.12a2.32 2.32 0 0 0 3.23 0l.13-.12C20 16.29 24.15 12.41 24 7.57a6.89 6.89 0 0 0-2.81-5.33zm-9.07 18.15l-.12.12-.12-.12C6.17 15 2.4 11.46 2.4 7.86a4.18 4.18 0 0 1 4.2-4.37 4.68 4.68 0 0 1 4.28 3h2.25a4.66 4.66 0 0 1 4.27-3 4.18 4.18 0 0 1 4.2 4.37c0 3.6-3.77 7.14-9.48 12.53z"></path>
                                        </svg>
                                    </span>
                                            </div>
                                            <div class="sc-fjdhpX bpzbGb">
                                                <div class="__react_component_tooltip place-top type-dark " id="toggleSavedFeaturedAd_1554842308" data-id="tooltip"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-3" style="margin-top: -10px;">
            @if(\Illuminate\Support\Facades\Auth::id() === $advert->user_id)
                @if($advert->isDraft())
                <div class="offer-sidebar__box elem">
                    <div class="offer-user__location-two">
                        <div class="offer-user__address ">
                            <span class="lnr lnr-user" style="color: red; font-weight: 900; font-size: 25px; "></span>
                            <address>
                                <p style="font-weight: 900; padding-left: 5px; padding-top: 4px;">Оголошення в статусі - чорновик</p>
                                <br>
                                <p>Для подальшої подачі оголошень</p>
                            </address>
                                {{ csrf_field() }}
                                <button class="user-offers add-to-card-link"  data-toggle="tooltip" title="Опублікувати!" style="color: #0198d0; font-size: 12px">Опублікувати оголошення</button>
                        </div>
                    </div>
                </div>
                @endif
            @endif
                @guest
                @else
                @if(\Illuminate\Support\Facades\Auth::id() === $advert->user_id || \Illuminate\Support\Facades\Auth::user()->isAdmin())
                    @if($advert->isDraft() || $advert->isOnModeration())
                        <section class="sky-form">
                            <h4><span class="lnr lnr-map" style="font-weight: 900; margin-right: 10px; color: #4387f4"></span>Дата публікації в газеті</h4>
                            <div class="row row1 scroll-pane">
                                <div class="col col-4">
                                    @foreach( $advert->dataPrint(\Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $advert->created_at)) as $k => $time)
                                        <label class="checkbox" checked>
                                            <input type="checkbox" name="checkbox" checked value="1"><i></i>{{$time}}</label>
                                    @endforeach
                                </div>
                            </div>
                        </section>
                        @elseif($advert->isActive())
                            <section class="sky-form">
                                <h4>Дати публікацій в газеті</h4>
                                <p style="color: cornflowerblue;font-size: 12px; font-weight: 900"><span class="lnr lnr-map" style="font-weight: 900; margin-right: 10px; color: #4387f4"></span>це бачите тільки Ви!</p>
                                <div class="row row1 scroll-pane">
                                    <div class="col col-4">
                                        @foreach( $advert->dataPrint(\Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $advert->published_at)) as $k => $time)
                                                @if($time < \Carbon\Carbon::createFromFormat('Y-m-d', \Carbon\Carbon::now()->toDateString())->toDateString())
                                                <label class="checkbox" style="color: darkgrey; font-size: 12px;">
                                                    <input type="checkbox" style="font-size: 8px;"  name="checkbox" checked value="1"><i style="background-color: #f6f6f6"></i>{{$time .' -друк відбувся'}}</label>
                                                @elseif($time === \Carbon\Carbon::createFromFormat('Y-m-d', \Carbon\Carbon::now()->toDateString())->toDateString())
                                                <label class="checkbox" style="color: red; font-size: 12px;">
                                                    <input type="checkbox"  style="font-size: 8px;" name="checkbox" checked value="1"><i style="background-color: red"></i>{{$time .' -сьогодні'}}</label>
                                                    @else
                                                <label class="checkbox" style="color: black; font-size: 12px;">
                                                <input type="checkbox" name="checkbox" checked value="1"><i style=""></i>{{$time .' -друк відбудиться'}}</label>
                                                @endif
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        @endif
                @endif
                @endguest
            <h5 style="font-weight: 900; font-size: 14px; text-align: center;">Реклама</h5>
            <div class="banner" data-format="240x440" data-category="{{  $advert->category ? $advert->category->id : '' }}" data-region="{{ $advert->region ? $advert->region->id : '' }}" data-url="{{ route('banner.get') }}">
            </div>
            <h5 style="font-weight: 900; font-size: 14px; text-align: center;">Пропозиції компаній</h5>
            @foreach($suggestions as $suggest)
                <li itemscope="" itemtype="http://schema.org/Offer" class="_3eDdy">
                    <a title="Maserati Granturismo 4.2" class="_2fKRW" data-qa-id="aditem_container" href="{{ route('suggestions.show', [$suggest->company_id, $suggest]) }}">
                        <div class="_2-jsN"><div class="LazyLoad is-visible">
                                <div>
                                    <div class="_6ntGx" style="background-image: url(&quot;https://img1.leboncoin.fr/ad-image/fd96e60b12d1c41565d4c3ffd45db7e4a01c2bb6.jpg&quot;);"></div>
                                    <img src="{{asset('app/'. $suggest->avatar)}}" itemprop="image" content="https://img1.leboncoin.fr/ad-image/fd96e60b12d1c41565d4c3ffd45db7e4a01c2bb6.jpg" alt="subject"></div>
                            </div>
                            <span class="_1sbqp">
                                <div class="_3jAsY">
                                    <div class="_3KcVT">
                                        <span class="_1vK7W" name="spotlight">
                                            <svg viewBox="0 0 24 24" data-name="Calque 1">
                                                <path d="M18.43 0H5.57A2.63 2.63 0 0 0 3 2.67V24l9-4 9 4V2.67A2.63 2.63 0 0 0 18.43 0z"></path>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="_1MBDf">{{$suggest->company->title}}</div>
                                </div>
                            </span>
                        </div>
                        <div class="_3beID">
                            <section class="irAof">
                                <p class="_3ZfBw">
                                    <span itemprop="name" data-qa-id="aditem_title">{{ \Illuminate\Support\Str::limit($suggest->title,30) }}</span>
                                </p>
                                <p class="_1s5WJ" itemprop="availableAtOrFrom" data-qa-id="aditem_location"><!-- react-text: 2239 -->{!! \Illuminate\Support\Str::limit($suggest->content,30) !!} <!-- /react-text --></p>
                                <div class="CeFtS" itemprop="priceSpecification" itemscope="" itemtype="http://schema.org/PriceSpecification" data-qa-id="aditem_price">
                                    <meta itemprop="priceCurrency" content="EUR">
                                    <span class="_1_bNq">
                                        <span itemprop="price">{{$suggest->price}}</span><!-- react-text: 2244 -->&nbsp;UAH<!-- /react-text --></span>
                                </div>
                            </section>
                            <div class="_3A9T7"></div>
                        </div>
                    </a>
                    <div class="_3Zm0x" data-qa-id="listitem_save_ad">
                        <div>
                            <div class="_3C4to">
                                <div class="" data-tip="Sauvegarder l'annonce" data-place="left" data-for="toggleSavedFeaturedAd_1554842308" currentitem="false">
                                    <span class="_1vK7W" name="heartoutline">
                                        <svg viewBox="0 0 59.2 59.2" style="enable-background:new 0 0 488.85 488.85;" xml:space="preserve" width="25px" height="25px">
                                          <path d="M51.062,21.561c-11.889-11.889-31.232-11.889-43.121,0L0,29.501l8.138,8.138c5.944,5.944,13.752,8.917,21.561,8.917
		s15.616-2.972,21.561-8.917l7.941-7.941L51.062,21.561z M49.845,36.225c-11.109,11.108-29.184,11.108-40.293,0l-6.724-6.724
		l6.527-6.527c11.109-11.108,29.184-11.108,40.293,0l6.724,6.724L49.845,36.225z"/>
	<path d="M28.572,21.57c-3.86,0-7,3.14-7,7c0,0.552,0.448,1,1,1s1-0.448,1-1c0-2.757,2.243-5,5-5c0.552,0,1-0.448,1-1
		S29.125,21.57,28.572,21.57z"/>
	<path d="M29.572,16.57c-7.168,0-13,5.832-13,13s5.832,13,13,13s13-5.832,13-13S36.741,16.57,29.572,16.57z M29.572,40.57
		c-6.065,0-11-4.935-11-11s4.935-11,11-11s11,4.935,11,11S35.638,40.57,29.572,40.57z"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="sc-fjdhpX bpzbGb">
                                    <div class="__react_component_tooltip place-top type-dark " id="toggleSavedFeaturedAd_1554842308" data-id="tooltip"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
            <div class="clearfix" style="margin-top: 30px;"></div>
        </div>
    </div>
@endsection