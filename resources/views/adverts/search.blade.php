@extends('layouts.app')
@section('breadcrumbs','')
@section('content')
    @include('layouts.search._search', ['categories' => $categories, 'regions' => $regions ])
    <div class="card">
        <div class="modal-header">
        </div>
        <div class="card-body" style="padding: 25px;">
            <div class="row">
                @foreach(array_chunk($categories, 3) as $chunk)
                    <div class="col-md-3">
                        <ul class="list-unstyled">
                            @foreach( $chunk as $current)
                                <li><a href="{{ route('adverts.all',  [$current]) }}">{{ $current->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="banner" data-format="820x143" data-category="{{   $category ?  $category->id : '' }}" data-region="{{ $region ? $region->id : '' }}" data-url="{{ route('banner.get') }}">

            </div>
        </div>
        <div class="col-md-3">
            <div class="offer-sidebar__box">
                <div class="offer-user__location-two">
                    <div class="offer-user__address">
                        <span class="lnr lnr-user" style="color: red; font-weight: 900; font-size: 25px; "></span>
                        <address>
                            <p style="font-weight: 900; padding-left: 5px; padding-top: 4px;">Слідкувати за кате</p>
                            <br>
                            <p>Для подальшої подачі та реклами оголошень</p>
                        </address>
                        <a class="user-offers" href="https://ruslanif.olx.ua">Поповніть рахунок!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-9">
            <div style="margin-top: 20px;">
                <h4>Оголошення</h4>
                <div class="adverts-list">
                    @foreach($adverts as $advert)
                        <div class="col-md-12">
                            <div class="adverts-box" style="background-color: white; margin-bottom: 30px; padding: 20px; border: 1px solid #fd4235; border-radius: 8px; height: 180px">
                                <div class="col-md-3">
                                    <div class="advert-back"style="background-color: #f5f5f5; height: 100px; width: 100%;">
                                        <img  class="center" src="{{  asset('/app/'. $advert->avatar ) }}" alt="телевизор &quot;TELEFUNKEN&quot; 26 LCD" height="100" style="text-align:center; display:block; margin:0 auto; border-radius: 5px">
                                        <p style="color: #fd4235; margin-top: 6px; font-weight: 900; font-size: 10px; border-radius: 20px;">{{ $advert->category->name }}</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="advert-back"style="width: 100%">
                                        <a href="{{ route('adverts.show', $advert) }}"><h3 class="advert-text" style="padding: 0px; margin: 0px;">{{ \Illuminate\Support\Str::limit($advert->title, 20) }}</h3></a>
                                        <p class="advert-content" style="padding-top: 10px;">{{ \Illuminate\Support\Str::limit($advert->content,90) }}</p>
                                        <p class="advert-loc" style="padding-top: 10px; background-color: #f5f5f5; border-radius: 20px; padding: 5px;"><span class="lnr lnr-map-marker" style="color: red; font-weight: 900; font-size: 15px; margin-left: 20px;">{{ $advert->region ?  $advert->region->name : 'Всі міста'}}</span><span class="lnr lnr-hourglass" style="color: red; font-weight: 900; font-size: 15px; margin-right: 20px; float: right">{{ date('m.d', strtotime($advert->created_at)) }}</span></p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="advert-back"style=" width: 100%">
                                        <h3 style="padding: 0px; margin: 0px; float: right"><span style="font-size: 14px; color: red; margin-right: 5px">ціна:</span>{{ $advert->price }}грн</h3>
                                        <a href="" class="" style="float: right; border-radius: 20px; background-color: #fd4235; padding-left: 10px; padding-right: 10px; padding-top: 5px; padding-bottom: 5px; color: white; margin-top: 10px;"><span class="lnr lnr-eye" style="padding-right: 5px; font-weight: 900; padding-top: 2px"></span>Переглянути</a>
                                        <div class="" style="float: right; margin-top: 32px; width: 100%">
                                            @if($advert->isCompany())
                                                <img src="{{asset('app/'. $advert->company->logo)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right;">
                                            @else
                                                @if($advert->user->avatar)
                                                    <img src="{{asset('app/'. $advert->user->avatar)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right;">
                                                @else
                                                    <img src="{{asset('images/no-avatar.jpg')}}" width="35" height="35" style="border-radius: 40px; float: right;">
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                {{$adverts->links()}}
            </div>
        </div>
        <div class="col-md-3"  style="margin-top: 20px;">
            <div class="banner" data-format="240x440" data-category="{{   $category ?  $category->id : '' }}" data-region="{{ $region ? $region->id : '' }}" data-url="{{ route('banner.get') }}">

            </div>
        </div>
    </div>
@endsection