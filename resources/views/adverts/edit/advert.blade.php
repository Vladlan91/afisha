@extends('layouts.app')

@section('content')
    <form method="POST" action="?">
    {{ csrf_field() }}
    {{method_field('PUT')}}
        <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title">Назва</label>
                    <input id="title" name="title" class="form-control"
                           value="{{ old('title', $advert->title) }}">
                    @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                    <label for="price">Ціна</label>
                    <input id="price" name="price" class="form-control"
                           value="{{ old('price', $advert->price) }}">
                    @if ($errors->has('price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-12">
        <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-9">
                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <label for="address">Адреса</label>
                    <input id="address" name="address" class="form-control" value="{{ old('address', $advert->address) }}" >
                    @if ($errors->has('address'))
                        <span class="help-block">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-2 col-sm-10 col-xs-3">
                <label for="address" style="color: #f5f5f5;">Авто</label>
                <span class="location-button" data-target="#address"><img style="height: 30px; width: 30px; cursor: pointer;"
                                                                          src="{{asset('images/map-location.png')}}" alt=""></span>
            </div>
        </div>
        </div>
        <div class="col-md-12">
            <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                <label for="content">Текст</label>
                <textarea id="content" name="content" rows="4" cols="50" class="form-control" >{{ old('content', $advert->content) }}</textarea>
                @if ($errors->has('content'))
                    <span class="help-block">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="col-md-12 new_phone">
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="new_phone">Оберіть телефон</label>
                    <select id="new_phone" class="form-control">
                        @if($advert->hasPhone())
                            <option data-phone="2" {{$errors->has('phone')  ? 'selected' : ''}}>Новий телефон</option>
                            <option data-phone="1">Телефон профіля</option>
                        @else
                            <option data-phone="1">Телефон профіля</option>
                            <option data-phone="2" {{$errors->has('phone')  ? 'selected' : ''}}>Новий телефон</option>
                        @endif
                    </select>
                </div>
            </div>
            @if($advert->hasPhone())
            <div class="col-md-8" id="pnones" style="">
            @else
            <div class="col-md-8" id="pnones" style="{{$errors->has('phone')  ? '' : 'display: none'}}">
            @endif
                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label for="phone">Новий телефон</label>
                    <input id="phone" name="{{ $errors->first('phone') ? 'phone': '' }}" class="form-control" value="{{ old('phone', $advert->phone) }}" placeholder="Формат: 0963544614">
                    @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>

        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"> </script>
        <script>
            $(document).ready(function(){
                $('.new_phone select').change(function(){
                    var pnone = $('select#new_phone option:selected').attr('data-phone');
                    if (pnone == 1) {
                        $('#pnones').hide('slow');
                        document.getElementById('phone').name = ''
                    }
                    if (pnone == 2) {
                        $('#pnones').show();
                        document.getElementById('phone').name = 'phone'

                        $('input#phone').name('phone');
                    }
                })
            })
        </script>
            @if(Auth::user()->isAdmin())
                <div class="col-md-12">
                    <div class="col-md-12">
                    <label class="checkbox" style="color: darkgrey">
                        <input type="checkbox" name="error" checked value="1"><i style="background-color: #f6f6f6"></i>Помилки усунуто</label>
                    </div>
                </div>
            @endif
        <div class="form-group">
            <button type="submit" class="btn btn-primary" style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px; border: none;">Зберегти</button>
        </div>
        </div>
    </form>

@endsection