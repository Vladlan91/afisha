@extends('layouts.app')

@section('content')
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('adverts.photos.save', $advert) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{method_field('PUT')}}

        <div class="form-group">
            <label for="uploadbtn" class="uploadButton">Загрузити фото</label>
            <input style="opacity: 0; z-index: -1;" type="file" name="file" id="uploadbtn">
        </div>
        {{--<div class="form-group">--}}
            {{--<label for="uploadbtn" class="uploadButton">Загрузити фото</label>--}}
            {{--<input style="opacity: 0; z-index: -1;" type="file" name="photos2" id="uploadbtn">--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
            {{--<label for="uploadbtn" class="uploadButton">Загрузити фото</label>--}}
            {{--<input style="opacity: 0; z-index: -1;" type="file" name="photos3" id="uploadbtn">--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
            {{--<label for="uploadbtn" class="uploadButton">Загрузити фото</label>--}}
            {{--<input style="opacity: 0; z-index: -1;" type="file" name="photos4" id="uploadbtn">--}}
        {{--</div>--}}

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Загрузити</button>
        </div>
    </form>
@endsection