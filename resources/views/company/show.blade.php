@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div style="border: 2px solid #0198d0; red; padding: 20px; margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-3">
                        <div class="log-company" style="margin: 20px;">
                            <p style="font-size: 24px; line-height: 28px; font-weight: 700; display: inline-block; max-width: 300px;">{{$company->title}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5"  style="margin: 20px;">
                                <span class="lnr lnr-map-marker"  style="color: #0a90eb; font-size: 14px; font-weight: 700; margin-right: 2px;"></span>{{$company->location}}
                            </div>
                            <div class="col-md-5"  style="margin: 20px;">
                                <span class="lnr lnr-phone" style="color: #0a90eb; font-size: 14px; font-weight: 700; margin-right: 2px;"></span>{{$company->phone}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div>
                            <img  class="center" src="{{  asset('/app/'. $company->logo ) }}" alt="телевизор &quot;TELEFUNKEN&quot; 26 LCD" height="70" style="text-align:right; display:block; margin:0 auto;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
                @can('manage-own-company', $company)
                <p>
                        <a style="margin-top: 10px;" href="{{ route('cabinet.company.edit', $company) }}" class="btn btn-primary btns-worning"  data-toggle="tooltip" title="Корегувати"><span class="lnr lnr-pencil" style="font-size: 24px; font-weight: 900"></span></a>
                    @if($company->isDraft())
                        <form method="POST" action="{{ route('cabinet.company.send', $company) }}">
                            {{ csrf_field() }}
                            <button class="bubbly-button-gre"  data-toggle="tooltip" title="На модерацію!" style="float: left; margin-right: 10px;"><span class="lnr lnr-bullhorn" style="font-size: 24px; font-weight: 900"></span></button>
                        </form>
                    @endif
                    <form method="POST" action="{{ route('cabinet.company.destroy', $company) }}" >
                        {{ csrf_field() }}
                        {{method_field('DELETE')}}
                        <button class="bubbly-button" data-toggle="tooltip" title="Видалити!"><span class="lnr lnr-unlink" style="font-size: 24px; font-weight: 900"></span></button>
                    </form>
                </p>
                @endcan

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#panel1">Про компанію</a></li>
                <li><a data-toggle="tab" href="#panel2">Оголошення</a></li>
                <li><a data-toggle="tab" href="#panel3">Пропозиції</a></li>
                <li><a data-toggle="tab" href="#panel4">Контакти</a></li>
            </ul>
    <div class="tab-content">
        <div id="panel1" class="tab-pane fade in active">
            <h3> {{$company->title}}</h3>
            <p> {{$company->content}}</p>
        </div>
        <div id="panel2" class="tab-pane fade">
            <div class="col-md-12">
                <h4 style="font-weight: 900;">Оголошення</h4>
                <ul class="_1rXe6">
                    @foreach($adverts as $advert)
                        <li>
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                @if($advert->type === 'A')
                                    <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                        <h5 style="background-color: #aaaaaa; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>А</h5>
                                    </div>
                                @elseif($advert->type === 'Б')
                                    <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                        <h5 style="background-color: #45c639; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Б</h5>
                                    </div>
                                @elseif($advert->type === 'В')
                                    <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                        <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>В</h5>
                                    </div>
                                @elseif($advert->type === 'Г')
                                    <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                        <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                                    </div>
                                @elseif($advert->type === 'Д')
                                    <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                        <h5 style="background-color: #fed700; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                                    </div>
                                @else
                                    <div style="position: absolute; left: 0;top: -12px; z-index: 1;">
                                        <h5 style="background-color: #ea1b25; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Е</h5>
                                    </div>
                                @endif
                                <div class="item_image">
                                <span class="item_imagePic">
                                        <img src="{{  asset('/app/'. $advert->avatar ) }}" alt="Appartement 4 pièces 101 m²" style="height: 150px;">
                                </span>
                                    <span class="item_imageNumber">
                        	<i class="icon-camera icon-2x nomargin"></i>
                        	<span>3</span>
                      	</span>
                                </div>
                                <div title="" class="saveAd" data-savead-id="1557273607">
                                    @if($advert->isCompany())
                                        <img src="{{asset('app/'. $advert->company->logo)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                    @else
                                        @if($advert->user->avatar)
                                            <img src="{{asset('app/'. $advert->user->avatar)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                        @else
                                            <img src="{{asset('images/no-avatar.jpg')}}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                        @endif
                                    @endif
                                </div>
                                <section class="item_infos">
                                    <h2 class="item_title">
                                        {{ \Illuminate\Support\Str::limit($advert->title,30) }}
                                    </h2>
                                    <h3 class="item_price">
                                        {{ $advert->price }}&nbsp;UAH
                                    </h3>
                                    <aside>
                                        <p class="item_supp">
                                            {{ $advert->category->name }}
                                            /
                                            {{ $advert->region ?  $advert->region->name : 'Всі міста'}}
                                        </p>
                                        <p class="item_supp item_suppDate">
                                            {{ $advert->user->name }}
                                        </p>
                                    </aside>
                                </section>
                                @if(\Illuminate\Support\Facades\Auth::user() && !\Illuminate\Support\Facades\Auth::user()->hasInFavorites($advert->id) )
                                    <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                        {{ csrf_field() }}
                                        <button class="" style="margin-right: 10px; color: black; border-radius: 20px; padding: 5px;border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-heart" style="font-size: 20px; font-weight: 900"></span></button>
                                    </form>
                                @elseif(\Illuminate\Support\Facades\Auth::user())
                                    <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                        {{ csrf_field() }}
                                        {{method_field('DELETE')}}
                                        <button class="" style="margin-right: 10px;  background-color: white; color: black; border-radius: 20px; padding: 5px; border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-unlink" style="font-size: 20px; font-weight: 900"></span></button>
                                    </form>
                                @endif
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div id="panel3" class="tab-pane fade">
            {{--<div class="bott" style="margin-top: 20px;">--}}
                {{--<a class="user-offers" style="border: 2px solid #4387f4; padding: 6px 15px; text-align: center; width: 20%;" href="https://ruslanif.olx.ua"><img--}}
                            {{--width="25" height="25" src="{{ asset('images/ecommerce.png') }}" alt="" style="margin: 5px;">Створити пропозицію</a>--}}
            {{--</div>--}}
            <h3>Пропозиції</h3>
            <ul class="_1rXe6">
                @foreach($suggestions as $item)
                    <li >
                        <a href="{{ route('suggestions.show', [$company, $item]) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div class="item_image">
                	<span class="item_imagePic">
                        	<img src="{{  asset('/app/'. $item->avatar) }}" alt="Appartement 4 pièces 101 m²" style="height: 150px;">
                    	</span>
                                <span class="item_imageNumber">
                        	<i class="icon-camera icon-2x nomargin"></i>
                        	<span>3</span>
                      	</span>
                            </div>
                            <div title="" class="saveAd" data-savead-id="1557273607">
                                    <img src="{{asset('app/'. $item->company->logo )}}" alt="{{ $item->company->title }}" width="35" height="35" style="border-radius: 40px; float: right;">
                            </div>
                            <section class="item_infos">
                                <h2 class="item_title">
                                    {{ $item->title }}
                                </h2>
                                <h3 class="item_price">
                                    {{ $item->price }}&nbsp;UAH
                                </h3>
                                <aside>
                                    <p class="item_supp">
                                        {{ $item->address }}
                                        /
                                        {{ $item->company->title }}
                                    </p>
                                    <p class="item_supp item_suppDate">
                                        {{ $item->created_ed }}
                                    </p>
                                </aside>
                            </section>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div id="panel4" class="tab-pane fade">
            <h3>Карта компанії</h3>
            <div style="background: black; background-image: url('/images/map.png')">
                <div id="map" style="width: 100%; height: 250px;"></div>
                @section('script')
                    <script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=uk-UA" type="text/javascript"></script>

                    <script type='text/javascript'>
                        ymaps.ready(init);
                        function init(){
                            var geocoder = new ymaps.geocode(
                                // Строка с адресом, который нужно геокодировать
                                '{{ $company->address }}',
                                // требуемое количество результатов
                                { results: 1 }
                            );
                            // После того, как поиск вернул результат, вызывается callback-функция
                            geocoder.then(
                                function (res) {
                                    // координаты объекта
                                    var coord = res.geoObjects.get(0).geometry.getCoordinates();
                                    var map = new ymaps.Map('map', {
                                        // Центр карты - координаты первого элемента
                                        center: coord,
                                        // Коэффициент масштабирования
                                        zoom: 7,
                                        // включаем масштабирование карты колесом
                                        behaviors: ['default', 'scrollZoom'],
                                        controls: ['mapTools']
                                    });
                                    // Добавление метки на карту
                                    map.geoObjects.add(res.geoObjects.get(0));
                                    // устанавливаем максимально возможный коэффициент масштабирования - 1
                                    map.zoomRange.get(coord).then(function(range){
                                        map.setCenter(coord, range[1] - 1)
                                    });
                                    // Добавление стандартного набора кнопок
                                    map.controls.add('mapTools')
                                    // Добавление кнопки изменения масштаба
                                        .add('zoomControl')
                                        // Добавление списка типов карты
                                        .add('typeSelector');
                                }
                            );
                        }
                    </script>
                @endsection
            </div>
            <h3>Додаткова інформація</h3>
            <p> {{$company->address}}</p>
        </div>
</div>

    </div>
        <div class="col-md-3"  style="margin-top: 20px;">
            <div class="offer-sidebar__box">
                <div class="offer-user__location-two">
                    <div class="offer-user__address">
                        <span class="lnr lnr-user" style="color: red; font-weight: 900; font-size: 25px; "></span>
                        <address>
                            <p style="font-weight: 900; padding-left: 5px; padding-top: 4px;">Ваш рахунок: 0 грн.</p>
                            <br>
                            <p>Для подальшої подачі та реклами оголошень</p>
                        </address>
                        <a class="user-offers" href="https://ruslanif.olx.ua">Поповніть рахунок!</a>
                    </div>
                </div>
            </div>
            <h5 style="font-weight: 900; font-size: 14px; text-align: center;">Реклама</h5>
            <div class="banner" data-format="240x440" data-category="{{  $company->category ? $company->category->id : '' }}" data-region="{{ $company->region ? $company->region->id : '' }}" data-url="{{ route('banner.get') }}">
            </div>
            <div class="clearfix" style="margin-top: 30px;"></div>
        </div>
@endsection