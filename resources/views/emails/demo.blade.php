<html>
<head>
    <title>Підтверження оплати | INHOUS</title>
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Fashionable templates, Email Templates, Newsletters, Marketing  templates,
	Advertising templates, free Newsletter" />
    <!-- //Custom Theme files -->

    <style type="text/css">

        body{
            width: 100%;
            margin:0;
            padding:0;
            -webkit-font-smoothing: antialiased;
        }
        p,h1,h2,h3,h4{
            margin-top:0;
            margin-bottom:0;
            padding-top:0;
            padding-bottom:0;
        }
        html{
            width: 100%;
        }
        a {
            color: #000;
            text-decoration: none;
        }
        .textbutton a {
            font-family: Candara, sans-serif !important;
            color: #ffffff !important;
        }
        table{
            font-size: 15px;
            border: 0;
        }
        .iebg {
            background: #ffffff;
        }
        .menulink a { color: #414a51 !important; text-decoration:none;}

        /* ----------- responsivity ----------- */
        @media only screen and (max-width: 768px){
            td.logo-w3l-h {
                height: 36px;
            }
            td.w3ls-ba a {
                padding: 10px 20px 12px 20px !important;
            }
            td.w3l-4h {
                height: 60px !important;
            }
            td.w3l-p{
            }

        }
        @media only screen and (max-width:600px){

            table {
                width: 96% !important;
            }
            table.wel {
                width: 90%!important;
            }
            table.b-text {
                width: 90%!important;
            }
            td.wthree-logo a {
                font-size: 36px !important;
            }
            td.agile-main {
                font-size: 19px !important;
            }
            td.w3ls-ba {
                width: 100%!important;
                text-align: center!important;
                padding-bottom: 48px!important;
                padding-left: 20px!important;
                padding-right: 20px!important;
            }
            td.w3ls-ba2 {
                width: 100%!important;
                text-align: center!important;
                padding-left: 0px!important;
                padding-right: 42px!important;
            }
            td.w3ls-ba a, .w3ls-ba2 a {
                padding: 11px 24px 12px 24px !important;
            }
            td.w3l-3h {
                height: 54px !important;
            }
            td.w3l-p2 {
                font-size: 17px !important;
            }
            img.intr2 {
                width: 232px !important;
            }
            table.scale-center-both1 {
                width: 254px !important;
            }
            table.scale-center-both2 {
                width: 280px !important;
            }
            td.w3layouts-1 {
                font-size: 19px !important;
            }
            td.go-agile a {
                padding: 10px 20px 11px 20px !important;
            }
            table.agile1.scale {
                width: 480px !important;
            }
            td.wls-6h {
                height: 38px !important;
            }
            table.textbutton {
                width: 140px !important;
                padding: 1px !important;
            }
            table.sale2-w3.scale {
                width: 486px !important;
            }
            table.sale3-w3.scale {
                width: 405px !important;
            }
            td.w3l-he1 {
                height: 20px !important;
            }
            td.w3l-he4 table {
                width: 9% !important;
            }
            font.sale-ha {
                font-size: 25px !important;
            }
            label.sale-ha2 {
                font-size: 34px !important;
            }
            table.pro1-w3.scale {
                text-align: center;
            }
            table.right-w3.scale {
                width: 303px !important;
            }
            table.agile-pro.scale {
                width: 207px !important;
            }
            table.w3ls-pa {
                width: 250px !important;
            }
            td.w3-gr {
                height: 70px !important;
            }
            table.fa3.scale {
                width: 205px !important;
            }
            table.fa2.scale {
                width: 142px !important;
            }
            table.fas.scale {
                width: 347px !important;
            }
            table.fa1.scale {
                width: 165px !important;
            }
            td.w3l-para {
                text-align: center;
            }
            table.pro1-w3.scale {
                width: 100% !important;
            }
            table.w3-a {
                width: 36% !important;
                margin: 0 0px 0px 165px!important;
                text-align: center !important;
            }
        }
        @media only screen and (max-width:568px){
            td.wthree-logo a {
                font-size: 33px !important;
            }
            table.scale-center-both2 {
                width: 272px !important;
            }
            table.scale-center-both1 {
                width: 233px !important;
            }
            td.agile-h {
                height: 39px !important;
            }
            table.right-w3.scale {
                width: 286px !important;
            }
            table.agile-pro.scale {
                width: 195px !important;
            }
            table.fa2.scale {
                width: 152px !important;
            }
            table.fa3.scale {
                width: 179px !important;
            }
            table.fas.scale {
                width: 331px !important;
            }
            table.fa1.scale {
                width: 154px !important;
            }
            table.w3-a {
                margin: 0 0px 0px 159px!important;
            }
        }
        @media only screen and (max-width:480px){
            td.w3l-1h {
                height: 34px !important;
            }
            td.wthree-logo a {
                font-size: 32px !important;
            }
            td.agile-main {
                font-size: 18px !important;
            }
            td.logo-w3l-h {
                height: 26px;
            }
            td.w3l-2h {
                height: 41px !important;
            }
            td.w3ls-ba a, .w3ls-ba2 a {
                padding: 9px 20px 11px 20px !important;
            }
            td.w3ls-ba {
                width: 100%!important;
                text-align: center!important;
                padding-bottom: 39px!important;
                padding-left: 20px!important;
                padding-right: 29px!important;
            }
            table.agile1.scale {
                width: 378px !important;
            }
            td.w3l-4h {
                height: 43px !important;
            }
            td.w3l-p2 {
                font-size: 16px !important;
            }
            table.scale-center-both1 {
                width: 426px !important;
                text-align: center !important;
            }
            .center-w3{
                background: Transparent !important;
            }
            table.center-img {
                width: 54% !important;
            }
            table.scale-center-both1 img {
                width: 289px !important;
                text-align: center !important;
            }
            table.scale-center-both2 {
                width: 425px !important;
                text-align: center !important;
            }
            td.agile-h {
                height: 0px !important;
            }
            td.h {
                height: 22px !important;
            }
            table.sale2-w3.scale {
                width: 394px !important;
            }
            td.w3l-he2 {
                height: 25px !important;
            }
            table.sale3-w3.scale {
                width: 341px !important;
            }
            label.sale-ha2 {
                font-size: 31px !important;
            }
            font.sale-ha {
                font-size: 25px !important;
            }
            td.w3l-he9 {
                height: 25px !important;
            }
            td.produ {
                height: 25px !important;
            }
            td.noborder {
                border-left: none !important;
            }
            table.agile-pro.scale {
                width: 405px !important;
            }
            table.right-w3.scale {
                width: 405px !important;
            }
            table.fa1.scale {
                width: 121px !important;
            }
            td.agile-main {
                font-size: 16px !important;
            }
            table.fa3.scale {
                width: 155px !important;
            }
            table.fa2.scale {
                width: 122px !important;
            }
            table.fas.scale {
                width: 277px !important;
            }
            td.go-agile {
                padding: 18px 0px 41px !important;
            }
            td.bu-w3l3 {
                height: 0px;
            }
            table.w3-a {
                margin: 0 0px 0px 135px!important;
                width: 34% !important;
            }
        }
        @media only screen and (max-width:414px){
            td.wthree-logo a {
                font-size: 30px !important;
            }
            td.w3l-1h {
                height: 18px !important;
            }
            td.agile-main {
                font-size: 15px !important;
            }
            td.logo-w3l-h {
                height: 8px !important;
            }
            td.w3l-2h {
                height: 33px !important;
            }
            td.w3ls-ba a, .w3ls-ba2 a {
                padding: 9px 18px 10px 18px !important;
            }
            td.w3ls-ba2 {
                width: 100%!important;
                text-align: center!important;
                padding-left: 0px!important;
                padding-right: 0px!important;
            }
            table.agile1.scale {
                width: 333px !important;
            }
            td.w3l-p2 {
                font-size: 15px !important;
                line-height: 27px !important;
            }
            table.scale-center-both1 {
                width: 371px !important;
            }
            table.center-img {
                width: 0% !important;
            }
            table.scale-center-both2 {
                width: 371px !important;
            }
            td.h1 {
                height: 13px !important;
            }
            td.w3layouts-1 {
                font-size: 18px !important;
            }
            .head {
                font-size: 20px !important;
                letter-spacing: 1px !important;
            }
            table.sale2-w3.scale {
                width: 323px !important;
            }
            td.w3l-he2 {
                height: 19px !important;
            }
            td.w3l-he9 {
                height: 19px !important;
            }
            td.w3l-he1 {
                height: 10px !important;
            }
            font.sale-ha {
                font-size: 22px !important;
            }
            label.sale-ha2 {
                font-size: 28px !important;
            }
            td.w3l-he6 {
                height: 12px !important;
            }
            td.scale-center-both {
                font-size: 14px !important;
            }
            table.agile-pro.scale {
                width: 346px !important;
            }
            table.right-w3.scale {
                width: 346px !important;
            }
            td.w3-hei {
                height: 22px !important;
            }
            td.height1 {
                height: 20px;
            }
            td.height3 {
                height: 11px;
            }
            td.w3-gr {
                height: 51px !important;
            }
            td.agile-main a {
                font-size: 17px !important;
            }
            table.fa3.scale {
                width: 359px !important;
                padding-top: 11px;
            }
            table.fa2.scale {
                width: 341px !important;
            }
            table.fas.scale {
                width: 227px !important;
                padding-top: 12px;
            }
            table.fa1.scale {
                width: 364px !important;
                text-align: center !important;
            }
            td.mail-w3 {
                font-size: 14px !important;
            }
            td.scale-center-both {
                line-height: 26px;
            }
            td.hg11 {
                height: 31px !important;
            }
            td.w3ls-ba {
                width: 100%!important;
                text-align: center!important;
                padding-bottom: 39px!important;
                padding-left: 58px!important;
                padding-right: 29px!important;
            }
            table.w3-a {
                margin: 0 0px 0px 115px!important;
            }
        }
        @media only screen and (max-width:384px){
            td.agile-main {
                font-size: 14px !important;
                line-height: 32px !important;
            }
            td.w3l-p2 {
                font-size: 14px !important;
            }
            table.agile1.scale {
                width: 295px !important;
            }
            td.w3l-4h {
                height: 36px !important;
            }
            td.wls-5h {
                height: 31px;
            }
            table.scale-center-both1 {
                width: 336px !important;
            }
            table.scale-center-both2 {
                width: 328px !important;
            }
            td.w3layouts-1 {
                font-size: 17px !important;
            }
            td.go-agile a {
                padding: 10px 19px 10px 19px !important;
            }
            td.w3ls-11h, td.w3ls-67h, td.w3ls-8h {
                height: 6px;
            }
            table.sale3-w3.scale {
                width: 284px !important;
            }
            font.sale-ha {
                font-size: 21px !important;
            }
            label.sale-ha2 {
                font-size: 26px !important;
            }
            td.produ {
                height: 14px !important;
            }
            table.agile-pro.scale {
                width: 321px !important;
            }
            table.right-w3.scale {
                width: 321px !important;
            }
            td.height1 {
                height: 13px;
            }
            td.mo {
                height: 38px;
            }
            td.w3l-p {
                font-size: 14px !important;
            }
            td.produ3 a, td.w3-a a {
                font-size: 15px !important;
            }
            table.w3-a {
                margin: 0 0px 0px 108px!important;
            }
            td.produ1 {
                height: 17px;
            }
        }
        @media only screen and (max-width:375px){
            table.scale-center-both1 {
                width: 325px !important;
            }
            table.right-w3.scale {
                width: 310px !important;
            }
            table.agile-pro.scale {
                width: 310px !important;
            }
            table.fa3.scale {
                width: 316px !important;
                padding-top: 5px;
            }
            table.fa2.scale {
                width: 299px !important;
            }
            table.fas.scale {
                width: 105px !important;
                padding-top: 7px;
            }
            table.fa1.scale {
                width: 312px !important;
            }
            td.wthree-logo a {
                font-size: 29px !important;
            }
            table.w3-a {
                width: 35% !important;
                margin: 0 0px 0px 103px!important;
            }
        }
        @media only screen and (max-width:320px){
            td.agile-main {
                font-size: 13px !important;
                line-height: 29px !important;
            }
            td.w3l-3h {
                height: 46px !important;
            }
            td.w3ls-ba a, .w3ls-ba2 a {
                padding: 8px 16px 10px 16px !important;
                font-size: 15px !important;
            }
            td.w3ls-ba2 {
                padding-right: 12px!important;
            }
            td.w3ls-ba {
                padding-left: 39px!important;
                padding-right: 29px!important;
            }
            td.wthree-logo a {
                font-size: 28px !important;
            }
            table.agile1.scale {
                width: 253px !important;
            }
            td.w3l-p2 {
                font-size: 13px !important;
            }
            table.scale-center-both1 {
                width: 279px !important;
            }
            table.scale-center-both2 {
                width: 280px !important;
            }
            td.w3layouts-1 {
                font-size: 16px !important;
            }
            .head {
                font-size: 18px !important;
                letter-spacing: 0px !important;
            }
            label.content {
                font-size: 14px !important;
            }
            table.sale2-w3.scale {
                width: 249px !important;
            }
            table.sale3-w3.scale {
                width: 260px !important;
            }
            font.sale-ha {
                font-size: 19px !important;
            }
            label.sale-ha2 {
                font-size: 24px !important;
            }
            td.scale-center-both {
                font-size: 13px !important;
            }
            td.w3l-p {
                font-size: 13px !important;
            }
            table.agile-pro.scale {
                width: 262px !important;
            }
            td.produ3 a, td.w3-a a {
                font-size: 14px !important;
            }
            table.right-w3.scale {
                width: 262px !important;
            }
            td.agile-main a {
                font-size: 16px !important;
            }
            table.fa1.scale {
                width: 273px !important;
            }
            table.fa2.scale {
                width: 273px !important;
            }
            table.fa3.scale {
                width: 260px !important;
                padding-top: 5px;
            }
            table.w3-a {
                width: 40% !important;
                margin: 0 0px 0px 77px!important;
            }
        }

    </style>

</head>
<body style="margin-top: 0;">

<!-- HEADER -->
<table class="w3-ban" width="100%" height="350" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#141416">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                <tr>
                    <td>

                        <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                            <tr><td class="w3l-1h" height="54">&nbsp;</td></tr>
                            <tr>
                                <td class="wthree-logo" align="center"><a href="#" style="font-family:'Bell Gothic Std', sans-serif; color: #fefefe; font-size: 40px; text-decoration: none; padding: 12px 26px 13px 26px; border-radius: 4px; text-transform:uppercase">
                                        <img class="intr2" src="https://ec.net.ua/wp-content/uploads/2018/06/logo.png" border="0" style="display: block; max-width: 275px; height:110px; border-top-right-radius: 8px; border-bottom-right-radius: 8px;" alt="" width="275" editable="true" /></a>
                                </td>
                            </tr>
                            <tr><td class="logo-w3l-h" height="54">&nbsp;</td></tr>
                            <tr>
                                <td class="agile-main" align="center" style="font-family:'Bell Gothic Std', sans-serif; color: #fefefe; font-size: 20px; line-height: 35px;" class="scale-center-both">
                                    <span style="color:#DF393A;font-weight: 600;"> </span>, ми дякуємо за те що Ви знами. Ваше повідомлення та тему <span style="color:#DF393A;font-weight: 600;"> </span> зареєстровано, очікуйте на відповідь нашого адміністратора. Ми звяжимось з Вами за номером <span style="color:#DF393A;font-weight: 600;"></span> який Ви вказали в листі!
                                </td>
                            </tr>
                            <tr><td class="w3l-2h" height="54">&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <table width="370" border="0" cellspacing="0" cellpadding="0" align="left" class="scale">
                                        <tr>
                                            <td class="w3ls-ba" align="right" class="scale-center-bottome-both"><a href="http://localhost:8888/verify/<?=$_SESSION['verify_token'];?>" style="font-family:'Candara', sans-serif; color: #fefefe; font-size: 16px; text-decoration: none; background-color: #e8514a; padding: 12px 26px 13px 26px; border-radius: 4px;">Перейти в кабінет</a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td class="w3l-3h" height="78">&nbsp;</td></tr>
                        </table>

                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>

<!-- TOP BREAKER -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#efe9e5" style="background: url(https://ec.net.ua/wp-content/uploads/2018/06/header-bgtop2.png) center center / cover no-repeat, #efe9e5;">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                <tr>
                    <td bgcolor="#FFFFFF" style="border-top-left-radius: 4px; border-top-right-radius: 4px;">

                        <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                            <tr><td class="w3l-4h" height="84">&nbsp;</td></tr>
                        </table>

                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>

<!-- INTRODUCTION -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#efe9e5">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                <tr>
                    <td bgcolor="#FFFFFF">

                        <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="agile1 scale">
                            <tr>
                                <td class="agile-main" style="font-family:Bell Gothic Std; color: #E0393A; text-transform: uppercase; font-size: 22px;" class="scale-center-both">

                                </td>
                            </tr>
                            <tr><td height="12" style="font-size: 1px;">&nbsp;</td></tr>
                            <tr>
                                <td class="w3l-p2" style="font-family: Candara, sans-serif; color: #7f8c8d; font-size: 18px; line-height: 28px;" class="scale-center-both">
                                </td>
                            </tr>
                            <tr><td class="wls-5h" height="60">&nbsp;</td></tr>
                        </table>

                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>

<!-- SPECIAL -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#E0393A">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                <tr>
                    <td bgcolor="#E0393A">

                        <table class="scale-center-both1" width="300" border="0" cellspacing="0" cellpadding="0" align="left" class="scale">
                            <tr>
                                <td class="center-w3" style="background: url(http://www.stampready.net/dashboard/templates/spacey/images/bgrep-right.png) center left repeat-y, #101011;">
                                    <table class="center-img" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr><td class="img-w3" height="24">&nbsp;</td></tr>
                                        <tr>
                                            <td>
                                                <img class="intr2" src="https://ec.net.ua/wp-content/uploads/2018/06/fa3.jpg" border="0" style="display: block; max-width: 275px; height:230px; border-top-right-radius: 8px; border-bottom-right-radius: 8px;" alt="" width="275" editable="true" />
                                            </td>
                                        </tr>
                                        <tr><td class="h1" height="24">&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table class="scale-center-both2" width="300" border="0" cellspacing="0" cellpadding="0" align="right" class="scale">
                            <tr>
                                <td>
                                    <table width="245" border="0" cellspacing="0" cellpadding="0" class="scale">
                                        <tr><td class="agile-h" height="54">&nbsp;</td></tr>
                                        <tr>
                                            <td class="w3layouts-1" align="center" style="font-family:'Candara', sans-serif; color: #fefefe; font-size: 20px; line-height: 35px;">
                                                Купуй частіше заощаджуй більше. <br />
                                            </td>
                                        </tr>
                                        <tr><td class="bu-w3l3" height="36">&nbsp;</td></tr>
                                        <tr>
                                            <td align="center" class="go-agile">
                                                <a href="#" style="font-family:'Candara', sans-serif; color: #fefefe; font-size: 16px; text-decoration: none; background-color: #E0393A; padding: 12px 26px 13px 26px; border-radius: 4px; border: 2px solid #FFFFFF;">Дізнатись</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr><td class="wls-6h" height="60" bgcolor="#FFFFFF">&nbsp;</td></tr>
            </table>

        </td>
    </tr>
</table>
<!-- //SPECIAL -->

<!-- 3/3 feature -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#efe9e5">
            <table bgcolor="#FFFFFF" align="center" width="620" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <table class="table-inner" align="center" width="600" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="25"></td>
                            </tr>
                            <!--headline-->
                            <tr>
                                <td class="head" align="center" style="font-family: Bell Gothic Std; font-weight: bold; color:#414a51; font-size:22px;letter-spacing: 2px;">
                                    Основні переваги платформи
                                    <span style="color:#d20962;">INHOUSE.</span>
                                </td>
                            </tr>
                            <!--end headline-->
                            <tr>
                                <td height="3"></td>
                            </tr>
                            <tr>
                                <td height="35"></td>
                            </tr>
                            <tr>
                                <td>
                                    <!--left-->

                                    <table class="table-full1" width="183" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <!--icon-->
                                        <tr>
                                            <td class="w3-w1" height="35" align="center" style="line-height: 0px;">
                                                <img editable label="icon-1" style="display:block; line-height:0px; font-size:0px; border:0px;" src="https://ec.net.ua/wp-content/uploads/2018/06/icon2.png" alt="img" width="34" height="35" />
                                            </td>
                                        </tr>
                                        <!--end icon-->
                                        <tr>
                                            <td class="w3ls-7h" height="15"></td>
                                        </tr>
                                        <!--content-->
                                        <tr>
                                            <td class="w3l-p" align="center" style="font-family: Candara, sans-serif; color:#7f8c8d; font-size:16px; line-height: 28px;">
                                                <label class="content">Партнерська програма та накопичувальна знижка.</label>
                                            </td>
                                        </tr>
                                        <!--end content-->
                                        <tr>
                                            <td class="w3ls-8h" height="15"></td>
                                        </tr>
                                        <!--link-->
                                        <tr>
                                            <td align="center" style="font-family:Candara, sans-serif; color:#91c444; font-size:16px; line-height: 28px;font-weight: bold;text-decoration: underline;">
                                                <a href="#">
                                                    <label="button">Читати більше</label>
                                                </a>
                                            </td>
                                        </tr>
                                        <!--end link-->
                                    </table>

                                    <!--end left-->

                                    <!--Space-->

                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                        <tr>
                                            <td class="h" height="40" style="font-size: 0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left: 24px;">&nbsp;</p>
                                            </td>
                                        </tr>
                                    </table>

                                    <!--End Space-->

                                    <!--middle-->

                                    <table class="table-full2" width="183" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <!--icon-->
                                        <tr>
                                            <td class="w3-w2" height="35" align="center" style="line-height: 0px;">
                                                <img editable label="icon-2" style="display:block; line-height:0px; font-size:0px; border:0px;"  src="https://ec.net.ua/wp-content/uploads/2018/06/icon1.png" alt="img" width="35" height="35" />
                                            </td>
                                        </tr>
                                        <!--end icon-->
                                        <tr>
                                            <td class="w3ls-9h" height="15"></td>
                                        </tr>
                                        <!--content-->
                                        <tr>
                                            <td class="w3l-p" align="center" style="font-family: Candara, sans-serif; color:#7f8c8d; font-size:16px; line-height: 28px;">
                                                <label class="content">Додатковий ринок збуту товарів та послуг.</label>
                                            </td>
                                        </tr>
                                        <!--end content-->
                                        <tr>
                                            <td class="w3ls-11h" height="15"></td>
                                        </tr>
                                        <!--link-->
                                        <tr>
                                            <td align="center" style="font-family: Candara, sans-serif; color:#91c444 !important; font-size:16px; line-height: 28px;font-weight: bold;text-decoration: underline;">
                                                <a href="#">
                                                    <label="button">Читати більше</label>
                                                </a>
                                            </td>
                                        </tr>
                                        <!--end link-->
                                    </table>

                                    <!--end middle-->

                                    <!--Space-->

                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left">
                                        <tr>
                                            <td class="h" height="40" style="font-size:0;line-height: 0px;border-collapse: collapse;">
                                                <p style="padding-left: 24px;">&nbsp;</p>
                                            </td>
                                        </tr>
                                    </table>

                                    <!--End Space-->

                                    <!--right-->

                                    <table class="table-full3" width="183" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <!--icon-->
                                        <tr>
                                            <td class="w3ls-w3" height="35" align="center" style="line-height: 0px;">
                                                <img editable label="icon-3" style="display:block; line-height:0px; font-size:0px; border:0px;"  src="https://ec.net.ua/wp-content/uploads/2018/06/icon3.png" alt="img" width="35" height="27" />
                                            </td>
                                        </tr>
                                        <!--end icon-->
                                        <tr>
                                            <td class="w3ls-77h" height="15"></td>
                                        </tr>
                                        <!--content-->
                                        <tr>
                                            <td class="w3l-p" align="center" style="font-family: Candara, sans-serif; color:#7f8c8d; font-size:16px; line-height: 28px;">
                                                <label class="content">Розширення клієнської бази.</label>
                                            </td>
                                        </tr>
                                        <!--end content-->
                                        <tr>
                                            <td class="w3ls-67h" height="15"></td>
                                        </tr>
                                        <!--link-->
                                        <tr>
                                            <td align="center" style="font-family:Candara, sans-serif; color:#91c444; font-size:16px; line-height: 28px;font-weight: bold;text-decoration: underline;">
                                                <a href="#">
                                                    <label="button">Читати більше</label>
                                                </a>
                                            </td>
                                        </tr>
                                        <!--end link-->
                                    </table>

                                    <!--end right-->
                                </td>
                            </tr>
                            <tr>
                                <td class="w3ls-72h" height="35"></td>
                            </tr>
                            <!--button-->
                            <tr>
                                <td align="center">
                                    <table class="textbutton" bgcolor="#363632" style="border-radius:50px;" align="center" border="0" cellpadding="0" cellspacing="0" >
                                        <tr>
                                            <td align="center" >
                                                <a href="#" style="font-family: Candara, sans-serif; color:#ffffff !important; font-size:14px; line-height: 28px; padding:6px 20px 7px 20px">
                                                    Стати партнером
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!--end button-->

                            <tr>
                                <td class="w3ls-71h" height="45"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- //end 3/3 feature -->

<!-- SALES-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#efe9e5">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="sale1-w3 scale">
                <tr>
                    <td style="background: url(images/bg.jpg) center center / cover no-repeat, #000000;">
                        <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="sale2-w3 scale">
                            <tr><td class="w3l-he2" height="40">&nbsp;</td></tr>
                            <tr>
                                <td class="w3l-sale" bgcolor="#ffffff" style="border: 1px solid #cccccc;">
                                    <table width="460" border="0" cellspacing="0" cellpadding="0" align="center" class="sale3-w3 scale">
                                        <tr><td class="w3l-he1" height="40">&nbsp;</td></tr>
                                        <tr>
                                            <td align="center">
                                                <img src="https://ec.net.ua/wp-content/uploads/2018/06/sale.png" style="max-width: 42px;" alt="" width="42" editable="true" />
                                            </td>
                                        </tr>
                                        <tr><td class="w3l-he3" height="6" style="font-size: 1px;">&nbsp;</td></tr>
                                        <tr>
                                            <td style="color: #103871; text-align: center; font-family: 'Azo Sans W01 Regular', Helvetica, Arial, sans-serif; font-size: 36px;" class="">
                                                <font class="sale-ha" style="font-size: 26px; font-family: 'Bell Gothic Std', sans-serif;">Наша нова тенденція</font><br /><label class="sale-ha2" >Кращі пропозиції 2018 року!</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="w3l-he4" height="18" valign="bottom">
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="25">
                                                    <tr>
                                                        <td class="w3l-he5" height="3" style="background-color: #ffcc00; border-radius: 3px"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr><td class="w3l-he6" height="18" style="font-size: 1px;">&nbsp;</td></tr>
                                        <tr>
                                            <td align="center" style="font-family: 'Candara', sans-serif; color: #000; font-size: 15px; line-height: 24px;" class="scale-center-both">
                                                Якість для професіоналів.
                                            </td>
                                        </tr>
                                        <tr><td class="w3l-he8" height="24">&nbsp;</td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td class="w3l-he9" height="40">&nbsp;</td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- //SALES-->

<!-- PRODUCTS -->
<!-- Side column 1-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#efe9e5">
            <table bgcolor="#FFFFFF" width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-90" bgcolor="FFFFFF">
                <tr>
                <tr><td height="40">&nbsp;</td></tr>
                <td style="border-left: 1px solid #e2e2e2; border-right: 1px solid #e2e2e2;">

                    <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-90">
                        <tr>
                            <td>

                                <table width="265" border="0" cellspacing="0" cellpadding="0" align="left" style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #9b9b9b;" class="pro1-w3 scale">
                                    <tr>
                                        <td>
                                            <img src="https://ec.net.ua/wp-content/uploads/2018/06/g1.png" style="border-radius: 5px; height:180px;" class="scale" alt="" width="130" editable="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="produ" height="50" style="font-size: 1px">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="265" border="0" cellspacing="0" cellpadding="0" align="right" style="font-family: Candara, sans-serif; font-size: 14px; color: #9b9b9b;" class="pro1-w3 scale">
                                    <tr>
                                        <td class="agile-main" style="font-size: 19px; color: #000; font-family: Bell Gothic Std, sans-serif;">Anserglob</td>
                                    </tr>
                                    <tr>
                                        <td height="16" style="font-size: 1px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="w3l-p" style="font-size: 16px; line-height: 24px; color: #7f8c8d; font-family: Candara, sans-serif;">

                                            Один із лідерів на ринку України, славиться своєю якістю і приємливою ціною...

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="produ1" height="30" style="font-size: 1px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="agile-bu">
                                            <table class="w3-a"  bgcolor="#00a78e" style="border-radius: 5px;" align="left" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="produ3" height="35" align="center" style="font-family: Candara, sans-serif; color:#FFFFFF; font-size:16px;">
                                                        <a href="#" class="sty-w3l" style="text-decoration: none; padding: 8px 20px 8px 20px; color:#FFFFFF">Читати більше</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="35" style="font-size: 1px;">&nbsp;</td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>

                </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<!-- //Side column 1-->

<!-- Side column 2 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="FCFCFC">
    <tr>
        <td bgcolor="#efe9e5">

            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-90" bgcolor="FFFFFF">
                <tr>
                    <td style="border-left: 1px solid #e2e2e2; border-right: 1px solid #e2e2e2;">

                        <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-90">
                            <tr>
                                <td>

                                    <table width="265" border="0" cellspacing="0" cellpadding="0" align="right" style="font-family: Candara, sans-serif; font-size: 14px; color: #9b9b9b;" class="pro1-w3 scale">
                                        <tr>
                                            <td>
                                                <img src="https://ec.net.ua/wp-content/uploads/2018/06/g4.jpg" style="border-radius: 5px; height:180px;" class="scale" alt="" width="200" editable="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="produ" height="50" style="font-size: 1px">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <table width="265" border="0" cellspacing="0" cellpadding="0" align="left" style="font-family: Candara, sans-serif; font-size: 14px; color: #9b9b9b;" class=" pro1-w3 scale">
                                        <tr>
                                            <td class="agile-main" style="font-size: 19px; color: #000; font-family: Bell Gothic Std, sans-serif;">Knauf</td>
                                        </tr>
                                        <tr>
                                            <td class="produ1" height="16" style="font-size: 1px">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="w3l-p" style="font-size: 16px; line-height: 24px; color: #7f8c8d; font-family: Candara, sans-serif;">

                                                Найбільший в світі виробник будівельних матеріалів для внутрішнього та зовнішнього оздоблення...

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="produ3" height="30" style="font-size: 1px">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="agile-bu" >
                                                <table class="w3-a" bgcolor="#00a78e" style="border-radius: 5px;" align="left" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="w3-a" height="35" align="center" style="font-family: Candara, sans-serif; color:#FFFFFF; font-size:16px;">
                                                            <a href="#" style="text-decoration: none; padding: 8px 20px 8px 20px; color:#FFFFFF">Читати більше</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="35" style="font-size: 1px;">&nbsp;</td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<!-- //Side column 2-->

<!-- Side column 3-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="FCFCFC">
    <tr>
        <td bgcolor="#efe9e5">

            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-90" bgcolor="FFFFFF">
                <tr>
                    <td style="border-left: 1px solid #e2e2e2; border-right: 1px solid #e2e2e2;">

                        <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-90">
                            <tr>
                                <td>

                                    <table width="265" border="0" cellspacing="0" cellpadding="0" align="left" style="font-family: Candara, sans-serif; font-size: 14px; color: #9b9b9b;" class="pro1-w3 scale">
                                        <tr>
                                            <td>
                                                <img src="https://ec.net.ua/wp-content/uploads/2018/06/g3.jpg" style="border-radius: 5px; height:180px;" class="scale" alt="" width="200" editable="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="produ" height="40" style="font-size: 1px">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <table width="265" border="0" cellspacing="0" cellpadding="0" align="right" style="font-family: Candara, sans-serif; font-size: 14px; color: #9b9b9b;" class="pro1-w3 scale">
                                        <tr>
                                            <td class="agile-main" style="font-size: 19px; color: #000; font-family: Bell Gothic Std, sans-serif;">Caparol</td>
                                        </tr>
                                        <tr>
                                            <td class="produ4" height="16" style="font-size: 1px">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="w3l-p" style="font-size: 16px; line-height: 24px; color: #7f8c8d; font-family: Candara, sans-serif;">

                                                В Європі є лідером в галузі лакофарбових матеріалів, систем теплоізоляції, декоративних матеріалів та архітектурних покриттів...

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="produ8" height="30" style="font-size: 1px">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="agile-bu">
                                                <table class="w3-a" bgcolor="#00a78e" style="border-radius: 5px;" align="left" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="w3-a" height="35" align="center" style="font-family: Candara, sans-serif; color:#FFFFFF; font-size:16px;">
                                                            <a href="#" style="text-decoration: none; padding: 8px 20px 8px 20px; color:#FFFFFF">Читати більше</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="produ9" height="30" style="font-size: 1px;">&nbsp;</td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<!-- //Side column 1-->

<!-- Side column 4 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="FCFCFC">
    <tr>
        <td bgcolor="#efe9e5">

            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-90" bgcolor="FFFFFF">
                <tr>
                    <td style="border-left: 1px solid #e2e2e2; border-right: 1px solid #e2e2e2;">

                        <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-90">
                            <tr>
                                <td>

                                    <table width="265" border="0" cellspacing="0" cellpadding="0" align="right" style="font-family:Candara,  Helvetica, Arial, sans-serif; font-size: 14px; color: #9b9b9b;" class="pro1-w3 scale">
                                        <tr>
                                            <td>
                                                <img src="https://ec.net.ua/wp-content/uploads/2018/06/g2.jpg" style="border-radius: 5px; height:180px;" class="scale" alt="" width="200" editable="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="produ" height="50" style="font-size: 1px">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <table width="265" border="0" cellspacing="0" cellpadding="0" align="left" style="font-family:Candara, Helvetica, Arial, sans-serif; font-size: 14px; color: #9b9b9b;" class="pro1-w3 scale">
                                        <tr>
                                            <td class="agile-main" style="font-size: 19px; color: #000; font-family: Bell Gothic Std, sans-serif;">Ceresit</td>
                                        </tr>
                                        <tr>
                                            <td class="prod" height="16" style="font-size: 1px">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="w3l-p" style="font-size: 16px; line-height: 24px; color: #7f8c8d; font-family: 'Candara', sans-serif;">

                                                В Україні продукцію Ceresit, купують більше, ніж матеріали будь-яких інших будівельних брендів...

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="produ" height="30" style="font-size: 1px">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="agile-bu">
                                                <table class="w3-a" bgcolor="#00a78e" style="border-radius: 5px;" align="left" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="w3-a" height="35" align="center" style="font-family: 'Candara', Helvetica, Arial, sans-serif; color:#FFFFFF; font-size:16px;">
                                                            <a href="#" style="text-decoration: none; padding: 8px 20px 8px 20px; color:#FFFFFF">Читати більше</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="progdu" height="35" style="font-size: 1px;">&nbsp;</td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<!-- //Side column 4 -->
<!-- //PRODUCTS -->


<!-- BOTTOM BREAKER -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#efe9e5" style="background: url(https://ec.net.ua/wp-content/uploads/2018/06/footer-bgbtm2.png) center center / cover no-repeat, #efe9e5;">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                <tr>
                    <td bgcolor="#FFFFFF" style="border-bottom-left-radius: 4px; border-bottom-right-radius: 4px;">

                        <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                            <tr><td class="w3-gr" height="84">&nbsp;</td></tr>
                        </table>

                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<!-- //BOTTOM BREAKER -->

<!-- FOOTER -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#141416">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                <tr>
                    <td>

                        <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="smocale">
                            <tr><td class="mo" height="54">&nbsp;</td></tr>
                            <tr>
                                <td align="center">
                                    <table width="170" border="0" cellspacing="0" cellpadding="0" align="left" class="fa1 scale">
                                        <tr>
                                            <td class="agile-main" align="center" style="font-family:'source_sans_proregular', Helvetica, Arial, sans-serif; color: #FFFFFF; font-size: 20px; line-height: 20px;" class="scale-center-bottom-both">
                                                <img src="https://ec.net.ua/wp-content/uploads/2018/06/1.png" border="0" style="display: inline-block; vertical-align: middle; max-width: 40px; height:24px;" alt="" width="24" editable="true" />&nbsp;&nbsp;<a href="" style="font-family:'Bell Gothic Std', sans-serif; color: #FFFFFF; font-size: 20px; line-height: 20px; text-decoration: none;">Facebook</a>
                                            </td>
                                        </tr>
                                    </table>

                                    <table width="355" border="0" cellspacing="0" cellpadding="0" align="right" class="fas scale">
                                        <tr>
                                            <td>

                                                <table width="170" border="0" cellspacing="0" cellpadding="0" align="left" class="fa2 scale">
                                                    <tr>
                                                        <td class="agile-main" align="center" style="font-family:'source_sans_proregular', Helvetica, Arial, sans-serif; color: #FFFFFF; font-size: 20px; line-height: 20px;" class="scale-center-bottom-both">
                                                            <img src="https://ec.net.ua/wp-content/uploads/2018/06/2.png" border="0" style="display: inline-block; vertical-align: middle; max-width: 40px; height:24px;" alt="" width="24" editable="true" />&nbsp;&nbsp;<a href="#" style="font-family:'Bell Gothic Std', sans-serif; color: #FFFFFF; font-size: 20px; line-height: 20px; text-decoration: none;">Twitter</a>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="170" border="0" cellspacing="0" cellpadding="0" align="right" class="fa3 scale">
                                                    <tr>
                                                        <td class="agile-main" align="center" style="font-family:'source_sans_proregular', Helvetica, Arial, sans-serif; color: #FFFFFF; font-size: 20px; line-height: 20px;" class="scale-center-both">
                                                            <img src="https://ec.net.ua/wp-content/uploads/2018/06/3.png" border="0" style="display: inline-block; vertical-align: middle; max-width: 40px; height:24px" alt="" width="24" height:24pxeditable="true" />&nbsp;&nbsp;<a href="#" style="font-family:'Bell Gothic Std', sans-serif; color: #FFFFFF; font-size: 20px; line-height: 20px; text-decoration: none;">Google +</a>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td height="36">&nbsp;</td></tr>
                            <tr align="center">
                                <td class="mail-w3" style="color:#fff;font-size:15px;font-family:Candara;line-height:1.9em;tex-align:center;">
                                    Ви отримуєте цей електронний лист, оскільки ви підписалися на нашу розсилку.
                                </td>
                            </tr>
                            <tr align="center">
                                <td class="mail-w3" style="color:#fff;font-size:15px;font-family:Candara;line-height:2em;">
                                    <a href="" style="color:#d20962;text-decoration:none; font-weight:bold">Натисніть </a> тут <a href="" style="color:#d20962; font-size: 1em;text-decoration:none;font-weight:bold"> аби відписатись</a> з розсилки.
                                </td>
                            </tr>
                            <tr><td class="hg1" height="20"></td></tr>
                            <tr><td class="hg11" height="54">&nbsp;</td></tr>
                        </table>

                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<tr>
    <!-- //FOOTER -->

    <!-- //INTRODUCTION -->
