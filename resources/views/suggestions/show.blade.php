@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div style="border: 2px solid #0198d0; red; padding: 20px; margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-3">
                        <div class="log-company" style="margin: 20px;">
                            <p style="font-size: 24px; line-height: 28px; font-weight: 700; display: inline-block; max-width: 300px;">{{$company->title}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5"  style="margin: 20px;">
                                <span class="lnr lnr-map-marker"  style="color: #0a90eb; font-size: 14px; font-weight: 700; margin-right: 2px;"></span>{{$company->location}}
                            </div>
                            <div class="col-md-5"  style="margin: 20px;">
                                <span class="lnr lnr-phone" style="color: #0a90eb; font-size: 14px; font-weight: 700; margin-right: 2px;"></span>{{$company->phone}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div>
                            <img  class="center" src="{{  asset('/app/'. $company->logo ) }}" alt="телевизор &quot;TELEFUNKEN&quot; 26 LCD" height="70" style="text-align:right; display:block; margin:0 auto;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#panel1">Про компанію</a></li>
                <li><a data-toggle="tab" href="#panel2">Про пропозицію</a></li>
                <li><a data-toggle="tab" href="#panel3">Відео</a></li>
                <li><a data-toggle="tab" href="#panel4">Фото</a></li>
                <li><a data-toggle="tab" href="#panel5">Контакти</a></li>
            </ul>

            <div class="tab-content">
                <div id="panel1" class="tab-pane fade in active">
                    <h3> {{$company->title}}</h3>
                    <p> {{$company->content}}</p>
                </div>
                <div id="panel2" class="tab-pane fade">
                    <div class="col-md-6">
                        <h3> {{$suggestions->title}}</h3>
                        <p> {!! $suggestions->content !!}</p>
                    </div>
                    <div class="col-md-6">
                        <img  class="center" src="{{  asset('/app/'. $suggestions->avatar ) }}" alt="телевизор &quot;TELEFUNKEN&quot; 26 LCD" height="200" style="text-align:right; display:block; margin:0 auto;">
                    </div>
                </div>
                <div id="panel3" class="tab-pane fade">
                    <h3 style=""> {!! $suggestions->movie!!}</h3>
                </div>
                <div id="panel4" class="tab-pane fade">
                    @foreach($suggestions->photo()->orderBy('id')->get() as $photo)
                        <div class="col-md-4">
                            <div style="width: 94%; height: 170px; border: 1px solid #efefef;position: relative; margin-top: 10px;">
                                <img  class="center" src="{{ asset('/app/'. $photo->file ) }}" alt="телевизор &quot;TELEFUNKEN&quot; 26 LCD" height="130" style="text-align:center; display:block; margin:0 auto; margin-bottom: 20px; margin-top: 10px;">
                            </div>
                            {{--<img width="100" height="150" src="{{ asset('/storage/'. $photo->file ) }}" alt="{{$suggestions->title}}">--}}
                            {{--<form method="POST" action="{{ route('admin.suggestions.photoDelete', [$suggestions, $photo->id]) }}">--}}
                                {{--{{ csrf_field() }}--}}
                                {{--{{method_field('DELETE')}}--}}
                                {{--<button class="btn btn-danger"  style=" border-radius: 20px; top: -24px; left: -5px; position: relative;">x</button>--}}
                            {{--</form>--}}
                        </div>
                    @endforeach
                </div>
                <div id="panel5" class="tab-pane fade">
                    <h3> {{$suggestions->address}}</h3>
                    <div style="background: black; background-image: url('/images/map.png')">
                        <div id="map" style="width: 100%; height: 250px;"></div>
                        @section('script')
                            <script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=uk-UA" type="text/javascript"></script>

                            <script type='text/javascript'>
                                ymaps.ready(init);
                                function init(){
                                    var geocoder = new ymaps.geocode(
                                        // Строка с адресом, который нужно геокодировать
                                        '{{ $suggestions->address }}',
                                        // требуемое количество результатов
                                        { results: 1 }
                                    );
                                    // После того, как поиск вернул результат, вызывается callback-функция
                                    geocoder.then(
                                        function (res) {
                                            // координаты объекта
                                            var coord = res.geoObjects.get(0).geometry.getCoordinates();
                                            var map = new ymaps.Map('map', {
                                                // Центр карты - координаты первого элемента
                                                center: coord,
                                                // Коэффициент масштабирования
                                                zoom: 7,
                                                // включаем масштабирование карты колесом
                                                behaviors: ['default', 'scrollZoom'],
                                                controls: ['mapTools']
                                            });
                                            // Добавление метки на карту
                                            map.geoObjects.add(res.geoObjects.get(0));
                                            // устанавливаем максимально возможный коэффициент масштабирования - 1
                                            map.zoomRange.get(coord).then(function(range){
                                                map.setCenter(coord, range[1] - 1)
                                            });
                                            // Добавление стандартного набора кнопок
                                            map.controls.add('mapTools')
                                            // Добавление кнопки изменения масштаба
                                                .add('zoomControl')
                                                // Добавление списка типов карты
                                                .add('typeSelector');
                                        }
                                    );
                                }
                            </script>
                        @endsection
                    </div>
                 </div>
            </div>

        </div>
        <div class="col-md-3"  style="margin-top: 20px;">
            <h5 style="font-weight: 900; font-size: 14px; text-align: center;">Реклама</h5>
            <div class="banner" data-format="240x440" data-category="{{  $company->category ? $company->category->id : '' }}" data-region="{{ $company->region ? $company->region->id : '' }}" data-url="{{ route('banner.get') }}">
            </div>
            <div class="clearfix" style="margin-top: 30px;"></div>
        </div>
@endsection