@extends('layouts.appmore')
@section('more')
    <div class="header" id="home">
        <div class="container">
            <div class="logo">
                <a href="index.html"><img src="web/images/logo.png" alt=""></a>
            </div>
            <div class="navigation">
                <span class="menu"></span>
                <ul class="navig">
                    <li><a href="{{ route('home') }}">Головна</a><span> </span></li>
                    <li><a href="#features" class="scroll">Функціонал</a><span> </span></li>
                    <li><a href="#countdown" class="scroll">Новини</a><span> </span></li>
                    <li><a href="#pricing" class="scroll">Тарифний план</a><span> </span></li>
                    <li><a href="#testimonials" class="scroll">Команда</a><span> </span></li>
                    <li><a href="#contact" class="scroll">Контакти</a><span> </span></li>
                </ul>
            </div>
            <!-- script-for-menu_tpl -->
            <script>
                $("span.menu_tpl").click(function(){
                    $(" ul.navig").slideToggle("slow" , function(){
                    });
                });
            </script>
            <!-- script-for-menu_tpl -->
        </div>
    </div>
    <!--start-banner-->
    <div class="banner">
        <div class="container">
            <div class="banner-top">
                <h1>Афіша Прикарпаття обновилась!</h1>
                <p>Подавати оголошення стало ще зручніше, з новими можливостями Афіша Прикарпаття!</p>
            </div>
            <div class="banner-bottom">
                <div class="col-md-4 banner-left">
                    <a href="#">Зареєструватись</a>
                </div>
                <div class="col-md-4 banner-middle">
                    <img src="web/images/app-1.png" alt="" />
                </div>
                <div class="col-md-4 banner-right">
                    <a href="#">Дізнатись більше</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--start-banner-->
    <!--start-feature-->
    <div class="feature" id="features">
        <div class="container">
            <div class="feature-main">
                <h3>Нові можливості</h3>
                <P>Ознайомтесь з новим функціоналом Афіша Прикарпаття, який допоможить Вам збільшити продажі, а нам стати кращим ресурсом оголошень для Вас!</P>
            </div>
            <div class="feature-bottom">
                <div class="col-md-6 feature-bottom-left">
                    <div class="ftr-one">
                        <div class="feature-left">
                            <img src="web/images/feature-1.png" alt="" />
                        </div>
                        <div class="feature-right">
                            <h4>Банерна система</h4>
                            <p> Допомагає обрати оптимальну стратегію і уникнути неефективного витрачання бюджету. Налаштуйте відображення банера на певну кількість переглядів, та в категоріїї, в якій Ви цікаві аудиторії.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ftr-one">
                        <div class="feature-left">
                            <img src="web/images/feature-3.png" alt="" />
                        </div>
                        <div class="feature-right">
                            <h4>Служба підтримки</h4>
                            <p>Ми реалізували персональну платформу Help Desk, аби бути поруч коли це необхідно, напишіть нам і ми допоможемо вирішити  Вашу проблему на сайті.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-6 feature-bottom-left">
                    <div class="ftr-one">
                        <div class="feature-left">
                            <img src="web/images/feature-2.png" alt="" />
                        </div>
                        <div class="feature-right">
                            <h4>Управління рекламою</h4>
                            <p>Рекламувати, піднімати в позиціях та переміщати в ТОП Ваші оголошення, відтепер все це стало можливим на сайті Афіша Прикарпаття.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ftr-one">
                        <div class="feature-left">
                            <img src="web/images/feature-5.png" alt="" />
                        </div>
                        <div class="feature-right">
                            <h4>Переписка з автором</h4>
                            <p>Задати питання автору оголошення, чи відповісти Вашому потенційному клієнту, такий необхідний функціонал повинен бути на кожному сайті, відтепер доступний і у нас.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    {{--<div class="ftr-one">--}}
                        {{--<div class="feature-left">--}}
                            {{--<img src="web/images/feature-4.png" alt="" />--}}
                        {{--</div>--}}
                        {{--<div class="feature-right">--}}
                            {{--<h4>Підписка на розсилку</h4>--}}
                            {{--<p>--}}
                                {{--Розвивайте свій бізнес на Афіша за допомогою Підписки. Розкажіть про свої товари, послуги або вакансії 1 мільйону відвідувачів сайту!</p>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="ftr-one">--}}
                        {{--<div class="feature-left">--}}
                            {{--<img src="web/images/feature-6.png" alt="" />--}}
                        {{--</div>--}}
                        {{--<div class="feature-right">--}}
                            {{--<h4>Загрузка резюме</h4>--}}
                            {{--<p>Знайдіть роботу в один клік, загружайте Ваші резюму на вакансії які Вам цікаві та відзивайтесь на вакансії лише одним натиском кнопки.</p>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--end-feature-->
    <!--start-news-->
    <div class="top-news">
        <div class="news">
            <div class="container">
                <div class="news-main">
                    <h3>Новини від Афіша</h3>
                    <p> Підпишіться на цікаву Вам тематику і будьте в центрі подій. </p>
                </div>
                <div class="news-top">
                    <input type="text" value="E-mail adres.." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'E-mail adres..';}"/>
                    <div class="contact-but">
                        <input type="submit" value="ПІДПИСАТИСЬ" />
                    </div>
                </div>
                <div class="new-bottom">
                    <img src="web/images/slide-back.png" alt="" />
                </div>
                <script src="{{ asset('web/js/responsiveslides.min.js') }}"></script>
                <script>
                    // You can also use "$(window).load(function() {"
                    $(function () {
                        // Slideshow 4
                        $("#slider4").responsiveSlides({
                            auto: true,
                            pager: false,
                            nav: false,
                            speed: 500,
                            namespace: "callbacks",
                            before: function () {
                                $('.events').append("<li>before event fired.</li>");
                            },
                            after: function () {
                                $('.events').append("<li>after event fired.</li>");
                            }
                        });

                    });
                </script>
                <!----//End-slider-script---->
                <div  id="top" class="callbacks_container">
                    <ul class="rslides" id="slider4">
                        <li>
                            <div class="new-slid">
                                <p>“Відтепер на нашому сайті самі свіжі новини на ринку нерухомості, бізнесу та продаж.”</p>
                                <h4>Редактор: Іван Іванович</h4>
                                <img src="web/images/slide-1.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="new-slid">
                                <p>“Відтепер на нашому сайті самі свіжі новини на ринку нерухомості, бізнесу та продаж. ”</p>
                                <h4>Редактор: Іван Іванович</h4>
                                <img src="web/images/slide-2.png" alt="">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--end-news-->
    <!--Slider-Starts-Here-->
    <script src="js/responsiveslides.min.js"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: true,
                pager: true,
                nav: true,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
    <!--End-slider-script-->
    <!--start-app1-->
    <div class="app1" id="countdown">
        <div class="container">
            <div class="app1-top">
                <h3>Остані новини Афіша</h3>
                <p></p>
            </div>
            <div class="app1-bottom">
                <div class="row">
                    @foreach($news as $item)
                        <div class="col-md-6">
                            <div class="app1-main">
                                <span>{{ date('y.m.d', strtotime($item->created_at)) }}</span>
                                <a href="{{route('news.show', $item)}}"><h4>{{ $item->title }}</h4>
                                <p>{!! \Illuminate\Support\Str::limit($item->content,60)  !!}</p></a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--end-app1-->
    <!--start-screen-->
    <div class="screen">
        <div class="container">
            <div class="screen-top">
                <h3>Скріншоти можливостей сайту!</h3>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur</p>
            </div>
            <div class="screen-bottom">
                <div class="col-md-3 screen-left">
                    <a href="single.html" class="b-link-stripe b-animate-go  thickbox">
                        <img class="port-pic" class="img-responsive" src="web/images/screen-1.png" />
                        <div class="b-wrapper">
                            <h2 class="b-animate b-from-left b-from   b-delay03 ">
                                <span>orem ipsum dolor sit amet, consectetuer adipiscing lit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis</span>
                            </h2>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 screen-left">
                    <a href="single.html" class="b-link-stripe b-animate-go  thickbox">
                        <img class="port-pic" class="img-responsive" src="web/images/screen-2.png" />
                        <div class="b-wrapper">
                            <h2 class="b-animate b-from-left b-from   b-delay03 ">
                                <span>orem ipsum dolor sit amet, consectetuer adipiscing lit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis</span>
                            </h2>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 screen-left">
                    <a href="single.html" class="b-link-stripe b-animate-go  thickbox">
                        <img class="port-pic" class="img-responsive" src="web/images/screen-3.png" />
                        <div class="b-wrapper">
                            <h2 class="b-animate b-from-left b-from   b-delay03 ">
                                <span>orem ipsum dolor sit amet, consectetuer adipiscing lit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis</span>
                            </h2>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 screen-left">
                    <a href="single.html" class="b-link-stripe b-animate-go  thickbox">
                        <img class="port-pic" class="img-responsive" src="web/images/screen-4.png" />
                        <div class="b-wrapper">
                            <h2 class="b-animate b-from-left b-from   b-delay03 ">
                                <span>orem ipsum dolor sit amet, consectetuer adipiscing lit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis</span>
                            </h2>
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--end-screen-->
    <!--start-member-->
    <div class="member" id="pricing">
        <div class="container">
            <div class="member-top">
                <h3>Тарифний План!</h3>
                <p>Оберіть вигідни тарифний план саме для Вас і заощаджуйте на подачі оголошень!</p>
            </div>
            <div class="member-bottom">
                <div class="col-md-3 member-bottom-left">
                    <div class="member-one">
                        <div class="mem-back">
                            <p>100<span>грн.</span></p>
                            <label>місяць</label>
                        </div>
                        <div class="mem-two">
                            <h5>СТАНДАРТ</h5>
                            <h5>100 оголошень</h5>
                            <h5>бізнес-сторінка</h5>
                            <h5>-</h5>
                            <h5>-</h5>
                            <h5>-</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 member-bottom-left">
                    <div class="member-one">
                        <div class="mem-back">
                            <p>500<span>грн.</span></p>
                            <label>місяць</label>
                        </div>
                        <div class="mem-two">
                            <h5>БІЗНЕС</h5>
                            <h5>100 оголошень</h5>
                            <h5>бізнес-сторінка</h5>
                            <h5>без реклами конкурентів</h5>
                            <h5>1 банер/10 тис. переглядів</h5>
                            <h5>-</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 member-bottom-left">
                    <div class="member-one">
                        <div class="mem-back">
                            <p>1000<span>грн.</span></p>
                            <label>місяць</label>
                        </div>
                        <div class="mem-two">
                            <h5>ПРЕМІУМ</h5>
                            <h5>200 оголошень</h5>
                            <h5>бізнес-сторінка</h5>
                            <h5>без реклами конкурентів</h5>
                            <h5>5 банер/10 тис. переглядів</h5>
                            <h5>-</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 member-bottom-left">
                    <div class="member-one">
                        <div class="mem-back">
                            <p>1500<span>грн.</span></p>
                            <label>місяць</label>
                        </div>
                        <div class="mem-two">
                            <h5>ГОЛД</h5>
                            <h5>300 оголошень</h5>
                            <h5>бізнес-сторінка</h5>
                            <h5>без реклами конкурентів</h5>
                            <h5>10 банер/10 тис. переглядів</h5>
                            <h5>персональна аналітика</h5>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--end-member-->
    <!--end-team-->
    <div class="team" id="testimonials">
        <div class="container">
            <div class="team-top">
                <h3>Наша команда</h3>
                <p></p>
            </div>
            <div class="team-bottom">
                <div class="col-md-4 team-left">
                    <img src="web/images/team-back.png" alt=""/>
                    <div class="team-one">
                        <img src="web/images/team-1.png" alt=""/>
                        <h4>Mohamed Saed</h4>
                        <h5>UI / UX</h5>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing Aenean commodo</p>
                    </div>
                </div>
                <div class="col-md-4 team-left">
                    <img src="web/images/team-back.png" alt=""/>
                    <div class="team-one">
                        <img src="web/images/team-2.png" alt=""/>
                        <h4>Aya Mostafa</h4>
                        <h5>Seo</h5>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing Aenean commodo</p>
                    </div>
                </div>
                <div class="col-md-4 team-left">
                    <img src="web/images/team-back.png" alt=""/>
                    <div class="team-one">
                        <img src="web/images/team-3.png" alt=""/>
                        <h4>Osama Elwan</h4>
                        <h5>Web Dsigne</h5>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing Aenean commodo</p>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!--end-team-->
    <!--start-support-->
    <div class="support">
        <div class="container">
            <div class="support-main">
                <div class="col-md-3 support-left">
                    <img src="web/images/support-1.png" alt=""/>
                </div>
                <div class="col-md-3 support-left">
                    <img src="web/images/support-2.png" alt=""/>
                </div>
                <div class="col-md-3 support-left">
                    <img src="web/images/support-3.png" alt=""/>
                </div>
                <div class="col-md-3 support-left">
                    <img src="web/images/support-4.png" alt=""/>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--end-support-->
    <!--start-map-->
    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2621.7708325458493!2d24.707267515676527!3d48.919758279293625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4730c16b257fcf2d%3A0x7728c247eb41e83c!2z0LLRg9C70LjRhtGPINCd0LXQt9Cw0LvQtdC20L3QvtGB0YLRliwgNCwg0IbQstCw0L3Qvi3QpNGA0LDQvdC60ZbQstGB0YzQuiwg0IbQstCw0L3Qvi3QpNGA0LDQvdC60ZbQstGB0YzQutCwINC-0LHQu9Cw0YHRgtGMLCA3NjAwMA!5e0!3m2!1suk!2sua!4v1544953914783" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>    </div>
    <!--end-map-->
    <!--start-contact-->
    <div class="contact" id="contact">
        <div class="container">
            <div class="contact-top">
                <h3>Задайте нам питання</h3>
                <p>При винекнені питань надішліть нам лист і ми обовязково з Вами зв'яжемось!</p>
            </div>
            <form action="{{ route('message') }}" method="POST">
                {{ csrf_field() }}
                {{method_field('PUT')}}
            <div class="contact-bottom">
                <div class="col-md-8 contact-left">
                    <input type="text"  name="name" placeholder="Ваше ім'я" value="{{ old('name') }}" required>
                    <input type="text" name="email" placeholder="Ваш Email" value="{{ old('email') }}" required>
                </div>
                <div class="col-md-4 contact-right">
                    <h6>Афіша Прикарпаття<span>Україна, м. Івано-Франківськ,
вул. Незалежності, 4, БЦ "Київ", 4 поверх</span></h6>
                    <p><a href="mailto:example@email.com">afisha.if.ua@gmail.com</a></p>
                </div>
                <div class="contact-textarea">
                    <textarea name="text" placeholder="Ваше Повідомлення">{{ old('text') }}</textarea>
                </div>
                <div class="contact-submit">
                    <input type="submit" value="Надіслати" />
                </div>
            </div>
            </form>
            <div class="social">
                <ul>
                    <li><a href="#"><span class="fb"> </span></a></li>
                    <li><a href="#"><span class="p"> </span></a></li>
                    <li><a href="#"><span class="twt"> </span></a></li>
                    <li><a href="#"><span class="g"> </span></a></li>
                    <li><a href="#"><span class="rss"> </span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--end-contact-->
    <!--start-footer-->
    <div class="footer">
        <div class="container">
            <div class="footer-text">
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                /*
                var defaults = {
                      containerID: 'toTop', // fading element id
                    containerHoverID: 'toTopHover', // fading element hover id
                    scrollSpeed: 1200,
                    easingType: 'linear'
                 };
                */

                $().UItoTop({ easingType: 'easeOutQuart' });

            });
        </script>
        <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    </div>
@endsection