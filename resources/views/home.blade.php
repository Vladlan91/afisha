@extends('layouts.app')
@section('breadcrumbs','')
@section('content')
    @include('layouts.search._search', ['categories' => $categories, 'regions' => $regions ])
    <div id="mobileAppsbadge" class="fix" style="display: block">
        <a href="{{ route('about') }}" class="icon tdnone abs" style="background:url(../images/icons2.png) no-repeat;"></a>
        <a href="#" id="mobileAppsbadgeClose" onclick="myFunction()" class="tdnone abs" title="Закрити"></a>
    </div>
    <script>
        function myFunction() {
            document.getElementById("mobileAppsbadge").style.display = "none";
        }
    </script>
    <div class="card">
        <div class="modal-header">
        </div>
        <div class="card-body" style="padding: 25px;">
            <div class="row">
                @foreach(array_chunk($categories, 3) as $chunk)
                    <div class="col-md-3">
                        <ul class="list-unstyled">
                            @foreach( $chunk as $current)
                                <li><a href="{{ route('adverts.all',  [$current]) }}">{{ $current->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <h4 style="font-weight: 900;">Оголошення</h4>
            <div class="banner" data-format="820x143" data-category="" data-region="" style="0.75em;">
                @foreach($banner1 as $ban)
                    <a href="{{ route('banner.click', $ban) }}" target="_blank">
                        <img width="850"
                             height="143" src="{{ asset('/app/'. $ban->file ) }}" alt="{{ $ban->name }}">
                    </a>
                @endforeach
            </div>
            <ul class="">
                @php($i = 1)
                @php($b = 0)
                @foreach($adverts as $advert)
                    @if($i == 6)
                </ul>
                    <div class="banner" data-format="820x143" data-category="" data-region="" style="margin-top: 10px;">
                        @foreach($banner2 as $ban)
                            <a href="{{ route('banner.click', $ban) }}" target="_blank">
                                <img width="850"
                                     height="143" src="{{ asset('/app/'. $ban->file ) }}" alt="{{ $ban->name }}">
                            </a>
                        @endforeach
                    </div>
                <ul class="">
                    @elseif($i == 17 or $i == 27 or $i == 37 )
                </ul>
                    <div class="banner" data-format="820x143" data-category="null" data-region="null" data-url="{{ route('banner.get') }}"  style="margin-top: 0.75em;">

                    </div>
                <ul class="">
                    @else
                        @if($advert->type === 'Е')
                            @php($b++ )
                        @endif
                    <li style="margin-top: 10px;">
                        @if($b < 6)
                            @if($advert->type === 'A')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" style="border: 1px solid #ccc !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #aaaaaa; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Д</h5>
                                </div>
                            @elseif($advert->type === 'Б')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" style="border: 1px solid #45c639 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #45c639; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                                </div>
                            @elseif($advert->type === 'В')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #219fde !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>В</h5>
                                </div>
                            @elseif($advert->type === 'Г')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #219fde !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Б</h5>
                                </div>
                            @elseif($advert->type === 'Д')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #fed700 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #fed700; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>А</h5>
                                </div>
                            @else
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #ea1b25 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                                <div style="position: absolute; left: 0;top: -12px; z-index: 1;">
                                    <h5 style="background-color: #ea1b25; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Р</h5>
                                </div>
                            @endif
                            <div class="item_image">
                                <span class="item_imagePic">
                                        <img src="{{  asset('/app/'. $advert->avatar ) }}" alt="Appartement 4 pièces 101 m²" style="height: 150px;">
                                </span>
                                <span class="item_imageNumber">
                        	<i class="icon-camera icon-2x nomargin"></i>
                        	<span>3</span>
                      	</span>
                            </div>
                            <div title="" class="saveAd" data-savead-id="1557273607">
                                @if($advert->isCompany())
                                    <img src="{{asset('app/'. $advert->company->logo)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                @else
                                    @if($advert->user->avatar)
                                        <img src="{{asset('app/'. $advert->user->avatar)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                    @else
                                        <img src="{{asset('images/no-avatar.jpg')}}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                    @endif
                                @endif
                            </div>
                            <section class="item_infos">
                                <h2 class="item_title">
                                    {{ \Illuminate\Support\Str::limit($advert->title,30) }}
                                </h2>
                                <h3 class="item_price">
                                    {{ $advert->price }}&nbsp;UAH
                                </h3>
                                <aside>
                                    <p class="item_supp">
                                        {{ $advert->category->name }}
                                        /
                                        {{ $advert->region ?  $advert->region->name : 'Всі міста'}}
                                    </p>
                                    <p class="item_supp item_suppDate">
                                        {{ $advert->user->name }}
                                    </p>
                                </aside>
                            </section>
                        @if(\Illuminate\Support\Facades\Auth::user() && !\Illuminate\Support\Facades\Auth::user()->hasInFavorites($advert->id) )
                            <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                {{ csrf_field() }}
                                <button class="" style="margin-right: 10px; color: black; border-radius: 20px; padding: 5px;border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-heart" style="font-size: 20px; font-weight: 900"></span></button>
                            </form>
                        @elseif(\Illuminate\Support\Facades\Auth::user())
                            <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button class="" style="margin-right: 10px;  background-color: white; color: black; border-radius: 20px; padding: 5px; border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-unlink" style="font-size: 20px; font-weight: 900"></span></button>
                            </form>
                        @endif
                        </a>
                    </li>
                    @else
                        @if($advert->type === 'Е')
                            @php($i--)
                        @else
                            @if($advert->type === 'A')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" style="border: 1px solid #ccc !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                <h5 style="background-color: #aaaaaa; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Д</h5>
                            </div>
                            @elseif($advert->type === 'Б')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" style="border: 1px solid #45c639 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                <h5 style="background-color: #45c639; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                            </div>
                            @elseif($advert->type === 'В')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #219fde !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>В</h5>
                            </div>
                            @elseif($advert->type === 'Г')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #219fde !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Б</h5>
                            </div>
                            @elseif($advert->type === 'Д')
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #fed700 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                <h5 style="background-color: #fed700; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>А</h5>
                            </div>
                            @else
                            <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable"  style="border: 1px solid #ea1b25 !important;" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            <div style="position: absolute; left: 0;top: -12px; z-index: 1;">
                                <h5 style="background-color: #ea1b25; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Р</h5>
                            </div>
                            @endif
                            <div class="item_image">
                            <span class="item_imagePic">
                                    <img src="{{  asset('/app/'. $advert->avatar ) }}" alt="Appartement 4 pièces 101 m²" style="height: 150px;">
                            </span>
                            <span class="item_imageNumber">
                        	<i class="icon-camera icon-2x nomargin"></i>
                        	<span>3</span>
                      	</span>
                        </div>
                        <div title="" class="saveAd" data-savead-id="1557273607">
                            @if($advert->isCompany())
                                <img src="{{asset('app/'. $advert->company->logo)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                            @else
                                @if($advert->user->avatar)
                                    <img src="{{asset('app/'. $advert->user->avatar)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                @else
                                    <img src="{{asset('images/no-avatar.jpg')}}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                @endif
                            @endif
                        </div>
                        <section class="item_infos">
                            <h2 class="item_title">
                                {{ \Illuminate\Support\Str::limit($advert->title,30) }}
                            </h2>
                            <h3 class="item_price">
                                {{ $advert->price }}&nbsp;UAH
                            </h3>
                            <aside>
                                <p class="item_supp">
                                    {{ $advert->category->name }}
                                    /
                                    {{ $advert->region ?  $advert->region->name : 'Всі міста'}}
                                </p>
                                <p class="item_supp item_suppDate">
                                    {{ $advert->user->name }}
                                </p>
                            </aside>
                        </section>
                        @if(\Illuminate\Support\Facades\Auth::user() && !\Illuminate\Support\Facades\Auth::user()->hasInFavorites($advert->id) )
                            <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                {{ csrf_field() }}
                                <button class="" style="margin-right: 10px; color: black; border-radius: 20px; padding: 5px;border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-heart" style="font-size: 20px; font-weight: 900"></span></button>
                            </form>
                        @elseif(\Illuminate\Support\Facades\Auth::user())
                            <form method="POST" action="{{ route('adverts.favorites', $advert) }}">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button class="" style="margin-right: 10px;  background-color: white; color: black; border-radius: 20px; padding: 5px; border: none; position: relative; float: right; top: 0px;"><span class="lnr lnr-unlink" style="font-size: 20px; font-weight: 900"></span></button>
                            </form>
                        @endif
                    </a>
                    </li>
                        @endif
                    @endif
                    @endif
                    @php($i++)
                @endforeach
            </ul>
            {{$adverts->links()}}
        </div>
        <div class="col-md-3">
            <h4 style="font-weight: 900;">Пропозиції</h4>
            <ul class="">
                @foreach($suggestions as $suggest)
                <li itemscope="" itemtype="http://schema.org/Offer" class="_3eDdy">
                    <a title="Maserati Granturismo 4.2" class="_2fKRW" data-qa-id="aditem_container" href="{{ route('suggestions.show', [$suggest->company_id, $suggest]) }}">
                        <div class="_2-jsN"><div class="LazyLoad is-visible">
                                <div>
                                    <div class="_6ntGx" style="background-image: url(&quot;https://img1.leboncoin.fr/ad-image/fd96e60b12d1c41565d4c3ffd45db7e4a01c2bb6.jpg&quot;);"></div>
                                    <img src="{{asset('app/'. $suggest->avatar)}}" itemprop="image" content="https://img1.leboncoin.fr/ad-image/fd96e60b12d1c41565d4c3ffd45db7e4a01c2bb6.jpg" alt="subject"></div>
                            </div>
                            <span class="_1sbqp">
                                <div class="_3jAsY">
                                    <div class="_3KcVT">
                                        <span class="_1vK7W" name="spotlight">
                                            <svg viewBox="0 0 24 24" data-name="Calque 1">
                                                <path d="M18.43 0H5.57A2.63 2.63 0 0 0 3 2.67V24l9-4 9 4V2.67A2.63 2.63 0 0 0 18.43 0z"></path>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="_1MBDf">{{$suggest->company->title}}</div>
                                </div>
                            </span>
                        </div>
                        <div class="_3beID">
                            <section class="irAof">
                                <p class="_3ZfBw">
                                    <span itemprop="name" data-qa-id="aditem_title">{{ \Illuminate\Support\Str::limit($suggest->title,30) }}</span>
                                </p>
                                <p class="_1s5WJ" itemprop="availableAtOrFrom" data-qa-id="aditem_location"><!-- react-text: 2239 -->{!! \Illuminate\Support\Str::limit($suggest->content,30) !!} <!-- /react-text --></p>
                                <div class="CeFtS" itemprop="priceSpecification" itemscope="" itemtype="http://schema.org/PriceSpecification" data-qa-id="aditem_price">
                                    <meta itemprop="priceCurrency" content="EUR">
                                    <span class="_1_bNq">
                                        <span itemprop="price">{{$suggest->price}}</span><!-- react-text: 2244 -->&nbsp;UAH<!-- /react-text --></span>
                                </div>
                            </section>
                            <div class="_3A9T7"></div>
                        </div>
                    </a>
                    <div class="_3Zm0x" data-qa-id="listitem_save_ad">
                        <div>
                            <div class="_3C4to">
                                <div class="" data-tip="Sauvegarder l'annonce" data-place="left" data-for="toggleSavedFeaturedAd_1554842308" currentitem="false">
                                    <span class="_1vK7W" name="heartoutline">
                                        <svg viewBox="0 0 59.2 59.2" style="enable-background:new 0 0 488.85 488.85;" xml:space="preserve" width="25px" height="25px">
                                          <path d="M51.062,21.561c-11.889-11.889-31.232-11.889-43.121,0L0,29.501l8.138,8.138c5.944,5.944,13.752,8.917,21.561,8.917
		s15.616-2.972,21.561-8.917l7.941-7.941L51.062,21.561z M49.845,36.225c-11.109,11.108-29.184,11.108-40.293,0l-6.724-6.724
		l6.527-6.527c11.109-11.108,29.184-11.108,40.293,0l6.724,6.724L49.845,36.225z"/>
	<path d="M28.572,21.57c-3.86,0-7,3.14-7,7c0,0.552,0.448,1,1,1s1-0.448,1-1c0-2.757,2.243-5,5-5c0.552,0,1-0.448,1-1
		S29.125,21.57,28.572,21.57z"/>
	<path d="M29.572,16.57c-7.168,0-13,5.832-13,13s5.832,13,13,13s13-5.832,13-13S36.741,16.57,29.572,16.57z M29.572,40.57
		c-6.065,0-11-4.935-11-11s4.935-11,11-11s11,4.935,11,11S35.638,40.57,29.572,40.57z"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="sc-fjdhpX bpzbGb">
                                    <div class="__react_component_tooltip place-top type-dark " id="toggleSavedFeaturedAd_1554842308" data-id="tooltip"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
