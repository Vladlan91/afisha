<ul class="is-hidden">
    <li class="go-back"><a href="#0">Accessories</a></li>
    <li class="see-all"><a href="{{route('adverts.all', $items)}}">Всі в категорії {{ $items->name }}</a></li>
    @foreach($items->children()->orderBy('id')->get() as $item)
        @if($item->hasChildren($item->id))
            <li class="has-children">
                <a href="#0">{{ $item->name }}</a>
                @include('layouts.dropdown.menu', ['items' => $item])
            </li>
        @else
            <li><a href="{{ route('adverts.index', $item) }}">{{ $item->name }}</a></li>
        @endif
    @endforeach
</ul>