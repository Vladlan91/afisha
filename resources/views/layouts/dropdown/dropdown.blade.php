
    <div class="cd-dropdown-wrapper">
        <a class="cd-dropdown-trigger" style="border-radius: 20px; " href="#0"> <span class="lnr lnr-laptop-phone" style="margin-right: 15px; font-weight: 600;"></span>Меню</a>
        <nav class="cd-dropdown">
            <h2>Title</h2>
            <a href="#0" class="cd-close">Close</a>
            <ul class="cd-dropdown-content">
                <li class="has-children">
                    <a href="http://codyhouse.co/?p=748">ОГОЛОШЕННЯ</a>

                    <ul class="cd-secondary-dropdown is-hidden" style="width: 300px !important;">
                        <li class="go-back"><a href="#0">Menu</a></li>
                        <li class="see-all"><a href="{{ route('home') }}"> Всі оголошення</a></li>
                        <li class="has-children" style="width: 100% !important;">
                            <a href="http://codyhouse.co/?p=748">Категорії</a>
                            {!! \Illuminate\Support\Facades\Cache::get('category') !!}
                            {{--<ul class="is-hidden">--}}
                                {{--<li class="go-back"><a href="#0">Clothing</a></li>--}}
                                {{--<li class="see-all"><a href="{{ route('home') }}">Всі категорії</a></li>--}}
                                {{--@foreach($cats as $category)--}}
                                    {{--@if($category->hasChildren($category->id))--}}
                                        {{--<li class="has-children">--}}
                                            {{--<a href="#0">{{ $category->name }}</a>--}}
                                            {{--@include('layouts.dropdown.menu', ['items' => $category])--}}
                                        {{--</li>--}}
                                    {{--@else--}}
                                        {{--<li><a href="{{ route('adverts.all', $category) }}">{{ $category->name }}</a></li>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                            {{--</ul>--}}
                        </li>

                        {{--<li class="has-children">--}}
                            {{--<a href="http://codyhouse.co/?p=748">Міста</a>--}}

                            {{--<ul class="is-hidden">--}}
                                {{--<li class="go-back"><a href="#0">Clothing</a></li>--}}
                                {{--<li class="see-all"><a href="{{ route('home') }}">Всі міста</a></li>--}}
                                {{--@foreach($regs as $region)--}}
                                    {{--@if($region->hasChildren($region->id))--}}
                                        {{--<li class="has-children">--}}
                                            {{--<a href="#0">{{ $region->name }}</a>--}}
                                            {{--@include('layouts.dropdown.menutoo', ['items' => $region])--}}
                                        {{--</li>--}}
                                    {{--@else--}}
                                        {{--<li><a href="{{ route('adverts.index', $region) }}">{{ $region->name }}</a></li>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    </ul> <!-- .cd-secondary-dropdown -->
                </li> <!-- .has-children -->

                <li class="has-children">
                    <a href="http://codyhouse.co/?p=748">НОВИНИ</a>

                    <ul class="cd-dropdown-gallery is-hidden">
                        <li class="go-back"><a href="#0">Menu</a></li>
                        <li class="see-all"><a href="{{ route('news') }}">Всі новини</a></li>
                        @foreach($news as $item)
                        <li>
                            <a class="cd-dropdown-item" href="{{ route('news.show', news_path($item)) }}">
                                <div style="height: 200px;">
                                    <img src="{{asset('app/'. $item->avatar)}}" alt="Product Image">
                                </div>
                                <h3>{{ $item->title }}</h3>
                            </a>
                        </li>
                        @endforeach
                    </ul> <!-- .cd-dropdown-gallery -->
                </li> <!-- .has-children -->

                <li class="has-children">
                    <a href="{{ route('company') }}">БІЗНЕС КАТАЛОГ</a>
                    <ul class="cd-dropdown-icons is-hidden">
                        <li class="go-back"><a href="#0">Menu</a></li>
                        <li class="see-all"><a href="{{ route('company') }}">Всі компанії</a></li>
                        @foreach($business as $item)
                        <li>
                            <img src="{{ asset('images/icons/'.$item->id.'.png') }}" alt="" style="width: 30px; height:30px; position: absolute;">
                            <a class="" href="{{ route('company', $item ) }}">
                                <h3 style="font-size: 12px !important; text-transform: uppercase; margin-left: 15px; margin-top: 15px;">{{ $item->name }}</h3>
                            </a>
                        </li>
                        @endforeach
                    </ul> <!-- .cd-dropdown-icons -->
                </li> <!-- .has-children -->

                <li class="cd-divider">СТОРІНКИ</li>
                @foreach($menuPage as $page)
                    <li><a href="{{ route('page', page_path($page)) }}">{{ $page->menu_title }}</a></li>
                @endforeach
                @guest
                    <li class="phone-size"><a href="{{ route('login') }}">Увійти</a></li>
                    <li class="phone-size"><a href="{{ route('register') }}">Зареєструватись</a></li>
                @else
                    <li class="phone-size">
                        @if(Auth::user()->avatar) <a href="{{route('cabinet.profile.home')}}">
                            <img src="{{asset('app/'. Auth::user()->avatar)}}" alt="{{ Auth::user()->name }}" width="40" height="40" style="border-radius: 40px; width: 40px; height: 40px !important;">
                        @elseif(Auth::user()->isFacebook())
                            <img src="https://graph.facebook.com/v2.9/{{Auth::user()->provider_id}}/picture?type=normal" alt="Facebook Avatar" width="40" height="40" style="border-radius: 40px; width: 40px; height: 40px !important;">
                        @elseif(Auth::user()->isGoogle())
                            <img src="{{Auth::user()->provider_id}}" alt="Google Avatar" width="40" height="40" style="border-radius: 40px; width: 40px; height: 40px !important;">
                        @else
                            <img src="{{asset('images/no-avatar.jpg')}}" alt="" width="40" height="40" style="border-radius: 40px; width: 40px; height: 40px !important;">
                        @endif
                            {{ Auth::user()->name }}</a>
                    </li>
                    <li class="phone-size">
                        @if(Auth::user()->isAdmin())
                            <a href="{{ route('admin.home') }}">
                                Адмін-панель
                            </a>
                        @endif
                    </li>
                    <li class="phone-size"><a href="{{ route('cabinet.home') }}">Кабінет</a>
                    </li>
                    <li class="phone-size"><a href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">Вийти</a></li>
                    <li class="phone-size"><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form></li>
                @endguest
            </ul> <!-- .cd-dropdown-content -->
        </nav> <!-- .cd-dropdown -->
    </div> <!-- .cd-dropdown-wrapper -->