<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="{{asset('images/lo.png')}}" type="image/png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @yield('meta')
    <link rel="stylesheet" href="{{ asset('drop/css/reset.css') }}"> <!-- CSS reset -->
    <link rel="stylesheet" href="{{ asset('drop/css/style.css') }}"> <!-- CSS reset -->
    <link rel="stylesheet" href="{{ asset('drop/js/modernizr.js') }}"> <!-- CSS reset -->
    <link href="{{ asset('css/post.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/icon-font.min.css') }}" type='text/css' />
    <link rel="stylesheet" href="{{ asset('fonts/font-awesome/css/font-awesome.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href="{{ asset('css/Footer-with-button-logo.css') }}" type="text/css" rel="stylesheet">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('drop/js/jquery-2.1.1.js') }}"></script>

</head>
<body id="app">

    <header class="header">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    @include('layouts.dropdown.dropdown')
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <p style="float: left; padding-left: 30px; margin-top: 15px;">
                            <span class="lnr lnr-phone" style="color: red">
                            </span>0-800-500650
                        </p>
                        &nbsp;<p style="float: left; padding-left: 30px; margin-top: 15px;">
                            <span class="lnr lnr-envelope" style="color: red">
                            </span>afisha.if.ua@gmail.com
                        </p>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li style="padding-top: 14px; padding-bottom: 14px; "><span class="lnr lnr-user" style="color: red;"></span></li>
                            <li><a href="{{ route('login') }}">Увійти</a></li>
                            <li style="padding-top: 14px; padding-bottom: 14px; color: darkgrey">aбо</li>
                            <li><a href="{{ route('register') }}">Зареєструватись</a></li>
                        @else
                            @if(Auth::user()->avatar)
                                <img src="{{asset('app/'. Auth::user()->avatar)}}" alt="{{ Auth::user()->name }}" width="50" height="50" style="border-radius: 40px; width: 50px; height: 50px !important;">
                            @elseif(Auth::user()->isFacebook())
                                <img src="https://graph.facebook.com/v2.9/{{Auth::user()->provider_id}}/picture?type=normal" alt="Facebook Avatar" width="50" height="50" style="border-radius: 40px; width: 50px; height: 50px !important;">
                            @elseif(Auth::user()->isGoogle())
                                <img src="{{Auth::user()->provider_id}}" alt="Google Avatar" width="50" height="50" style="border-radius: 40px; width: 50px; height: 50px !important;">
                            @else
                                <img src="{{asset('images/no-avatar.jpg')}}" alt="" width="50" height="50" style="border-radius: 40px; width: 50px; height: 50px !important;">
                            @endif
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        @if(Auth::user()->isAdmin())
                                        <a href="{{ route('admin.home') }}">
                                            Адмін-панель
                                        </a>
                                        @endif
                                        <a href="{{ route('cabinet.home') }}">
                                            Кабінет
                                        </a>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Вийти
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="ff" style="position: absolute; width: 100%; z-index: 1000; left: 76%; margin-top: 120px;">
        @include('layouts.partials.flash')
    </div>
    <div class="ff" style="position: absolute; width: 100%; z-index: 1000; left: 76%; margin-top: 120px;">
        @include('flash::message')
    </div>
@include('layouts.header_block._headerblock')
    <main class="app-content py-3">
        <div class="container">
            @section('breadcrumbs', Breadcrumbs::render())
            @yield('breadcrumbs')
            <div style="background-color: white; padding: 20px;     box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);">
                @yield('content')
            </div>


        </div>
    </main>

    <div id="circularMenu" class="circular-menu">

        <a class="floating-btn icon-plus" onclick="document.getElementById('circularMenu').classList.toggle('active');">
        </a>

        <menu class="items-wrapper">
            <a href="#" class="menu-item icon-facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" class="menu-item icon-twitter"></a>
            <a href="#" class="menu-item icon-google-plus"></a>
            <a href="#" class="menu-item icon-linkedin"></a>
        </menu>

    </div>


    <div id="circularMenu1" class="circular-menu circular-menu-left ">

        <a class="floating-btn" style="text-decoration: none;" onclick="document.getElementById('circularMenu1').classList.toggle('active');">
            <span class="lnr lnr-menu-circle" style="font-size: 20px; font-weight: 900; "></span>
        </a>

        <menu class="items-wrapper">
            <a href="{{route('home')}}" class="menu-item fa fa-home"><span class="lnr lnr-home" style="font-size: 20px; font-weight: 900"></span></a>
            <a href="{{route('company')}}" class="menu-item fa fa-user"><span class="lnr lnr-users" style="font-size: 20px; font-weight: 900"></span></a>
            <a href="{{'cabinet'}}" class="menu-item fa fa-pie-chart"><span class="lnr lnr-tag" style="font-size: 20px; font-weight: 900"></span></a>
            <a href="{{route('news')}}" class="menu-item fa fa-cog"><span class="lnr lnr-earth" style="font-size: 20px; font-weight: 900"></span></a>
        </menu>

    </div>
    @yield('script')
   @include('layouts.footer._footer')
    <script src="{{ asset('js/extention/choices.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        const choices = new Choices('[data-trigger]',
            {
                searchEnabled: false
            });

    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('drop/js/jquery.menu-aim.js') }}"></script>
    <script src="{{ asset('drop/js/main.js') }}"></script>

</body>
</html>
