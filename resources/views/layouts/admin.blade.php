<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('images/lo.png')}}" type="image/png">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Афіша Прикарпаття') }}</title>
    <link href="{{ asset('css/app.css') }}" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/style.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/icon-font.min.css') }}" type='text/css' />
    <script src="{{ asset('js/Chart.js') }}"></script>
    <link href="{{ asset('css/animate.cs') }}" rel="stylesheet" type="text/css" media="all">
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script src="{{ asset('js/classie.js') }}"></script>
    <script src="{{ asset('js/uisearch.js') }}"></script>
    <script>
        new WOW().init();
    </script>
    <link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
    <script src="{{ asset('js/jquery-1.10.2.min.js') }}"></script>

</head>
{{--<body class="sticky-header"  onload="initMap()"  id="app">--}}
<body class="sticky-header">
<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <h1><a href="{{ url('/') }}">Afi <span>Sha</span></a></h1>
        </div>
        <div class="logo-icon text-center">
            <a href="index.html"><i class="lnr lnr-home"></i> </a>
        </div>

        <div class="left-side-inner">

            <ul class="nav nav-pills nav-stacked custom-nav mt-2">
                <li class="nav-item"><a href="{{ route('admin.home') }}"><i class="lnr lnr-power-switch"></i><span>Статистика</span></a></li>
                {{--<li class="menu_tpl-list">--}}
                    {{--<a href="#"><i class="lnr lnr-cog"></i>--}}
                        {{--<span>Components</span></a>--}}
                    {{--<ul class="sub-menu_tpl-list">--}}
                        {{--<li><a href="?">Grids</a> </li>--}}
                        {{--<li><a href="?">Widgets</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li class="nav-item "><a href="{{ route('admin.regions.index') }}"><i class="lnr lnr-apartment"></i> <span>Міста</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.users.index') }}"><i class="lnr lnr-users"></i> <span>Користувачі</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.adverts.adverts.index') }}"><i class="lnr lnr-bullhorn"></i> <span>Оголошення</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.adverts.categories.index') }}"><i class="lnr lnr-tag"></i> <span>Категорії</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.business.index') }}"><i class="lnr lnr-layers"></i> <span>Бізнес каталог</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.company.index') }}"><i class="lnr lnr-layers"></i> <span>Список компаній</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.banners.index') }}"><i class="lnr lnr-select"></i> <span>Банери</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.afisha.index') }}"><i class="lnr lnr-select"></i> <span>Банери Афіша</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.pages.index') }}"><i class="lnr lnr-file-empty"></i> <span>Cтатичні сторінки</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.news.index') }}"><i class="lnr lnr-eye"></i> <span>Новини</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.newscat.index') }}"><i class="lnr lnr-file-empty"></i> <span>Категорії новин</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.tickets.index') }}"><i class="lnr lnr-phone-handset"></i> <span>Help Desk</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.messages.index') }}"><i class="lnr lnr-bubble"></i> <span>Повідомлення</span></a></li>
                <li class="nav-item "><a href="{{ route('admin.order.index') }}"><i class="lnr lnr-history"></i> <span>Проплати</span></a></li>
            </ul>
            <!--sidebar nav end-->
        </div>
    </div>

    <div class="main-content">
        <!-- header-starts -->
        <div class="header-section">

            <a class="toggle-btn  menu-collapsed"><i class="fa fa-bars"></i></a>

            <div class="menu-right" >
                <div class="user-panel-top">
                    <div class="profile_details_left">
                        <ul class="nofitications-dropdown">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge">3</span></a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="notification_header">
                                            <h3>You have 3 new messages</h3>
                                        </div>
                                    </li>
                                    <li><a href="#">
                                            <div class="user_img"><img src="images/1.png" alt=""></div>
                                            <div class="notification_desc">
                                                <p>Lorem ipsum dolor sit amet</p>
                                                <p><span>1 hour ago</span></p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </a></li>
                                    <li class="odd"><a href="#">
                                            <div class="user_img"><img src="images/1.png" alt=""></div>
                                            <div class="notification_desc">
                                                <p>Lorem ipsum dolor sit amet </p>
                                                <p><span>1 hour ago</span></p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="user_img"><img src="images/1.png" alt=""></div>
                                            <div class="notification_desc">
                                                <p>Lorem ipsum dolor sit amet </p>
                                                <p><span>1 hour ago</span></p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </a></li>
                                    <li>
                                        <div class="notification_bottom">
                                            <a href="#">See all messages</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="notification_header">
                                            <h3>You have 3 new notification</h3>
                                        </div>
                                    </li>
                                    <li><a href="#">
                                            <div class="user_img"><img src="images/1.png" alt=""></div>
                                            <div class="notification_desc">
                                                <p>Lorem ipsum dolor sit amet</p>
                                                <p><span>1 hour ago</span></p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </a></li>
                                    <li class="odd"><a href="#">
                                            <div class="user_img"><img src="images/1.png" alt=""></div>
                                            <div class="notification_desc">
                                                <p>Lorem ipsum dolor sit amet </p>
                                                <p><span>1 hour ago</span></p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="user_img"><img src="images/1.png" alt=""></div>
                                            <div class="notification_desc">
                                                <p>Lorem ipsum dolor sit amet </p>
                                                <p><span>1 hour ago</span></p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </a></li>
                                    <li>
                                        <div class="notification_bottom">
                                            <a href="#">See all notification</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">22</span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="notification_header">
                                            <h3>You have 8 pending task</h3>
                                        </div>
                                    </li>
                                    <li><a href="#">
                                            <div class="task-info">
                                                <span class="task-desc">Database update</span><span class="percentage">40%</span>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="progress progress-striped active">
                                                <div class="bar yellow" style="width:40%;"></div>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="task-info">
                                                <span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="progress progress-striped active">
                                                <div class="bar green" style="width:90%;"></div>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="task-info">
                                                <span class="task-desc">Mobile App</span><span class="percentage">33%</span>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="progress progress-striped active">
                                                <div class="bar red" style="width: 33%;"></div>
                                            </div>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="task-info">
                                                <span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="progress progress-striped active">
                                                <div class="bar  blue" style="width: 80%;"></div>
                                            </div>
                                        </a></li>
                                    <li>
                                        <div class="notification_bottom">
                                            <a href="#">See all pending task</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <div class="clearfix"></div>
                        </ul>
                    </div>
                    <div class="profile_details">
                        <ul>
                            <li class="dropdown profile_details_drop">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <div class="profile_img">
                                        @if(Auth::user()->avatar)
                                            <img src="{{asset('app/'. Auth::user()->avatar)}}" alt="{{ Auth::user()->name }}" width="50" height="50" style="border-radius: 40px; width: 50px; height: 50px !important;">
                                        @elseif(Auth::user()->isFacebook())
                                            <img src="https://graph.facebook.com/v2.9/{{Auth::user()->provider_id}}/picture?type=normal" alt="Facebook Avatar" width="50" height="50" style="border-radius: 40px; width: 50px; height: 50px !important;">
                                        @elseif(Auth::user()->isGoogle())
                                            <img src="{{Auth::user()->provider_id}}" alt="Google Avatar" width="50" height="50" style="border-radius: 40px; width: 50px; height: 50px !important;">
                                        @else
                                            <img src="{{asset('images/no-avatar.jpg')}}" alt="" width="50" height="50" style="border-radius: 40px; width: 50px; height: 50px !important;">
                                        @endif
                                        <div class="user-name">
                                            <p>{{ Auth::user()->name }}<span>{{ Auth::user()->role }}</span></p>
                                        </div>
                                        {{--<i class="lnr lnr-chevron-down"></i>--}}
                                        {{--<i class="lnr lnr-chevron-up"></i>--}}
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                                <ul class="dropdown-menu drp-mnu">
                                    <li> <a href="{{ route('admin.home') }}"><i class="fa fa-cog"></i> Admin панель</a> </li>
                                    <li> <a href="{{ route('cabinet.home') }}"><i class="fa fa-user"></i>Кабінет</a> </li>
                                    <li> <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();"><i class="fa fa-refresh"></i>Вихід</a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form> </li>
                                </ul>
                            </li>
                            <div class="clearfix"> </div>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!--notification menu_tpl end -->
        </div>
        <!-- //header-ends -->

        <main class="app-content py-3">
            <div class="container">
                <div class="ff" style="position: absolute; width: 100%; z-index: 1000; left: 76%;">
                    @include('flash::message')
                </div>
                @section('breadcrumbs', Breadcrumbs::render())
                @yield('breadcrumbs')
            <div class="ff" style="position: absolute; width: 100%; z-index: 1000; left: 76%;">
                @include('layouts.partials.flash')
            </div>
                @yield('content')
            </div>
        </main>


        <!--body wrapper end-->
    </div>
    <!--footer section start-->
    <footer>
        <div class="container">
            <div class="border-top pt-3">
                <div class="copyright-w3layouts py-xl-3 py-2 mt-xl-5 mt-4 text-center">
                    <p>© 2018 Afisha . All Rights Reserved | Design by
                        <a href=""> ITM </a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!--footer section end-->

    <!-- main content end-->
</section>
<script>
    var path = '{{ asset('') }}';
</script>
@yield('js')
<script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
</body>
</html>


