<div class="container" style="margin-bottom: 10px;">
    <div class="row" style="padding-top: 20px;">
        <div class="col-md-3 col-lx-2 col-sm-3 col-" style="padding-bottom: 30px; max-width: 400px; display: block; margin: 0 auto;">
            <a href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" alt="" style=" margin-right: 30px;"></a>
        </div>
            @include('layouts.dropdown.headerbanner')
        <div class="col-md-2 col-lx-12 col-sm-12" style="font-size: 16px; margin-top: 20px;">
            <a href="{{ route('cabinet.adverts.create') }}" style="text-align: center; color: #333e48">
            <div class="miniCartWrap" style="display:block; margin: 0 auto">
                <div class="mini-maincart" style="font-size: 13px; color: #333333">
                    <div class="cartSummary">
                        <div class="icon-plus" style="margin: auto;display: block; font-size: 20px">
                        </div>
                    </div>
                    Додати оголошення
                </div>
            </div>
            </a>
        </div>
    </div>
</div>