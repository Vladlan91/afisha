<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="{{asset('images/lo.png')}}" type="image/png">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('meta')
    <link href="{{ asset('css/post.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('css/mmstyle.css') }}" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/icon-font.min.css') }}" type='text/css' />
    <link rel="stylesheet" href="{{ asset('fonts/font-awesome/css/font-awesome.css') }}" />
    <link href="{{ asset('css/Footer-with-button-logo.css') }}" type="text/css" rel="stylesheet">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</head>
<body id="app">
<div class="ff" style="position: absolute; width: 100%; z-index: 1000; left: 61%; margin-top: 119px;">
    @include('layouts.partials.flash')
</div>
<div class="ff" style="position: absolute; width: 100%; z-index: 1000; left: 61%; margin-top: 119px;">
    @include('flash::message')
</div>
<div class="row">
    <div class="col-md-2 col-sm-2 col-lg-2 col-xs-2">
        <div class="page-container">
            <div class="sidebar-menu">
                <header class="logo1">

                </header>
                <div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
                <div class="menu">
                    <ul id="menu">
                        <li class="nav-item"><a href="{{ route('cabinet.home') }}"><i class="lnr lnr-pencil"></i> <span>Кабінет</span></a></li>
                        <li class="nav-item"><a href="{{ route('cabinet.favorites.index') }}"><i class="lnr lnr-pencil"></i> <span>Вподобані</span></a></li>
                        <li class="nav-item" id="menu-academico" ><a href="{{ route('cabinet.adverts.index') }}"><i class="lnr lnr-book"></i> <span>Оголошення</span></a></li>
                        <li class="nav-item"><a href="{{ route('cabinet.messages.index') }}"><i class="lnr lnr-envelope"></i> <span>Повідомлення</span></a></li>
                        <li class="nav-item"><a href="{{ route('cabinet.banners.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Мої банери</span></a></li>
                        <li class="nav-item"><a href="{{ route('cabinet.company.index') }}"><i class="lnr lnr-chart-bars"></i> <span>Мої компанії</span></a></li>
                        <li id="menu-academico" ><a href="{{ route('cabinet.tickets.index') }}"><i class="lnr lnr-layers"></i> <span>Служба підтримки</span> <span class="fa fa-angle-right" style="float: right"></span></a></li>
                        <li><a href="{{ route('cabinet.profile.home') }}"><i class="lnr lnr-chart-bars"></i> <span>Налащтування</span> <span class="fa fa-angle-right" style="float: right"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <script>
            var toggle = true;

            $(".sidebar-icon").click(function() {
                if (toggle)
                {
                    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
                    $("#menu_tpl span").css({"position":"absolute"});
                }
                else
                {
                    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
                    setTimeout(function() {
                        $("#menu_tpl span").css({"position":"relative"});
                    }, 400);
                }

                toggle = !toggle;
            });
        </script>
    </div>
    <div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">
        @include('layouts.header_block._headerblock')
        <main class="app-content py-3">
            <div class="container">
                @section('breadcrumbs', Breadcrumbs::render())
                @yield('breadcrumbs')
                <div style="background-color: white; padding: 20px;     box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);">
                    @yield('content')
                </div>
            </div>
        </main>
        @include('layouts.footer._footer')
        <div id="circularMenu" class="circular-menu">

            <a class="floating-btn icon-plus" onclick="document.getElementById('circularMenu').classList.toggle('active');">
            </a>

            <menu class="items-wrapper">
                <a href="#" class="menu-item icon-facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="menu-item icon-twitter"></a>
                <a href="#" class="menu-item icon-google-plus"></a>
                <a href="#" class="menu-item icon-linkedin"></a>
            </menu>

        </div>
        <div id="circularMenu1" class="circular-menu circular-menu-left ">

            <a class="floating-btn" style="text-decoration: none;" onclick="document.getElementById('circularMenu1').classList.toggle('active');">
                <span class="lnr lnr-menu-circle" style="font-size: 20px; font-weight: 900; "></span>
            </a>
            <menu class="items-wrapper">
                <a href="#" class="menu-item fa fa-home"><span class="lnr lnr-home" style="font-size: 20px; font-weight: 900"></span></a>
                <a href="#" class="menu-item fa fa-user"><span class="lnr lnr-users" style="font-size: 20px; font-weight: 900"></span></a>
                <a href="#" class="menu-item fa fa-pie-chart"><span class="lnr lnr-tag" style="font-size: 20px; font-weight: 900"></span></a>
                <a href="#" class="menu-item fa fa-cog"><span class="lnr lnr-earth" style="font-size: 20px; font-weight: 900"></span></a>
            </menu>
        </div>
    </div>
</div>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/extention/choices.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@yield('script')
<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
