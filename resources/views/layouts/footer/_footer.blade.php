<footer id="myFooter">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h2 class="logo"><a href="#"> <img src="{{ asset('images/logo.png') }}" alt="" style="width: 250px;"></a></h2>
            </div>
            <div class="col-sm-2">
                <h5>Інформація</h5>
                <ul>
                    <li><a href="#">Порадник</a></li>
                    <li><a href="#">Вартість реклами</a></li>
                    <li><a href="#">Довідка</a></li>
                    <li><a href="#">Правила розміщення</a></li>
                    <li><a href="#">Користувацька угоду</a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <h5>Контакти</h5>
                <ul>
                    <li>76000, Україна, м. Івано-Франківськ, <br>
                        вул. Незалежності, 4, БЦ "Київ", 4 поверх</li>
                </ul>
            </div>
            <div class="col-sm-2">
                <h5>Tелефон</h5>
                <ul>
                    <li><a href="#">+38 (095) 751-43-89</a></li>
                    <li><a href="#">+38 (0342) 72-59-59</a></li>
                    <li><a href="#">+38 (098) 877-59-59</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <div class="social-networks">
                    <span class="lnr lnr-users" style="font-size: 65px; color: #fd4235;"></span>
                </div>
                <button type="button" class="btn btn-default">Зв'язатись</button>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <p>© 2016 - 2018 Афіша Прикарпаття. Всі права застережено. </p>
    </div>
</footer>