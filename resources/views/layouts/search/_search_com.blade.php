<div class="s131">
    <form action="{{route('company.search')}}" method="POST">
        {{ csrf_field() }}
        <div class="inner-form">
            <div class="input-field first-wrap">
                <input name="title" id="title"  type="text" placeholder="Пошук по тексту" />
            </div>
            <div class="input-field second-wrap">
                <div class="input-select">
                    <select data-trigger=""  id="category"  name="category">
                        @if($categories)
                            <option  value="{{ null }}">Категорії</option>
                            @foreach($categories as $item )
                                <option  value="{{ $item->id }}"{{ $item->id === $category->id ? 'selected': '' }}>{{ $item->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="input-field second-wrap">
                <div class="input-select">
                    <select data-trigger=""  id="region"  name="region">
                        @if($regions)
                            <option value="{{ null }}">Міста</option>
                            @foreach($regions as $region )
                                <option value="{{ $region->id }}">{{ $region->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="input-field third-wrap">
                <button class="btn-search" type="">Пошук</button>
            </div>
        </div>
</div>
</form>
</div>