@extends('layouts.cabinet')

@section('content')
    {{--@include('cabinet.adverts._nav')--}}
        <div class="adverts-list" style="margin: 10px;">
            <ul class="_1rXe6">
                @foreach($adverts as $advert)
                    <li>
                        <a href="{{ route('adverts.show', $advert) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                            @if($advert->type === 'A')
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #aaaaaa; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>А</h5>
                                </div>
                            @elseif($advert->type === 'Б')
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #45c639; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Б</h5>
                                </div>
                            @elseif($advert->type === 'В')
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>В</h5>
                                </div>
                            @elseif($advert->type === 'Г')
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #219fde; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                                </div>
                            @elseif($advert->type === 'Д')
                                <div style="position: absolute; left: 0;top: -12px; z-index:1;">
                                    <h5 style="background-color: #fed700; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                                </div>
                            @else
                                <div style="position: absolute; left: 0;top: -12px; z-index: 1;">
                                    <h5 style="background-color: #ea1b25; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Е</h5>
                                </div>
                            @endif
                            <div class="item_image">
                                <span class="item_imagePic">
                                        <img src="{{  asset('/app/'. $advert->avatar ) }}" alt="Appartement 4 pièces 101 m²" style="height: 150px;">
                                </span>
                                <span class="item_imageNumber">
                        	<i class="icon-camera icon-2x nomargin"></i>
                        	<span>3</span>
                      	</span>
                            </div>
                            <div title="" class="saveAd" data-savead-id="1557273607">
                                @if($advert->isCompany())
                                    <img src="{{asset('app/'. $advert->company->logo)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                @else
                                    @if($advert->user->avatar)
                                        <img src="{{asset('app/'. $advert->user->avatar)}}" alt="{{ $advert->user->name }}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                    @else
                                        <img src="{{asset('images/no-avatar.jpg')}}" width="35" height="35" style="border-radius: 40px; float: right; z-index: -100">
                                    @endif
                                @endif
                            </div>
                            <section class="item_infos">
                                <h2 class="item_title">
                                    {{ \Illuminate\Support\Str::limit($advert->content,30) }}
                                </h2>
                                <h3 class="item_price" style="font-size: 12px !important; font-weight: 900 !important;">
                                    {{ $advert->price }}&nbsp;UAH
                                </h3>
                                <aside>
                                    <p class="item_supp"  style="font-size: 12px !important; font-weight: 900 !important;">
                                        {{ $advert->category->name }}
                                        /
                                        {{ $advert->region ?  $advert->region->name : 'Всі міста'}}
                                    </p>
                                    <p class="item_supp item_suppDate" style="font-size: 12px !important; font-weight: 900 !important;">
                                        Дата завершення публікації: {{ $advert->expires_at }}
                                    </p>
                                </aside>
                            </section>
                            @if($advert->isActive())
                                <div style="position: absolute; right: 0;bottom: -12px; z-index:1;">
                                    <h5 style="background-color: #4CAF50; padding: 10px; color: white; border-top-left-radius: 25px;: 30px;"><span style="margin-right: 10px;">Cтатус:</span>Активний</h5>
                                </div>
                            @elseif($advert->isDraft())
                                <div style="position: absolute; right: 0;bottom: -12px; z-index:1;">
                                    <h5 style="background-color: #aaaaaa; padding: 10px; color: white; border-top-left-radius: 25px;: 30px;"><span style="margin-right: 10px;">Cтатус:</span>Чорновик</h5>
                                </div>
                            @elseif($advert->isClosed())
                                <div style="position: absolute; right: 0;bottom: -12px; z-index:1;">
                                    <h5 style="background-color: #de5749; padding: 10px; color: white; border-top-left-radius: 25px;: 30px;"><span style="margin-right: 10px;">Cтатус:</span>Закритий</h5>
                                </div>
                            @elseif($advert->isOnModeration())
                                <div style="position: absolute; right: 0;bottom: -12px; z-index:1;">
                                    <h5 style="background-color: #cac80a; padding: 10px; color: white; border-top-left-radius: 25px;: 30px;"><span style="margin-right: 10px;">Cтатус:</span>На модерації</h5>
                                </div>
                            @endif
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        {{ $adverts->links() }}

@endsection
