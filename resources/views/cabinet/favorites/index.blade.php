@extends('layouts.cabinet')

@section('content')
    <div class="row">
        @foreach($adverts as $item)
            <div class="col-sm-6 col-md-4">
                <ul class="">
                    <li itemscope="" itemtype="http://schema.org/Offer" class="_3eDdy">
                        <a title="{{ $item->title }}" class="_2fKRW" data-qa-id="aditem_container" href="{{ route('adverts.show', $item) }}">
                            <div class="_2-jsN"><div class="LazyLoad is-visible">
                                    <div>
                                        <div class="_6ntGx" style="background-image: url(&quot;https://img1.leboncoin.fr/ad-image/fd96e60b12d1c41565d4c3ffd45db7e4a01c2bb6.jpg&quot;);"></div>
                                        <img src="{{  asset('/app/'. $item->avatar ) }}" itemprop="image" content="{{  asset('/app/'. $item->avatar ) }}" alt="subject"></div>
                                </div>
                                <span class="_1sbqp">
                                <div class="_3jAsY">
                                    <div class="_3KcVT">
                                        <span class="_1vK7W" name="spotlight">
                                            <svg viewBox="0 0 24 24" data-name="Calque 1">
                                                <path d="M18.43 0H5.57A2.63 2.63 0 0 0 3 2.67V24l9-4 9 4V2.67A2.63 2.63 0 0 0 18.43 0z"></path>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="_1MBDf">Оголошення</div>
                                </div>
                            </span>
                            </div>
                            <div class="_3beID">
                                <section class="irAof">
                                    <p class="_3ZfBw">
                                        <span itemprop="name" data-qa-id="aditem_title">{{ $item->title }}</span>
                                    </p>
                                    <p class="_1s5WJ" itemprop="availableAtOrFrom" data-qa-id="aditem_location"><!-- react-text: 2239 -->{{ \Illuminate\Support\Str::limit($item->content,30) }}<!-- /react-text --></p>
                                    <div class="CeFtS" itemprop="priceSpecification" itemscope="" itemtype="http://schema.org/PriceSpecification" data-qa-id="aditem_price">
                                        <meta itemprop="priceCurrency" content="UAH">
                                        <span class="_1_bNq">
                                        <span itemprop="price">{{ $item->price }}</span><!-- react-text: 2244 -->&nbsp;UAH<!-- /react-text --></span>
                                    </div>
                                </section>
                                <div class="_3A9T7"></div>
                            </div>
                        </a>
                        <div class="_3Zm0x" data-qa-id="listitem_save_ad">
                            <div>
                                <div class="_3C4to">
                                    <div class="" data-tip="Sauvegarder l'annonce" data-place="left" data-for="toggleSavedFeaturedAd_1554842308" currentitem="false">
                                    <span class="_1vK7W" name="heartoutline">
                                         <form  method="POST" action="{{ route('cabinet.favorites.remove', $item) }}">
                                             {{ csrf_field() }}
                                             {{method_field('DELETE')}}
                                             <button class="btn btn-sm btn-danger">X</button>
                                        </form>
                                    </span>
                                    </div>
                                    <div class="sc-fjdhpX bpzbGb">
                                        <div class="__react_component_tooltip place-top type-dark " id="toggleSavedFeaturedAd_1554842308" data-id="tooltip"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </li>
                </ul>
            </div>
        @endforeach
    </div>
            {{ $adverts->links() }}
@endsection

