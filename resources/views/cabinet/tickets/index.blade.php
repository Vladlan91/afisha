@extends('layouts.cabinet')

@section('content')
    {{--@include('cabinet.tickets._nav')--}}
    <p><a href="{{ route('cabinet.tickets.create') }}"  class="btn btn-primary" style="color: white !important; border-radius: 20px;">Написати в службу підтримки</a></p>
    <table class="table table-striped" style="margin-top: 20px;">
        <thead>
        <tr>
            <th>ID</th>
            <th>Створено</th>
            <th>Змінено</th>
            <th>Тема листа</th>
            <th>Статус</th>
        </tr>
        </thead>

        <tbody>
        @foreach($tickets as $ticket)
            <tr>
                <td>{{ $ticket->id }}</td>
                <td>{{ $ticket->created_at }}</td>
                <td>{{ $ticket->updated_at }}</td>
                <td><a href="{{ route('cabinet.tickets.show', $ticket) }}">{{ $ticket->subject }}</a></td>
                @if($ticket->isOpen())
                    <td><span class="info">Відкритий</span></td>
                @elseif($ticket->isApproved())
                    <td><span class="primary">Активований</span></td>
                @elseif($ticket->isClosed())
                    <td><span class="errorss">Закритий</span></td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $tickets->links() }}
@endsection