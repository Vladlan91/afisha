@extends('layouts.cabinet')

@section('content')
    {{--@include('cabinet.tickets._nav')--}}
    <form action="{{ route('cabinet.tickets.store', Auth::id()) }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="subject">Тема листа</label>
            <input id="subject" name="subject" style="height: 50px;" class="form-control{{ $errors->has('subject') ? 'is-invalid': '' }}"
                   value="{{ old('subject') }}" required>
            @if( $errors->has('subject'))
                <span class="invalid-feedback"><stron>{{ $errors->first('subject') }}</stron></span>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Опис проблеми</label>
            <textarea name="content" id="content" cols="30" rows="4" class="form-control{{ $errors->has('content') ? 'is-invalid' : '' }}" required></textarea>
            @if($errors->has('content'))
                <span class="invalid-feedback"><strong>{{ $errors->first('content') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" style="color: white !important; border-radius: 20px;">Надіслати</button>
        </div>
    </form>

@endsection