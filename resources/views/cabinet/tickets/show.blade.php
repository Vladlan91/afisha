@extends('layouts.cabinet')

@section('content')
    {{--@include('cabinet.tickets._nav')--}}
    <p><div class="d-flex flex-row mb-3">
        @if($ticket->canBeRemoved())
            <form  method="POST" action="{{ route('cabinet.tickets.destroy', $ticket) }}">
                {{ csrf_field() }}
                {{method_field('DELETE')}}
                <button class="btn btn-success">Видалити</button>
            </form>
        @endif

    </div></p><br>
    <br>
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered table-striped pt-2">
                <tbody>
                <tr>
                    <th>ІD</th> <td>{{$ticket->id}}</td>
                </tr>
                <tr>
                    <th>Створено</th> <td>{{ $ticket->created_at }}</td>
                </tr>
                <tr>
                    <th>Змінено</th> <td>{{ $ticket->updated_at }}</td>
                </tr>
                <tr>
                    <th>Статус</th>
                    @if($ticket->isOpen())
                        <td><span class="info">Відкритий</span></td>
                    @elseif($ticket->isApproved())
                        <td><span class="primary">Активований</span></td>
                    @elseif($ticket->isClosed())
                        <td><span class="errorss">Закритий</span></td>
                        @endif
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Дата</th>
                    <th>Користувач</th>
                    <th>Тема</th>
                    <th>Статус</th>
                </tr>
                </thead>

                <tbody>
                @foreach($ticket->statuses()->orderBy('id')->with('user')->get() as $status)
                    <tr>
                        <td>{{ $status->created_at }}</td>
                        <td>{{ $status->user->name }}</td>
                        <td><a href="{{ route('cabinet.tickets.show', $ticket) }}">{{ $ticket->subject }}</a></td>
                        @if($status->isOpen())
                            <td><span class="info">Відкритий</span></td>
                        @elseif($status->isApproved())
                            <td><span class="primary">Активований</span></td>
                        @elseif($status->isClosed())
                            <td><span class="errorss">Закритий</span></td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="messaging">
        <div class="inbox_msg">
            <div class="inbox_people" style="width: 100% !important;">
                <div class="headind_srch">
                    <div class="recent_heading">
                        <h4> {{ $ticket->subject }}</h4>
                        <p>  {!! nl2br(e($ticket->content)) !!}</p>
                    </div>
                </div>
                {{--active_chat--}}
                <div class="inbox_chat">
                    @foreach($ticket->messages()->orderBy('id')->with('user')->get() as $message)
                    <div class="chat_list">
                    <div class="chat_people">
                        <div class="chat_img">
                            @if($message->user->avatar)
                                <img src="{{asset('/app/'. $message->user->avatar)}}" alt="{{ $message->user->name }}" width="45" height="45" style="border-radius: 40px;">
                            @else
                                <img src="{{asset('images/no-avatar.jpg')}}" width="45" height="45" style="border-radius: 40px;">
                            @endif
                        </div>
                        <div class="chat_ib">
                            <h5>{{ $message->user->name }}<span class="chat_date">{{ $message->created_at }}</span></h5>
                                <p> {!! nl2br(e($message->message)) !!}</p>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </div>
    </div>
    @if($ticket->allowsMessage())
        <form action="{{ route('cabinet.tickets.message', $ticket) }}" method="POST">
            {{ csrf_field() }}
        <div class="form-group">
            <textarea name="message" id="message" cols="30" rows="4" class="form-control{{ $errors->has('message') ? 'is-invalid' : '' }}" required>{{ old('message') }}</textarea>
        @if($errors->has('message'))
            <span class="invalid-feedback"><strong>{{ $errors->first('message') }}</strong></span>
        @endif
        </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary" style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px; border: none;">Відправити</button>
            </div>
        </form>
    @endif
@endsection