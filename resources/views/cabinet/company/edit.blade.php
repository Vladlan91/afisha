@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #999999">
                <h1  style="font-size: 15px">Зміст компанії</h1>
            </div>
            <div class="col-md-12" style="background-color: #f5f5f5; padding: 15px; border-radius: 20px;">
                <form method="POST" action="{{ route('cabinet.company.edit', $company)}}" enctype="multipart/form-data" novalidate>
                    {{ csrf_field() }}
                    {{method_field('PUT')}}
                    <div class="">
                        <div class="">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="title">Назва</label>
                                        <input id="title" name="title" class="form-control{{ $errors->has('title') ? 'is-invalid' : '' }}" value="{{ old('title', $company->title) }}" required>
                                        @if($errors->has('title'))
                                            <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone">Телефон</label>
                                        <input id="phone" name="phone" class="form-control{{ $errors->has('phone') ? 'is-invalid' : '' }}" value="{{ old('phone', $company->phone) }}" required>
                                        @if($errors->has('phone'))
                                            <span class="invalid-feedback"><strong>{{ $errors->first('phone') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="site">Сайт</label>
                                        <input id="site" name="site" class="form-control{{ $errors->has('site') ? 'is-invalid' : '' }}" value="{{ old('site', $company->site) }}" required>
                                        @if($errors->has('site'))
                                            <span class="invalid-feedback"><strong>{{ $errors->first('site') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Емейл</label>
                                        <input id="email" name="email" class="form-control{{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email', $company->email) }}" required>
                                        @if($errors->has('email'))
                                            <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12" >
                                    <div class="row">
                                        <div class="col-md-10 col-sm-10 col-xs-9">
                                            <div class="form-group">
                                                <label for="address">Адреса</label>
                                                <input id="address" name="address" class="form-control{{ $errors->has('address') ? 'is-invalid' : '' }}" value="{{ old('address', $company->address) }}" required>
                                                @if($errors->has('address'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('address') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-10 col-xs-3">
                                            <label for="address" style="color: #f5f5f5;">Авто</label>
                                            <span class="location-button" data-target="#address"><img style="height: 30px; width: 30px; cursor: pointer;"
                                                                                                      src="{{asset('images/map-location.png')}}" alt=""></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="content">Опис</label>
                                        <textarea id="content" name="content" rows="3" class="form-control{{ $errors->has('content') ? 'is-invalid' : '' }}"  required>{{ old('content', $company->content) }}</textarea>
                                        @if($errors->has('content'))
                                            <span class="invalid-feedback"><strong>{{ $errors->first('content') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="location">Контакти</label>
                                        <textarea id="location" name="location" rows="3" class="form-control{{ $errors->has('location') ? 'is-invalid' : '' }}" required>{{ old('location', $company->location) }}</textarea>
                                        @if($errors->has('location'))
                                            <span class="invalid-feedback"><strong>{{ $errors->first('location') }}</strong></span>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <img src="{{asset('app/'. $company->logo)}}" alt="{{ $company->logo }}" width="50" height="50" style="border-radius: 40px; float: left">
                        <label for="uploadbtn" class="uploadButton">Загрузити фото</label>
                        <input style="opacity: 0; z-index: -1;" type="file" name="logo" id="uploadbtn">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px; border: none;">Зберегти</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection