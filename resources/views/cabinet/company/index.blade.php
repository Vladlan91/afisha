@extends('layouts.cabinet')

@section('content')
    <a href="{{route('cabinet.company.create')}}" class="btn btn-primary" style="color: white !important; border-radius: 20px;">Створити компанію</a>
    <ul class="tabsContent block-white" style="margin-top: 20px;">
        @foreach($company as $advert)
            <li style="border-bottom: 1px solid #ccc !important;">
                <a href="{{ route('company.show', company_path($advert)) }}" class="list_item clearfix trackable" data-info="{&quot;event_type&quot;: &quot;publisher&quot;, &quot;campaign&quot;: &quot;shop_listing&quot;, &quot;shop_cat&quot;: &quot;immobilier&quot;, &quot;shop_region&quot;: &quot;bourgogne&quot;, &quot;shop_subregion&quot;: &quot;saone_et_loire&quot;, &quot;shop_city&quot;: &quot;chalon_sur_saone&quot;, &quot;shop_id&quot;: &quot;39200&quot;, &quot;shop_name&quot;: &quot;neyrat_immobilier&quot;}">
                    <div class="item_image">
                            <span class="item_imagePic">
                                <span class="lazyload loaded" style="display:block; width:100%; height:100%;" data-imgsrc="{{  asset('/app/'. $advert->logo ) }}" data-imgalt="l'Immobilier réinventé"><img itemprop="image" content="https://img7.leboncoin.fr/bo-thumb/084730036580176.jpg" src="{{  asset('/app/'. $advert->logo ) }}" alt="l'Immobilier réinventé"></span>
                            </span>
                    </div>
                    @if($advert->type === 'A')
                        <div style="position: absolute; left: 0;top: -12px;">
                            <h5 style="background-color: #ea1b25; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>А</h5>
                        </div>
                    @elseif($advert->type === 'Б')
                        <div style="position: absolute; left: 0;top: -12px;">
                            <h5 style="background-color: #4387f4; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Б</h5>
                        </div>
                    @elseif($advert->type === 'В')
                        <div style="position: absolute; left: 0;top: -12px;">
                            <h5 style="background-color: #45c639; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>В</h5>
                        </div>
                    @else
                        <div style="position: absolute; left: 0;top: -12px;">
                            <h5 style="background-color: #aaaaaa; padding: 15px; color: white;"><span style="margin-right: 10px;">Тип</span>Г</h5>
                        </div>
                    @endif
                    <section class="item_infos">
                        <h2 class="item_title mb0">
                            {{ \Illuminate\Support\Str::limit($advert->title, 20) }}
                        </h2>
                        <aside>
                            <p style="line-height: 1.5!important; margin-top: 0 !important; font-weight: 900!important; font-size: 12px !important;" class="item_slogan small-hidden tiny-hidden">{{ \Illuminate\Support\Str::limit($advert->content,30) }}</p>
                            <p style="line-height: 1.5!important; margin-top: 0 !important; font-weight: 900!important; font-size: 12px !important;"class="item_nbAds"><b>{{ $advert->adverts()->count() }} оголошень</b></p>
                            <p style="line-height: 1.5!important; margin-top: 0 !important; font-weight: 900!important; font-size: 12px !important;"class="item_nbAds"><b>{{ $advert->suggestion()->count() }} пропозицій</b></p>
                            <p style="line-height: 1.5!important; margin-top: 0 !important; font-weight: 900!important; font-size: 12px !important;"class="item_supp small-hidden tiny-hidden">{{ $advert->region ?  $advert->region->name : 'Всі міста'}}</p>
                            <p style="line-height: 1.5!important; margin-top: 0 !important; font-weight: 900!important; font-size: 12px !important;"class="item_supp">{{ $advert->business->name }}</p>
                        </aside>
                    </section>
                </a>
            </li>
        @endforeach
    </ul>

    {{ $company->links() }}
@endsection
