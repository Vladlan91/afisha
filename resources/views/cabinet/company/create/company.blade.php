@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: #999999; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">1 крок</h1>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: #999999; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">2 крок</h1>
            </div>
            <div class="sect" style="background-color: #fd4235; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: white; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">3 крок</h1>
            </div>
        </div>
        <div class="col-md-9">
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #000000">
                <div class="row">
                    <div class="col-md-6">
                        <h1 style="font-size: 15px; color: #0098d0; padding-top: 6px; float: right; margin-top: 0!important; margin-bottom: 0!important;">{{ $business->getPa() }}</h1>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('cabinet.company.create') }}" style="text-decoration: none;"><p style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px;">Змінити</p></a>
                    </div>
                </div>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #000000">
                <div class="row">
                    <div class="col-md-6">
                        <h1 style="font-size: 15px; color: #0098d0; padding-top: 6px; float: right; margin-top: 0!important; margin-bottom: 0!important;">{{ $region ? $region->getAddress() : 'Івано-Франківська область' }}</h1>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('cabinet.company.create.region' , $business) }}" style="text-decoration: none;"><p style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center; margin-top: 5px;">Змінити</p></a>
                    </div>
                </div>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #999999">
                <h1  style="font-size: 15px; margin-top: 0!important; padding-top: 6px; padding-bottom: 6px; margin-bottom: 0!important;">Зміст оголошення</h1>
            </div>
            <div class="col-md-12" style="background-color: #f5f5f5; padding: 15px; border-radius: 20px;">
                <form method="POST" action="{{ route('cabinet.company.create.company.store', [$business, $region]) }}" enctype="multipart/form-data" novalidate>
                    {{ csrf_field() }}
                    <div class="">
                        <div class="">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                        <label for="title">Назва</label>
                                        <input id="title" name="title" class="form-control" value="{{ old('title') }}" >
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone">Телефон</label>
                                        <input id="phone" name="phone" class="form-control" value="{{ old('phone') }}" >
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('site') ? ' has-error' : '' }}">
                                        <label for="site">Сайт</label>
                                        <input id="site" name="site" class="form-control{{ $errors->has('site') ? 'is-invalid' : '' }}" value="{{ old('site') }}" >
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('site') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email">Емейл</label>
                                        <input id="email" name="email" class="form-control" value="{{ old('email') }}" >
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12" >
                                    <div class="row">
                                        <div class="col-md-10 col-sm-10 col-xs-9">
                                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                                <label for="address">Адреса</label>
                                                <input id="address" name="address" class="form-control" value="{{ old('address', $region ? $region->getAddress() : 'Івано-Франківська область') }}" required>
                                                @if ($errors->has('address'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-10 col-xs-3">
                                            <label for="address" style="color: #f5f5f5;">Авто</label>
                                            <span class="location-button" data-target="#address"><img style="height: 30px; width: 30px; cursor: pointer;"
                                                        src="{{asset('images/map-location.png')}}" alt=""></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="content">Опис</label>
                                        <textarea id="content" name="content" rows="3" class="form-control{{ $errors->has('content') ? 'is-invalid' : '' }}"  required>{{ old('content') }}</textarea>
                                        @if($errors->has('content'))
                                            <span class="invalid-feedback"><strong>{{ $errors->first('content') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="location">Контакти</label>
                                        <textarea id="location" name="location" rows="3" class="form-control{{ $errors->has('location') ? 'is-invalid' : '' }}" required>{{ old('location') }}</textarea>
                                        @if($errors->has('location'))
                                            <span class="invalid-feedback"><strong>{{ $errors->first('location') }}</strong></span>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="file-upload">
                            <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Додати фото</button>

                            <div class="image-upload-wrap">
                                <input class="file-upload-input" id="avatar" name="logo" type='file' onchange="readURL(this);" accept="image/*" />
                                <div class="drag-text">
                                    <h3>Перетягніть файл або виберіть Додати зображення</h3>
                                </div>
                            </div>
                            <div class="file-upload-content">
                                <img class="file-upload-image" src="#" alt="your image" />
                                <div class="image-title-wrap">
                                    <button type="button" onclick="removeUpload()" class="remove-image">Видалити <span class="image-title">Завантажено зображення</span></button>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('avatar'))
                            <span class="invalid-feedback"><strong style="color: red">{{ $errors->first('avatar') }}</strong></span>
                        @endif
                    </div>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"> </script>
                    <script>
                        function readURL(input) {
                            if (input.files && input.files[0]) {

                                var reader = new FileReader();

                                reader.onload = function(e) {
                                    $('.image-upload-wrap').hide();

                                    $('.file-upload-image').attr('src', e.target.result);
                                    $('.file-upload-content').show();

                                    $('.image-title').html(input.files[0].name);
                                };

                                reader.readAsDataURL(input.files[0]);

                            } else {
                                removeUpload();
                            }
                        }

                        function removeUpload() {
                            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                            $('.file-upload-content').hide();
                            $('.image-upload-wrap').show();
                        }
                        $('.image-upload-wrap').bind('dragover', function () {
                            $('.image-upload-wrap').addClass('image-dropping');
                        });
                        $('.image-upload-wrap').bind('dragleave', function () {
                            $('.image-upload-wrap').removeClass('image-dropping');
                        });
                    </script>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px; border: none;">Зберегти</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection