@extends('layouts.cabinet')

@section('content')
    <div class="messaging">
        <div class="inbox_msg">
            <div class="inbox_people">
                <div class="headind_srch">
                    <div class="recent_heading">
                        <h4>Діалоги</h4>
                    </div>
                </div>
                <div class="inbox_chat">
                    @foreach($dialogs as $item)
                        <a href="{{route('cabinet.messages.show', $item)}}">
                            @if(!empty($id) && $id === $item->id)
                                <div class="chat_list active_chat">
                            @else
                                <div class="chat_list">
                            @endif
                            <div class="chat_people">
                                <div class="chat_img">
                                    @if($item->client->avatar)
                                        <img src="{{asset('app/'. $item->client->avatar)}}" alt="{{ $item->client->name }}" width="45" height="45" style="border-radius: 40px">
                                    @else
                                        <img src="{{asset('images/no-avatar.jpg')}}" width="45" height="45" style="border-radius: 40px">
                                    @endif
                                    @if($dialog->client_id !== Illuminate\Support\Facades\Auth::id())
                                        @if($dialog->client_new_message)
                                            <span class="lnr lnr-location" style="color: white; margin-top: -15px;margin-left: -15px; position: absolute;  font-weight: 900; background-color: #fd4235; padding: 5px; border-radius: 35%">{{$dialog->client_new_message}}</span>
                                        @endif
                                    @else
                                        @if($dialog->user_new_message)
                                            <span class="lnr lnr-location" style="color: white; margin-top: -15px;margin-left: -15px; position: absolute;  font-weight: 900; background-color: #fd4235; padding: 5px; border-radius: 35%">{{$dialog->user_new_message}}</span>
                                        @endif
                                    @endif
                                </div>
                                <div class="chat_ib">
                                    <h5>{{ $item->client->name }}<span>{{date('y.m.d-h:m', strtotime($dialog->created_at))}}</span></h5>
                                    @foreach($item->messages()->orderByDesc('id')->limit(1)->get() as $messages)
                                        <p>{{ \Illuminate\Support\Str::limit($messages->message, 30) }}</p>
                                    @endforeach
                                </div>
                            </div>
                        </a>
                </div>
                    @endforeach
                </div>
            </div>
        @include('cabinet.messages.dialog', $dialog)
        </div>

    </div>
    </div>
@endsection