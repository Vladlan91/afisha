<div class="mesgs">
<div class="msg_history">
    @foreach($dialog->messages()->get() as $messages)
        @if(\Illuminate\Support\Facades\Auth::id() !== $messages->user_id)
        <div class="incoming_msg">
            <div class="incoming_msg_img">
                @if($messages->user->avatar)
                    <img src="{{asset('app/'. $messages->user->avatar)}}" width="35" height="35" style="border-radius: 40px">
                @else
                    <img src="{{asset('images/no-avatar.jpg')}}" width="35" height="35" style="border-radius: 40px">
                @endif
            </div>
            <div class="received_msg">
                <div class="received_withd_msg">
                    <p>{{$messages->message}}</p>
                    <span class="time_date">{{date('y.m.d-h:m', strtotime($messages->created_at))}}</span>
                </div>
            </div>
        </div>
        @else
            <div class="outgoing_msg">
                <div class="sent_msg">
                    <p>{{$messages->message}}</p>
                    <span class="time_date">{{date('y.m.d-h:m', strtotime($messages->created_at))}}</span> </div>
            </div>
        @endif
    @endforeach
</div>
<div class="type_msg">
<div class="input_msg_write">
    <form action="{{ route('cabinet.messages.send' , $dialog) }}" method="POST">
        {{ csrf_field() }}
        <input type="text" name="message"  id="message" class="write_msg" placeholder="Відповісти" required/>
        <button class="msg_send_btn" type="submit"><span class="lnr lnr-location"></span></button>
    </form>
</div>
</div>
</div>
</div>