@extends('layouts.cabinet')

@section('content')
    {{--@include('cabinet.profile._nav')--}}
    <form method="POST" action="{{ route('cabinet.profile.update') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{method_field('PUT')}}
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Ім'я</label>
            <input id="name" name="name" class="form-control"
                   value="{{ old('name', $user->name) }}" required>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
            <label for="last_name">Фамілія</label>
            <input id="last_name" name="last_name" class="form-control"
                   value="{{ old('last_name', $user->last_name) }}" required>
            @if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">Емейл</label>
            <input id="email" name="email" class="form-control"
                   value="{{ old('email', $user->email) }}" required>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
            <label for="phone">Телефон</label>
            <input type="tel" id="phone" name="phone" class="form-control"
                   value="{{ old('phone', $user->phone) }}" required>
            @if ($errors->has('phone'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <img src="{{asset('app/'. $user->avatar)}}" alt="{{ $user->avatar }}" width="50" height="50" style="border-radius: 40px; float: left">
            <label for="uploadbtn" class="uploadButton">Загрузити фото</label>
            <input style="opacity: 0; z-index: -1;" type="file" name="avatar" id="uploadbtn">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" style="color: white !important; border-radius: 20px;">Зберегти</button>
        </div>
    </form>
@endsection