@extends('layouts.cabinet')

@section('content')
@include('cabinet.profile._nav')
<form method="POST" action="{{ route('cabinet.profile.phone.verify') }}">
    {{ csrf_field() }}
    {{method_field('PUT')}}
    <br>
    <div class="form-group">
        <label for="phone_verify_token">SMS</label>
        <input type="tel" id="token" name="token" class="form-control{{ $errors->has('phone') ? 'is-invalid': '' }}" value="{{ old('phone_verify_token') }}">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>
@endsection