@extends('layouts.cabinet')

@section('content')
    {{--@include('cabinet.profile._nav')--}}
    <p style=" position: absolute;">
        <a href="{{ route('cabinet.profile.edit') }}" class="btn btn-primary" style="color: white !important; border-radius: 20px;">Корегувати</a>
    </p>

    <br>
    <table class="table table-bordered table-striped pt-2" style="margin-top: 30px;">
        <tbody>
        <tr>
            <th>Ім'я</th> <td>{{$user->name}}</td>
        </tr>
        <tr>
            <th>Фамілія</th> <td>{{$user->last_name}}</td>
        </tr>
        <tr>
            <th>Баланс на рахунку</th> <td>{{$user->balance}}&nbsp;грн</td>
        </tr>
        <tr>
            <th>Емейл</th> <td>{{$user->email}}</td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td>
                @if($user->phone)
                    {{ $user->phone }}
                    {{--@if(!$user->isPhoneVerified())--}}
                        {{--<form method="POST" action="{{ route('cabinet.profile.phone') }}">--}}
                            {{--{{ csrf_field() }}--}}
                            {{--<button type="submit" class="btn btn-sm btn-success bottom-right">Verify</button>--}}
                        {{--</form>--}}
                        {{--<i>is not verified</i><br>--}}
                    {{--@endif--}}
                @endif
            </td>
        </tr>
        {{--@if($user->phone)--}}
            {{--<tr><th>Two factor auth</th>--}}
                {{--<td>--}}
                    {{--<form method="POST" action="{{ route('cabinet.profile.phone.auth') }}">--}}
                        {{--{{ csrf_field() }}--}}
                        {{--@if($user->isPhoneAuthEnabled())--}}
                            {{--<button type="submit" class="btn btn-sm btn-success bottom-right">On</button>--}}
                        {{--@else--}}
                            {{--<button type="submit" class="btn btn-sm btn-danger bottom-right">Off</button>--}}
                        {{--@endif--}}
                    {{--</form>--}}
                {{--</td>--}}
            {{--</tr>--}}
        {{--@endif--}}
        <tr>
            <th>Фото</th> <td> <img src="{{asset('app/'. $user->avatar)}}" alt="{{ $user->avatar }}" width="50" height="50" style="border-radius: 40px"></td>
        </tr>
        </tbody>
    </table>
@endsection
