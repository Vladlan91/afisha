<div class="container">
    <div class="row">
        <div class="col-sm-12 col-lg-12" style="margin-bottom: 30px;">
            <div class="tab" role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="{{ route('cabinet.home') }}">Кабінет</a></li>
                    <li role="presentation"><a href="{{ route('cabinet.favorites.index') }}">Вподобані</a></li>
                    <li role="presentation"><a href="{{ route('cabinet.adverts.index') }}">Оголошення</a></li>
                    <li role="presentation"><a href="{{ route('cabinet.banners.index') }}">Мої банери</a></li>
                    <li role="presentation"><a href="{{ route('cabinet.tickets.index') }}">Служба підтримки</a></li>
                    <li role="presentation" class="active"><a href="{{ route('cabinet.profile.home') }}">Налащтування</a></li>
                </ul>
                <!-- Tab panes -->
            </div>
        </div>
    </div>
</div>