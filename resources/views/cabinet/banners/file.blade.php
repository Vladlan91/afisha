@extends('layouts.cabinet')

@section('content')
    {{--@include('cabinet.banners._nav')--}}
    <div class="row">
        <div class="col-md-12" style="background-color: #f5f5f5; padding: 15px; border-radius: 20px;">
            <form method="POST" action="{{ route('cabinet.banners.file', $banner) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div class="">
                    <div class="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="format">Формат</label>
                                    <select id="format" name="format" class="form-control{{ $errors->has('format') ? 'is-invalid' : '' }}" value="{{ old('format', $banner->format) }}" required>
                                        @foreach($formats as $format)
                                            <option value="{{ $format }}"{{ $format === old('format', $banner->format) ? 'selected': '' }}>{{ $format }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('format'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('format') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="file">Фото</label>
                                    <input id="file" name="file" type="file" class="form-control{{ $errors->has('file') ? 'is-invalid' : '' }}" value="{{ old('file', $banner->file) }}" required>
                                    @if($errors->has('file'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('file') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px; border: none;">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
@endsection