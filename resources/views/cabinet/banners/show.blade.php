@extends('layouts.cabinet')

@section('content')
    {{--@include('cabinet.banners._nav')--}}
<p><div class="d-flex flex-row mb-3">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"> </script>
        <div class="modal fade" id="myModal"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title" style="font-weight: 900">Публікація оголошення</h4>
                        <h6 class="modal-title" style="font-weight: 900; font-size: 14px; color: red;">Недостатньо коштів на рахунку</h6>
                        <button type="button" class="close" style="margin-top: -33px!important;" data-dismiss="modal">×</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
    <p style="line-height: 0.5;">Вартість публікації оголошення складає - <span style="font-size: 20px !important;">{{$banner->cost}}&nbsp;грн</span></p>
    <p style="line-height: 0.5;">Ваш баланс - <span style="font-size: 20px !important;">{{Auth::user()->balance}}&nbsp;грн</span></p>
    <p style="line-height: 0.5;">Для подальшої публікації поповніть Ваш рахунок на суму - <span style="font-size: 20px !important;">{{$banner->cost - Auth::user()->balance}}&nbsp;грн</span></p>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <a href="{{route('home')}}" style="background-color: #5cd963; border-radius: 20px;"  class="btn btn-success">Поповнити баланс</a>
        <button type="button" class="btn btn-danger"  style="background-color: #fd4235; border-radius: 20px;" data-dismiss="modal">Закрити</button>
    </div>

    </div>
    {{ csrf_field() }}
    </div>
    </div>
    <script>
        $(document).on('click','.add-to-card-link', function () {
            var id = '{{Auth::id()}}';
            var banner = '{{$banner->cost}}';
            var bannerid = '{{$banner->id}}';
            var _token = $('input[name="_token"]').val();
            console.log(id, banner );
            $.ajax({
                url: "{{ route('order.banner') }}",
                method: "POST",
                data: {id: id, price: banner, bannerid: bannerid, _token:_token},
                cache: false, success: function(result){
                    location.href = '{{route('cabinet.banners.show', $banner)}}';
                },
                error: function () {
                    $("#myModal").modal("show");
                }

            });
        });
    </script>
    @if($banner->canBeChanged())
        <a href="{{ route('cabinet.banners.edit', $banner) }}" class="btn btn-primary" style="float: left; border-radius: 20px; margin-right: 5px;">Корегувати</a>
        <a href="{{ route('cabinet.banners.file', $banner) }}" class="btn btn-primary" style="float: left; border-radius: 20px; margin-right: 5px;">Змінити формат</a>
    @endif
    @if($banner->isDraft())
        <form method="POST" action="{{ route('cabinet.banners.send', $banner) }}">
            {{ csrf_field() }}
            <button class="btn btn-success" style="float: left; border-radius: 20px; margin-right: 5px;">Надіслати на модерацію</button>
        </form>
    @endif
    @if($banner->isOnModeration())
        <form  method="POST" action="{{ route('cabinet.banners.cancel', $banner) }}">
            {{ csrf_field() }}
            <button class="btn btn-success" style="float: left; border-radius: 20px; margin-right: 5px;">Відмінити модерацію</button>
        </form>
    @endif
        @if(Auth::user()->isAdmin())
            @if($banner->isOnModeration())
                <form  method="POST" action="{{ route('admin.banners.moderateAdmin', $banner) }}">
                    {{ csrf_field() }}
                    <button class="btn btn-success" style="float: left; border-radius: 20px; margin-right: 5px;">Відмодерувати</button>
                </form>
            @endif
        @endif
    @if($banner->isModerated())
            {{ csrf_field() }}
            <button class="btn btn-success add-to-card-link" style="float: left; border-radius: 20px; margin-right: 5px;">Оплатити</button>
    @endif
    @if($banner->canBeRemoved())
        <form  method="POST" action="{{ route('cabinet.banners.destroy', $banner) }}">
            {{ csrf_field() }}
            {{method_field('DELETE')}}
            <button class="btn btn-success" style="float: left; border-radius: 20px; margin-right: 5px;">Видалити</button>
        </form>
    @endif

</div></p><br>
<br>
    <table class="table table-bordered table-striped pt-2">
        <tbody>
        <tr>
            <th>ІD</th> <td>{{$banner->id}}</td>
        </tr>
        <tr>
            <th>Назва</th> <td>{{$banner->name}}</td>
        </tr>
        <tr>
            <th>Місто</th>

            @if($banner->region)
                <td>{{$banner->region->name}}</td>
            @else
                <td>Івано-Франківська область</td>
            @endif

        </tr>
        <tr>
            <th>Категорія</th>
                @if($banner->category)
                    <td> {{$banner->category->name}}</td>
                @else
                    <td>Всі категорії</td>
                @endif
        </tr>
        <tr>
            <th>Статус</th>
            <td>
                @if($banner->isDraft())
                    <span class="success">Чорновик</span>
                @elseif($banner->isOnModeration())
                    <span class="primary">На модерації</span>
                @elseif($banner->isModerated())
                    <span class="primary">Готовий до оплати</span>
                @elseif($banner->isOrdered())
                    <span class="primary">Очікує оплати</span>
                @elseif($banner->isActive())
                    <span class="primary">Активний</span>
                @elseif($banner->isClosed())
                    <span class="primary">Закритий</span>
                @endif
            </td>
        </tr>
        <tr>
            <th>Кількість показів</th>
            <td>{{$banner->views}}</td>
        </tr>
        <tr>
            <th>Ліміт показів</th>
            <td>{{$banner->limit}}</td>
        </tr>
        <tr>
            <th>Дата публікації</th>
            <td>{{$banner->published_at}}</td>
        </tr>
        </tbody>
    </table>
    <div class="card">
        <div class="card-body">
            <img src="{{asset('app/'. $banner->file)}}" alt="{{ $banner->name }}">
        </div>
    </div>
@endsection