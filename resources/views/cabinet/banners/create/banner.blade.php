@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--Plugin JavaScript file-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js"></script>
    <div class="row">
        <div class="col-md-3">
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: #999999; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">1 крок</h1>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: #999999; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">2 крок</h1>
            </div>
            <div class="sect" style="background-color: #fd4235; margin: 3px; padding-top: 4px !important; padding-bottom: 4px !important; border-radius: 155px; color: white; border: 4px solid white">
                <h1 style="font-size: 15px; margin-top: 6px !important; margin-bottom: 6px !important; ">3 крок</h1>
            </div>
        </div>
        <div class="col-md-9">
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #000000">
                <div class="row">
                    <div class="col-md-6">
                        <h1 style="font-size: 15px; color: #0098d0; padding-top: 6px; float: right; margin-top: 0!important; margin-bottom: 0!important;">{{ $category->getPa() }}</h1>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('cabinet.banners.create') }}" style="text-decoration: none;"><p style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px;">Змінити</p></a>
                    </div>
                </div>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #000000">
                <div class="row">
                    <div class="col-md-6">
                        <h1 style="font-size: 15px; color: #0098d0; padding-top: 6px;  float: right; margin-top: 0!important; margin-bottom: 0!important;">{{ $region ? $region->getAddress() : 'Івано-Франківська область' }}</h1>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('cabinet.banners.create.region' , $category) }}" style="text-decoration: none;"><p style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center; margin-top: 5px;">Змінити</p></a>
                    </div>
                </div>
            </div>
            <div class="sect" style="background-color: #f5f5f5; margin: 3px; padding-top: 10px; padding-bottom: 10px; border-radius: 155px; color: #999999">
                <h1  style="font-size: 15px; margin-top: 0!important; padding-top: 6px; padding-bottom: 6px; margin-bottom: 0!important;">Зміст оголошення</h1>
            </div>
            <div class="col-md-12" style="background-color: #f5f5f5; padding: 15px; border-radius: 20px;">
                <form method="POST" action="{{ route('cabinet.banners.create.banner.store', [$category, $region]) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="">
                        <div class="">
                            <div class="row">
                                <div id="one" class="col-md-12">
                                    <div class="col-md-6">
                                        <img src="{{asset('images/AFISHA2.png')}}" alt="" style="padding: 20px;">
                                        <img src="{{asset('images/typearrow.png')}}" alt="" style="float: right; top: 50%; right:5px; position: absolute;">
                                    </div>
                                    <div class="col-md-6" >
                                        <p style="padding-top: 10%; font-weight: bold;">Боковий банер.</p>
                                        <p style="padding-top: 5%;">Відображається в категоріях оголошень та на сторінці оголошень з привязкою до категорії і міста.</p>
                                    </div>
                                </div>
                                <div id="two" class="col-md-12" style="display: none">
                                    <div class="col-md-6">
                                        <img src="{{asset('images/AFISHA1.png')}}" alt="" style="padding: 20px;">
                                        <img src="{{asset('images/typearrow.png')}}" alt="" style="float: right; top: 50%; right:5px; position: absolute;">
                                    </div>
                                    <div class="col-md-6">
                                        <p style="padding-top: 10%; font-weight: bold;">Скрізний банер.</p>
                                        <p style="padding-top: 5%;">Відображається в категоріях оголошень та на сторінці оголошень з привязкою до категорії і міста.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('format') ? ' has-error' : '' }} calc">
                                        <label for="format">Формат</label>
                                        <select id="format"  name="format" class="form-control{{ $errors->has('format') ? 'is-invalid' : '' }}" value="{{ old('format') }}" >
                                            @php($i=1)
                                            @foreach($formats as $format)
                                                <option data-type="{{ $i }}" value="{{ $format }}"{{ $format === old('format') ? 'selected': '' }}>{{$format}}</option>
                                                @php($i++)
                                            @endforeach
                                        </select>
                                        @if ($errors->has('format'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('format') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name">Назва</label>
                                        <input id="name" name="name" class="form-control{{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name') }}" >
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                                        <label for="url">Посилання</label>
                                        <input id="url" name="url" class="form-control{{ $errors->has('url') ? 'is-invalid' : '' }}" value="{{ old('url') }}" >
                                        @if ($errors->has('url'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('url') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="file-upload">
                                        <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Додати фото</button>

                                        <div class="image-upload-wrap">
                                            <input class="file-upload-input" id="file" name="file" type='file' onchange="readURL(this);" accept="image/*" />
                                            <div class="drag-text">
                                                <h3>Перетягніть файл або виберіть Додати зображення</h3>
                                            </div>
                                        </div>
                                        <div class="file-upload-content">
                                            <img class="file-upload-image" src="#" alt="your image" />
                                            <div class="image-title-wrap">
                                                <button type="button" onclick="removeUpload()" class="remove-image">Видалити <span class="image-title">Завантажено зображення</span></button>
                                            </div>
                                        </div>
                                    </div>
                                    @if($errors->has('file'))
                                        <span class="invalid-feedback"><strong style="color: red">{{ $errors->first('file') }}</strong></span>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <div style="text-align: left;">
                                        <p style="font-weight: 900; padding: 20px;color: grey;">Ліміт на кількість перегладів</p>
                                    </div>
                                </div>
                                <div class="col-md-offset-1 col-md-10">
                                    <div class="form-group{{ $errors->has('limit') ? ' has-error' : '' }}">
                                        <input type="text" class="js-range-slider" name="limit" id="limit" value=""
                                               data-min="0"
                                               data-max="10000"
                                               data-from="1000"
                                               data-grid="true"
                                               data-step="10"
                                        />
                                        @if ($errors->has('limit'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('limit') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <script>
                                    $(".js-range-slider").ionRangeSlider({


                                        onFinish: function (data) {
                                            // Called every time handle position is changed
                                            var final_prices = 0.1 * parseInt(data.from);
                                            $('span#final-price').text(final_prices.toFixed(2));
                                            // $.getElementById('limit').value = 5;

                                            // $('input#limit').text(final_prices.toFixed(2));
                                            // $('input#limit').value(data.to);
                                        }
                                    });
                                </script>
                                <div class="col-md-12">
                                    <div style="text-align: left;  font-weight: 900; color: black;">
                                        <p>Допомагає обрати оптимальну стратегію і уникнути неефективного витрачання бюджету. Після досягнення зазначеного ліміту, Ваш банер станеть неактивний для перегляду.</p>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div style="text-align: left;  font-weight: 900; color: black;">
                                        <p style="font-weight: 900; color: grey;">Вартість 1 перегляду: 0.1 грн</p>
                                        <p style="font-weight: 900; font-size: 20px; color: grey;">Всього до сплати: <span style="color: #ed5565" id="final-price">100</span>&nbsp;грн</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"> </script>
                    <script>
                        function readURL(input) {
                            if (input.files && input.files[0]) {

                                var reader = new FileReader();

                                reader.onload = function(e) {
                                    $('.image-upload-wrap').hide();

                                    $('.file-upload-image').attr('src', e.target.result);
                                    $('.file-upload-content').show();

                                    $('.image-title').html(input.files[0].name);
                                };

                                reader.readAsDataURL(input.files[0]);

                            } else {
                                removeUpload();
                            }
                        }

                        function removeUpload() {
                            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                            $('.file-upload-content').hide();
                            $('.image-upload-wrap').show();
                        }
                        $('.image-upload-wrap').bind('dragover', function () {
                            $('.image-upload-wrap').addClass('image-dropping');
                        });
                        $('.image-upload-wrap').bind('dragleave', function () {
                            $('.image-upload-wrap').removeClass('image-dropping');
                        });
                    </script>
                    <script>
                        $(document).ready(function(){
                            $('.calc select').change(function(){
                                var type_format = $('select#format option:selected').attr('data-type');
                                if (type_format == 1) {
                                    $('#one').show();
                                    $('#two').hide('slow');
                                }
                                if (type_format == 2) {
                                    $('#one').hide('slow');
                                    $('#two').show();

                                }
                            })
                        })
                    </script>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px; border: none;">Зберегти</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection