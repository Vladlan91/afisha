@extends('layouts.cabinet')

@section('content')
    {{--@include('cabinet.banners._nav')--}}
    <div class="row">
        <div class="col-md-12" style="background-color: #f5f5f5; padding: 15px; border-radius: 20px;">
            <form method="POST" action="{{ route('cabinet.banners.edit', $banner) }}">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div class="">
                    <div class="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Назва</label>
                                    <input id="name" name="name" class="form-control{{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name', $banner->name) }}" required>
                                    @if($errors->has('name'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="limit">Кількість переглядів</label>
                                    <input id="limit" name="limit" class="form-control{{ $errors->has('limit') ? 'is-invalid' : '' }}" value="{{ old('limit', $banner->limit) }}" required>
                                    @if($errors->has('limit'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('limit') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="url">Посилання</label>
                                    <input id="url" name="url" class="form-control{{ $errors->has('url') ? 'is-invalid' : '' }}" value="{{ $banner->url }}" required>
                                    @if($errors->has('url'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('url') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" style="background-color: red; width: 100px; border-radius: 20px; color: white; text-align: center;     margin-top: 5px; border: none;">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
@endsection