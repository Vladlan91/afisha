@extends('layouts.cabinet')

@section('content')
    <a href="{{route('cabinet.banners.create')}}" class="btn btn-primary" style="color: white !important; border-radius: 20px;">Створити банер</a>
    <ul class="_1rXe6">
        @foreach($banners as $banner)
            <li>
                <a href="{{ route('cabinet.banners.show', $banner) }}" title="Appartement 4 pièces 101 m²" class="list_item clearfix trackable" data-info="{&quot;event_type&quot; : &quot;selfpromotion&quot;, &quot;campaign&quot; : &quot;store_adview&quot;, &quot;ad_listid&quot; : &quot;1557273607&quot;, &quot;ad_location&quot; : &quot;list_content&quot;, &quot;ad_position&quot; : &quot;1&quot;, &quot;ad_type&quot; : &quot;offres&quot;, &quot;ad_offres&quot; : &quot;pro&quot;}">
                    <div class="item_image">
                	<span class="item_imagePic">
                        @if($banner->format === '240x440')
                        	<img src="{{  asset('/app/'. $banner->file ) }}" alt="Appartement 4 pièces 101 m²" style="height: 150px;">
                            @else
                            <img src="{{  asset('/app/'. $banner->file ) }}" alt="Appartement 4 pièces 101 m²" style="width: 100%; height: auto;">
                        @endif
                    	</span>
                        <span class="item_imageNumber">
                        	<i class="icon-camera icon-2x nomargin"></i>
                        	<span>3</span>
                      	</span>
                    </div>
                    <div title="" class="saveAd" data-savead-id="1557273607">
                            @if($banner->user->avatar)
                                <img src="{{asset('/app/'. $banner->user->avatar)}}" alt="{{ $banner->user->name }}" width="35" height="35" style="border-radius: 40px; float: right;">
                            @else
                                <img src="{{asset('images/no-avatar.jpg')}}" width="35" height="35" style="border-radius: 40px; float: right;">
                            @endif
                    </div>
                    <section class="item_infos">
                        <h2 class="item_title">
                            @if($banner->isDraft())
                                <span class="success">Чорновик</span>
                            @elseif($banner->isOnModeration())
                                <span class="primary">На модерації</span>
                            @elseif($banner->isModerated())
                                <span class="primary">Готовий до оплати</span>
                            @elseif($banner->isOrdered())
                                <span class="primary">Очікує оплати</span>
                            @elseif($banner->isActive())
                                <span class="primary">Активний</span>
                            @elseif($banner->isClosed())
                                <span class="primary">Закритий</span>
                            @endif
                        </h2>
                        <h3 class="item_price" style="font-size: 12px !important; font-weight: 900 !important;">
                            Переглядів {{$banner->views}} з  {{$banner->limit }} доступних
                        </h3>
                        <aside>
                            <p class="item_supp" style="font-size: 12px !important; font-weight: 900 !important;">
                                {{ $banner->category ? $banner->category->name : 'Всі категорії'}}
                                /
                                {{ $banner->region ?  $banner->region->name : 'Всі міста'}}
                            </p>
                            <p class="item_supp item_suppDate" style="font-size: 12px !important; font-weight: 900 !important;">
                                Переглядів: {{$banner->views}} /  Переходів: {{$banner->clicks}}
                            </p>
                        </aside>
                    </section>
                </a>
            </li>
        @endforeach
    </ul>

    {{ $banners->links() }}
@endsection
