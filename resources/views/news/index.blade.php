@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-3">
            <h4 style="font-weight: 900">Категорії</h4>
            <p style="width: 100px; background-color: red; height: 2px;"></p>
            <div class="item" style="">
                <a href="{{ route('news') }}"><h5  style="line-height: 20px !important; padding: 5px 0px !important;" >Усі новини</h5></a>
            </div>
            @foreach($newsCat as $cat)
                <div class="item" style="">
                    <a href="{{ route('news', $cat) }}"><h5  style="line-height: 20px !important; padding: 5px 0px !important;" >{{ $cat->title }}</h5></a>
                </div>
            @endforeach
        </div>
        <div class="col-md-9">
            @foreach($news as $item)
                <a href="{{route('news.show', news_path($item))}}">
                <div class="col-md-6" style="height: 420px;">
                    <img src="{{asset('app/'. $item->avatar)}}" alt="Product Image">
                    <h5 style="font-weight: 900; font-size: 16px; color: darkgrey">{{ $item->title }}</h5>
                    <p style="background-color: darkgrey; border-radius: 20px; color: white; width: 200px; padding: 10px; text-align: center;">Переглянути</p>
                </div>
                </a>
            @endforeach
        </div>

    {{ $news->links() }}
@endsection