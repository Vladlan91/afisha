@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-3">
            <h4 style="font-weight: 900">Категорії</h4>
            <p style="width: 100px; background-color: red; height: 2px;"></p>
            <ul class="list-unstyled">
                @foreach($newsCat as $cat)
                    <li style="padding: 5px !important;"><a style="font-size: 16px !important; " href="{{ route('news', $cat) }}">{{ $cat->title }}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-9">
            <div class="col-md-12">
                <h1 style="font-weight: 900; font-size: 19px; color: darkgrey">{{ $news->title }}</h1>
                {!! $news->content !!}
            </div>
        </div>
@endsection