
@foreach($banner as $ban)
    <a href="{{ route('banner.click', $ban) }}" target="_blank">
        <img style="width: 100%"
             height="{{ $ban->getHeight() }}" src="{{ asset('/app/'. $ban->file ) }}" alt="{{ $ban->name }}">
    </a>
@endforeach