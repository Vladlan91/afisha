
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(document).on('click', '.phone-button', function () {
    var button = $(this);
    axios.post(button.data('source')).then(function (response) {
        button.find('.number').html(response.data)
    }).catch(function (reason) {
        console.log(reason);
    });
});

$('.mt-2 a').each(function () {
    var location = window.location.protocol + '//' + window.location.host + window.location.pathname;
    var link = this.href;
    if (link == location) {
        console.log(location);
        $(this).closest('.nav-item').addClass('active');
    }
});

$('.menu_tpl a').each(function () {
    var location = window.location.protocol + '//' + window.location.host + window.location.pathname;
    var link = this.href;
    if (link == location) {
        console.log(location);
        $(this).closest('.nav-item').addClass('active');
    }
});

// const customSelects = document.querySelectorAll("select");
// const deleteBtn = document.getElementById('delete')
// const choices = new Choices('select',
//     {
//         searchEnabled: false,
//         itemSelectText: '',
//         removeItemButton: true,
//     });
// for (let i = 0; i < customSelects.length; i++)
// {
//     customSelects[i].addEventListener('addItem', function(event)
//     {
//         if (event.detail.value)
//         {
//             let parent = this.parentNode.parentNode
//             parent.classList.add('valid')
//             parent.classList.remove('invalid')
//         }
//         else
//         {
//             let parent = this.parentNode.parentNode
//             parent.classList.add('invalid')
//             parent.classList.remove('valid')
//         }
//     }, false);
// }
// deleteBtn.addEventListener("click", function(e)
// {
//     e.preventDefault()
//     const deleteAll = document.querySelectorAll('.choices__button')
//     for (let i = 0; i < deleteAll.length; i++)
//     {
//         deleteAll[i].click();
//     }
// });

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).getElementById('mobileAppsbadgeClose').onclick = function() {document.getElementById('mobileAppsbadge').style.display = 'none';};

$(function() {
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('.link');
        // Evento
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
        $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        };
    }

    var accordion = new Accordion($('#accordion'), false);
});
//////////////////// input
function readURL(input) {
    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {
            $('.image-upload-wrap').hide();

            $('.file-upload-image').attr('src', e.target.result);
            $('.file-upload-content').show();

            $('.image-title').html(input.files[0].name);
        };

        reader.readAsDataURL(input.files[0]);

    } else {
        removeUpload();
    }
}

function removeUpload() {
    $('.file-upload-input').replaceWith($('.file-upload-input').clone());
    $('.file-upload-content').hide();
    $('.image-upload-wrap').show();
}
$('.image-upload-wrap').bind('dragover', function () {
    $('.image-upload-wrap').addClass('image-dropping');
});
$('.image-upload-wrap').bind('dragleave', function () {
    $('.image-upload-wrap').removeClass('image-dropping');
});
