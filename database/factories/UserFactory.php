<?php

use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $active = $faker->boolean;
    $phoneActive = $faker->boolean;
    $phoneAuth = $faker->boolean;
    return [
        'name' => $faker->name,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->unique()->phoneNumber,
        'phone_verified' => $phoneActive,
        'avatar'=> 'users/p1_2295104_8d88ab8d.jpg',
        'phone_auth' => $phoneActive,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'verify_token' => !$active ? null : Str::random(),
        'phone_verify_token' => !$phoneActive ? null : Str::random(),
        'phone_verify_token_expire' => !$phoneActive ? null : Carbon::now()->addSeconds(300),
        'status' => $active ? User::STATUS_ACTIV : User::STATUS_WAITE,
        'role' => 'user',
    ];
});
