<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->integer('category_id')->references('id')->on('business')->onDelete('CASCADE');
            $table->integer('region_id')->nullable()->references('id')->on('advert_regions')->onDelete('CASCADE');
            $table->string('title');
            $table->string('slug');
            $table->string('phone')->nullable();
            $table->string('site')->nullable();
            $table->string('email')->nullable();
            $table->text('address');
            $table->text('content')->nullable();
            $table->text('location')->nullable();
            $table->string('logo')->nullable();
            $table->string('type')->nullable();
            $table->string('status',16);
            $table->text('reject_reason')->nullable();
            $table->timestamps();
            $table->timestamp('published_at')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->string('package')->nullable();
            $table->timestamp('bay_at')->nullable();
            $table->timestamp('end_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
