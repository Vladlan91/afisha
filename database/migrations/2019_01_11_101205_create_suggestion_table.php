<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuggestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggestion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->references('id')->on('companies')->onDelete('CASCADE');
            $table->string('title');
            $table->string('slug');
            $table->text('movie')->nullable();
            $table->string('avatar')->nullable();
            $table->integer('price')->nullable();
            $table->text('address');
            $table->text('content');
            $table->timestamps();
        });

        Schema::create('suggestion_photos', function (Blueprint $table){
            $table->increments('id');
            $table->integer('suggestion_id')->references('id')->on('suggestion')->onDelete('CASCADE');
            $table->string('file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggestion_photos');
        Schema::dropIfExists('suggestion');
    }
}
