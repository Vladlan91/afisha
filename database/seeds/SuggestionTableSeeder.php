<?php

use Illuminate\Database\Seeder;
use App\Entity\Suggestions\Suggestions;
use App\Entity\Suggestions\Photo;
class SuggestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Suggestions::create(['id'=>'1', 'company_id'=>'1','title'=>'ЖК «СТОЖАРИ»', 'movie'=>'<p><iframe src="//www.youtube.com/embed/JW5C9LNgxe0" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>', 'slug'=>'zhk-stozhari' , 'avatar'=>'suggestions/L9nfiLhMOGUNFzp1ofZCgJiPOY3vt9P5IIEnvXe8.png', 'price'=>'19000', 'address'=>'вул. Івасюка (поруч ТЦ ЕПІЦЕНТР)' , 'content'=>'<p>Тільки уявіть, яким співочим буде ваше життя у &ldquo;Стожарах&rdquo;, що на вул. Івасюка.</p>
<p><em>Так чом, стожари, ви знов заходите за хмари,</em><br /><em>Коли я з вами і мрію, і живу.</em></p>
<p>Дев&rsquo;ять поверхів майже достатньо, щоби заходити за хмари. Але крім романтики комплекс має суттєві реальні переваги: тут безпечний, закритий двір під охороною, броньовані двері та розвинена інфраструктура. Сюди приємно буде повертатись навіть вночі &ndash; територія будинку освітлюється LED-лампами, а місця для паркування вистачить усім.</p>
<p>Сучасне планування дозволить втілити у життя найсміливіші інтер&rsquo;єрні рішення, а швидкісні безшумні ліфти &ndash; зберегти затишок та спокій мешканців у будинку.</p>.','created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Photo::create(['id'=>'1', 'suggestion_id'=>'1','file'=>'suggestions/4o3eA5Nc3x8XyyTgz7XNA4UkzMDkiptMJpKn1e0v.png']);
        Photo::create(['id'=>'2', 'suggestion_id'=>'1','file'=>'suggestions/bE6ft3Cs2Az9hy2DgTSHjGuoYvcsqCU6Ga6sK25b.png']);
        Photo::create(['id'=>'3', 'suggestion_id'=>'1','file'=>'suggestions/saRApHt3cBy7SdmRJ9F6fMbX3bM7BHB2xqCSmsdU.jpeg']);

        Suggestions::create(['id'=>'2', 'company_id'=>'1','title'=>'ЖК “КВАРТАЛ ВІДЕНСЬКИЙ” ЧЕРГА ІІ', 'movie'=>'<p><iframe src="//www.youtube.com/embed/JW5C9LNgxe0" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>', 'slug'=>'zhk-stozhari12' , 'avatar'=>'suggestions/ЖК «Стожари» - ВАМБУД 2019-01-23 20-33-54.png', 'price'=>'12300', 'address'=>'вул. Івасюка (поруч ТЦ ЕПІЦЕНТР)' , 'content'=>'<p>Тільки уявіть, яким співочим буде ваше життя у &ldquo;Стожарах&rdquo;, що на вул. Івасюка.</p>
<p><em>Так чом, стожари, ви знов заходите за хмари,</em><br /><em>Коли я з вами і мрію, і живу.</em></p>
<p>Дев&rsquo;ять поверхів майже достатньо, щоби заходити за хмари. Але крім романтики комплекс має суттєві реальні переваги: тут безпечний, закритий двір під охороною, броньовані двері та розвинена інфраструктура. Сюди приємно буде повертатись навіть вночі &ndash; територія будинку освітлюється LED-лампами, а місця для паркування вистачить усім.</p>
<p>Сучасне планування дозволить втілити у життя найсміливіші інтер&rsquo;єрні рішення, а швидкісні безшумні ліфти &ndash; зберегти затишок та спокій мешканців у будинку.</p>.','created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Photo::create(['id'=>'4', 'suggestion_id'=>'2','file'=>'suggestions/4o3eA5Nc3x8XyyTgz7XNA4UkzMDkiptMJpKn1e0v.png']);
        Photo::create(['id'=>'5', 'suggestion_id'=>'2','file'=>'suggestions/bE6ft3Cs2Az9hy2DgTSHjGuoYvcsqCU6Ga6sK25b.png']);
        Photo::create(['id'=>'6', 'suggestion_id'=>'2','file'=>'suggestions/saRApHt3cBy7SdmRJ9F6fMbX3bM7BHB2xqCSmsdU.jpeg']);

        Suggestions::create(['id'=>'3', 'company_id'=>'1','title'=>'ЖК “КВАРТАЛ КРАКІВСЬКИЙ” ЧЕРГА IV', 'movie'=>'<p><iframe src="//www.youtube.com/embed/JW5C9LNgxe0" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>', 'slug'=>'zhk-stozhari123' , 'avatar'=>'suggestions/ЖК "Квартал Краківський" черга IV - ВАМБУД 2019-01-23 21-09-13.png', 'price'=>'12300', 'address'=>'вул. Івасюка (поруч ТЦ ЕПІЦЕНТР)' , 'content'=>'<p>Тільки уявіть, яким співочим буде ваше життя у &ldquo;Стожарах&rdquo;, що на вул. Івасюка.</p>
<p><em>Так чом, стожари, ви знов заходите за хмари,</em><br /><em>Коли я з вами і мрію, і живу.</em></p>
<p>Дев&rsquo;ять поверхів майже достатньо, щоби заходити за хмари. Але крім романтики комплекс має суттєві реальні переваги: тут безпечний, закритий двір під охороною, броньовані двері та розвинена інфраструктура. Сюди приємно буде повертатись навіть вночі &ndash; територія будинку освітлюється LED-лампами, а місця для паркування вистачить усім.</p>
<p>Сучасне планування дозволить втілити у життя найсміливіші інтер&rsquo;єрні рішення, а швидкісні безшумні ліфти &ndash; зберегти затишок та спокій мешканців у будинку.</p>.','created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Photo::create(['id'=>'7', 'suggestion_id'=>'3','file'=>'suggestions/4o3eA5Nc3x8XyyTgz7XNA4UkzMDkiptMJpKn1e0v.png']);
        Photo::create(['id'=>'8', 'suggestion_id'=>'3','file'=>'suggestions/bE6ft3Cs2Az9hy2DgTSHjGuoYvcsqCU6Ga6sK25b.png']);
        Photo::create(['id'=>'9', 'suggestion_id'=>'3','file'=>'suggestions/saRApHt3cBy7SdmRJ9F6fMbX3bM7BHB2xqCSmsdU.jpeg']);

        Suggestions::create(['id'=>'4', 'company_id'=>'2','title'=>'ЖК Avalon Zelena Street', 'movie'=>'<p><iframe src="//www.youtube.com/embed/JW5C9LNgxe0" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>', 'slug'=>'zhk-stozhari123' , 'avatar'=>'suggestions/ЖК Avalon Zelena Street, Львів — Квартири в новобудовах — ЛУН 2019-01-23 21-15-04.png', 'price'=>'12300', 'address'=>'вул. Івасюка (поруч ТЦ ЕПІЦЕНТР)' , 'content'=>'<p>Тільки уявіть, яким співочим буде ваше життя у &ldquo;Стожарах&rdquo;, що на вул. Івасюка.</p>
<p><em>Так чом, стожари, ви знов заходите за хмари,</em><br /><em>Коли я з вами і мрію, і живу.</em></p>
<p>Дев&rsquo;ять поверхів майже достатньо, щоби заходити за хмари. Але крім романтики комплекс має суттєві реальні переваги: тут безпечний, закритий двір під охороною, броньовані двері та розвинена інфраструктура. Сюди приємно буде повертатись навіть вночі &ndash; територія будинку освітлюється LED-лампами, а місця для паркування вистачить усім.</p>
<p>Сучасне планування дозволить втілити у життя найсміливіші інтер&rsquo;єрні рішення, а швидкісні безшумні ліфти &ndash; зберегти затишок та спокій мешканців у будинку.</p>.','created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Photo::create(['id'=>'10', 'suggestion_id'=>'4','file'=>'suggestions/4o3eA5Nc3x8XyyTgz7XNA4UkzMDkiptMJpKn1e0v.png']);
        Photo::create(['id'=>'11', 'suggestion_id'=>'4','file'=>'suggestions/bE6ft3Cs2Az9hy2DgTSHjGuoYvcsqCU6Ga6sK25b.png']);
        Photo::create(['id'=>'12', 'suggestion_id'=>'4','file'=>'suggestions/saRApHt3cBy7SdmRJ9F6fMbX3bM7BHB2xqCSmsdU.jpeg']);

        Suggestions::create(['id'=>'5', 'company_id'=>'2','title'=>'ЖК Avalon Zelena Street ЧЕРГА IV', 'movie'=>'<p><iframe src="//www.youtube.com/embed/JW5C9LNgxe0" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>', 'slug'=>'zhk-stozhari123' , 'avatar'=>'suggestions/ЖК Avalon Zelena Street, Львів — Квартири в новобудовах — ЛУН 2019-01-23 21-16-11.png', 'price'=>'12300', 'address'=>'вул. Івасюка (поруч ТЦ ЕПІЦЕНТР)' , 'content'=>'<p>Тільки уявіть, яким співочим буде ваше життя у &ldquo;Стожарах&rdquo;, що на вул. Івасюка.</p>
<p><em>Так чом, стожари, ви знов заходите за хмари,</em><br /><em>Коли я з вами і мрію, і живу.</em></p>
<p>Дев&rsquo;ять поверхів майже достатньо, щоби заходити за хмари. Але крім романтики комплекс має суттєві реальні переваги: тут безпечний, закритий двір під охороною, броньовані двері та розвинена інфраструктура. Сюди приємно буде повертатись навіть вночі &ndash; територія будинку освітлюється LED-лампами, а місця для паркування вистачить усім.</p>
<p>Сучасне планування дозволить втілити у життя найсміливіші інтер&rsquo;єрні рішення, а швидкісні безшумні ліфти &ndash; зберегти затишок та спокій мешканців у будинку.</p>.','created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Photo::create(['id'=>'13', 'suggestion_id'=>'5','file'=>'suggestions/4o3eA5Nc3x8XyyTgz7XNA4UkzMDkiptMJpKn1e0v.png']);
        Photo::create(['id'=>'14', 'suggestion_id'=>'5','file'=>'suggestions/bE6ft3Cs2Az9hy2DgTSHjGuoYvcsqCU6Ga6sK25b.png']);
        Photo::create(['id'=>'15', 'suggestion_id'=>'5','file'=>'suggestions/saRApHt3cBy7SdmRJ9F6fMbX3bM7BHB2xqCSmsdU.jpeg']);

        Suggestions::create(['id'=>'6', 'company_id'=>'3','title'=>'ЖК Avalon Zelena Street ЧЕРГА IV', 'movie'=>'<p><iframe src="//www.youtube.com/embed/JW5C9LNgxe0" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>', 'slug'=>'zhk-stozhari123' , 'avatar'=>'suggestions/86842181c73fc60609937e13b4b5b19d.jpg', 'price'=>'12300', 'address'=>'вул. Івасюка (поруч ТЦ ЕПІЦЕНТР)' , 'content'=>'<p>Тільки уявіть, яким співочим буде ваше життя у &ldquo;Стожарах&rdquo;, що на вул. Івасюка.</p>
<p><em>Так чом, стожари, ви знов заходите за хмари,</em><br /><em>Коли я з вами і мрію, і живу.</em></p>
<p>Дев&rsquo;ять поверхів майже достатньо, щоби заходити за хмари. Але крім романтики комплекс має суттєві реальні переваги: тут безпечний, закритий двір під охороною, броньовані двері та розвинена інфраструктура. Сюди приємно буде повертатись навіть вночі &ndash; територія будинку освітлюється LED-лампами, а місця для паркування вистачить усім.</p>
<p>Сучасне планування дозволить втілити у життя найсміливіші інтер&rsquo;єрні рішення, а швидкісні безшумні ліфти &ndash; зберегти затишок та спокій мешканців у будинку.</p>.','created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Photo::create(['id'=>'16', 'suggestion_id'=>'6','file'=>'suggestions/4o3eA5Nc3x8XyyTgz7XNA4UkzMDkiptMJpKn1e0v.png']);
        Photo::create(['id'=>'17', 'suggestion_id'=>'6','file'=>'suggestions/bE6ft3Cs2Az9hy2DgTSHjGuoYvcsqCU6Ga6sK25b.png']);
        Photo::create(['id'=>'18', 'suggestion_id'=>'6','file'=>'suggestions/saRApHt3cBy7SdmRJ9F6fMbX3bM7BHB2xqCSmsdU.jpeg']);

        Suggestions::create(['id'=>'7', 'company_id'=>'4','title'=>'ЖК Avalon Zelena Street ЧЕРГА IV', 'movie'=>'<p><iframe src="//www.youtube.com/embed/JW5C9LNgxe0" width="560" height="314" allowfullscreen="allowfullscreen"></iframe></p>', 'slug'=>'zhk-stozhari123' , 'avatar'=>'suggestions/Головна - ВАМБУД 2019-01-23 18-44-02.png', 'price'=>'12300', 'address'=>'вул. Івасюка (поруч ТЦ ЕПІЦЕНТР)' , 'content'=>'<p>Тільки уявіть, яким співочим буде ваше життя у &ldquo;Стожарах&rdquo;, що на вул. Івасюка.</p>
<p><em>Так чом, стожари, ви знов заходите за хмари,</em><br /><em>Коли я з вами і мрію, і живу.</em></p>
<p>Дев&rsquo;ять поверхів майже достатньо, щоби заходити за хмари. Але крім романтики комплекс має суттєві реальні переваги: тут безпечний, закритий двір під охороною, броньовані двері та розвинена інфраструктура. Сюди приємно буде повертатись навіть вночі &ndash; територія будинку освітлюється LED-лампами, а місця для паркування вистачить усім.</p>
<p>Сучасне планування дозволить втілити у життя найсміливіші інтер&rsquo;єрні рішення, а швидкісні безшумні ліфти &ndash; зберегти затишок та спокій мешканців у будинку.</p>.','created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Photo::create(['id'=>'19', 'suggestion_id'=>'7','file'=>'suggestions/4o3eA5Nc3x8XyyTgz7XNA4UkzMDkiptMJpKn1e0v.png']);
        Photo::create(['id'=>'20', 'suggestion_id'=>'7','file'=>'suggestions/bE6ft3Cs2Az9hy2DgTSHjGuoYvcsqCU6Ga6sK25b.png']);
        Photo::create(['id'=>'21', 'suggestion_id'=>'7','file'=>'suggestions/saRApHt3cBy7SdmRJ9F6fMbX3bM7BHB2xqCSmsdU.jpeg']);
    }

}
