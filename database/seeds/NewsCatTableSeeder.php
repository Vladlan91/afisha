<?php

use Illuminate\Database\Seeder;
use App\Entity\NewsCat;

class NewsCatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NewsCat::create(['id'=>'1', 'title'=>'Усі новини','created_at'=>'2019-01-30 10:07:28','updated_at'=>'2019-01-30 10:07:28' ]);
        NewsCat::create(['id'=>'2', 'title'=>'Варто переглянути','created_at'=>'2019-01-30 10:07:28','updated_at'=>'2019-01-30 10:07:28' ]);
        NewsCat::create(['id'=>'3', 'title'=>'Новини краю','created_at'=>'2019-01-30 10:07:28','updated_at'=>'2019-01-30 10:07:28' ]);
        NewsCat::create(['id'=>'4', 'title'=>'Бізнес та податки','created_at'=>'2019-01-30 10:07:28','updated_at'=>'2019-01-30 10:07:28' ]);
        NewsCat::create(['id'=>'5', 'title'=>'Освіта і наука','created_at'=>'2019-01-30 10:07:28','updated_at'=>'2019-01-30 10:07:28' ]);
        NewsCat::create(['id'=>'6', 'title'=>'Культура та мистецтво','created_at'=>'2019-01-30 10:07:28','updated_at'=>'2019-01-30 10:07:28' ]);
        NewsCat::create(['id'=>'7', 'title'=>'Здоров’я і спорт','created_at'=>'2019-01-30 10:07:28','updated_at'=>'2019-01-30 10:07:28' ]);
        NewsCat::create(['id'=>'8', 'title'=>'Кримінал','created_at'=>'2019-01-30 10:07:28','updated_at'=>'2019-01-30 10:07:28' ]);
        NewsCat::create(['id'=>'9', 'title'=>'Надзвичайні події','created_at'=>'2019-01-30 10:07:28','updated_at'=>'2019-01-30 10:07:28' ]);
    }
}
