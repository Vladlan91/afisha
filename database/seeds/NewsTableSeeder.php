<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Entity\News::create(['id'=>'1', 'title'=>'Усі новини', 'category_id'=>'1', 'slug'=>'', 'avatar'=>'',  'view'=>'1200', 'content'=>'','created_at'=>'2019-01-30 10:07:28','updated_at'=>'2019-01-30 10:07:28' ]);
    }
}
