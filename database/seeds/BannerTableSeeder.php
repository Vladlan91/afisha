<?php

use Illuminate\Database\Seeder;
use App\Entity\Banner\Banner;

class BannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banner::create(['id'=>'1', 'user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0' , 'cost'=>'1000' ,'url'=>'https://inhous.com.ua', 'format'=>'820x143' , 'file'=>'banners/banner7.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
        Banner::create(['id'=>'2', 'user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0' , 'cost'=>'1000' , 'url'=>'https://inhous.com.ua', 'format'=>'820x143' , 'file'=>'banners/banner9.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
        Banner::create(['id'=>'3', 'user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0' , 'cost'=>'1000' , 'url'=>'https://inhous.com.ua', 'format'=>'820x143' , 'file'=>'banners/banner8.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
        Banner::create(['id'=>'4', 'user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0' , 'cost'=>'1000' , 'url'=>'https://inhous.com.ua', 'format'=>'820x143' , 'file'=>'banners/banner5.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
        Banner::create(['id'=>'5', 'user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0' , 'cost'=>'1000' , 'url'=>'https://inhous.com.ua', 'format'=>'820x143' , 'file'=>'banners/banner10.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
        Banner::create(['id'=>'6', 'user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0' , 'cost'=>'1000' , 'url'=>'https://inhous.com.ua', 'format'=>'820x143' , 'file'=>'banners/OfNohsxhvvSMUiSCfUkfVI8rhyQI9hnAaz2zFCnb.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
        Banner::create(['id'=>'7', 'user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0' , 'cost'=>'1000' , 'url'=>'https://inhous.com.ua', 'format'=>'240x440' , 'file'=>'banners/banner1.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
        Banner::create(['id'=>'8', 'user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0' , 'cost'=>'1000' , 'url'=>'https://inhous.com.ua', 'format'=>'240x440' , 'file'=>'banners/banner4.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
        Banner::create(['id'=>'9', 'user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0' , 'cost'=>'1000' , 'url'=>'https://inhous.com.ua', 'format'=>'240x440' , 'file'=>'banners/banner6.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
        Banner::create(['id'=>'10','user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0' , 'cost'=>'1000' , 'url'=>'https://inhous.com.ua', 'format'=>'240x440' , 'file'=>'banners/SPNW9wjArUalCLT87N4nLeVJ8ayOqrItC3MTVCYQ.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
        Banner::create(['id'=>'11', 'user_id'=>'20000', 'category_id'=>'1', 'region_id'=>'1','name'=>'VAMBUD', 'views'=>'0' , 'limit'=>'1000' , 'clicks'=>'0', 'cost'=>'1000' , 'url'=>'https://inhous.com.ua', 'format'=>'240x440' , 'file'=>'banners/yDIhUYgiylhEf3wB8HrlWQF5CqkTtXqKbfFBOFWE.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28' , 'published_at'=>'2019-01-13 11:51:28'   ]);
    }
}
