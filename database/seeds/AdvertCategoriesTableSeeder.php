<?php

use App\Entity\Adverts\Category;
use Illuminate\Database\Seeder;

class AdvertCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(Category::class, 10)->create()->each(function (Category $category){
//            $counts = [0, random_int(3, 7)];
//            $category->children()->saveMany(factory(Category::class, $counts[array_rand($counts)])->create()->each(function (Category $category){
//                $counts = [0, random_int(3, 7)];
//                $category->children()->saveMany(factory(Category::class, $counts[array_rand($counts)])->create());
//            }));
//        });


        Category::create(['id'=>'1', 'name'=>'Нерухомість','slug'=>'maiores-i','parent_id'=>'0' ]);
            Category::create(['id'=>'13', 'name'=>'Квартири','slug'=>'ma-incunt23','parent_id'=>'1' ]);
                Category::create(['id'=>'19', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'13' ]);
                Category::create(['id'=>'20', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'13' ]);
                Category::create(['id'=>'21', 'name'=>'Здам','slug'=>'ma-incusdfnt43','parent_id'=>'13' ]);
                Category::create(['id'=>'22', 'name'=>'Здам подобово','slug'=>'ma-incusdfnt43','parent_id'=>'13' ]);
                Category::create(['id'=>'23', 'name'=>'Візьму на квартиру','slug'=>'ma-incusdfnt43','parent_id'=>'13' ]);
                Category::create(['id'=>'24', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'13' ]);
                Category::create(['id'=>'25', 'name'=>'Міняю','slug'=>'ma-incusdfnt43','parent_id'=>'13' ]);

            Category::create(['id'=>'14', 'name'=>'Будинки','slug'=>'ma-incunt43','parent_id'=>'1' ]);
                Category::create(['id'=>'26', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'14' ]);
                Category::create(['id'=>'27', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'14' ]);
                Category::create(['id'=>'28', 'name'=>'Здам','slug'=>'ma-incusdfnt43','parent_id'=>'14' ]);
                Category::create(['id'=>'29', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'14' ]);
                Category::create(['id'=>'30', 'name'=>'Міняю','slug'=>'ma-incusdfnt43','parent_id'=>'14' ]);

            Category::create(['id'=>'15', 'name'=>'Земельні ділянки','slug'=>'ma-incunt32','parent_id'=>'1' ]);
                Category::create(['id'=>'31', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'15' ]);
                Category::create(['id'=>'32', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'15' ]);
                Category::create(['id'=>'33', 'name'=>'Здам','slug'=>'ma-incusdfnt43','parent_id'=>'15' ]);
                Category::create(['id'=>'34', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'15' ]);
                Category::create(['id'=>'35', 'name'=>'Міняю','slug'=>'ma-incusdfnt43','parent_id'=>'15' ]);

            Category::create(['id'=>'16', 'name'=>'Комерційна нерухомість','slug'=>'ma-incunt13','parent_id'=>'1' ]);
                Category::create(['id'=>'36', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'16' ]);
                Category::create(['id'=>'37', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'16' ]);
                Category::create(['id'=>'38', 'name'=>'Здам','slug'=>'ma-incusdfnt43','parent_id'=>'16' ]);
                Category::create(['id'=>'39', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'16' ]);
                Category::create(['id'=>'40', 'name'=>'Міняю','slug'=>'ma-incusdfnt43','parent_id'=>'16' ]);

            Category::create(['id'=>'17', 'name'=>'Кіоски','slug'=>'ma-incunt11','parent_id'=>'1' ]);
                Category::create(['id'=>'41', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'17' ]);
                Category::create(['id'=>'42', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'17' ]);
                Category::create(['id'=>'43', 'name'=>'Здам','slug'=>'ma-incusdfnt43','parent_id'=>'17' ]);
                Category::create(['id'=>'44', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'17' ]);
                Category::create(['id'=>'45', 'name'=>'Міняю','slug'=>'ma-incusdfnt43','parent_id'=>'17' ]);

            Category::create(['id'=>'18', 'name'=>'Гаражі','slug'=>'ma-incunt12','parent_id'=>'1' ]);
                Category::create(['id'=>'46', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'18' ]);
                Category::create(['id'=>'47', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'18' ]);
                Category::create(['id'=>'48', 'name'=>'Здам','slug'=>'ma-incusdfnt43','parent_id'=>'18' ]);
                Category::create(['id'=>'49', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'18' ]);
                Category::create(['id'=>'50', 'name'=>'Міняю','slug'=>'ma-incusdfnt43','parent_id'=>'18' ]);

        Category::create(['id'=>'2', 'name'=>'Будівництво та ремонт','slug'=>'maiores-in','parent_id'=>'0' ]);
            Category::create(['id'=>'51', 'name'=>'Цегла, камінь, будівельні блоки, з/б вироби','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'60', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'51' ]);
                Category::create(['id'=>'61', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'51' ]);

            Category::create(['id'=>'52', 'name'=>'Покрівля, ізоляція, утеплення','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'62', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'52' ]);
                Category::create(['id'=>'63', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'52' ]);

            Category::create(['id'=>'53', 'name'=>'Лако-фарбові вироби, будівельні суміші і розчини','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'64', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'53' ]);
                Category::create(['id'=>'65', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'53' ]);

            Category::create(['id'=>'54', 'name'=>'Пиломатеріали, вагонка','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'66', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'54' ]);
                Category::create(['id'=>'67', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'54' ]);

            Category::create(['id'=>'55', 'name'=>'Сипучі матеріали','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'68', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'55' ]);
                Category::create(['id'=>'69', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'55' ]);

            Category::create(['id'=>'56', 'name'=>'Інші матеріали','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'70', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'56' ]);
                Category::create(['id'=>'71', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'56' ]);

            Category::create(['id'=>'57', 'name'=>'Вікна, двері, балкони, жалюзі','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'72', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'57' ]);
                Category::create(['id'=>'73', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'57' ]);

            Category::create(['id'=>'58', 'name'=>'Брами, автоматика, огорожа','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'74', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'58' ]);
                Category::create(['id'=>'75', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'58' ]);

            Category::create(['id'=>'228', 'name'=>'Ковані вироби, каміни','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'237', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'228' ]);
                Category::create(['id'=>'238', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'228' ]);

            Category::create(['id'=>'229', 'name'=>'Металопрокат, брухт','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'239', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'229' ]);
                Category::create(['id'=>'240', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'229' ]);

            Category::create(['id'=>'230', 'name'=>'Електрика та електрообладнання','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'241', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'230' ]);
                Category::create(['id'=>'242', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'230' ]);

            Category::create(['id'=>'231', 'name'=>'Сантехніка, опалення','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'243', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'231' ]);
                Category::create(['id'=>'244', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'231' ]);

            Category::create(['id'=>'232', 'name'=>'Вентиляція, кондиціювання','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'245', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'232' ]);
                Category::create(['id'=>'246', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'232' ]);

            Category::create(['id'=>'233', 'name'=>'Будівельні інструменти та обладнання','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'247', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'233' ]);
                Category::create(['id'=>'248', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'233' ]);
                Category::create(['id'=>'249', 'name'=>'Здам в оренду','slug'=>'ma-incunt12','parent_id'=>'233' ]);

            Category::create(['id'=>'234', 'name'=>'Деревообробні інструменти та обладнання','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'250', 'name'=>'Продам','slug'=>'ma-incunt12','parent_id'=>'234' ]);
                Category::create(['id'=>'251', 'name'=>'Куплю','slug'=>'ma-incunt12','parent_id'=>'234' ]);

            Category::create(['id'=>'235', 'name'=>'Будівельні та ремонтні роботи','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'252', 'name'=>'Пропоную','slug'=>'ma-incunt12','parent_id'=>'235' ]);
                Category::create(['id'=>'253', 'name'=>'Замовлю','slug'=>'ma-incunt12','parent_id'=>'235' ]);

            Category::create(['id'=>'236', 'name'=>'Столярні роботи','slug'=>'ma-incunt12','parent_id'=>'2' ]);
                Category::create(['id'=>'254', 'name'=>'Пропоную','slug'=>'ma-incunt12','parent_id'=>'236' ]);
                Category::create(['id'=>'255', 'name'=>'Замовлю','slug'=>'ma-incunt12','parent_id'=>'236' ]);


        Category::create(['id'=>'3', 'name'=>'Автоафіша','slug'=>'maiores-inc','parent_id'=>'0' ]);
            Category::create(['id'=>'78', 'name'=>'Легкові автомобілі','slug'=>'maiores-inc','parent_id'=>'3' ]);
                Category::create(['id'=>'87', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'78' ]);
                Category::create(['id'=>'88', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'78' ]);
                Category::create(['id'=>'89', 'name'=>'Здам в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'78' ]);
                Category::create(['id'=>'90', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'78' ]);

            Category::create(['id'=>'79', 'name'=>'Вантажні автомобілі','slug'=>'maiores-inc','parent_id'=>'3' ]);
                Category::create(['id'=>'91', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'79' ]);
                Category::create(['id'=>'92', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'79' ]);
                Category::create(['id'=>'93', 'name'=>'Здам в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'79' ]);
                Category::create(['id'=>'94', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'79' ]);

            Category::create(['id'=>'80', 'name'=>'Автобуси, мікроавтобуси','slug'=>'maiores-inc','parent_id'=>'3' ]);
                Category::create(['id'=>'95', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'80' ]);
                Category::create(['id'=>'96', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'80' ]);
                Category::create(['id'=>'97', 'name'=>'Здам в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'80' ]);
                Category::create(['id'=>'98', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'80' ]);

            Category::create(['id'=>'81', 'name'=>'Спецтехніка','slug'=>'maiores-inc','parent_id'=>'3' ]);
                Category::create(['id'=>'99', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'81' ]);
                Category::create(['id'=>'100', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'81' ]);
                Category::create(['id'=>'101', 'name'=>'Здам в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'81' ]);
                Category::create(['id'=>'102', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'81' ]);

            Category::create(['id'=>'82', 'name'=>'Вело-мото техніка','slug'=>'maiores-inc','parent_id'=>'3' ]);
                Category::create(['id'=>'103', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'82' ]);
                Category::create(['id'=>'104', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'82' ]);

            Category::create(['id'=>'83', 'name'=>'Причепи','slug'=>'maiores-inc','parent_id'=>'3' ]);
                Category::create(['id'=>'105', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'83' ]);
                Category::create(['id'=>'106', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'83' ]);
                Category::create(['id'=>'107', 'name'=>'Здам в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'83' ]);
                Category::create(['id'=>'108', 'name'=>'Візьму в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'83' ]);

            Category::create(['id'=>'84', 'name'=>'Документи','slug'=>'maiores-inc','parent_id'=>'3' ]);
                Category::create(['id'=>'109', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'84' ]);
                Category::create(['id'=>'110', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'84' ]);

            Category::create(['id'=>'85', 'name'=>'Автозапчастини','slug'=>'maiores-inc','parent_id'=>'3' ]);
                Category::create(['id'=>'111', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'85' ]);
                Category::create(['id'=>'112', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'85' ]);

            Category::create(['id'=>'86', 'name'=>'Ремонт автомобілів','slug'=>'maiores-inc','parent_id'=>'3' ]);
                Category::create(['id'=>'113', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'86' ]);
                Category::create(['id'=>'114', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'86' ]);

            Category::create(['id'=>'115', 'name'=>'Транспортні послуги','slug'=>'maiores-inc','parent_id'=>'3' ]);
                Category::create(['id'=>'116', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'115' ]);
                Category::create(['id'=>'117', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'115' ]);

        Category::create(['id'=>'4', 'name'=>'Працевлаштування','slug'=>'maiores-incu','parent_id'=>'0' ]);
            Category::create(['id'=>'118', 'name'=>'Пропоную роботу','slug'=>'maiores-inc','parent_id'=>'4' ]);
                Category::create(['id'=>'120', 'name'=>'Керівники, помічники керівників','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'121', 'name'=>'Бухгалтери, економісти','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'122', 'name'=>'Юристи, правознавці','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'123', 'name'=>'Медики, фармацевти','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'124', 'name'=>'Педагоги, психологи','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'125', 'name'=>'Перекладачі','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'126', 'name'=>'Працівники ЗМІ, журналісти, рекламісти','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'127', 'name'=>'Працівники культури і мистецтва','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'128', 'name'=>'Комп’ютерні спеціалісти','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'129', 'name'=>'Інженери, архітектори, технічні спеціалісти','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'130', 'name'=>'Офісний персонал, секретарі','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'131', 'name'=>'Менеджери, ріелтери','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'132', 'name'=>'Торговий персонал','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'133', 'name'=>'Продавці, касири','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'134', 'name'=>'Водії, експедитори, кур’єри','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'135', 'name'=>'Будівельники','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'136', 'name'=>'Робітники','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'137', 'name'=>'Вантажники, підсобники','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'138', 'name'=>'Охоронці','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'139', 'name'=>'Доглядальники і прибиральники','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'140', 'name'=>'Кухарі, працівники кухні','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'141', 'name'=>'Офіціанти, бармени, баристи','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'142', 'name'=>'Посудомийниці, прибиральниці, покоївки','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'143', 'name'=>'Робота для студентів, додатковий заробіток','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'144', 'name'=>'Перукарі, косметологи','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'145', 'name'=>'Робота за кордоном','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);
                Category::create(['id'=>'146', 'name'=>'Різне','slug'=>'ma-incusdfnt43','parent_id'=>'118' ]);


            Category::create(['id'=>'119', 'name'=>'Шукаю роботу','slug'=>'ma-incusdfnt43','parent_id'=>'4' ]);
                Category::create(['id'=>'147', 'name'=>'Керівники, помічники керівників','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'148', 'name'=>'Бухгалтери, економісти','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'149', 'name'=>'Юристи, правознавці','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'150', 'name'=>'Медики, фармацевти','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'151', 'name'=>'Педагоги, психологи','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'152', 'name'=>'Перекладачі','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'153', 'name'=>'Працівники ЗМІ, журналісти, рекламісти','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'154', 'name'=>'Працівники культури і мистецтва','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'155', 'name'=>'Комп’ютерні спеціалісти','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'156', 'name'=>'Інженери, архітектори, технічні спеціалісти','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'157', 'name'=>'Офісний персонал, секретарі','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'158', 'name'=>'Менеджери, ріелтери','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'159', 'name'=>'Торговий персонал','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'160', 'name'=>'Продавці, касири','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'161', 'name'=>'Водії, експедитори, кур’єри','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'162', 'name'=>'Будівельники','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'163', 'name'=>'Робітники','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'164', 'name'=>'Вантажники, підсобники','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'165', 'name'=>'Охоронці','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'166', 'name'=>'Доглядальники і прибиральники','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'167', 'name'=>'Кухарі, працівники кухні','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'168', 'name'=>'Офіціанти, бармени, баристи','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'169', 'name'=>'Посудомийниці, прибиральниці, покоївки','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'170', 'name'=>'Робота для студентів, додатковий заробіток','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'171', 'name'=>'Перукарі, косметологи','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'172', 'name'=>'Робота за кордоном','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);
                Category::create(['id'=>'173', 'name'=>'Різне','slug'=>'ma-incusdfnt43','parent_id'=>'119' ]);


        Category::create(['id'=>'5', 'name'=>'Бюро та послуги','slug'=>'maiores-incun','parent_id'=>'0' ]);
            Category::create(['id'=>'174', 'name'=>'Візова підтримка','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'192', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'174' ]);
                Category::create(['id'=>'193', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'174' ]);

            Category::create(['id'=>'175', 'name'=>'Медичні та косметичні послуги','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'194', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'175' ]);
                Category::create(['id'=>'195', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'175' ]);

            Category::create(['id'=>'176', 'name'=>'Фото- відеопослуги','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'196', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'176' ]);
                Category::create(['id'=>'197', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'176' ]);


            Category::create(['id'=>'177', 'name'=>'Прибирання','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'198', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'177' ]);
                Category::create(['id'=>'199', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'177' ]);


            Category::create(['id'=>'178', 'name'=>'Поліграфічні, рекламні та інформаційні послуги','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'200', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'178' ]);
                Category::create(['id'=>'201', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'178' ]);


            Category::create(['id'=>'179', 'name'=>'Організація святкувань','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'202', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'179' ]);
                Category::create(['id'=>'203', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'179' ]);


            Category::create(['id'=>'180', 'name'=>'Навчання, курси, переклади, тренінги','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'204', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'180' ]);
                Category::create(['id'=>'205', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'180' ]);


            Category::create(['id'=>'181', 'name'=>'Програмування','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'206', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'181' ]);
                Category::create(['id'=>'207', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'181' ]);


            Category::create(['id'=>'182', 'name'=>'Юридичні та консалтингові послуги','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'208', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'182' ]);
                Category::create(['id'=>'209', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'182' ]);


            Category::create(['id'=>'183', 'name'=>'Фінансові послуги та аудит','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'210', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'183' ]);
                Category::create(['id'=>'211', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'183' ]);


            Category::create(['id'=>'184', 'name'=>'Послуги дизайнера','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'212', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'184' ]);
                Category::create(['id'=>'213', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'184' ]);


            Category::create(['id'=>'185', 'name'=>'Ремонт і обслуговування техніки та обладнання','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'214', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'185' ]);
                Category::create(['id'=>'215', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'185' ]);


            Category::create(['id'=>'186', 'name'=>'Охорона, безпека','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'216', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'186' ]);
                Category::create(['id'=>'217', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'186' ]);


            Category::create(['id'=>'187', 'name'=>'Рукоділля','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'218', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'187' ]);
                Category::create(['id'=>'219', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'187' ]);


            Category::create(['id'=>'188', 'name'=>'Ритуальні послуги','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'220', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'188' ]);
                Category::create(['id'=>'221', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'188' ]);


            Category::create(['id'=>'189', 'name'=>'Інші послуги','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'222', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'189' ]);
                Category::create(['id'=>'223', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'189' ]);


            Category::create(['id'=>'190', 'name'=>'Знайомства','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'224', 'name'=>'Жінки','slug'=>'ma-incusdfnt43','parent_id'=>'190' ]);
                Category::create(['id'=>'225', 'name'=>'Чоловіки','slug'=>'ma-incusdfnt43','parent_id'=>'190' ]);


            Category::create(['id'=>'191', 'name'=>'Загублене-знайдене','slug'=>'maiores-incun','parent_id'=>'5' ]);
                Category::create(['id'=>'226', 'name'=>'Загублене','slug'=>'ma-incusdfnt43','parent_id'=>'191' ]);
                Category::create(['id'=>'227', 'name'=>'Знайдене','slug'=>'ma-incusdfnt43','parent_id'=>'191' ]);




        Category::create(['id'=>'6', 'name'=>'Туризм, спорт, відпочинок','slug'=>'maiore-incunt','parent_id'=>'0' ]);
            Category::create(['id'=>'256', 'name'=>'Житло для відпочинку','slug'=>'ma-incunt12','parent_id'=>'6' ]);
                Category::create(['id'=>'260', 'name'=>'Здам','slug'=>'ma-incusdfnt43','parent_id'=>'256' ]);
                Category::create(['id'=>'261', 'name'=>'Винайму','slug'=>'ma-incusdfnt43','parent_id'=>'256' ]);


            Category::create(['id'=>'257', 'name'=>'Організація відпочинку','slug'=>'ma-incunt12','parent_id'=>'6' ]);
                Category::create(['id'=>'262', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'257' ]);
                Category::create(['id'=>'263', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'257' ]);


            Category::create(['id'=>'258', 'name'=>'Спорттовари','slug'=>'ma-incunt12','parent_id'=>'6' ]);
                Category::create(['id'=>'264', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'258' ]);
                Category::create(['id'=>'265', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'258' ]);


            Category::create(['id'=>'259', 'name'=>'Інше','slug'=>'ma-incunt12','parent_id'=>'6' ]);
                Category::create(['id'=>'266', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'259' ]);
                Category::create(['id'=>'267', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'259' ]);


        Category::create(['id'=>'7', 'name'=>'Техніка та обладнання','slug'=>'maior-incunt','parent_id'=>'0' ]);
            Category::create(['id'=>'268', 'name'=>'Телефони','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'284', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'268' ]);
                Category::create(['id'=>'285', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'268' ]);

            Category::create(['id'=>'269', 'name'=>'Супутникове телебачення, інтернет','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'286', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'269' ]);
                Category::create(['id'=>'287', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'269' ]);


            Category::create(['id'=>'270', 'name'=>'Телевізори','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'288', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'270' ]);
                Category::create(['id'=>'289', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'270' ]);


            Category::create(['id'=>'271', 'name'=>'Фото- та відеотовари','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'290', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'271' ]);
                Category::create(['id'=>'291', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'271' ]);


            Category::create(['id'=>'272', 'name'=>'Аудіоапаратура','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'292', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'272' ]);
                Category::create(['id'=>'293', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'272' ]);


            Category::create(['id'=>'273', 'name'=>'Музичні інструменти','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'294', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'273' ]);
                Category::create(['id'=>'295', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'273' ]);


            Category::create(['id'=>'274', 'name'=>'Промислове обладнання','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'296', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'274' ]);
                Category::create(['id'=>'297', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'274' ]);


            Category::create(['id'=>'275', 'name'=>'Торговельне обладнання','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'298', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'275' ]);
                Category::create(['id'=>'299', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'275' ]);


            Category::create(['id'=>'276', 'name'=>'Швейне обладнання','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'300', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'276' ]);
                Category::create(['id'=>'301', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'276' ]);


            Category::create(['id'=>'277', 'name'=>'Пральні машинки','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'302', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'277' ]);
                Category::create(['id'=>'303', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'277' ]);


            Category::create(['id'=>'278', 'name'=>'Плити, витяжки','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'304', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);
                Category::create(['id'=>'305', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);


            Category::create(['id'=>'279', 'name'=>'Холодильне обладнання','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'306', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);
                Category::create(['id'=>'307', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);


            Category::create(['id'=>'280', 'name'=>'Побутова техніка, електротовари','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'308', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);
                Category::create(['id'=>'309', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);


            Category::create(['id'=>'281', 'name'=>'Прилади і механізми','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'310', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);
                Category::create(['id'=>'311', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);


            Category::create(['id'=>'282', 'name'=>'Інше','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'312', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);
                Category::create(['id'=>'313', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);


            Category::create(['id'=>'283', 'name'=>'Комп\'ютерна техніка','slug'=>'ma-incusdfnt43','parent_id'=>'7' ]);
                Category::create(['id'=>'314', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);
                Category::create(['id'=>'315', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'278' ]);



        Category::create(['id'=>'8', 'name'=>'Меблі та предмети інтер’єру','slug'=>'maio-incunt','parent_id'=>'0' ]);
            Category::create(['id'=>'316', 'name'=>'Виготовлення  та ремонт меблів','slug'=>'ma-incusdfnt43','parent_id'=>'8' ]);
                Category::create(['id'=>'323', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'316' ]);
                Category::create(['id'=>'324', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'316' ]);

            Category::create(['id'=>'317', 'name'=>'Меблі','slug'=>'ma-incusdfnt43','parent_id'=>'8' ]);
                Category::create(['id'=>'325', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'317' ]);
                Category::create(['id'=>'326', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'317' ]);


            Category::create(['id'=>'318', 'name'=>'Меблева фурнітура','slug'=>'ma-incusdfnt43','parent_id'=>'8' ]);
                Category::create(['id'=>'327', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'318' ]);
                Category::create(['id'=>'328', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'318' ]);


            Category::create(['id'=>'319', 'name'=>'Матраци','slug'=>'ma-incusdfnt43','parent_id'=>'8' ]);
                Category::create(['id'=>'329', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'319' ]);
                Category::create(['id'=>'330', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'319' ]);


            Category::create(['id'=>'320', 'name'=>'Предмети інтер\'єру та декору','slug'=>'ma-incusdfnt43','parent_id'=>'8' ]);
                Category::create(['id'=>'331', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'320' ]);
                Category::create(['id'=>'332', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'320' ]);


            Category::create(['id'=>'321', 'name'=>'Освітлення','slug'=>'ma-incusdfnt43','parent_id'=>'8' ]);
                Category::create(['id'=>'333', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'321' ]);
                Category::create(['id'=>'334', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'321' ]);


            Category::create(['id'=>'322', 'name'=>'Килими, килимове покриття','slug'=>'ma-incusdfnt43','parent_id'=>'8' ]);
                Category::create(['id'=>'335', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'322' ]);
                Category::create(['id'=>'336', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'322' ]);


        Category::create(['id'=>'9', 'name'=>'Все для побуту','slug'=>'mai-incunt','parent_id'=>'0' ]);
            Category::create(['id'=>'337', 'name'=>'Постіль, тканини, текстиль','slug'=>'ma-incusdfnt43','parent_id'=>'9' ]);
                Category::create(['id'=>'344', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'337' ]);
                Category::create(['id'=>'345', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'337' ]);

            Category::create(['id'=>'338', 'name'=>'Друкована продукція','slug'=>'ma-incusdfnt43','parent_id'=>'9' ]);
                Category::create(['id'=>'346', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'338' ]);
                Category::create(['id'=>'347', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'338' ]);


            Category::create(['id'=>'339', 'name'=>'Посуд, кухонні товари','slug'=>'ma-incusdfnt43','parent_id'=>'9' ]);
                Category::create(['id'=>'348', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'339' ]);
                Category::create(['id'=>'349', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'339' ]);


            Category::create(['id'=>'340', 'name'=>'Продукти харчування','slug'=>'ma-incusdfnt43','parent_id'=>'9' ]);
                Category::create(['id'=>'350', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'340' ]);
                Category::create(['id'=>'351', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'340' ]);


            Category::create(['id'=>'341', 'name'=>'Дача, сад, город','slug'=>'ma-incusdfnt43','parent_id'=>'9' ]);
                Category::create(['id'=>'352', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'341' ]);
                Category::create(['id'=>'353', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'341' ]);


            Category::create(['id'=>'342', 'name'=>'Тара і упаковка','slug'=>'ma-incusdfnt43','parent_id'=>'9' ]);
                Category::create(['id'=>'354', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'342' ]);
                Category::create(['id'=>'355', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'342' ]);


            Category::create(['id'=>'343', 'name'=>'Інше','slug'=>'ma-incusdfnt43','parent_id'=>'9' ]);
                Category::create(['id'=>'356', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'343' ]);
                Category::create(['id'=>'357', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'343' ]);


        Category::create(['id'=>'10', 'name'=>'Мода, стиль, краса','slug'=>'ma-incunt','parent_id'=>'0' ]);
            Category::create(['id'=>'358', 'name'=>'Постіль, тканини, текстиль','slug'=>'ma-incusdfnt43','parent_id'=>'10' ]);
                Category::create(['id'=>'364', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'358' ]);
                Category::create(['id'=>'365', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'358' ]);

            Category::create(['id'=>'359', 'name'=>'Друкована продукція','slug'=>'ma-incusdfnt43','parent_id'=>'10' ]);
                Category::create(['id'=>'366', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'359' ]);
                Category::create(['id'=>'367', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'359' ]);


            Category::create(['id'=>'360', 'name'=>'Посуд, кухонні товари','slug'=>'ma-incusdfnt43','parent_id'=>'10' ]);
                Category::create(['id'=>'368', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'360' ]);
                Category::create(['id'=>'369', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'360' ]);


            Category::create(['id'=>'361', 'name'=>'Продукти харчування','slug'=>'ma-incusdfnt43','parent_id'=>'10' ]);
                Category::create(['id'=>'370', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'361' ]);
                Category::create(['id'=>'371', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'361' ]);


            Category::create(['id'=>'362', 'name'=>'Дача, сад, город','slug'=>'ma-incusdfnt43','parent_id'=>'10' ]);
                Category::create(['id'=>'372', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'362' ]);
                Category::create(['id'=>'373', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'362' ]);


            Category::create(['id'=>'363', 'name'=>'Тара і упаковка','slug'=>'ma-incusdfnt43','parent_id'=>'10' ]);
                Category::create(['id'=>'374', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'363' ]);
                Category::create(['id'=>'375', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'363' ]);


        Category::create(['id'=>'11', 'name'=>'Дитячий світ','slug'=>'ma-incunt','parent_id'=>'0' ]);
            Category::create(['id'=>'376', 'name'=>'Дитячий одяг','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'390', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'376' ]);
                Category::create(['id'=>'391', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'376' ]);

            Category::create(['id'=>'377', 'name'=>'Дитяче взуття','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'392', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'377' ]);
                Category::create(['id'=>'393', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'377' ]);


            Category::create(['id'=>'378', 'name'=>'Візки','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'394', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'378' ]);
                Category::create(['id'=>'395', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'378' ]);


            Category::create(['id'=>'379', 'name'=>'Автокрісла, велокрісла','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'396', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'379' ]);
                Category::create(['id'=>'397', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'379' ]);


            Category::create(['id'=>'380', 'name'=>'Дитячий транспорт','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'398', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'380' ]);
                Category::create(['id'=>'399', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'380' ]);


            Category::create(['id'=>'381', 'name'=>'Іграшки','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'400', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'381' ]);
                Category::create(['id'=>'401', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'381' ]);


            Category::create(['id'=>'382', 'name'=>'Дитячий текстиль','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'402', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'382' ]);
                Category::create(['id'=>'403', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'382' ]);


            Category::create(['id'=>'383', 'name'=>'Дитячі меблі','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'404', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'383' ]);
                Category::create(['id'=>'405', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'383' ]);


            Category::create(['id'=>'384', 'name'=>'Догляд, гігієна','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'406', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'384' ]);
                Category::create(['id'=>'407', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'384' ]);


            Category::create(['id'=>'385', 'name'=>'Дитяче харчування, посуд','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'408', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'385' ]);
                Category::create(['id'=>'409', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'385' ]);


            Category::create(['id'=>'386', 'name'=>'Електротовари','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'410', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'386' ]);
                Category::create(['id'=>'411', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'386' ]);


            Category::create(['id'=>'387', 'name'=>'Товари для школи','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'412', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'387' ]);
                Category::create(['id'=>'413', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'387' ]);


            Category::create(['id'=>'388', 'name'=>'Спорттовари','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'414', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'388' ]);
                Category::create(['id'=>'415', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'388' ]);


            Category::create(['id'=>'389', 'name'=>'Інше','slug'=>'ma-incusdfnt43','parent_id'=>'11' ]);
                Category::create(['id'=>'416', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'389' ]);
                Category::create(['id'=>'417', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'389' ]);
                Category::create(['id'=>'418', 'name'=>'Здам в оренду','slug'=>'ma-incusdfnt43','parent_id'=>'389' ]);

        Category::create(['id'=>'12', 'name'=>'Зоокуточок та рослини','slug'=>'ma-incunt','parent_id'=>'0' ]);
            Category::create(['id'=>'419', 'name'=>'Тварини','slug'=>'ma-incusdfnt43','parent_id'=>'12' ]);
                Category::create(['id'=>'427', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'381' ]);
                Category::create(['id'=>'428', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'381' ]);


            Category::create(['id'=>'420', 'name'=>'Рослини','slug'=>'ma-incusdfnt43','parent_id'=>'12' ]);
                Category::create(['id'=>'429', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'420' ]);
                Category::create(['id'=>'430', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'420' ]);


            Category::create(['id'=>'421', 'name'=>'Комахи','slug'=>'ma-incusdfnt43','parent_id'=>'12' ]);
                Category::create(['id'=>'431', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'421' ]);
                Category::create(['id'=>'432', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'421' ]);


            Category::create(['id'=>'422', 'name'=>'Риби та все для акваріумів','slug'=>'ma-incusdfnt43','parent_id'=>'12' ]);
                Category::create(['id'=>'433', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'422' ]);
                Category::create(['id'=>'434', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'422' ]);


            Category::create(['id'=>'423', 'name'=>'Зоотовари та корми','slug'=>'ma-incusdfnt43','parent_id'=>'12' ]);
                Category::create(['id'=>'435', 'name'=>'Продам','slug'=>'ma-incusdfnt43','parent_id'=>'423' ]);
                Category::create(['id'=>'436', 'name'=>'Куплю','slug'=>'ma-incusdfnt43','parent_id'=>'423' ]);


            Category::create(['id'=>'424', 'name'=>'Послуги для тварин','slug'=>'ma-incusdfnt43','parent_id'=>'12' ]);
                Category::create(['id'=>'437', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'424' ]);
                Category::create(['id'=>'438', 'name'=>'Замовлю','slug'=>'ma-incusdfnt43','parent_id'=>'424' ]);


            Category::create(['id'=>'425', 'name'=>'Віддам в добрі руки','slug'=>'ma-incusdfnt43','parent_id'=>'12' ]);
                Category::create(['id'=>'439', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'425' ]);
                Category::create(['id'=>'440', 'name'=>'Шукаю','slug'=>'ma-incusdfnt43','parent_id'=>'425' ]);


            Category::create(['id'=>'426', 'name'=>'Інше','slug'=>'ma-incusdfnt43','parent_id'=>'12' ]);
                Category::create(['id'=>'441', 'name'=>'Пропоную','slug'=>'ma-incusdfnt43','parent_id'=>'426' ]);
                Category::create(['id'=>'442', 'name'=>'Шукаю','slug'=>'ma-incusdfnt43','parent_id'=>'426' ]);

    }
}
