<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['id'=>'20000', 'name'=>'Владислав','last_name'=>'Іваніцький','email'=>'ivanitskiyvv32@gmail.com', 'phone'=>'+38(096)-456-36-14', 'phone_auth'=>'1', 'phone_verified'=>'1', 'password'=>'9db06bcff9248837f86d1a6bcf41c9e7', 'avatar'=>'users/44389104_1930932467211951_4943574415020392448_n.jpg', 'created_at'=>'2019-01-13 11:51:28', 'updated_at'=>'2019-01-13 11:51:28', 'status'=>'activ', 'role'=>'admin', 'package'=>'gold']);
    }
}
