<?php

use Illuminate\Database\Seeder;
use App\Entity\Banner\Afisha;

class BannerAfishaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Afisha::create(['id'=>'1','name'=>'Авто','url'=>'https://inhous.com.ua' , 'file'=>'banners/banner.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Afisha::create(['id'=>'2','name'=>'Будівництво','url'=>'https://inhous.com.ua' , 'file'=>'banners/budivnyctvo.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Afisha::create(['id'=>'3','name'=>'Робота','url'=>'https://inhous.com.ua' , 'file'=>'banners/buro.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Afisha::create(['id'=>'4','name'=>'Діти','url'=>'https://inhous.com.ua' , 'file'=>'banners/dity.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Afisha::create(['id'=>'5','name'=>'Мода','url'=>'https://inhous.com.ua' , 'file'=>'banners/moda.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
        Afisha::create(['id'=>'6','name'=>'Нерухомість','url'=>'https://inhous.com.ua' , 'file'=>'banners/neruhomist.png' , 'status'=>'active' , 'created_at'=>'2019-01-13 11:51:28'  , 'updated_at'=>'2019-01-13 11:51:28']);
    }
}
