<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RegionsTableSeeder::class);
//        $this->call(AdvertCategoriesTableSeeder::class);
        $this->call(BusinessTableSeeder::class);
        $this->call(BannerTableSeeder::class);
        $this->call(CompanyTableSeeder::class);
        $this->call(AdvertsTableSeeder::class);
        $this->call(SuggestionTableSeeder::class);
        $this->call(BannerAfishaTableSeeder::class);
        $this->call(PageTableSeeder::class);
        $this->call(NewsCatTableSeeder::class);
        $this->call(NewsTableSeeder::class);
    }
}

