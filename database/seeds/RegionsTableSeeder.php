<?php

use App\Entity\Region;
use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(Region::class, 10)->create()->each(function (Region $region){
//            $region->children()->saveMany(factory(Region::class, random_int(3, 10))->create()->each(function (Region $region){
//                $region->children()->saveMany(factory(Region::class, random_int(3, 10))->make());
//            }));
//        });

        Region::create(['id'=>'1', 'name'=>'Івано-Франківськ','slug'=>'maiores-incidunt' ]);
        Region::create(['id'=>'2', 'name'=>'Калуш','slug'=>'deleniti-dolore' ]);
        Region::create(['id'=>'3', 'name'=>'Коломия','slug'=>'est-id']);
        Region::create(['id'=>'4', 'name'=>'Надвірна','slug'=>'enim-itaque-nesciunt' ]);
        Region::create(['id'=>'5', 'name'=>'Долина','slug'=>'nobis-est' ]);
        Region::create(['id'=>'6', 'name'=>'Бурштин','slug'=>'sequi-porro-dolores' ]);
        Region::create(['id'=>'7', 'name'=>'Болехів','slug'=>'dolor-est' ]);
        Region::create(['id'=>'8', 'name'=>'Снятин','slug'=>'accusamus-voluptatibus' ]);
        Region::create(['id'=>'9', 'name'=>'Тисмениця','slug'=>'rerum-velit-qui' ]);
        Region::create(['id'=>'10', 'name'=>'Городенка','slug'=>'fuga-est-enim' ]);
        Region::create(['id'=>'11', 'name'=>'Тлумач','slug'=>'alias-velit-quidem' ]);
        Region::create(['id'=>'12', 'name'=>'Косів','slug'=>'reprehenderit-voluptatibus' ]);
        Region::create(['id'=>'13', 'name'=>'Яремче','slug'=>'rerum-necessitatibus-excepturi' ]);
        Region::create(['id'=>'14', 'name'=>'Рогатин','slug'=>'aliquid-ut-dolore' ]);
        Region::create(['id'=>'15', 'name'=>'Галич','slug'=>'laudantium-repellendus']);




    }
}
