<?php

use Illuminate\Database\Seeder;
use App\Entity\Business\Business;

class BusinessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Business::create(['id'=>'1', 'name'=>'Нерухомість','slug'=>'masiores-i','parent_id'=>'0' ]);
        Business::create(['id'=>'2', 'name'=>'Авто','slug'=>'maiossres-in','parent_id'=>'0' ]);
        Business::create(['id'=>'3', 'name'=>'Електроніка, електротехніка','slug'=>'maiosssres-inc','parent_id'=>'0' ]);
        Business::create(['id'=>'4', 'name'=>'Будівництво','slug'=>'maiddores-incu','parent_id'=>'0' ]);
        Business::create(['id'=>'5', 'name'=>'Виробництво і постачання','slug'=>'maiodres-incun','parent_id'=>'0' ]);
        Business::create(['id'=>'6', 'name'=>'Магазини','slug'=>'maiore-inddcunt','parent_id'=>'0' ]);
        Business::create(['id'=>'7', 'name'=>'Послуги та обладнання','slug'=>'maffdior-incunt','parent_id'=>'0' ]);
        Business::create(['id'=>'8', 'name'=>'Організації, установи','slug'=>'madio-incunt','parent_id'=>'0' ]);
        Business::create(['id'=>'9', 'name'=>'Медицина, ветеринарія','slug'=>'mafddi-incunt','parent_id'=>'0' ]);
        Business::create(['id'=>'10', 'name'=>'Реклама, поліграфія','slug'=>'ma-incdffdunt','parent_id'=>'0' ]);
        Business::create(['id'=>'11', 'name'=>'Фінанси','slug'=>'ma-incunt','parent_id'=>'0' ]);
        Business::create(['id'=>'12', 'name'=>'Комунальні, аварійні служби','slug'=>'ma-igncunt','parent_id'=>'0' ]);
        Business::create(['id'=>'13', 'name'=>'Розваги, відпочинок','slug'=>'ma-incfffgunt','parent_id'=>'0' ]);
        Business::create(['id'=>'14', 'name'=>'Транспорт','slug'=>'ma-incuhgnt','parent_id'=>'0' ]);
        Business::create(['id'=>'15', 'name'=>'Спорт, краса, здоров’я','slug'=>'ma-inchhggunt','parent_id'=>'0' ]);
        Business::create(['id'=>'16', 'name'=>'Торгівля','slug'=>'ma-incjhunt','parent_id'=>'0' ]);
        Business::create(['id'=>'17', 'name'=>'Hi-Tech, комп’ютери','slug'=>'ma-incjjhunt','parent_id'=>'0' ]);
        Business::create(['id'=>'18', 'name'=>'Зв’язок, телекомунікації','slug'=>'ma-ihhjncunt','parent_id'=>'0' ]);
        Business::create(['id'=>'19', 'name'=>'Культура, мистецтво','slug'=>'ma-incjjjhhjunt','parent_id'=>'0' ]);
    }
}
